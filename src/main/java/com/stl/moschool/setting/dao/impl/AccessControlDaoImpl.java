package com.stl.moschool.setting.dao.impl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.stl.moschool.changepassword.controller.ChangePasswordModel;
import com.stl.moschool.common.model.LoginUserDetailsModel;
import com.stl.moschool.setting.dao.AccessControlDao;
import com.stl.moschool.setting.model.AllLocationModel;
import com.stl.moschool.setting.model.ApplicationModel;
import com.stl.moschool.setting.model.CityModel;
import com.stl.moschool.setting.model.CountryModal;
import com.stl.moschool.setting.model.EmailModel;
import com.stl.moschool.setting.model.Employee;
import com.stl.moschool.setting.model.Group;
import com.stl.moschool.setting.model.HybridSeededSurveyConfigDto;
import com.stl.moschool.setting.model.HybridSeededSurveyDto;
import com.stl.moschool.setting.model.LocationModel;
import com.stl.moschool.setting.model.MaappedUserDetailsResponseModel;
import com.stl.moschool.setting.model.Parameter;
import com.stl.moschool.setting.model.ParameterModel;
import com.stl.moschool.setting.model.Resource;
import com.stl.moschool.setting.model.ResourceModel;
import com.stl.moschool.setting.model.ResourceVO;
import com.stl.moschool.setting.model.Role;
import com.stl.moschool.setting.model.RoleModel;
import com.stl.moschool.setting.model.SmsModel;
import com.stl.moschool.setting.model.UserGroupParameter;
import com.stl.moschool.setting.model.UserModel;

@Repository("accessControlDao")
public class AccessControlDaoImpl implements AccessControlDao {
	
final static Logger logger = Logger.getLogger(AccessControlDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory ;
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	private Properties adminQueries;
	
	/*@Override	
	public List<Users> getUserDetails() {
		String hql = "from Users";				
		Session session = this.sessionFactory.openSession();
		Query query = session.createQuery(hql);
		List<Users> users = query.list();
		session.close();		
		return users;
	}	
	@Override
	public boolean saveUserDetails(Users user) {
		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.persist(user);
		tx.commit();
		session.close();
		return true;
	}*/
	@Override
	public boolean saveEmployeeDetails(Employee user) {
		String sql = adminQueries.getProperty("saveEmployeeDetailsSQL");		
		jdbcTemplate.update(sql, new Object[] { 
			user.getEmp_id(),
			user.getGpf_no(),
			user.getName(),
			user.getEmail(),
			user.getMobile(),
			user.getSurvey_type(),
			user.getCreated_by()
		});
		int a[]=user.getQus_type();
		String s[]=user.getAnswer();
		for (int i = 0; i < a.length; i++) {
		System.out.println("array looping "+i);
			String sql1 = adminQueries.getProperty("saveAnswerDetailsSQL");	
			jdbcTemplate.update(sql1,a[i],s[i],user.getEmp_id());
		}
		return true;
	}
	

	
	@Override
	public List<UserModel> getUserDetails() {
		String sql = adminQueries.getProperty("getUserListSQL");
		List<UserModel> users = jdbcTemplate.query(sql, new RowMapper<UserModel>() {
			@Override
			public UserModel mapRow(ResultSet rs, int rownumber) throws SQLException {
				UserModel user = new UserModel();
				user.setUserId(rs.getString(1));
				user.setFirstName(rs.getString(2));
				user.setLastName(rs.getString(3));
				user.setEmail(rs.getString(4));
				user.setContactNo(rs.getString(5));
				user.setUserType(rs.getString(6));
				user.setFirstLogin(rs.getBoolean(7));
				user.setAdmin(rs.getBoolean(8));
				user.setActive(rs.getBoolean(9));
				user.setCreatedBy(rs.getString(10));
				user.setCreatedDate(rs.getString(11));
				user.setMenuVisible(rs.getBoolean(12));
				user.setPhotoUploaded(rs.getBoolean(13));
				return user;
			}
		});
		return users;
	}	
	
	@Override
	public boolean saveUserDetails(UserModel user) {
		System.out.println(user.getUserId());
		String sql = adminQueries.getProperty("saveUserDetailsSQL");		
		jdbcTemplate.update(sql, new Object[] { 
				user.getUserId(), 
				user.getFirstName(),
				user.getLastName(),
				user.getEmail(),
				user.getContactNo(),
				user.getPassword(),
				user.getUserType(),
				user.isFirstLogin(),
				user.isAdmin(),
				user.isActive(),
				user.getCreatedBy(),
				user.getCreatedDate(),
				user.isMenuVisible(),
				user.isPhotoUploaded()
		});		
		return true;
	}
	
	@Override
	public boolean updateUserDetails(UserModel user) {
		String sql = adminQueries.getProperty("updateUserDetailsSQL");
		jdbcTemplate.update(sql, new Object[] {
				user.getFirstName(),
				user.getLastName(),
				user.getEmail(),
				user.getContactNo(),				 
				user.getUserId()
		});		
		return true;
	}
	
	@Override
	public boolean deleteUser(String userId) {
		System.out.println("To delete"+ userId);
		String sql = adminQueries.getProperty("deleteUserSQL");
		jdbcTemplate.update(sql, new Object[] {	false, userId });		
		return true;
	}
	
	
	@Override
	public List<EmailModel> getEmailDetails() {
		String sql = adminQueries.getProperty("getEmailListSQL");
		List<EmailModel> users = jdbcTemplate.query(sql, new RowMapper<EmailModel>() {
			@Override
			public EmailModel mapRow(ResultSet rs, int rownumber) throws SQLException {
				EmailModel user = new EmailModel();
				user.setEmail_name(rs.getString(1));
				user.setEmail_pwd(rs.getString(2));
				user.setPort(rs.getInt(3));
				user.setDescription(rs.getString(4));
				user.setUpdated_by(rs.getString(5));
				user.setUpdated_dt(rs.getTimestamp(6));
				user.setCreated_by(rs.getString(7));
				user.setCreated_dt(rs.getTimestamp(8));
				user.setIsactive(rs.getBoolean(9));
				return user;
			}
		});
		return users;
	}	
	
	@Override
	public boolean saveEmailDetails(EmailModel user) {
		String sql = adminQueries.getProperty("saveEmailDetailsSQL");	
//		System.out.println(user.getPort().get);
		jdbcTemplate.update(sql, new Object[] { 
				user.getEmail_name(), 
				user.getEmail_pwd(),
				user.getPort(),
				user.getDescription(),
				user.getUpdated_by(),
				user.getUpdated_dt(),
				user.getCreated_by(),
				user.getCreated_dt(),
				user.getIsactive()
		});		
		return true;
	}
	
	@Override
	public boolean updateEmailDetails(EmailModel user) {
//		System.out.println((Integer.valueOf(user.getPort())));
		String sql = adminQueries.getProperty("updateEmailDetailsSQL");
		jdbcTemplate.update(sql, new Object[] {
				user.getEmail_name(), 
				user.getEmail_pwd(),
				user.getPort(),
				user.getDescription(),
				user.getEmail_name()
		});		
		return true;
	}
	
	@Override
	public boolean deleteEmail(EmailModel user) {
		String userId=user.getEmail_name();
		boolean active=!(user.getIsactive());
		String sql = adminQueries.getProperty("deleteEmailSQL");
		jdbcTemplate.update(sql, new Object[] {	active, userId });		
		return true;
	}
	
	/*@Override
	public boolean deleteEmailF(String userId) {
		String sql = adminQueries.getProperty("deleteEmailFSQL");
		jdbcTemplate.update(sql, new Object[] {	true, userId });		
		return true;
	}*/
	
	
	@Override
	public List<SmsModel> getSMSDetails() {
		String sql = adminQueries.getProperty("getSMSListSQL");
		List<SmsModel> users = jdbcTemplate.query(sql, new RowMapper<SmsModel>() {
			@Override
			public SmsModel mapRow(ResultSet rs, int rownumber) throws SQLException {
				SmsModel user = new SmsModel();
				user.setSms_url(rs.getString(1));
				user.setSms_user(rs.getString(2));
				user.setSms_pwd(rs.getString(3));
				user.setUser_agent(rs.getString(4));
				user.setUpdated_by(rs.getString(5));
				user.setUpdated_dt(rs.getTimestamp(6));
				user.setCreated_by(rs.getString(7));
				user.setCreated_dt(rs.getTimestamp(8));
				user.setIsactive(rs.getBoolean(9));
				return user;
			}
		});
		return users;
	}	
	
	@Override
	public boolean saveSMSDetails(SmsModel user) {
		String sql = adminQueries.getProperty("saveSMSDetailsSQL");	
//		System.out.println(user.getPort().get);
		jdbcTemplate.update(sql, new Object[] { 
				user.getSms_url(), 
				user.getSms_user(),
				user.getSms_pwd(),
				user.getUser_agent(),
				user.getUpdated_by(),
				user.getUpdated_dt(),
				user.getCreated_by(),
				user.getCreated_dt()
		});		
		return true;
	}
	
	@Override
	public boolean updateSMSDetails(SmsModel user) {
//		System.out.println((Integer.valueOf(user.getPort())));
		String sql = adminQueries.getProperty("updateSMSDetailsSQL");
		jdbcTemplate.update(sql, new Object[] {
				user.getSms_url(), 
				user.getSms_user(),
				user.getSms_pwd(),
				user.getUser_agent(),
				user.getSms_user()
		});		
		return true;
	}
	
	@Override
	public boolean deleteSMS(SmsModel user) {
		boolean active= !(user.getIsactive());
		String userId=user.getSms_user();
		String sql = adminQueries.getProperty("deleteSMSSQL");
		jdbcTemplate.update(sql, new Object[] {	active, userId });		
		return true;
	}
	
	
	/*@Override
	public boolean deleteSMSF(String userId) {
		String sql = adminQueries.getProperty("deleteSMSFSQL");
		jdbcTemplate.update(sql, new Object[] {	true, userId });		
		return true;
	}*/
	
	@Override
	public List<Group> getGroupList() {
		String sql = adminQueries.getProperty("getGroupListSQL");
		List<Group> groups = jdbcTemplate.query(sql, new RowMapper<Group>() {
			@Override
			public Group mapRow(ResultSet rs, int rownumber) throws SQLException {
				Group group = new Group();
				group.setGroupId(rs.getLong(1));
				group.setGroupName(rs.getString(2));
				group.setDescription(rs.getString(3));
				group.setActive(rs.getBoolean(4));
				group.setNoOfUsers(rs.getInt(5));
				return group;
			}
		});
		return groups;
	}	
	
	@Override
	public boolean saveGroup(Group group) {
		String sql = adminQueries.getProperty("saveGroupSQL");		
		jdbcTemplate.update(sql, new Object[] { 
				group.getGroupName(), 
				group.getDescription(),
				group.isActive()
		});		
		return true;
	}
	
	@Override
	public boolean updateGroup(Group group) {
		String sql = adminQueries.getProperty("updateGroupSQL");
		jdbcTemplate.update(sql, new Object[] {
				group.getGroupName(),
				group.getDescription(),				 
				group.getGroupId()
		});		
		return true;
	}
	
	@Override
	public boolean deleteGroup(long groupId) {
		System.out.println("groupId"+groupId);
		String sql = adminQueries.getProperty("deleteGroupSQL");
		jdbcTemplate.update(sql, new Object[] {false,groupId});		
		return true;
	}

	@Override
	public List<UserModel> getMappedUserDetails(long groupId) {
		String sql = adminQueries.getProperty("getMappedUserListSQL");
		List<UserModel> users = jdbcTemplate.query(sql, new RowMapper<UserModel>() {
			@Override
			public UserModel mapRow(ResultSet rs, int rownumber) throws SQLException {
				UserModel user = new UserModel();
				user.setUserId(rs.getString(1));
				user.setFirstName(rs.getString(2));
				user.setLastName(rs.getString(3));
				user.setEmail(rs.getString(4));
				user.setContactNo(rs.getString(5));
				user.setPassword(rs.getString(6));
				user.setUserType(rs.getString(7));
				user.setFirstLogin(rs.getBoolean(8));
				user.setAdmin(rs.getBoolean(9));
				user.setActive(rs.getBoolean(10));
				user.setCreatedBy(rs.getString(11));
				user.setCreatedDate(rs.getString(12));
				user.setMenuVisible(rs.getBoolean(13));
				user.setPhotoUploaded(rs.getBoolean(14));
				return user;
			}
		},groupId);
		return users;
	}
	
	@Override
	public boolean mapUsersToGroup(long groupId, UserModel[] users) {
		String sql = adminQueries.getProperty("mapUsersToGroupSQL");		
		for(UserModel user : users){
			jdbcTemplate.update(sql, new Object[] {
					groupId,
					user.getUserId()
			});		
		}
		return true;
	}
	
	@Override
	public boolean deleteMappedUser(String userId) {
		String sql = adminQueries.getProperty("deleteMappedUserSQL");		
		jdbcTemplate.update(sql, new Object[] {userId});	
		return true;
	}
	
	@Override
	public List<Resource> getResourceList() {
		String sql = adminQueries.getProperty("getResourceListSQL");
		List<Resource> resources = jdbcTemplate.query(sql, new RowMapper<Resource>() {
			@Override
			public Resource mapRow(ResultSet rs, int rownumber) throws SQLException {
				Resource resource = new Resource();
				resource.setResourceId(rs.getLong(1));
				resource.setResourceName(rs.getString(2));
				resource.setResourceType(rs.getString(3));
				resource.setDescription(rs.getString(4));
				resource.setActive(rs.getBoolean(5));
				return resource;
			}
		});
		return resources;
	}
	
	/*****************************************Create Resource************************************************/	
	@Override
	public boolean saveUserResourceTable(String resource_name, String resourceType,String description, String type,long resourceId) {
		System.out.println(resourceId);
		System.out.println(resource_name);
		System.out.println(resourceType);
		System.out.println(description);
		System.out.println("Inside saveUserResourceTable daoimpl............");
		if("ADD".equalsIgnoreCase(type)){
			System.out.println("Inside type Add");
			String sql = adminQueries.getProperty("saveResourceSQL");
			
			try {
				jdbcTemplate.update(sql, new Object[] { 
						resource_name,resourceType,description,true
				});
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}	
		}else if("UPDATE".equalsIgnoreCase(type)){
			System.out.println("Inside type Update");
			String sql = adminQueries.getProperty("updateResourceSQL");
			
			try {
				jdbcTemplate.update(sql, new Object[] { 
						resource_name,resourceType,description,resourceId
				});
			} catch (DataAccessException e) {
				e.printStackTrace();
				return false;
			}
		}
			
		return true;
	}
	
	
	@Override
	public List<ResourceModel> getDataResourceTable() {
		String sql = adminQueries.getProperty("getResourceListSQL");
		System.out.println("sql:"+sql);
		List<ResourceModel> resourceModal = jdbcTemplate.query(sql, new RowMapper<ResourceModel>() {
			@Override
			public ResourceModel mapRow(ResultSet rs, int rownumber) throws SQLException {
				ResourceModel resource = new ResourceModel();
				resource.setResource_id(rs.getLong(1));
				resource.setResource_name(rs.getString(2));
				resource.setResource_type(rs.getString(3));
				resource.setDescription(rs.getString(4));
				return resource;
			}
		});
		return resourceModal;
	}
	
	@Override
	public boolean deleteCreateUserResource(String resource_id) {
		String sql = adminQueries.getProperty("deleteResourceSQL");
		int id = Integer.parseInt(resource_id);
		jdbcTemplate.update(sql, new Object[] {false,id});		
		return true;
	}


	@Override
	public List<Resource> getMappedResources(long roleId) {
		String sql = adminQueries.getProperty("getMappedResourcesSQL");
		List<Resource> resources = jdbcTemplate.query(sql, new RowMapper<Resource>() {
			@Override
			public Resource mapRow(ResultSet rs, int rownumber) throws SQLException {
				Resource resource = new Resource();
				resource.setResourceId(rs.getLong(1));
				resource.setResourceName(rs.getString(2));
				resource.setResourceType(rs.getString(3));
				resource.setDescription(rs.getString(4));
				resource.setActive(rs.getBoolean(5));
				return resource;
			}
		}, roleId);
		return resources;
	}
	
	@Override
	public boolean deleteMappedResources(long roleId, long resourceId) {
		String sql = adminQueries.getProperty("deleteMappedResourceSQL");
		jdbcTemplate.update(sql, new Object[] {	roleId, resourceId });		
		return true;
	}
	
	@Override
	public boolean mapResourcesToRole(long roleId, Resource[] resources) {
		String sql = adminQueries.getProperty("mapResourcesToRoleSQL");		
		for(Resource resource : resources){
			jdbcTemplate.update(sql, new Object[] {
					roleId,
					resource.getResourceId()
			});		
		}
		return true;
	}
	
	/*********************************************CREATE ROLE**************************************************/	
	@Override
	public boolean saveUserRoleTable(String role_name, String description,
			String type, String id) {
		System.out.println(role_name);
		System.out.println(description);
		System.out.println(id);
		System.out.println("Inside saveUserRoleTable daoimpl............");
		
		if("ADD".equalsIgnoreCase(type)){
			System.out.println("Inside type Add");
			String sql = adminQueries.getProperty("saveRoleSQL");
			
			try {
				jdbcTemplate.update(sql, new Object[] { 
						role_name,description,true
				});
			} catch (DataAccessException e) {
				e.printStackTrace();
				return false;
			}	
		}else if("UPDATE".equalsIgnoreCase(type)){
			System.out.println("Inside type Update");
			int role_id  = Integer.parseInt(id);
			String sql = adminQueries.getProperty("updateRoleSQL");
			System.out.println(sql);
			try {
				jdbcTemplate.update(sql, new Object[] { 
						role_name,description,role_id
				});
			} catch (DataAccessException e) {
				e.printStackTrace();
				return false;
			}
		}
			
		return true;
	}
	
	@Override
	public boolean deleteCreateUserRole(String role_id) {
		String sql = adminQueries.getProperty("deleteRoleSQL");
		System.out.println(role_id);
		int id = Integer.parseInt(role_id);
		jdbcTemplate.update(sql, new Object[] {	
				false,id 
		});		
		return true;
	}
	
	
	
	@Override
	public List<RoleModel> getRoleDetails() {
		String sql = adminQueries.getProperty("getRoleListSQL");
		System.out.println("sql:"+sql);
		List<RoleModel> RoleModel = jdbcTemplate.query(sql, new RowMapper<RoleModel>() {
			@Override
			public RoleModel mapRow(ResultSet rs, int rownumber) throws SQLException {
				RoleModel Role = new RoleModel();
				Role.setrole_id(rs.getString(1));
				Role.setrole_name(rs.getString(2));
				Role.setdescription(rs.getString(3));
				return Role;
				
			}
		});
		return RoleModel;
	}
	
	/*********************************************CREATE PARAMETER**************************************************/
	@Override
	public boolean saveUserParameterTable(String param_name, String tbl_name,String type, String id) {
		System.out.println("Inside Parameter Dao Impl....");
		System.out.println(param_name);
		System.out.println(tbl_name);
	
		System.out.println("Inside saveUserParameterTable daoimpl............");
		if("ADD".equalsIgnoreCase(type)){
			System.out.println(type);
			String sql = adminQueries.getProperty("saveParameterDetailsSQL");
			System.out.println(sql);
			try {
				jdbcTemplate.update(sql, new Object[] { 
						param_name,tbl_name
				});
			} catch (DataAccessException e) {
				e.printStackTrace();
				return false;
			}	
		}else if("UPDATE".equalsIgnoreCase(type)){
			System.out.println(type);
			int parm_id = Integer.parseInt(id);
			System.out.println("id   "+parm_id);
			String sql = adminQueries.getProperty("updateParameterSQL");
			System.out.println(sql);
			try {
				jdbcTemplate.update(sql, new Object[] { 
						param_name,tbl_name,parm_id
				});
			} catch (DataAccessException e) {
				e.printStackTrace();
				return false;
			}
		}
			
		return true;
	}
	
	
	@Override
	 public List<ParameterModel> getParameterTableData() {
		String sql = adminQueries.getProperty("getParameterListSql");
		System.out.println("sql:"+sql);
		List<ParameterModel> ParameterModel = jdbcTemplate.query(sql, new RowMapper<ParameterModel>() {
			@Override
			public ParameterModel mapRow(ResultSet rs, int rownumber) throws SQLException {
				ParameterModel Parameter = new ParameterModel();
				Parameter.setParam_id(rs.getString(1));
				Parameter.setParam_name(rs.getString(2));
				Parameter.setTbl_name(rs.getString(3));
				return Parameter;
				
			}
		});
		return ParameterModel;
	}
	 
	 
	 @Override
		public boolean deleteCreateParameter(String param_id) {
			String sql = adminQueries.getProperty("deleteParameterSQL");
			int id = Integer.parseInt(param_id);
			jdbcTemplate.update(sql, new Object[] {	
					id 
			});		
			return true;
		}
	@Override
	public List<Role> getRoleList() {
		String sql = adminQueries.getProperty("getRoleListSQL");
		List<Role> roles = jdbcTemplate.query(sql, new RowMapper<Role>() {
			@Override
			public Role mapRow(ResultSet rs, int rownumber) throws SQLException {
				Role role = new Role();
				role.setRoleId(rs.getLong(1));
				role.setRoleName(rs.getString(2));
				role.setDescription(rs.getString(3));
				role.setActive(rs.getBoolean(4));
				return role;
			}
		});
		return roles;
	}

	
	
	@Override
	public List<Role> getMappedRolesToGroup(long groupId) {
		String sql = adminQueries.getProperty("getMappedRolesToGroupSQL");
		List<Role> roles = jdbcTemplate.query(sql, new RowMapper<Role>() {
			@Override
			public Role mapRow(ResultSet rs, int rownumber) throws SQLException {
				Role role = new Role();
				role.setRoleId(rs.getLong(1));
				role.setRoleName(rs.getString(2));
				role.setDescription(rs.getString(3));
				role.setActive(rs.getBoolean(4));
				return role;
			}
		}, groupId);
		return roles;
	}
	
	@Override
	public boolean deleteMappedRole(long groupId, long roleId) {
		String sql = adminQueries.getProperty("deleteMappedRoleSQL");
		try {
			jdbcTemplate.update(sql, new Object[] {	groupId, roleId });
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}		
		return true;
	}
	@Override
	public boolean mapRoleToGroup(long groupId, Role[] roles) {
		String sql = adminQueries.getProperty("mapRoleToGroupSQL");		
		for(Role role : roles){
			jdbcTemplate.update(sql, new Object[] {
				groupId,
				role.getRoleId()
			});		
		}
		return true;
	}

	@Override
	public List<Parameter> getParameterList() {
		String sql = adminQueries.getProperty("getParameterListSql");
		List<Parameter> param = jdbcTemplate.query(sql, new RowMapper<Parameter>() {
			@Override
			public Parameter mapRow(ResultSet rs, int rownumber) throws SQLException {
				Parameter param = new Parameter();
				param.setParam_id(rs.getLong(1));
				param.setParam_name(rs.getString(2));
				param.setTbl_name(rs.getString(3));
				
				return param;
			}
		});
		
		return param;
	}

	@Override
	public boolean saveParameter(Parameter param) {
		String sql = adminQueries.getProperty("saveParameterDetailsSQL");		
		jdbcTemplate.update(sql, new Object[] { 
				param.getParam_name(),
				param.getTbl_name()
		});		
		return true;
	}
	
	
	@Override
	public List<UserModel> getMappedParametersToGroup(long groupId) {
		String sql = adminQueries.getProperty("getMappedParametersToGroupSQL");
		
		List<UserModel> mappedParams = jdbcTemplate.query(sql, new RowMapper<UserModel>() {
			@Override
			public UserModel mapRow(ResultSet rs, int rownumber) throws SQLException {
				UserModel mappedParam = new UserModel();
				/*mappedParam.setSlno(rs.getLong(1));
				mappedParam.setParam_name(rs.getString(2));
				mappedParam.setParam_val(rs.getString(3));*/
				mappedParam.setUserId(rs.getString(1));
				mappedParam.setFirstName(rs.getString(2));
				mappedParam.setEmail(rs.getString(3));
				mappedParam.setContactNo(rs.getString(4));
				mappedParam.setLocationId(rs.getInt(5));
				return mappedParam;
			}
		}, groupId);
		//user_id,first_name,email_id,contact_no
		return mappedParams;
	}

	@Override
	public JSONObject getParameterValueTable(String tableName, String paramName,
			long paramId)  {
		      JSONArray dataArr = new JSONArray();
		      JSONArray colArr = new JSONArray();
		      JSONObject retObj = new JSONObject();
		      ArrayList<String> strColName = new ArrayList<String>();
		      JSONObject obj = null;
			  String sql = "select * from "+tableName+"";
			 // System.out.println("sql is:"+sql);
			  List<Map<String, Object>> list = jdbcTemplate.queryForList(sql, new  Object[] {}, new int[] {});
			  for (Map<String, Object> row : list) {
				  List<String> keys = new ArrayList<String>(row.keySet()) ;
				  obj = new JSONObject();
				  for (int j = 0;j<keys.size();j++) {
					strColName.add(keys.get(j));
					try {
						obj.put(keys.get(j), row.get(keys.get(j)));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				  }//end of inner for loop
					 dataArr.put(obj);
		        } //outer loop  
			  HashSet<String> hs=new HashSet<>(strColName);
			  List<String> col = new ArrayList<String>(hs);
			  for(int i =0;i<col.size();i++) {
				  colArr.put(col.get(i));
			  }
			  try {
				retObj.put("data", dataArr);
				retObj.put("col_name", colArr);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return retObj;
	}

	@Override
	public boolean mapParamvaluesToParam(List<String> paramList,
			long selectedGroupId, String paramName) {
			List<UserGroupParameter> arList = new ArrayList<UserGroupParameter>();
			for(int i=0;i<paramList.size();i++) {
				UserGroupParameter userGrpParams = new UserGroupParameter();
				userGrpParams.setGroup_id(selectedGroupId);
				userGrpParams.setParam_name(paramName);
				userGrpParams.setParam_val(paramList.get(i));
				arList.add(userGrpParams);
			}
		
			String sql = adminQueries.getProperty("mapParamvaluesToParamSql");
			for(UserGroupParameter ugp : arList){
				jdbcTemplate.update(sql, new Object[] {
						ugp.getGroup_id(),
						ugp.getParam_name(),
						ugp.getParam_val()
					});	
			}
		return true;
	}

	@Override
	public boolean deleteMappedParam(long sl_no) {
		String sql = adminQueries.getProperty("deleteMappedParamSQL");
		jdbcTemplate.update(sql, new Object[] {	sl_no });		
		return true;
		
	}

	//commented by lusi on Dt-26-10-2017
/*	@Override
	public UserModel getUserInfo(String user_id, String password) {
		logger.info("user_id-->"+user_id+" Password-->"+password);
		String sql = adminQueries.getProperty("getUserInfoSql");
		UserModel userInfo = null;
		List<UserModel> userList = jdbcTemplate.query(sql, new RowMapper<UserModel>() {
			@Override
			public UserModel mapRow(ResultSet rs, int rownumber) throws SQLException {
				UserModel users = new UserModel();
				users.setUserId(rs.getString(1));
				users.setFirstName(rs.getString(2));
				users.setLastName(rs.getString(3));
				users.setEmail(rs.getString(4));
				users.setContactNo(rs.getString(5));
				users.setUserType(rs.getString(7));
				return users;
			}
		}, user_id,password);
		if(null != userList && !userList.isEmpty()){
			userInfo = userList.get(0);
		}
		return userInfo;
	}*/

	@Override
	public List<Long> getGroupListByUserId(String uname) {
		String sql = adminQueries.getProperty("getGroupIdListByUserIdSql");
		List<Long> groupIdList = jdbcTemplate.query(sql, new RowMapper<Long>() {
			@Override
			public  Long mapRow(ResultSet rs, int rownumber) throws SQLException {
				Long groupId = rs.getLong(1);
				return groupId;
			}
		}, uname);
		
		return groupIdList;
	}

	@Override
	public JSONObject getResourcesAndParamList(
			List<Long> lstGroupId) {
		JSONObject retObj = new JSONObject();
		JSONArray jArrResourceName = new JSONArray();
		String getListResourceNameSql = adminQueries.getProperty("getGroupIdListByUserIdSql");
		List<Map<String, Object>> resourceList = jdbcTemplate.queryForList(getListResourceNameSql,lstGroupId.toString().replace("[", "(").replace("]", ")"));
		 for (Map<String, Object> row : resourceList) {       
			 for(int i =0;i<row.size();i++) {
				 jArrResourceName.put(row.get(i));
			 }
		 }
		try {
			retObj.put("resourceName", jArrResourceName);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return retObj;
	}
	
	/**
	 * This method will give the mapped roles to a particular
	 * user by its user id
	 * @param userId 
	 * @return List<String>
	 * @date 04-03-2017
	 */
	@Override
	public List<String> getMappedRolesToUser(final String userId) {
		final String sql = adminQueries.getProperty("getMappedRolesToUserSQL");
		List<String> roles = (List<String>)jdbcTemplate.query(new PreparedStatementCreator() {
		      public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
		          return connection
		              .prepareStatement(sql);
		        }
		      }, new PreparedStatementSetter() {
		        public void setValues(PreparedStatement preparedStatement) throws SQLException {
		          preparedStatement.setString(1, userId);
		        }
		      }, new ResultSetExtractor<List<String>>() {
		        public List<String> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
		        	List<String> roleList = new ArrayList<>();
		          while(resultSet.next()) {
		        	  roleList.add(resultSet.getString(1));
		          }
		          return roleList;
		        }
		      });
		return roles;
	}//end of getMappedRolesToUser
	/**
	 * This method will return the unique resources mapped to these
	 * given roles
	 * Note:- Always an User will invoke with single role
	 *  
	 * @param allowedRoles
	 * @return List<ResourceVO>
	 * @date 04-03-2017
	 */
	@Override
	public List<ResourceVO> getUniqueResourcesForRoles(List<String> allowedRoles) {
		
		StringBuffer sb = new StringBuffer();
		if(allowedRoles != null && !allowedRoles.isEmpty()){
			int i = 0 ; 
			for(String role : allowedRoles){
				i++;
				sb.append(role);
				if(i < allowedRoles.size()){
					sb.append(",");
				}
			}
		}
		final String param = sb.toString();
		final String sql = adminQueries.getProperty("getUniqueResourcesForRolesSQL");
		
		return (List<ResourceVO>)jdbcTemplate.query(new PreparedStatementCreator() {
		      public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
		          return connection
		              .prepareStatement(sql);
		        }
		      }, new PreparedStatementSetter() {
		        public void setValues(PreparedStatement preparedStatement) throws SQLException {
		          preparedStatement.setInt(1, Integer.parseInt(param));
		        }
		      }, new ResultSetExtractor<List<ResourceVO>>() {
		        public List<ResourceVO> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
		        	List<ResourceVO> resourceList = new ArrayList<>();
		        	ResourceVO resource = null ;
		        	//rs.resource_id, rs.resource_name, rs.resource_type, rs.description, rs.is_active
		          while(resultSet.next()) {
		        	  resource = new ResourceVO();
		        	  resource.setResourceId(resultSet.getLong(1));
		        	  resource.setResourceName(resultSet.getString(2));
		        	  resource.setResourceType(resultSet.getString(3));
		        	  resource.setDescription(resultSet.getString(4));
		        	  resource.setActive(resultSet.getBoolean(5));
		        	  resourceList.add(resource);
		          }
		          return resourceList;
		        }
		      });
		
	}//end of getUniqueResourcesForRoles
	/**
	 * Retrieve all the available additional parameters to the user/user_group
	 * with the provided values to these parameters for dynamic data control
	 * @param userId
	 * @return HashMap<String, List<String>>
	 * @date 06-03-2017
	 */
	@Override
	public HashMap<String, List<String>> getDataControlDetails(final String userId) {
		final String sql = adminQueries.getProperty("getDataControlSQL");
		HashMap<String, List<String>> hmParamVal = (HashMap<String, List<String>>)jdbcTemplate.query(new PreparedStatementCreator() {
		      public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
		          return connection
		              .prepareStatement(sql);
		        }
		      }, new PreparedStatementSetter() {
		        public void setValues(PreparedStatement preparedStatement) throws SQLException {
		          preparedStatement.setString(1, userId);
		        }
		      }, new ResultSetExtractor<HashMap<String, List<String>>>() {
		        public HashMap<String, List<String>> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
		        	HashMap<String, List<String>> hmParamVal = new HashMap<>();
		        	String keyParam = "";
		        	String tempPrevParam = "";
		        	List<String> listVals = null;
		        	String value = "";
		          while(resultSet.next()) {
		        	  keyParam = resultSet.getString(1);
		        	  value = resultSet.getString(2);
		        	  if(StringUtils.isEmpty(tempPrevParam) || !StringUtils.equals(tempPrevParam, keyParam)){
		        		  listVals = new ArrayList<>();
		        	  }
		        	  tempPrevParam = resultSet.getString(1);
		        	  listVals.add(value);
		        	  hmParamVal.put(keyParam, listVals);
		          }
		          return hmParamVal;
		        }
		      });
		return hmParamVal;
	}//end of getDataControlDetails
	
	/**********************************************************************
	 * Method that will used to check the existance of userId and emailId
	 ***********************************************************************/
	@Override
	public JSONObject validateUidAndEmail(String userid, String emailId) {
		String methodName = "validateUidAndEmail";
		logger.info("EntryDao--------------->"+methodName);
		JSONObject retObj = new JSONObject();
		String sql = "select * from admin.user where user_id=? and email_id=?";
		String result = jdbcTemplate.query(sql, new Object[] {userid,emailId}, new ResultSetExtractor<String>(){
			@Override
			public String extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				if(rs.next()) {
					return "success";
				}else {
					return "failure";
				}
			}
			
		});
		try {
			retObj.put("status", result);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("Exitdao--------------->"+methodName);
		return retObj;
	}
	/************************************************
	 * After sending the mail successfully the corresponding userid and captcha 
	 * will save in tha database table
	 ************************************************/
	@Override
	public boolean saveOtpInDb(String captchaCode, String userid) {
		String methodName = "saveOtpInDb";
		logger.info("EntryDao--------------->"+methodName);
		logger.info("OTP"+captchaCode);
		boolean state = false;
		String sql = "INSERT INTO admin.otp (key,value) select '"+userid+"','"+captchaCode+"'  WHERE NOT EXISTS (SELECT value FROM admin.otp WHERE key = '"+userid+"')";
		int status = jdbcTemplate.update(sql, new Object[] {}, new int[] {});
		System.out.println(status);
		if(status == 1) {
			state = true;
		}
		if(status < 1 ) {
			String sql2 = "update admin.otp set value='"+captchaCode+"' where key='"+userid+"'";
			jdbcTemplate.update(sql2);	
			state= true;
		}
		logger.info("Exitdao--------------->"+methodName);
		return state;
	}
	/**********************************************
	 * Method that will take the otp and userid as input and 
	 * check in the table that wheather this otp is matching for this userid or not
	 ***************************************************/
	@Override
	public boolean validateOtp(String uid, String input_otp) {
		String methodName = "validateOtp";
		logger.info("EntryDao--------------->"+methodName);
		String sql = "select * from admin.otp where key=? and value=? ";
		boolean status = jdbcTemplate.query(sql, new Object[] {uid,input_otp}, new ResultSetExtractor<Boolean>(){
			@Override
			public Boolean extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				if(rs.next()) {
					return true;
				}else {
					return false;
				}
			}
		});
		logger.info("Exitdao--------------->"+methodName);
		return status;
	}
	/**********************************************************************
	 * method thaqt will check wheather any otp is present or not if present update or else insert
	 * @input:-uid
	 ************************************************/
	@Override
	public boolean resendOtp(String uid, String captchaCode) {
		String methodName = "resendOtp";
		logger.info("EntryDao--------------->"+methodName);
		logger.info("OTP"+captchaCode);
		boolean state = true;
		String sql = "INSERT INTO admin.otp (key,value) select '"+uid+"','"+captchaCode+"'  WHERE NOT EXISTS (SELECT value FROM admin.otp WHERE key = '"+uid+"')";
		int status = jdbcTemplate.update(sql, new Object[] {}, new int[] {});
		System.out.println(status);
		if(status < 1 ) {
			String sql2 = "update admin.otp set value='"+captchaCode+"' where key='"+uid+"'";
			jdbcTemplate.update(sql2);	
			state= true;
		}
		logger.info("Exitdao--------------->"+methodName);
		return state;
	}
	
	/***************************************************************************
	 * Method will set the new password of a corresponding UserId
	 * Input is password and userid
	 ***************************************************************************/
	@Override
	public boolean savePassword(String uid, String encryptPassword) {
		String methodName = "savePassword";
		logger.info("EntryDao--------------->"+methodName);
		String sql = "update admin.user set password=? where user_id=?";
		jdbcTemplate.update(sql, new Object[] {
				encryptPassword,uid
		});		
		logger.info("Exitdao--------------->"+methodName);
		return true;
	}
	/***************************************************
	 * Method to used to regester a new user
	 * it will insert the information in userTable
	 * it will insert the groupid and userid in usergroup table
	 * insert the groupid and role id in the group_role table
	 ****************************************************/
	@Override
	public boolean newUserRegestration(HashMap<String, String> hm) {
		// TODO Auto-generated method stub
		String methodName = "newUserRegestration";
		logger.info("EntryDao--------------->"+methodName);
		
		String fName = hm.get("fName");
		String uid = hm.get("uid");
		String mob = hm.get("mob");
		String email = hm.get("email");
		String epwd = hm.get("epwd");
		
		String insertUserTableSql = "insert into admin.user(user_id, first_name,email_id, contact_no, password, user_type, is_active, created_by, created_date) values(?, ?, ?, ?, ?, ?, ?, ?, now())";
		jdbcTemplate.update(insertUserTableSql, new Object[] { 
				uid.toLowerCase().trim(),fName,email,mob,epwd,"user",true,"system"
		});	
	
		String groupSql = adminQueries.getProperty("mapUsersToGroupSQL");
		jdbcTemplate.update(groupSql, new Object[] { 
				5,uid.toLowerCase().trim()
		});	
		
		/*String sql = adminQueries.getProperty("mapRoleToGroupSQL");		
		jdbcTemplate.update(sql, new Object[] { 
				5,4
		});	*/
		
		logger.info("Exitdao--------------->"+methodName);
		return true;
	}
	/************************************
	method to check wheather this nitiaayogid is present/not for ngo
	 * (non-Javadoc)
	 * @see com.stl.moschool.setting.service.AccessControlService#isNgoRegistered(java.util.HashMap)
	 ****************************************************/
	@Override
	public boolean isNgoRegistered(HashMap<String, String> hm) {
		String methodName = "isNgoRegistered";
		logger.info("Entry Dao--->"+methodName);
		String ngoId = hm.get("ngoId").toString();
		logger.info(ngoId);
		String sql = "select * from workflow.app_ngo_registration where ngo_unique_id = ? ";
		logger.info("Query:"+sql);
		boolean result = jdbcTemplate.query(sql, new Object[] {ngoId}, new ResultSetExtractor<Boolean>(){
			@Override
			public Boolean extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				if(rs.next()) {
					return true; //false
				}else {
					return false; //true
				}
			}
		});
		logger.info("Exit Dao----->"+methodName);
		return result;
	}

	/*****************************************
	 * method to create unique userId(non-Javadoc)
	 * @see com.stl.moschool.setting.dao.AccessControlDao#getUniqueNoFromSeq()
	 **********************************************/
	@Override
	public long getUniqueNoFromSeq() {
		String methodName = "getUniqueNoFromSeq";
		logger.info("Entry Dao--->"+methodName);
		String sql = "select nextval('admin.ngo_institute_seq')";
		logger.info("Query:"+sql);
		long result = jdbcTemplate.query(sql, new ResultSetExtractor<Long>(){
			@Override
			public Long extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				long seqNo = 0;
				if(rs.next()) {
					seqNo = rs.getLong(1);
				}
				return seqNo;
			}
		});
		logger.info("seq no is:"+result);
		logger.info("Exit Dao--->"+methodName);
		return result;
	}
	/***************************************************************************************************
	 * method that will update the ngo credentials in user table as well as app_ngo_registration table
	 **************************************************************************************************/
	@Override
	public boolean insertNgoDetails(HashMap<String, String> hm) {
		String methodName = "insertNgoDetails";
		logger.info("Entry Dao--->"+methodName);
		String panno = hm.get("panno").toString();
		String ngoId = hm.get("ngoId").toString();
		String ngoName = hm.get("ngoName").toString();
		String regdNo =  hm.get("regdNo").toString();
		String regdDateweb = hm.get("regdDate").toString();
		 /*String deliverydate="02-SEP-2012";
		 SimpleDateFormat sdf=new SimpleDateFormat("YYYY-MM-DD");

		 Date date=sdf.parse(deliveryDate);

		 sdf=new SimpleDateFormat("DD-MM-YYYY");
		 System.out.println(sdf.format(date));*/
		/* DateFormat dateFormat = new SimpleDateFormat("YYYY-DD-MM");
		 String strDate = dateFormat.format(regdDateweb);
		 System.out.println("dateFormat " + strDate);
		 SimpleDateFormat sdf = new SimpleDateFormat("DD-MM-YYYY");
		 System.out.println("sdf "+sdf);*/
		
		String deliverydate=regdDateweb;
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-mm-dd");

		Date date = null;
		try {
			date = sdf.parse(deliverydate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		sdf=new SimpleDateFormat("DD-MM-yyyy");
		System.out.println(sdf.format(date));
		
		String regdDate = sdf.format(date);
		
/*		 Date conDate = null;
		try {
			conDate = dateFormat.parse(regdDateweb);
			System.out.println("conDate  "+conDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
/*		 String regdDate = sdf.format(conDate);
		 System.out.println("regdDate "+regdDate);*/
		 
		String keyContactName = hm.get("keyContactName").toString();
		String keyDesgName = hm.get("keyDesgName").toString();
		String actName = hm.get("actName").toString();
		String address = hm.get("address").toString();
		String city = hm.get("city").toString();
		String district = hm.get("district").toString();
		String state = hm.get("state").toString();
		String pincode = hm.get("pincode").toString();
		String website = hm.get("website").toString();
		String email = hm.get("email").toString();
		String phoneNo = hm.get("phoneNo").toString();
		String serPanStatus = hm.get("serPanStatus").toString();
		String userId = hm.get("userId").toString();
		String password = hm.get("password").toString();
		//inserted in user table
		String insertUserTableSql = "insert into admin.user(user_id, first_name,email_id, contact_no, password, user_type, is_active, created_by, created_date) values(?, ?, ?, ?, ?, ?, ?, ?, now())";
		jdbcTemplate.update(insertUserTableSql, new Object[] { 
				userId.toLowerCase(),ngoName,email,phoneNo,password,"NGO",true,"system"
		});
		//inserted in ngo registration table
		
		String insertNgoTableSql = "insert into workflow.app_ngo_registration(user_id, ngo_unique_id,pan_no, contact_name, registration_no,"
				+ "registration_date, pan_status, city, district,state,pincode,website,act_name,created_by,created_date,updated_by,updated_date,address) "
				+ "values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now(),?,now(),?)";
		System.out.println("insertNgoTableSql::"+insertNgoTableSql);
		jdbcTemplate.update(insertNgoTableSql, new Object[] { 
				userId.toLowerCase(),ngoId,panno,keyContactName,regdNo,regdDate,serPanStatus,city,district,state,pincode,website,actName,"system","system",address
		});
		String groupSql = adminQueries.getProperty("mapUsersToGroupSQL");
		System.out.println("groupSql::"+groupSql);
		jdbcTemplate.update(groupSql, new Object[] { 
				2,userId.toLowerCase()
		});	
		logger.info("Exit Dao--->"+methodName);
		
		return true;
	}
	/*******************************************************************
	 * method to check wheather this govt bodies is registered or not
	 *********************************************************************/
	@Override
	public boolean isGovtOrganisationRegistered(HashMap<String, String> hm) {
		String methodName = "isGovtOrganisationRegistered";
		logger.info("Entry Controller--->"+methodName);
		//String userId = hm.get("userId").toString();
		String regstrationNo = hm.get("regdNo").toString();
		String chairmanPanNo = hm.get("chairmanPanNo").toString();
		String sql = "select * from workflow.app_ngo_registration where registration_no = ? or pan_no=?";
		logger.info("Query:"+sql);
		boolean result = jdbcTemplate.query(sql, new Object[] {regstrationNo,chairmanPanNo}, new ResultSetExtractor<Boolean>(){
			@Override
			public Boolean extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				if(rs.next()) {
					return false;
				}else {
					return true;
				}
			}
		});
		logger.info("Exit Dao----->"+methodName);
		return result;
	}
	
	/***************************************
	 * Method to store government details
	 ****************************************/
	@Override
	public boolean insertGovtDetails(HashMap<String, String> hm) {
		String methodName = "insertGovtDetails";
		logger.info("Entry Dao---->"+methodName);
		String userId = hm.get("user_id").toString();
		String password = hm.get("password").toString();
		String nameOfOrganization = hm.get("nameOfOrganization").toString();
		System.out.println(nameOfOrganization+"5555555555555555555555");
		String regdNo = hm.get("regdNo").toString();
		String regdDate = hm.get("regDate").toString();
		String chairmanName = hm.get("chairmanName").toString();
		String address = hm.get("regdAddress");
		String org_address = hm.get("orgAddress");
		String city = hm.get("city").toString();
		String district = hm.get("district").toString();
		String state = hm.get("state").toString();
		String webSite = hm.get("webSite");
		String email = hm.get("orgEmail").toString();
		String phoneNo = hm.get("orgMobileNo").toString();
		String panNo = hm.get("chairmanPanNo").toString();
		String pinCode = hm.get("pinCode").toString();
		String govtType = hm.get("govtType").toString();
		if(StringUtils.equalsIgnoreCase(govtType, "PSUs")){
			govtType = "PSU";
		}
		//inserted in user table
		//added by Lusi Dash on Date-09-April-2018
		String orgName = null;
		if(nameOfOrganization.equalsIgnoreCase("") || nameOfOrganization.equalsIgnoreCase(null)){
			if(StringUtils.equalsIgnoreCase(govtType, "CIPET")){
				orgName = "CIPET(Central Institute of Plastics Engineering & Technology)";
			}else if(StringUtils.equalsIgnoreCase(govtType, "IPFT")){
				orgName = "IPFT(Institute of Pesticides Formulation Technology)";
			}
		}else{
			orgName = nameOfOrganization;
		}
		System.out.println("orgName---------------------------"+orgName);
		//end of add by Lusi Dash on Date-09-April-2018
		String insertUserTableSql = "insert into admin.user(user_id, first_name,email_id, contact_no, password, user_type, is_active, created_by, created_date) values(?, ?, ?, ?, ?, ?, ?, ?, now())";
		jdbcTemplate.update(insertUserTableSql, new Object[] { 
				userId.toLowerCase(),orgName,email,phoneNo,password,govtType,true,"system"
		});
		//getting Active Financial Year
		String finYearql = adminQueries.getProperty("getFinYearSQL");
		String finYear = jdbcTemplate.queryForObject(finYearql,String.class);
		//inserted in ngo registration table
				
		String insertNgoTableSql = "insert into workflow.app_ngo_registration(user_id,pan_no,contact_name, registration_no,"
				+ "registration_date, city, district,state,pincode,website,created_by,created_date,updated_by,updated_date,address,org_address,financial_year) "
				+ "values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, now(),?,now(),?,?,?)";
		System.out.println("insertNgoTableSql::"+insertNgoTableSql);
		jdbcTemplate.update(insertNgoTableSql, new Object[] { 
				userId.toLowerCase(),panNo,chairmanName,regdNo,regdDate,city,district,state,pinCode,webSite,"system","system",address,org_address,finYear
		});
		String groupSql = adminQueries.getProperty("mapUsersToGroupSQL");
		System.out.println("groupSql::"+groupSql);
		jdbcTemplate.update(groupSql, new Object[] { 
				2,userId.toLowerCase()
		});	
		logger.info("Exit Dao--->"+methodName);
		return true;
	}
	/**
	 * This method is to get state from backend table state_master.
	 * @return List<Map<String, Object>>
	 * @author lusi
	 * @date 08-09-2017
	 */
	@Override
	public List<Map<String, Object>> getState() {
		logger.info("inside getState DaoImpl");
		final String sql = adminQueries.getProperty("getStateSQL");
		logger.info("sql--"+sql);
		return jdbcTemplate.query(sql, new RowMapper<Map<String,Object>>(){
	        @Override
	        public Map<String, Object> mapRow(ResultSet rs, int rowNum)
	          throws SQLException {
	        	Map<String,Object> map = new HashMap<String, Object>(); 
	        	map.put("state", rs.getString(1));
	         return map;
	        }
	     });
	}
	/*
	 * Used for security authentication
	 * @since 23-10-2017
	 * @author Lusi Dash
	 */
	@Override
	public UserModel getUserByUserId(String userName) {
		String sql = adminQueries.getProperty("getUserByUserId");
		UserModel user = null;
		List<UserModel> userList = jdbcTemplate.query(sql, new RowMapper<UserModel>() {
			@Override
			public UserModel mapRow(ResultSet rs, int rownumber) throws SQLException {
				UserModel user = new UserModel();
				user.setUserId(rs.getString("user_id"));
				user.setFirstName(rs.getString("first_name"));
				user.setLastName(rs.getString("last_name"));
				user.setEmail(rs.getString("email_id"));
				user.setContactNo(rs.getString("contact_no"));
				user.setPassword(rs.getString("password"));
				user.setUserType(rs.getString("user_type"));
				user.setActive(rs.getBoolean("is_active"));
				user.setPhotoUploaded(rs.getBoolean("photo_uploaded"));
				return user;
			}
		}, userName);
		if(null != userList && !userList.isEmpty()){
			user = userList.get(0);
		}
		return user;
	}

	
	/***************************************
	 * Method to store government details
	 * @author Vamsi Krish
	 ****************************************/
	/*@Override
	public boolean registerNgo(NgoModel ngoModel, HashMap<String, String>  hm) {
		String methodName = "insertNgoDetails";
		logger.info("Entry Dao--->"+methodName);
		String panno = ngoModel.getPanNumber();
		String ngoId = ngoModel.getNitiAayogId();
		String ngoName = ngoModel.getName();
		String regdNo =  ngoModel.getRegistrationNumber();
		String regdDateweb = ngoModel.getDateReg();
		 String deliverydate="02-SEP-2012";
		 SimpleDateFormat sdf=new SimpleDateFormat("YYYY-MM-DD");

		 Date date=sdf.parse(deliveryDate);

		 sdf=new SimpleDateFormat("DD-MM-YYYY");
		 System.out.println(sdf.format(date));
		 DateFormat dateFormat = new SimpleDateFormat("YYYY-DD-MM");
		 String strDate = dateFormat.format(regdDateweb);
		 System.out.println("dateFormat " + strDate);
		 SimpleDateFormat sdf = new SimpleDateFormat("DD-MM-YYYY");
		 System.out.println("sdf "+sdf);
		
		String deliverydate=regdDateweb;
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-mm-dd");

		Date date = null;
		try {
			date = sdf.parse(deliverydate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		sdf=new SimpleDateFormat("DD-MM-yyyy");
		System.out.println(sdf.format(date));
		
		String regdDate = sdf.format(date);
		
		 Date conDate = null;
		try {
			conDate = dateFormat.parse(regdDateweb);
			System.out.println("conDate  "+conDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 String regdDate = sdf.format(conDate);
		 System.out.println("regdDate "+regdDate);
		 
		String keyContactName = ngoModel.getKeycontact_name();
		String actName = ngoModel.getActName();
		String address = ngoModel.getAddress();
		String city = ngoModel.getCityReg();
		String district = ngoModel.getDistrictName();
		String state = ngoModel.getStateName();
		String pincode = ngoModel.getPincode();
		String website = ngoModel.getWebsite();
		String email = ngoModel.getEmail();
		String phoneNo = ngoModel.getMobileNo();
		String serPanStatus = ngoModel.getPanVerifyStatus();
		String userId = hm.get("user_id"); // has to change
		String password = hm.get("password"); // has to change
		//inserted in user table
		String insertUserTableSql = "insert into admin.user(user_id, first_name,email_id, contact_no, password, user_type, is_active, created_by, created_date) values(?, ?, ?, ?, ?, ?, ?, ?, now())";
		jdbcTemplate.update(insertUserTableSql, new Object[] { 
				userId.toLowerCase().trim().replace(" ",""),ngoName,email,phoneNo,password,"NGO",true,"system"
		});
		//getting Active Financial Year
				String finYearql = adminQueries.getProperty("getFinYearSQL");
				String finYear = jdbcTemplate.queryForObject(finYearql,String.class);
		//inserted in ngo registration table
		
		String insertNgoTableSql = "insert into workflow.app_ngo_registration(user_id, ngo_unique_id,pan_no, contact_name, registration_no,"
				+ "registration_date, pan_status, city, district,state,pincode,website,act_name,created_by,created_date,updated_by,updated_date,address,financial_year) "
				+ "values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now(),?,now(),?,?)";
		System.out.println("insertNgoTableSql::"+insertNgoTableSql);
		jdbcTemplate.update(insertNgoTableSql, new Object[] { 
				userId.toLowerCase().trim().replace(" ",""),ngoId,panno,keyContactName,regdNo,regdDate,serPanStatus,city,district,state,pincode,website,actName,"system","system",address,finYear
		});
		String groupSql = adminQueries.getProperty("mapUsersToGroupSQL");
		System.out.println("groupSql::"+groupSql);
		jdbcTemplate.update(groupSql, new Object[] { 
				2,userId.toLowerCase().trim().replace(" ","")
		});	
		logger.info("Exit Dao--->"+methodName);
		
		return true;
	}
	
	@Override
	public boolean updatePassword(NewRegistrationModel adminUser) {
		String methodName = "updatePassword";
		logger.info("Entry--------------->"+methodName);
		boolean status = true ;
		final String sql = adminQueries.getProperty("updatePasswordSql");
		logger.info("query--->"+sql);
		int rc = jdbcTemplate.update(sql, new Object[] {
					StringUtils.isEmpty(adminUser.getPassword()) ? "" :adminUser.getPassword(),
							 adminUser.getUser_id(),
			});
		if(rc != 1){
			status = false;
		}
		
//		final String passwordQuery = adminQueries.getProperty("forgetPasswordLogHistory");
//		final String action="FP";
//		logger.info("query--->"+passwordQuery);
//		int rc2 = jdbcTemplate.update(passwordQuery, new Object[] { 
//					adminUser.getUser_id(),
//					StringUtils.isEmpty(adminUser.getPassword()) ? "" :adminUser.getPassword(),
//					action,
//			});
//		if(rc2 != 1){
//			status = false;
//		}
		logger.info("Exit--------------->"+methodName);
		return status;
	}

	@Override
	public UserRegistrationModel getRegistrationDetails(String uid) {
		// TODO Auto-generated method stub
		String methodName = "getRegistrationDetails";
		logger.info("Entry--------------->"+methodName);
		logger.info("UserId --------------->"+uid);
		final String sql = adminQueries.getProperty("getAdminUserDetailsSQLOne");
		logger.info("query to get UserId --->"+sql);
		
		UserRegistrationModel users = jdbcTemplate.query(sql, new Object[] {uid}, new ResultSetExtractor<UserRegistrationModel>() {

			@Override
			public UserRegistrationModel extractData(ResultSet rs)
					throws SQLException, DataAccessException {
				UserRegistrationModel userModel = new UserRegistrationModel();
				if(rs.next()) {
					userModel.setUserId(rs.getString("user_id"));
					userModel.setEmail(rs.getString("email_id"));
				}
				return userModel;
			}
		});
		logger.info("Exit--------------->"+methodName);
		return users;
	}//end of getRegistrationDetails
	@Override
	public UserRegistrationModel getUserDetails(String userId,String email) {
		logger.info("user_id-->"+userId);
		String sql = null;
	
		UserRegistrationModel userInfo = null;
		 List<UserRegistrationModel> userList = null;
		if(email.equalsIgnoreCase("none")) {
			 sql = adminQueries.getProperty("getAdminUserDetailsByUserId");
			userList = jdbcTemplate.query(sql,new Object[]{userId}, new RowMapper<UserRegistrationModel>() {
					@Override
					public UserRegistrationModel mapRow(ResultSet rs, int rownumber) throws SQLException {
						UserRegistrationModel users = new UserRegistrationModel();
						users.setEmail(rs.getString(1));
						users.setUserId(rs.getString(2));
						return users;
					}
				});
		}else {
			 sql = adminQueries.getProperty("getAdminUserDetailsSQL");
			 userList = jdbcTemplate.query(sql,new Object[]{userId,email}, new RowMapper<UserRegistrationModel>() {
					@Override
					public UserRegistrationModel mapRow(ResultSet rs, int rownumber) throws SQLException {
						UserRegistrationModel users = new UserRegistrationModel();
						users.setEmail(rs.getString(1));
						users.setUserId(rs.getString(2));
						return users;
					}
				});
		}
		
		
		
		
		if(null != userList && !userList.isEmpty()){
			userInfo = userList.get(0);
			System.out.println(userInfo.toString());
		}
		return userInfo; //userInfo;
	}*/
	/*********************************************
	 * Method to store government reghister files
	 * @author Lusi Dash
	 * @param hm ,fileName
	 * @Date 05-April-2018
	 ****************************************/
	@Override
	public boolean saveGovtRegisteredFileName(HashMap<String, String> hm,
			String fileName, String fileType, String userId) {
		String methodName = "saveGovtRegisteredFileName";
		logger.info("Entry--------------->"+methodName);
		boolean status = true ;
		//String fileNameRoc = hm.get("fileNameRoc");
		//String fileNameOperation = hm.get("fileNameOperation");
		final String sql = adminQueries.getProperty("saveGovtRegisteredFileNameSql");
		logger.info("query--->"+sql);
		int rc = jdbcTemplate.update(sql, new Object[] {
				"NGO",
				fileName,
				fileType,
				userId,
				userId
			});
		if(rc != 1){
			status = false;
		}
		return status;
	}
	/*********************************************
	 * Method to get max slno from government reghister files
	 * @author Lusi Dash
	 * @Date 05-April-2018
	 ****************************************/
	@Override
	public long geMaxSlNo() {
		String getMaxFileIdeql = adminQueries.getProperty("getMaxFileIdeSQL");
		long maxId = jdbcTemplate.queryForObject(getMaxFileIdeql, long.class);
		return maxId;
	}
	/*********************************************
	 * Method to insert userId in corresponding max slno and max slno-1 row in 
	 * registered_file_upload table
	 * @param maxSlNo
	 * @author Lusi Dash
	 * @Date 05-April-2018
	 ****************************************/
	@Override
	public boolean updateFile(long maxSlNo,String userIdGovt) {
		String methodName = "updateUserIdInFile";
		logger.info("Entry--------------->"+methodName);
		boolean status = true ;
		final String sql = adminQueries.getProperty("updateUserIdInFileSql");
		logger.info("query--->"+sql);
		int rc = jdbcTemplate.update(sql, new Object[] {
				userIdGovt.toLowerCase().trim().replace(" ",""),
				userIdGovt.toLowerCase().trim().replace(" ",""),
				maxSlNo-2
			});
		if(rc != 1){
			status = false;
		}else{
			final String sql1 = adminQueries.getProperty("updateUserIdInFileSql");
			logger.info("query--->"+sql1);
			int rc1 = jdbcTemplate.update(sql1, new Object[] {
					userIdGovt.toLowerCase().trim().replace(" ",""),
					userIdGovt.toLowerCase().trim().replace(" ",""),
					maxSlNo-1
				});
			if(rc1 != 1){
				status = false;
			}else{
				final String sql2 = adminQueries.getProperty("updateUserIdInFileSql");
				logger.info("query--->"+sql);
				int rc2 = jdbcTemplate.update(sql2, new Object[] {
						userIdGovt.toLowerCase().trim().replace(" ",""),
						userIdGovt.toLowerCase().trim().replace(" ",""),
						maxSlNo
					});
				if(rc2 != 1){
					status = false;
				}
			}
		}
		return status;
	}

	/*********************************************
	 * Method to insert register file's(multiple) upload aganist userId
	 * registered_file_upload table
	 * @author Vamsi Krish
	 * @Date 16-April-2018
	 ****************************************/
	@Override
	public boolean insertRegFileUpload(HashMap<String, String> hm, String fileName, String filetype) {
		boolean status = true;
		final String sql = adminQueries.getProperty("insertRegFileUploadSQL");
		logger.info("query--->"+sql);
		int rc1 = jdbcTemplate.update(sql, new Object[] {
				hm.get("user_id"),
				hm.get("govtType"),
				true,
				fileName,
				hm.get("user_id"),
				filetype
			});
		if(rc1 != 1){
			status = false;
		}
		return status;
	}



	/*******************************************
	 * method to get all mapped user datails
	 * @author sankar patra
	 * @Date 25-08-2018
	 *****************************************/
	@Override
	public List<MaappedUserDetailsResponseModel> getListMappedUserDetails() {
		String method = "getMappedUserDetails";
		logger.info("Entry dao---->"+method);
		String mappedUserDetailsQuery = adminQueries.getProperty("mappedUserDetailsQuery");
		logger.info("query to get user mapped details-->"+mappedUserDetailsQuery);
		List<MaappedUserDetailsResponseModel> lstRspnse = jdbcTemplate.query(mappedUserDetailsQuery, new RowMapper<MaappedUserDetailsResponseModel>() {
			@Override
			public MaappedUserDetailsResponseModel mapRow(ResultSet rs,
					int rowNum) throws SQLException {
				// TODO Auto-generated method stub
				MaappedUserDetailsResponseModel mappedUser = new MaappedUserDetailsResponseModel();
				mappedUser.setSurveyId(rs.getLong(1));
				mappedUser.setClusterId(rs.getLong(3));
				mappedUser.setSurveyName(rs.getString(5));
				mappedUser.setClusterName(rs.getString(6));
				mappedUser.setSchoolName(rs.getString(7));
				mappedUser.setSchoolCategory(rs.getString(8));
				return mappedUser;
			}
		});
		logger.info("Exit dao---->"+method);
		return lstRspnse;
	}
	@Override
	public List<AllLocationModel> getAllLocationAccordingToRole(long groupId) {
		// TODO Auto-generated method stub
		System.out.println("inside method to get all location like block and district");
		//here 33 is for deo i.e. district officer
		//here 34 is for beo i.e. block officer
		String sql = null;
		if(groupId == 2) {
			sql = adminQueries.getProperty("getAllDistrictDetails");
			System.out.println("query for all district:->"+sql);
		}else {
			sql = adminQueries.getProperty("getAllBlockDetails");
			System.out.println("query for all district:->"+sql);
		}
		
		List<AllLocationModel> lstLocation = jdbcTemplate.query(sql, new RowMapper<AllLocationModel>() {
			@Override
			public AllLocationModel mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				// TODO Auto-generated method stub
				AllLocationModel location = new AllLocationModel();
				location.setDistrictId(rs.getLong(1));
				location.setDistrictName(rs.getString(2));
				return location;
			}
		});
		return lstLocation;
	}
	@Override
	public boolean mapLocationToUser(String mapUserId, long districtId) {
		// TODO Auto-generated method stub
		System.out.println("method to mapped the location id to user id");
		System.out.println("Location id:-"+districtId);
		System.out.println("User id:-"+mapUserId);
		String sql = adminQueries.getProperty("mappingUseridToLocation");
		jdbcTemplate.update(sql,new Object[] {districtId,mapUserId});
		return true;
	}
	@Override
	public List<AllLocationModel> getAllDistrict() {
		String sql =  adminQueries.getProperty("getAllDistrictDetails");
		List<AllLocationModel> lstDistrict= jdbcTemplate.query(sql, new RowMapper<AllLocationModel>() {
			@Override
			public AllLocationModel mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				// TODO Auto-generated method stub
				AllLocationModel location = new AllLocationModel();
				location.setDistrictId(rs.getLong(1));
				location.setDistrictName(rs.getString(2));
				return location;
			}
		});
		return lstDistrict;
		
	}
	@Override
	public List<AllLocationModel> getAllBlockByDistrict(long districtcode) {
		// TODO Auto-generated method stub
		
		String sql =  adminQueries.getProperty("getAllBlockByDist");
		List<AllLocationModel> lstDistrict= jdbcTemplate.query(sql,new Object[] {districtcode}, new RowMapper<AllLocationModel>() {
			@Override
			public AllLocationModel mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				// TODO Auto-generated method stub
				AllLocationModel location = new AllLocationModel();
				location.setDistrictId(rs.getLong(1));
				location.setDistrictName(rs.getString(2));
				return location;
			}
		});
		return lstDistrict;
	}
	
	@Override
	public Map<String, Object> getUserIdLocationId(String userId) {
		List<Map<String, Object>> groupIdLocationId = new ArrayList<Map<String,Object>>();
		try{
			final String insertConfigSql = adminQueries.getProperty("getUserIdLocationIdSQL");
				groupIdLocationId = jdbcTemplate.queryForList(insertConfigSql,userId);
				System.out.println(groupIdLocationId.toString());
		}catch (Exception e) {
			e.printStackTrace();
		}
		BigDecimal locationId = (BigDecimal) groupIdLocationId.get(0).get("location_id");
		String grpName = (String) groupIdLocationId.get(0).get("group_name");// USED for Cross Check
		
		Integer blockId = null, clusterId = null;
		
		if(grpName.contains("stc")){
			String districtName = jdbcTemplate.queryForObject("select district_name from master_district  where  district_id = ? limit 1", String.class, locationId);
			groupIdLocationId.get(0).put("districtId", locationId);
			groupIdLocationId.get(0).put("districtName", districtName);
		}else if(grpName.contains("tc")){
			Map<String, Object> districtId = jdbcTemplate.queryForMap("select a.district_id, b.district_name, a.block_name from master_block a,  master_district b  where a.district_id = b.district_id  and block_id = ? limit 1", locationId);
			groupIdLocationId.get(0).put("districtId", districtId.get("district_id"));
			groupIdLocationId.get(0).put("districtName", districtId.get("district_name"));
			groupIdLocationId.get(0).put("blockName", districtId.get("block_name"));
			groupIdLocationId.get(0).put("blockId", locationId);
		}/*else if(grpName.contains("CRCC")){
			Map<String, Object> blockIddistrictId = jdbcTemplate.queryForMap(
					//"select block_id, district_id from master_block where block_id = (select block_id from master_cluster where cluster_id = ? limit 1) limit 1",locationId);
					"select a.block_id, a.block_name, a.district_id, b.district_name "
					+ " from master_block a, master_district b where a.district_id = b.district_id and a.block_id = (select block_id from master_cluster  where cluster_id = ? limit 1) limit 1",locationId);
			groupIdLocationId.get(0).put("districtId", blockIddistrictId.get("district_id"));
			groupIdLocationId.get(0).put("blockId", blockIddistrictId.get("block_id"));
			groupIdLocationId.get(0).put("districtName", blockIddistrictId.get("district_name"));
			groupIdLocationId.get(0).put("blockName", blockIddistrictId.get("block_name"));
			groupIdLocationId.get(0).put("clusterId", locationId);
		}*/
		
		return groupIdLocationId.get(0);
	}
	@Override
	public List<LoginUserDetailsModel> getFirstNameLastName(String userId) {
		String sql = adminQueries.getProperty("getFirstNameLastNameSQL");
		List<LoginUserDetailsModel> firstNameLastName = jdbcTemplate.query(sql, new RowMapper<LoginUserDetailsModel>() {
			@Override
			public LoginUserDetailsModel mapRow(ResultSet rs, int rownumber) throws SQLException {
				LoginUserDetailsModel fnln = new LoginUserDetailsModel();
				fnln.setFirst_name(rs.getString("first_name"));
				fnln.setLast_name(rs.getString("last_name"));
				return fnln;
			}
		}, userId);
		return firstNameLastName;
	}
	@Override
	public boolean changePasswordOnValid(ChangePasswordModel password, String userId) {
		String sql = adminQueries.getProperty("changePasswordSQL");

		String newPassword = password.getPassword();
		String confPassword = password.getConfPassword();
		
		if(newPassword.equalsIgnoreCase(confPassword)){
			jdbcTemplate.update(sql, newPassword, userId);
			return true;
		}else{
			return false;
		}
		
	}
	@Override
	public String getOldPasswordFromDB(String userId) {
		String sql1 = adminQueries.getProperty("getPasswordByuserIdSQL");
		String passwordindb = jdbcTemplate.queryForObject(sql1, String.class, new Object[] { userId });		
	
		return passwordindb;
	}
	
	
	
}