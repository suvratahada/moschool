package com.stl.moschool.setting.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import com.stl.moschool.changepassword.controller.ChangePasswordModel;
import com.stl.moschool.common.model.LoginUserDetailsModel;
import com.stl.moschool.setting.model.AllLocationModel;
import com.stl.moschool.setting.model.ApplicationModel;
import com.stl.moschool.setting.model.CityModel;
import com.stl.moschool.setting.model.CountryModal;
import com.stl.moschool.setting.model.EmailModel;
import com.stl.moschool.setting.model.Employee;
import com.stl.moschool.setting.model.Group;
import com.stl.moschool.setting.model.HybridSeededSurveyConfigDto;
import com.stl.moschool.setting.model.HybridSeededSurveyDto;
import com.stl.moschool.setting.model.LocationModel;
import com.stl.moschool.setting.model.MaappedUserDetailsResponseModel;
import com.stl.moschool.setting.model.Parameter;
import com.stl.moschool.setting.model.ParameterModel;
import com.stl.moschool.setting.model.Resource;
import com.stl.moschool.setting.model.ResourceModel;
import com.stl.moschool.setting.model.ResourceVO;
import com.stl.moschool.setting.model.Role;
import com.stl.moschool.setting.model.RoleModel;
import com.stl.moschool.setting.model.SmsModel;
import com.stl.moschool.setting.model.UserModel;

public interface AccessControlService{
	
	/*boolean saveUserDetails(UserModel user);
	List<UserModel> getUserDetails();
	boolean updateUserDetails(UserModel user);
	boolean deleteUser(String userId);
	boolean saveGroupDetails(Group group);
	List<Group> getGroupList();
	boolean updateGroup(Group group);
	boolean deleteUserGroup(long groupId);*/
	List<UserModel> getUserDetails();
	boolean saveUserDetails(UserModel user);
	boolean updateUserDetails(UserModel user);
	boolean deleteUser(String userId);
	
	
	
	
	List<Group> getGroupList();
	boolean saveGroup(Group group);
	boolean updateGroup(Group group);
	boolean deleteGroup(long groupId);	
	List<UserModel> getMappedUserDetails(long groupId);	
	boolean mapUsersToGroup(long groupId, UserModel[] selectedUsers);
	boolean deleteMappedUser(String userId);
	List<Resource> getResourceList();
	boolean saveUserResourceTable(String resource_name, String resourceType,String description, String type,long resourceId);
	List<ResourceModel> getDataResourceTable();
	boolean deleteCreateUserResource(String resource_id);
	/*boolean saveResource(Resource resource);
	boolean updateResource(Resource resource);
	boolean deleteResource(long resourceId);*/	
	List<Resource> getMappedResources(long roleId);	
	boolean deleteMappedResources(long roleId, long resourceId);
	boolean mapResourcesToRole(long roleId, Resource[] resources);
	boolean saveUserRoleTable(String role_name, String descriptaion,String type, String id);
	List<RoleModel> getRoleDetails();
	boolean deleteCreateUserRole(String role_id);
	List<ParameterModel> getParameterTableData();
	boolean saveUserParameterTable(String param_name, String tbl_name,String type, String id);
	boolean deleteCreateParameter(String param_id);
	List<Role> getRoleList();
	/*boolean saveRole(Role role);
	boolean updateRole(Role role);
	boolean deleteRole(long roleId);*/	
	
	List<Role> getMappedRolesToGroup(long groupId);	
	boolean deleteMappedRole(long groupId, long roleId);
	boolean mapRoleToGroup(long groupId, Role[] roles);
	List<Parameter> getParameterList();
	boolean saveParameter(Parameter param);
	List<UserModel> getMappedParametersToGroup(long groupId);
	JSONObject getParameterValueTable(String tableName, String paramName,
			long paramId);
	boolean mapParamvaluesToParam(List<String> paramList, long selectedGroupId,
			String paramName);
	boolean deleteMappedParam(long sl_no);
	
	//commented by lusi on Dt-26-10-2017
//	UserModel getUserInfo(String user_id, String password);
	
	List<Long> getGroupListByUserId(String uname);
	JSONObject getResourcesAndParamList(List<Long> lstGroupId);
	/**
	 * This method will give the mapped roles to a particular
	 * user by its user id
	 * @param userId 
	 * @return List<String>
	 * @date 04-03-2017
	 */
	public List<String> getMappedRolesToUser(final String userId);
	/**
	 * This method will return the unique resources mapped to these
	 * given roles
	 * Note:- Always an User will invoke with single role
	 *  
	 * @param allowedRoles
	 * @return List<ResourceVO>
	 * @date 04-03-2017
	 */
	List<ResourceVO> getUniqueResourcesForRoles(List<String> allowedRoles);
	/**
	 * Retrieve all the available additional parameters to the user/user_group
	 * with the provided values to these parameters for dynamic data control
	 * @param userId
	 * @return HashMap<String, List<String>>
	 * @date 06-03-2017
	 */
	HashMap<String, List<String>> getDataControlDetails(String userId);
	
	/**********************************************************************
	 * Method that will used to check the existance of userId and emailId
	 ***********************************************************************/
	JSONObject validateUidAndEmail(String userid, String emailId);
	/************************************************
	 * After sending the mail successfully the corresponding userid and captcha 
	 * will save in tha database table
	 ************************************************/
	boolean saveOtpInDb(String captchaCode, String userid);
	
	/**********************************************
	 * Method that will take the otp and userid as input and 
	 * check in the table that wheather this otp is matching for this userid or not
	 ***************************************************/
	boolean validateOtp(String uid, String input_otp);
	
	/**********************************************************************
	 * method thaqt will check wheather any otp is present or not if present update or else insert
	 * @input:-uid
	 ************************************************/
	boolean resendOtp(String uid, String captchaCode);
	
	/***************************************************************************
	 * Method will set the new password of a corresponding UserId
	 * Input is password and userid
	 ***************************************************************************/
	boolean savePassword(String uid, String encryptPassword);
	/***************************************************
	 * Method to used to regester a new user
	 * 
	 ****************************************************/
	boolean newUserRegestration(HashMap<String, String> hm);
	
	/********************************************************
	 * method to check wheather this nitiaayogid is present/not for ngo
	 ********************************************************/
	boolean isNgoRegistered(HashMap<String, String> hm);
	/**********************************************************
	 * method to create unique userId
	 *************************************************************/
	long getUniqueNoFromSeq();
	/***************************************************************************************************
	 * method that will update the ngo credentials in user table as well as app_ngo_registration table
	 **************************************************************************************************/
	boolean insertNgoDetails(HashMap<String, String> hm);
	
	/********************************************************************
	 * method to check wheather this govt bodies is registered or not
	 ********************************************************************/
	
	boolean isGovtOrganisationRegistered(HashMap<String, String> hm);
	/***************************************
	 * Method to store government details
	 ****************************************/
	boolean insertGovtDetails(HashMap<String, String> hm);
	/**
	 * This method is to get state from backend table state_master.
	 * @return List<Map<String, Object>>
	 * @author lusi
	 * @date 08-09-2017
	 */
	List<Map<String, Object>> getState();
	/*
	 * Used for security authentication
	 * @since 26-10-2017
	 * @author Lusi Dash
	 */
	UserModel getUserByUserId(String userName);
	
	
	/***************************************
	 * Method to store government details
	 * @author Vamsi Krish
	 * @param hm 
	 ****************************************/
	//boolean registerNgo(NgoModel ngoModel, HashMap<String, String> hm);
	
	
	//UserRegistrationModel getUserDetails(String userId,String email);
	//boolean updatePassword(NewRegistrationModel adminUser);
	//UserRegistrationModel getRegistrationDetails(String uid);
	/*********************************************
	 * Method to store government reghister files
	 * @author Lusi Dash
	 * @param hm ,fileName
	 * @param string 
	 * @param string2 
	 * @Date 05-April-2018
	 ****************************************/
	boolean saveGovtRegisteredFileName(HashMap<String, String> hm,
			String fileName, String string, String string2);
	/*********************************************
	 * Method to get max slno from government reghister files
	 * @author Lusi Dash
	 * @Date 05-April-2018
	 ****************************************/
	long geMaxSlNo();
	/*********************************************
	 * Method to insert userId in corresponding max slno and max slno-1 row in 
	 * registered_file_upload table
	 * @param maxSlNo
	 * @author Lusi Dash
	 * @param userIdGovt 
	 * @Date 05-April-2018
	 ****************************************/
	boolean updateFile(long maxSlNo, String userIdGovt);
	/*********************************************
	 * Method to insert register file's(multiple) upload aganist userId
	 * registered_file_upload table
	 * @author Vamsi Krish
	 * @param fileType 
	 * @param string 
	 * @Date 16-April-2018
	 ****************************************/
	boolean insertRegFileUpload(HashMap<String, String> hm, String fileName, String fileType);
	
	boolean deleteEmail(EmailModel user);
	boolean updateEmailDetails(EmailModel user);
	boolean saveEmailDetails(EmailModel user);
	List<EmailModel> getEmailDetails();
	
	boolean deleteSMS(SmsModel user);
	
	boolean updateSMSDetails(SmsModel user);
	boolean saveSMSDetails(SmsModel user);
	List<SmsModel> getSMSDetails();
	//boolean deleteSMSF(String sms_user);
	//boolean deleteEmailF(String email_name);
	
	
	boolean saveEmployeeDetails(Employee user);
	List<MaappedUserDetailsResponseModel> getListMappedUserDetails();
	List<AllLocationModel> getAllLocationAccordingToRole(long groupId);
	boolean mapLocationToUser(String mapUserId, long districtId);
	List<AllLocationModel> getAllDistrict();
	List<AllLocationModel> getAllBlockByDistrict(long districtcode);
	Map<String, Object> getUserIdLocationId(String userId);
	List<LoginUserDetailsModel> getFirstNameLastName(String userId);
	boolean changePasswordOnValid(ChangePasswordModel password, String userId);
	String getOldPasswordFromDB(String userId);
	
	
}