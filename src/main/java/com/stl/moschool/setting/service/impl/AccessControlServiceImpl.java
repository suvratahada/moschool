package com.stl.moschool.setting.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.stl.moschool.changepassword.controller.ChangePasswordModel;
import com.stl.moschool.common.model.LoginUserDetailsModel;
import com.stl.moschool.setting.dao.AccessControlDao;
import com.stl.moschool.setting.model.AllLocationModel;
import com.stl.moschool.setting.model.ApplicationModel;
import com.stl.moschool.setting.model.CityModel;
import com.stl.moschool.setting.model.CountryModal;
import com.stl.moschool.setting.model.EmailModel;
import com.stl.moschool.setting.model.Employee;
import com.stl.moschool.setting.model.Group;
import com.stl.moschool.setting.model.HybridSeededSurveyConfigDto;
import com.stl.moschool.setting.model.HybridSeededSurveyDto;
import com.stl.moschool.setting.model.LocationModel;
import com.stl.moschool.setting.model.MaappedUserDetailsResponseModel;
import com.stl.moschool.setting.model.Parameter;
import com.stl.moschool.setting.model.ParameterModel;
import com.stl.moschool.setting.model.Resource;
import com.stl.moschool.setting.model.ResourceModel;
import com.stl.moschool.setting.model.ResourceVO;
import com.stl.moschool.setting.model.Role;
import com.stl.moschool.setting.model.RoleModel;
import com.stl.moschool.setting.model.SmsModel;
import com.stl.moschool.setting.model.UserModel;
import com.stl.moschool.setting.service.AccessControlService;


@Service("accessControlService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)

public class AccessControlServiceImpl implements AccessControlService {

	@Autowired
    private AccessControlDao accessControlDao;
	
	
	@Value("${default.user.password}")
	private String defaultUserPassword;
	
	@Override
	public List<UserModel> getUserDetails() {
		return accessControlDao.getUserDetails();
	}
	
	@Override
	public boolean saveUserDetails(UserModel user) {
		if(user.getPassword()==null){			;
			user.setPassword(defaultUserPassword);
		}
//		String encryptPassword = encoder.getEncriptedPassword(user.getPassword());
//		user.setPassword(encryptPassword);
		user.setActive(true);
		return accessControlDao.saveUserDetails(user);
	}
	
	@Override
	public boolean updateUserDetails(UserModel user) {
		return accessControlDao.updateUserDetails(user);
	}
	
	@Override
	public boolean deleteUser(String userId) {
		return accessControlDao.deleteUser(userId);
	}
	

	
	@Override
	public boolean saveEmployeeDetails(Employee user) {
		user.setSurvey_type("PF");
		user.setCreated_by(user.getName());
		return accessControlDao.saveEmployeeDetails(user);
	}
	
	
	@Override
	public List<EmailModel> getEmailDetails() {
		return accessControlDao.getEmailDetails();
	}
	
	@Override
	public boolean saveEmailDetails(EmailModel user) {
		
		user.setIsactive(true);
		return accessControlDao.saveEmailDetails(user);
	}
	
	@Override
	public boolean updateEmailDetails(EmailModel user) {
		return accessControlDao.updateEmailDetails(user);
	}
	
	@Override
	public boolean deleteEmail(EmailModel userId) {
		return accessControlDao.deleteEmail(userId);
	}
	/*@Override
	public boolean deleteEmailF(String userId) {
		return accessControlDao.deleteEmailF(userId);
	}*/
	
	@Override
	public boolean deleteSMS(SmsModel user) {
		// TODO Auto-generated method stub
		return accessControlDao.deleteSMS(user);
	}

	/*@Override
	public boolean deleteSMSF(String sms_user) {
		// TODO Auto-generated method stub
		return accessControlDao.deleteSMSF(sms_user);
	}*/
	
	@Override
	public boolean updateSMSDetails(SmsModel user) {
		// TODO Auto-generated method stub
		return accessControlDao.updateSMSDetails(user);
	}

	@Override
	public boolean saveSMSDetails(SmsModel user) {
		// TODO Auto-generated method stub
		return accessControlDao.saveSMSDetails(user);
	}

	@Override
	public List<SmsModel> getSMSDetails() {
		// TODO Auto-generated method stub
		return accessControlDao.getSMSDetails();
	}
	
	@Override
	public List<Group> getGroupList() {
		return accessControlDao.getGroupList();
	}

	@Override
	public boolean saveGroup(Group group) {
		group.setActive(true);
		return accessControlDao.saveGroup(group);
	}

	@Override
	public boolean updateGroup(Group group) {
		return accessControlDao.updateGroup(group);
	}

	@Override
	public boolean deleteGroup(long groupId) {
		return accessControlDao.deleteGroup(groupId);
	}

	@Override
	public List<UserModel> getMappedUserDetails(long groupId) {
		return accessControlDao.getMappedUserDetails(groupId);
	}
	@Override
	public boolean mapUsersToGroup(long groupId, UserModel[] users) {
		return accessControlDao.mapUsersToGroup(groupId, users);
	}
	@Override
	public boolean deleteMappedUser(String userId) {
		return accessControlDao.deleteMappedUser(userId);
	}
	/*************************Create Resouce**********************************************************/	
	@Override
	public boolean saveUserResourceTable(String resource_name,String resourceType,String description,String type,long resourceId) {
		// TODO Auto-generated method stub
		return accessControlDao.saveUserResourceTable(resource_name,resourceType,description,type,resourceId);
	}
	
	@Override
	public List<ResourceModel> getDataResourceTable() {
		return accessControlDao.getDataResourceTable();
	}
	@Override
	public boolean deleteCreateUserResource(String resource_id) {
		return accessControlDao.deleteCreateUserResource(resource_id);
	}
	
	@Override
	public List<Resource> getResourceList() {
		return accessControlDao.getResourceList();
	}

/*	@Override
	public boolean saveResource(Resource resource) {
		resource.setActive(true);
		return accessControlDao.saveResource(resource);
	}

	@Override
	public boolean updateResource(Resource resource) {
		return accessControlDao.updateResource(resource);
	}

	@Override
	public boolean deleteResource(long resourceId) {
		return accessControlDao.deleteResource(resourceId);
	}*/
	
	@Override
	public List<Resource> getMappedResources(long roleId) {
		return accessControlDao.getMappedResources(roleId);
	}
	
	@Override
	public boolean deleteMappedResources(long roleId, long resourceId) {
		return accessControlDao.deleteMappedResources(roleId, resourceId);
	}
	@Override
	public boolean mapResourcesToRole(long roleId, Resource[] resources) {
		return accessControlDao.mapResourcesToRole(roleId, resources);
	}	
	
	/*************************Create Role**********************************************************/		
	@Override
	public boolean saveUserRoleTable(String role_name, String description,
			String type, String id) {
		// TODO Auto-generated method stub
		return accessControlDao.saveUserRoleTable(role_name,description,type,id);
	}
	
	@Override
	public List<RoleModel> getRoleDetails() {
		return accessControlDao.getRoleDetails();
	}
	@Override
	public boolean deleteCreateUserRole(String role_id) {
		return accessControlDao.deleteCreateUserRole(role_id);
	}
	
/*********************************************CREATE PARAMETER**************************************************/
	@Override
	public boolean saveUserParameterTable(String param_name, String tbl_name,
			String type, String id) {
		// TODO Auto-generated method stub
		return accessControlDao.saveUserParameterTable(param_name,tbl_name,type,id);
	}
	@Override
	public List<ParameterModel> getParameterTableData() {
		return accessControlDao.getParameterTableData();
	}
	@Override
	public boolean deleteCreateParameter(String param_id) {
		return accessControlDao.deleteCreateParameter(param_id);
	}
	
	@Override
	public List<Role> getRoleList() {
		return accessControlDao.getRoleList();
	}

/*	@Override
	public boolean saveRole(Role role) {
		role.setActive(true);
		return accessControlDao.saveRole(role);
	}

	@Override
	public boolean updateRole(Role role) {
		return accessControlDao.updateRole(role);
	}

	@Override
	public boolean deleteRole(long roleId) {
		return accessControlDao.deleteRole(roleId);
	}*/
	
	@Override
	public List<Role> getMappedRolesToGroup(long groupId) {
		return accessControlDao.getMappedRolesToGroup(groupId);
	}
	
	@Override
	public boolean deleteMappedRole(long groupId, long roleId) {
		return accessControlDao.deleteMappedRole(groupId, roleId);
	}
	@Override
	public boolean mapRoleToGroup(long groupId, Role[] roles) {
		return accessControlDao.mapRoleToGroup(groupId, roles);
	}

	@Override
	public List<Parameter> getParameterList() {
		// TODO Auto-generated method stub
		return accessControlDao.getParameterList();
	}

	@Override
	public boolean saveParameter(Parameter param) {
		// TODO Auto-generated method stub
		return accessControlDao.saveParameter(param);
	}
	/**************************************
	 * list of mapped parameter to group
	 *************************************/
	@Override
	public List<UserModel> getMappedParametersToGroup(long groupId) {
		// TODO Auto-generated method stub
		return accessControlDao.getMappedParametersToGroup(groupId);
	}

	@Override
	public JSONObject getParameterValueTable(String tableName, String paramName,
			long paramId) {
		// TODO Auto-generated method stub
		return accessControlDao.getParameterValueTable(tableName,paramName,paramId);
	}

	@Override
	public boolean mapParamvaluesToParam(List<String> paramList,
			long selectedGroupId, String paramName) {
		// TODO Auto-generated method stub
		return accessControlDao.mapParamvaluesToParam(paramList,selectedGroupId,paramName);
	}

	@Override
	public boolean deleteMappedParam(long sl_no) {
		// TODO Auto-generated method stub
		return accessControlDao.deleteMappedParam(sl_no);
	}

	//commented by lusi on Dt-26-10-2017
	/*@Override
	public UserModel getUserInfo(String user_id, String password) {
		return accessControlDao.getUserInfo(user_id,password);
	}*/

	@Override
	public List<Long> getGroupListByUserId(String uname) {
		// TODO Auto-generated method stub
		return accessControlDao.getGroupListByUserId(uname);
	}

	@Override
	public JSONObject getResourcesAndParamList(
			List<Long> lstGroupId) {
		// TODO Auto-generated method stub
		return accessControlDao.getResourcesAndParamList(lstGroupId);
	}
	/**
	 * This method will give the mapped roles to a particular
	 * user by its user id
	 * @param userId 
	 * @return List<String>
	 * @date 04-03-2017
	 */
	@Override
	public List<String> getMappedRolesToUser(String userId) {
		
		return accessControlDao.getMappedRolesToUser(userId);
	}
	/**
	 * This method will return the unique resources mapped to these
	 * given roles
	 * Note:- Always an User will invoke with single role
	 *  
	 * @param allowedRoles
	 * @return List<ResourceVO>
	 * @date 04-03-2017
	 */
	@Override
	public List<ResourceVO> getUniqueResourcesForRoles(List<String> allowedRoles) {
		return accessControlDao.getUniqueResourcesForRoles(allowedRoles);
	}
	/**
	 * Retrieve all the available additional parameters to the user/user_group
	 * with the provided values to these parameters for dynamic data control
	 * @param userId
	 * @return HashMap<String, List<String>>
	 * @date 06-03-2017
	 */
	@Override
	public HashMap<String, List<String>> getDataControlDetails(String userId) {
		return accessControlDao.getDataControlDetails(userId);
	}
	/**********************************************************************
	 * Method that will used to check the existance of userId and emailId
	 ***********************************************************************/
	@Override
	public JSONObject validateUidAndEmail(String userid, String emailId) {
		// TODO Auto-generated method stub
		return accessControlDao.validateUidAndEmail(userid,emailId);
	}
	
	/************************************************
	 * After sending the mail successfully the corresponding userid and captcha 
	 * will save in tha database table
	 ************************************************/
	@Override
	public boolean saveOtpInDb(String captchaCode, String userid) {
		// TODO Auto-generated method stub
		return accessControlDao.saveOtpInDb(captchaCode,userid);
	}
	/**********************************************
	 * Method that will take the otp and userid as input and 
	 * check in the table that wheather this otp is matching for this userid or not
	 ***************************************************/
	@Override
	public boolean validateOtp(String uid, String input_otp) {
		// TODO Auto-generated method stub
		return accessControlDao.validateOtp(uid,input_otp);
	}
	/**********************************************************************
	 * method thaqt will check wheather any otp is present or not if present update or else insert
	 * @input:-uid
	 ************************************************/
	@Override
	public boolean resendOtp(String uid, String captchaCode) {
		// TODO Auto-generated method stub
		return accessControlDao.resendOtp(uid,captchaCode);
	}
	/***************************************************************************
	 * Method will set the new password of a corresponding UserId
	 * Input is password and userid
	 ***************************************************************************/
	@Override
	public boolean savePassword(String uid, String encryptPassword) {
		// TODO Auto-generated method stub
		return accessControlDao.savePassword(uid,encryptPassword);
	}	
	
	/***************************************************
	 * Method to used to regester a new user
	 * 
	 ****************************************************/
	@Override
	public boolean newUserRegestration(HashMap<String, String> hm) {
		// TODO Auto-generated method stub
		return accessControlDao.newUserRegestration(hm);
	}
	/*
	//method to check wheather this nitiaayogid is present/not for ngo
	 * (non-Javadoc)
	 * @see com.stl.dpci.setting.service.AccessControlService#isNgoRegistered(java.util.HashMap)
	 */
	@Override
	public boolean isNgoRegistered(HashMap<String, String> hm) {
		// TODO Auto-generated method stub
		return accessControlDao.isNgoRegistered(hm);
	}
	
	//method to create unique userId
	@Override
	public long getUniqueNoFromSeq() {
		// TODO Auto-generated method stub
		return accessControlDao.getUniqueNoFromSeq();
	}
	/***************************************************************************************************
	 * method that will update the ngo credentials in user table as well as app_ngo_registration table
	 **************************************************************************************************/
	@Override
	public boolean insertNgoDetails(HashMap<String, String> hm) {
		// TODO Auto-generated method stub
		return accessControlDao.insertNgoDetails(hm);
	}
	
	/*******************************************************************
	 * method to check wheather this govt bodies is registered or not
	 *********************************************************************/
	
	@Override
	public boolean isGovtOrganisationRegistered(HashMap<String, String> hm) {
		// TODO Auto-generated method stub
		return accessControlDao.isGovtOrganisationRegistered(hm);
	}
	
	/***************************************
	 * Method to store government details
	 ****************************************/
	@Override
	public boolean insertGovtDetails(HashMap<String, String> hm) {
		// TODO Auto-generated method stub
		return accessControlDao.insertGovtDetails(hm);
	}
	/**
	 * This method is to get state from backend table state_master.
	 * @return List<Map<String, Object>>
	 * @author lusi
	 * @date 08-09-2017
	 */
	@Override
	public List<Map<String, Object>> getState() {
		return accessControlDao.getState();
	}
	/*
	 * Used for security authentication
	 * @since 23-10-2017
	 * @author Lusi Dash
	 */
	@Override
	public UserModel getUserByUserId(String userName) {
		return accessControlDao.getUserByUserId(userName);
	}

	/***************************************
	 * Method to store government details
	 * @author Vamsi Krish
	 ****************************************/
	/*@Override
	public boolean registerNgo(NgoModel ngoModel, HashMap<String, String> hm) {
		return accessControlDao.registerNgo(ngoModel, hm);
	}*/
	
	/*@Override
	public boolean updatePassword(NewRegistrationModel adminUser) {
		// TODO Auto-generated method stub
		return accessControlDao.updatePassword(adminUser);
	}*/

	/*@Override
	public UserRegistrationModel getRegistrationDetails(String uid) {
		// TODO Auto-generated method stub
		return accessControlDao.getRegistrationDetails(uid);
	}*/
	

	/*@Override
	public UserRegistrationModel getUserDetails(String userId,String email) {
		// TODO Auto-generated method stub
		return accessControlDao.getUserDetails(userId,email);
	}*/
	/*********************************************
	 * Method to store government reghister files
	 * @author Lusi Dash
	 * @param hm ,fileName
	 * @Date 05-April-2018
	 ****************************************/
	@Override
	public boolean saveGovtRegisteredFileName(HashMap<String, String> hm,
			String fileName, String fileType, String userId) {
		return accessControlDao.saveGovtRegisteredFileName(hm,fileName, fileType, userId);
	}
	/*********************************************
	 * Method to get max slno from government reghister files
	 * @author Lusi Dash
	 * @Date 05-April-2018
	 ****************************************/
	@Override
	public long geMaxSlNo() {
		return accessControlDao.geMaxSlNo();
	}
	/*********************************************
	 * Method to insert userId in corresponding max slno and max slno-1 row in 
	 * registered_file_upload table
	 * @param maxSlNo
	 * @author Lusi Dash
	 * @Date 05-April-2018
	 ****************************************/
	@Override
	public boolean updateFile(long maxSlNo,String userIdGovt) {
		return accessControlDao.updateFile(maxSlNo,userIdGovt);
	}

	/*********************************************
	 * Method to insert register file's(multiple) upload aganist userId
	 * registered_file_upload table
	 * @author Vamsi Krish
	 * @Date 16-April-2018
	 ****************************************/
	@Override
	public boolean insertRegFileUpload(HashMap<String, String> hm, String fileName, String fileType) {
		return accessControlDao.insertRegFileUpload(hm, fileName, fileType);
	}

	@Override
	public List<MaappedUserDetailsResponseModel> getListMappedUserDetails() {
		return accessControlDao.getListMappedUserDetails();
	}

	@Override
	public List<AllLocationModel> getAllLocationAccordingToRole(long groupId) {
		// TODO Auto-generated method stub
		return accessControlDao.getAllLocationAccordingToRole(groupId);
	}

	@Override
	public boolean mapLocationToUser(String mapUserId, long districtId) {
		// TODO Auto-generated method stub
		return accessControlDao.mapLocationToUser(mapUserId,districtId);
	}

	@Override
	public List<AllLocationModel> getAllDistrict() {
		// TODO Auto-generated method stub
		return accessControlDao.getAllDistrict();
	}

	@Override
	public List<AllLocationModel> getAllBlockByDistrict(long districtcode) {
		// TODO Auto-generated method stub
		return accessControlDao.getAllBlockByDistrict(districtcode);
	}

	@Override
	public Map<String, Object> getUserIdLocationId(String userId) {

		return accessControlDao.getUserIdLocationId(userId);
	}

	@Override
	public List<LoginUserDetailsModel> getFirstNameLastName(String userId) {
		return accessControlDao.getFirstNameLastName(userId);
	}

	@Override
	public boolean changePasswordOnValid(ChangePasswordModel password, String userId) {
		return accessControlDao.changePasswordOnValid(password, userId);
	}

	@Override
	public String getOldPasswordFromDB(String userId) {
		return accessControlDao.getOldPasswordFromDB(userId);
	}

	

	

	

	

	

	
	

	
	
	
	
}

