package com.stl.moschool.setting.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.stl.moschool.setting.model.AllLocationModel;
import com.stl.moschool.setting.model.ApplicationModel;
import com.stl.moschool.setting.model.CityModel;
import com.stl.moschool.setting.model.CountryModal;
import com.stl.moschool.setting.model.EmailModel;
import com.stl.moschool.setting.model.Employee;
import com.stl.moschool.setting.model.Group;
import com.stl.moschool.setting.model.GroupRoleMapModel;
import com.stl.moschool.setting.model.HybridSeededSurveyConfigDto;
import com.stl.moschool.setting.model.HybridSeededSurveyDto;
import com.stl.moschool.setting.model.LocationModel;
import com.stl.moschool.setting.model.MaappedUserDetailsResponseModel;
import com.stl.moschool.setting.model.Parameter;
import com.stl.moschool.setting.model.ParameterModel;
import com.stl.moschool.setting.model.Resource;
import com.stl.moschool.setting.model.ResourceModel;
import com.stl.moschool.setting.model.Role;
import com.stl.moschool.setting.model.RoleModel;
import com.stl.moschool.setting.model.SmsModel;
import com.stl.moschool.setting.model.UserGroupParameter;
import com.stl.moschool.setting.model.UserModel;
import com.stl.moschool.setting.service.AccessControlService;

@Controller
@RequestMapping("/setting")
public class AccessControlController {
	
	
	final static Logger logger = Logger.getLogger(AccessControlController.class);
	
	@Autowired
	private AccessControlService accessControlService;
	
	@RequestMapping(value = { "/default" }, method = RequestMethod.GET)
	public String defaultSettingView(HttpSession session) {
		session.setAttribute("sessionDashboardTab", "styleActiveSettings");
		logger.debug("redirecting to defaultSettingView");
		return "defaultSettingView";
	}
	
	@RequestMapping(value = { "/createUser" }, method = RequestMethod.GET)
	public String createUserView(HttpSession session) {
		logger.debug("redirecting to  createUserView");
		session.setAttribute("sessionDashboardTab", "styleActiveSettings");
		return "createUserView";
	}
	
	
	/*@RequestMapping(value = { "/createEmail" }, method = RequestMethod.GET)
	public String createEmailView(HttpSession session) {
		logger.debug("redirecting to  createUserView");
		session.setAttribute("sessionDashboardTab", "styleActiveConfiguration");
		return "createEmailView";
	}
	
	
	@RequestMapping(value = { "/createSms" }, method = RequestMethod.GET)
	public String createSmsView(HttpSession session) {
		logger.debug("redirecting to  createSmsView");
		session.setAttribute("sessionDashboardTab", "styleActiveConfiguration");
		return "createSmsView";
	}
	
	*/
	@RequestMapping(value = { "/userGroups" }, method = RequestMethod.GET)
	public String userGroupsView(HttpSession session) {
		session.setAttribute("sessionDashboardTab", "styleActiveSettings");
		logger.debug("redirecting to  userGroupsView");

		return "userGroupsView";
	}
	
	@RequestMapping(value = { "/createResources" }, method = RequestMethod.GET)
	public String defineResourcesView(HttpSession session) {
		session.setAttribute("sessionDashboardTab", "styleActiveSettings");
		logger.debug("redirecting to  defineResourcesView");
		return "createResourcesView";
	}
	
	@RequestMapping(value = { "/createRole" }, method = RequestMethod.GET)
	public String createRoleView(HttpSession session) {
		session.setAttribute("sessionDashboardTab", "styleActiveSettings");
		logger.debug("redirecting to  createRoleView");
		return "createRoleView";
	}
	
	
	@RequestMapping(value = { "/groupRoleMap" }, method = RequestMethod.GET)
	public String groupRoleMap(HttpSession session) {
		session.setAttribute("sessionDashboardTab", "styleActiveSettings");
		logger.debug("redirecting to  createRoleView");

		return "roleResourcesMapView";
	}
	
	@RequestMapping(value = { "/userRoleMap" }, method = RequestMethod.GET)
	public String userRoleMapView(HttpSession session) {
		session.setAttribute("sessionDashboardTab", "styleActiveSettings");
		logger.debug("redirecting to  userRoleMapView");

		return "userRoleMapView";
	}
	
	//Method that will return view for craeate Parameter submenu
	
	@RequestMapping(value = { "/createParameter"}, method = RequestMethod.GET  )
	public String createParameterView(HttpSession session) {
		session.setAttribute("sessionDashboardTab", "styleActiveSettings");
		logger.info("redirecting to  creatParameter");
		return "createParameterView";
	}
	
	
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}
	
	/*****************************************************
	 * Method to Get UserDetails
	*******************************************************/
	@RequestMapping(value={"/getUserDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getUserDetails(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		List <UserModel> lstUserDetails = accessControlService.getUserDetails();
		
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", lstUserDetails);
		
		return resp;
	}
	
	
	
	@RequestMapping(value={"/saveUserDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> saveUserDetails( @RequestBody @Valid UserModel user, BindingResult bresult) { //@ModelAttribute UserModel user,
		System.out.println("Inside:: @@ Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug(user.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = accessControlService.saveUserDetails(user);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully saved user details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to save user details");
			}
		}
		
		return resp;
	}
	
	@RequestMapping(value={"/updateUserDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> updateUserDetails(@RequestBody @Valid UserModel user, BindingResult bresult) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug(user.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			System.out.println("Inside:: @@ hasnotErrors"); 
			boolean status = accessControlService.updateUserDetails(user);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully updated user details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to update user details");
			}
		}
		
		
		return resp;
	}
	
	@RequestMapping(value={"/deleteUser"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteUser(@RequestBody @Valid UserModel user, BindingResult bresult) {//@RequestParam("userId") String userId
		
		Map<String, Object> resp = new HashMap<String, Object>();
		System.out.println("userId: " + user.getUserId());
		logger.debug("userId: " + user.getUserId());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			boolean status = accessControlService.deleteUser(user.getUserId());
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully deleted user details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to delete user details");
			}
		}
		return resp;
	}
	
	
	
	@RequestMapping(value={"/saveEmployeeDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> saveEmployeeDetails( @RequestBody @Valid Employee user, BindingResult bresult) { //@ModelAttribute EmployeeModel user,
		System.out.println("Inside:: @@ Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug(user.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			System.out.println("Inside:: @@ NotErrors"); 
			System.out.println(user.toString());
			boolean status = accessControlService.saveEmployeeDetails(user);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully saved user details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to save user details");
			}
		}
		
		return resp;
	}
	
	
	
	@RequestMapping(value={"/getEmailDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getEmailDetails(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		List <EmailModel> lstUserDetails = accessControlService.getEmailDetails();
		
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", lstUserDetails);
		
		return resp;
	}
	
	
	
	@RequestMapping(value={"/saveEmailDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> saveEmailDetails( @RequestBody @Valid EmailModel user, BindingResult bresult) { //@ModelAttribute UserModel user,
		System.out.println("Inside:: @@ Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug(user.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = accessControlService.saveEmailDetails(user);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully saved user details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to save user details");
			}
		}
		
		return resp;
	}
	
	@RequestMapping(value={"/updateEmailDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> updateEmailDetails(@RequestBody @Valid EmailModel user, BindingResult bresult) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug(user.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			System.out.println("Inside:: @@ hasnotErrors"); 
			boolean status = accessControlService.updateEmailDetails(user);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully udated user details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to update user details");
			}
		}
		
		
		return resp;
	}
	
	@RequestMapping(value={"/deleteEmail"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteEmail(@RequestBody @Valid EmailModel user, BindingResult bresult) {//@RequestParam("userId") String userId
		
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug("userId: " + user.getEmail_name());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			boolean status = accessControlService.deleteEmail(user);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully deleted user details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to delete user details");
			}
		}
		return resp;
	}
	
	/*@RequestMapping(value={"/deleteEmailF"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteEmailF(@RequestBody @Valid EmailModel user, BindingResult bresult) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug("userId: " + user.getEmail_name());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			boolean status = accessControlService.deleteEmailF(user.getEmail_name());
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully deleted user details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to delete user details");
			}
		}
		return resp;
	}*/
	
	@RequestMapping(value={"/getSMSDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getSMSDetails(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		List <SmsModel> lstUserDetails = accessControlService.getSMSDetails();
		
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", lstUserDetails);
		
		return resp;
	}
	
	
	
	@RequestMapping(value={"/saveSMSDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> saveSMSDetails( @RequestBody @Valid SmsModel user, BindingResult bresult) { //@ModelAttribute UserModel user,
		System.out.println("Inside:: @@ Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug(user.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = accessControlService.saveSMSDetails(user);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully saved user details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to save user details");
			}
		}
		
		return resp;
	}
	
	@RequestMapping(value={"/updateSMSDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> updateSMSDetails(@RequestBody @Valid SmsModel user, BindingResult bresult) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug(user.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			System.out.println("Inside:: @@ hasnotErrors"); 
			boolean status = accessControlService.updateSMSDetails(user);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully udated user details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to update user details");
			}
		}
		
		
		return resp;
	}
	
	@RequestMapping(value={"/deleteSMS"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteSMS(@RequestBody @Valid SmsModel user, BindingResult bresult) {//@RequestParam("userId") String userId
		
		Map<String, Object> resp = new HashMap<String, Object>();
		System.out.println(user);
		logger.debug("userId: " + user.getSms_user());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			boolean status = accessControlService.deleteSMS(user);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully deleted user details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to delete user details");
			}
		}
		return resp;
	}
	
	/*@RequestMapping(value={"/deleteSMSF"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteSMSF(@RequestBody @Valid SmsModel user, BindingResult bresult) {//@RequestParam("userId") String userId
		
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug("userId: " + user.getSms_user());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			boolean status = accessControlService.deleteSMSF(user.getSms_user());
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully deleted user details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to delete user details");
			}
		}
		return resp;
	}*/
	
	/*@RequestMapping(value={"/changeSMS"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> changeSMS(@RequestBody @Valid SmsModel user, BindingResult bresult) {//@RequestParam("userId") String userId
		
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug("userId: " + user.getSms_user());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			boolean status = accessControlService.deleteSMS(user.getSms_user());
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully deleted user details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to delete user details");
			}
		}
		return resp;
	}*/
	
	/*****************************************************
	 * Method to Get Group Details
	*******************************************************/
	@RequestMapping(value={"/getGroupDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getGroupDetails(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		List <Group> groupList = accessControlService.getGroupList();
		
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", groupList);
		
		return resp;
	}
	
	@RequestMapping(value={"/saveGroupDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> saveGroupDetails(@ModelAttribute Group group) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug(group.toString());
		
		boolean status = accessControlService.saveGroup(group);
		if(status){
			resp.put("success", true);
			resp.put("msg", "Successfully saved group details");
		}else{
			resp.put("success", false);
			resp.put("msg", "Failed to save group details");
		}
		
		return resp;
	}
	
	@RequestMapping(value={"/updateGroupDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> updateGroupDetails(@ModelAttribute Group group) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug(group.toString());
		
		boolean status = accessControlService.updateGroup(group);
		if(status){
			resp.put("success", true);
			resp.put("msg", "Successfully updated user details");
		}else{
			resp.put("success", false);
			resp.put("msg", "Failed to update user details");
		}
		
		return resp;
	}
	
	@RequestMapping(value={"/deleteGroup"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteGroup(@RequestBody Group group) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
		System.out.println("groupId: " + group.getGroupId());
		logger.debug("groupId: " + group.getGroupId());
		
		boolean status = accessControlService.deleteGroup(group.getGroupId());
		if(status){
			resp.put("success", true);
			resp.put("msg", "Successfully deleted group details");
		}else{
			resp.put("success", false);
			resp.put("msg", "Failed to delete group details");
		}
		
		return resp;
	}

	@RequestMapping(value={"/getMappedUserDetails/{groupId}"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getMappedUserDetails(@PathVariable("groupId") long groupId, HttpServletRequest request, HttpServletResponse response) {
		System.out.println(groupId);
		Map<String, Object> resp = new HashMap<String, Object>();
		
		List<UserModel> lstUser = accessControlService.getMappedUserDetails(groupId);
		
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", lstUser);
		
		return resp;
	}
	
	@RequestMapping(value={"/mapUsersToGroup"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> mapUsersToGroup(HttpServletRequest request, HttpServletResponse response, @RequestBody UserModel[]  selectedUsers, @RequestParam("groupId") long groupId) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		boolean status = accessControlService.mapUsersToGroup(groupId, selectedUsers);
		if(status){
			resp.put("success", true);
			resp.put("msg", "Successfully mapped");
		}else{
			resp.put("success", false);
			resp.put("msg", "Failed to mapped");
		}
		return resp;
	}
	
	@RequestMapping(value={"/deleteMappedUser/{userId}"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteMappedUser(@PathVariable("userId") String userId, HttpServletRequest request, HttpServletResponse response) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug("userId : " + userId);
		System.out.println(userId);
		boolean status = accessControlService.deleteMappedUser(userId);
		if(status){
			resp.put("success", true);
			resp.put("msg", "Successfully removed user from group");
		}else{
			resp.put("success", false);
			resp.put("msg", "Failed to remove user from group");
		}
		
		return resp;
	}
	
/*****************************************Create Resource************************************************/
 
	
	/*****************************************************
	 * Method to Add saveUserResourceTable
	*******************************************************/
	
	@RequestMapping(value={"/saveUserResourceTable"}, method = RequestMethod.POST)

	public @ResponseBody Map<String, Object> saveUserResourceTable( @RequestBody @Valid ResourceModel resource, BindingResult bresult,HttpServletRequest req,HttpServletResponse res){ //@ModelAttribute ResourceModel resource,HttpServletRequest req,HttpServletResponse res) {
		logger.info("Inside saveUserResourceTable Controller");
		Map<String, Object> resp = new HashMap<String, Object>();
		System.out.println("resource----"+resource.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			System.out.println("Inside:: @@ hasNoErrors"); 
			String resource_name = resource.getResource_name();
			String resourceType =resource.getResource_type();
			String description = resource.getDescription();
			String type = resource.getType();
			String id = resource.getId();
			long resourceId = Long.parseLong(id); 
		
			boolean status = accessControlService.saveUserResourceTable(resource_name,resourceType,description,type,resourceId);
			//logger.info("resource_id------"+resource_id+" "+"resource_name-----"+resource_name+" "+"resource_type-----"+resource_type+" "+"description-----"+description+" "+"type-----"+type+"");
			
			if(status){
				resp.put("success", true);
				resp.put("msg", "Record has been saved Successfully.");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to add resource");
			}
		}
//		String resource_name = req.getParameter("resourcename");
//		String resourceType = req.getParameter("resourceType");
//		String description = req.getParameter("description");
//		String type = req.getParameter("type");
//		String id = req.getParameter("id");
//		long resourceId = Long.parseLong(id); 
//	
//		boolean status = accessControlService.saveUserResourceTable(resource_name,resourceType,description,type,resourceId);
//		//logger.info("resource_id------"+resource_id+" "+"resource_name-----"+resource_name+" "+"resource_type-----"+resource_type+" "+"description-----"+description+" "+"type-----"+type+"");
//		Map<String, Object> resp = new HashMap<String, Object>();
//		if(status){
//			resp.put("success", true);
//			resp.put("msg", "Record has been saved successfully.............");
//		}else{
//			resp.put("success", false);
//			resp.put("msg", "Failed to add resource");
//		}
		return resp;
	}
	
	/*****************************************************
	 * Method to Get getDataResourceTable
	*******************************************************/
	@RequestMapping(value={"/getDataResourceTable"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getDataResourceTable(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		List <ResourceModel> lstUserDetails = accessControlService.getDataResourceTable();
		
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", lstUserDetails);
		
		return resp;
	}
	
	
	/*****************************************************
	 * Method to deleteCreateUserResource
	*******************************************************/
	@RequestMapping(value={"/deleteCreateUserResource"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteCreateUserResource(@RequestBody @Valid ResourceModel resource, BindingResult bresult){ //@RequestParam("resource_id") String resource_id) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		System.out.println("resource_id: " + resource.getResource_id());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			System.out.println("Inside:: @@ hasNoErrors"); 
			boolean status = accessControlService.deleteCreateUserResource(Long.toString(resource.getResource_id()));
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully deleted user details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to delete user details");
			}
		}
		
		return resp;
	}
	

	/*****************************************************
	 * Method to Get Resource Details
	*******************************************************/
	/*@RequestMapping(value={"/getResourceList"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getResourceList(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		List<Resource> resourceList = accessControlService.getResourceList();
		
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", resourceList);
		
		return resp;
	}
	
	@RequestMapping(value={"/saveResource"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> saveResource(@ModelAttribute Resource resource) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug(resource.toString());
		
		boolean status = accessControlService.saveResource(resource);
		if(status){
			resp.put("success", true);
			resp.put("msg", "Successfully saved resource details");
		}else{
			resp.put("success", false);
			resp.put("msg", "Failed to save resource details");
		}
		
		return resp;
	}
	
	@RequestMapping(value={"/updateResource"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> updateResource(@ModelAttribute Resource resource) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug(resource.toString());
		
		boolean status = accessControlService.updateResource(resource);
		if(status){
			resp.put("success", true);
			resp.put("msg", "Successfully updated resource details");
		}else{
			resp.put("success", false);
			resp.put("msg", "Failed to update resource details");
		}
		
		return resp;
	}
	
	@RequestMapping(value={"/deleteResource"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteResource(@RequestParam("resourceId") long resourceId) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug("resourceId: " + resourceId);
		
		boolean status = accessControlService.deleteResource(resourceId);
		if(status){
			resp.put("success", true);
			resp.put("msg", "Successfully deleted resource details");
		}else{
			resp.put("success", false);
			resp.put("msg", "Failed to delete resource details");
		}
		
		return resp;
	}*/
	
	/*@RequestMapping(value={"/getMappedResources"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getMappedResources(@RequestParam("roleId") long roleId) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug("roleId: " + roleId);
		
		List<Resource> resourceList = accessControlService.getMappedResources(roleId);
		
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", resourceList);
		
		return resp;
	}
	
	@RequestMapping(value={"/deleteMappedResources"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteMappedResources(@RequestParam("resourceId") long resourceId, @RequestParam("roleId") long roleId) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug("resourceId: " + resourceId);
		logger.debug("roleId: " + roleId);
		
		boolean status = accessControlService.deleteMappedResources(roleId, resourceId);
		if(status){
			resp.put("success", true);
			resp.put("msg", "Successfully deleted resource");
		}else{
			resp.put("success", false);
			resp.put("msg", "Failed to delete resource");
		}
		
		return resp;
	}
	
	@RequestMapping(value={"/mapResourcesToRole.do"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> mapResourcesToRole(HttpServletRequest request, HttpServletResponse response, @RequestBody Resource[]  selectedResources, @RequestParam("roleId") long roleId) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		boolean status = accessControlService.mapResourcesToRole(roleId, selectedResources);
		if(status){
			resp.put("success", true);
			resp.put("msg", "Successfully mapped");
		}else{
			resp.put("success", false);
			resp.put("msg", "Failed to mapped");
		}
		return resp;
	}*/
	
/*********************************************CREATE ROLE**************************************************/
	
	/*****************************************************
	 * Method to Add CREATE ROLE
	*******************************************************/
	
	@RequestMapping(value={"/saveUserRoleTable"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> saveUserRoleTable(HttpServletRequest req,HttpServletResponse res, 
			@RequestBody @Valid Role role, BindingResult bresult) {
		logger.info("Inside saveUserRoleTable Controller");
		
			
		Map<String, Object> resp = new HashMap<String, Object>();
		
			System.out.println("role---: " + role.toString());
			if(bresult.hasErrors()){
				System.out.println("Inside:: @@ hasErrors"); 
				resp.put("success", false);
				resp.put("status", "failure");
				resp.put("msg", "Invalid Request.");
//				return result.toString();
				
			}else{
				System.out.println("Inside:: @@ hasNotErrors"); 
				String rolename = role.getRoleName();
				String descriptaion = role.getDescription();
				String type = role.getType();
				String id = role.getId();
				boolean status = accessControlService.saveUserRoleTable(rolename,descriptaion,type,id);
				
					if(status){
						if(type.equalsIgnoreCase("ADD")){
							resp.put("success", true);
							resp.put("msg", "Record has been saved successfully.");
						}else if(type.equalsIgnoreCase("UPDATE")){
							resp.put("success", true);
							resp.put("msg", "Record has been updated successfully.");
						}
					}else{
						resp.put("success", false);
						resp.put("msg", "Failed.");
					}
			}
		
		
		return resp;
	}
	
	/*****************************************************
	 * Method to Get getDataRoleTable
	*******************************************************/
	@RequestMapping(value={"/getRoleDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getRoleDetails(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		List <RoleModel> lstRoleDetails = accessControlService.getRoleDetails();
		
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", lstRoleDetails);
		
		return resp;
	}
	
	/*****************************************************
	 * Method to deleteCreateUserRole
	*******************************************************/
	@RequestMapping(value={"/deleteCreateUserRole"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteCreateUserRole(@RequestBody @Valid Role role, BindingResult bresult) {//@RequestParam("role_id") String role_id
		
		Map<String, Object> resp = new HashMap<String, Object>();
	
		System.out.println("role_id: " + role.getRoleId());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			boolean status = accessControlService.deleteCreateUserRole( Long.toString(role.getRoleId()));
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully deleted user details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to delete user details");
			}
		}
		return resp;
	}
	@RequestMapping(value={"/getMappedResources"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getMappedResources(@RequestBody @Valid Role role, BindingResult bresult) {//@RequestParam("roleId") long roleId
		
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug("roleId: " + role.getRoleId());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			System.out.println("Inside:: @@ hasNotErrors"); 
			List<Resource> resourceList = accessControlService.getMappedResources(role.getRoleId());
			
			resp.put("success", true);
			resp.put("msg", "Successfully received");
			resp.put("data", resourceList);
		}
		
		
		return resp;
	}
	
	@RequestMapping(value={"/deleteMappedResources"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteMappedResources(@RequestBody @Valid Role role, BindingResult bresult) {//@RequestParam("resourceId") long resourceId, @RequestParam("roleId") long roleId
		
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug("resourceId: " + role.getResource_id());
		logger.debug("roleId: " + role.getRoleId());
		
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			boolean status = accessControlService.deleteMappedResources(role.getRoleId(), role.getResource_id());
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully deleted resource");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to delete resource");
			}
		}
		
		return resp;
	}
	
	
	/*****************************************************
	 * Method to Get Resource Details
	*******************************************************/
	@RequestMapping(value={"/getResourceList"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getResourceList(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		List<Resource> resourceList = accessControlService.getResourceList();
		
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", resourceList);
		
		return resp;
	}
	
	@RequestMapping(value={"/mapResourcesToRole.do"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> mapResourcesToRole(HttpServletRequest request, HttpServletResponse response, @RequestBody Resource[]  selectedResources,
			@RequestParam("roleId") String role_Id) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
		if(role_Id.length() >= 8){
			resp.put("success", false);
			resp.put("msg", "Invalid Request");
			return resp;
		}
		long roleId = Long.parseLong(role_Id);
		boolean status = accessControlService.mapResourcesToRole(roleId, selectedResources);
		if(status){
			resp.put("success", true);
			resp.put("msg", "Successfully mapped");
		}else{
			resp.put("success", false);
			resp.put("msg", "Failed to mapped");
		}
		return resp;
	}
	
	

	/*****************************************************
	 * Method to Get Role Details
	*******************************************************/
	@RequestMapping(value={"/getRoleList"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getRoleList(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		List<Role> resourceList = accessControlService.getRoleList();
		
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", resourceList);
		
		return resp;
	}
	
	
	@RequestMapping(value={"/getMappedRolesToGroup"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getMappedRolesToGroup(@RequestBody @Valid GroupRoleMapModel grmap, BindingResult bresult) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug("groupId: " + grmap.getGroupId());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			System.out.println("Inside:: @@ hasNotErrors"); 
			List<Role> roleList = accessControlService.getMappedRolesToGroup(grmap.getGroupId());
			
			resp.put("success", true);
			resp.put("msg", "Successfully received");
			resp.put("data", roleList);
		}
		
		
		return resp;
	}
	
	@RequestMapping(value={"/deleteMappedRole"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteMappedRole(@RequestBody @Valid GroupRoleMapModel grmap, BindingResult bresult) {//@RequestParam("groupId") long groupId, @RequestParam("roleId") long roleId
		
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug("groupId: " + grmap.getGroupId());
		logger.debug("roleId: " + grmap.getRoleId());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors asdasd asd" + grmap.getGroupId()); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			System.out.println("Inside:: @@ hasNotErrors");
			boolean status = accessControlService.deleteMappedRole(grmap.getGroupId(), grmap.getRoleId());
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully deleted resource");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to delete resource");
			}
		}
		
		return resp;
	}
	
	@RequestMapping(value={"/mapRoleToGroup.do"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> mapRoleToGroup(HttpServletRequest request, HttpServletResponse response, @RequestBody Role[]  selectedRoles, @RequestParam("groupId") long groupId) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		boolean status = accessControlService.mapRoleToGroup(groupId, selectedRoles);
		if(status){
			resp.put("success", true);
			resp.put("msg", "Successfully mapped");
		}else{
			resp.put("success", false);
			resp.put("msg", "Failed to mapped");
		}
		return resp;
	}
	
	
	/*********************************************CREATE PARAMETER**************************************************/
	/*****************************************************
	 * Method to Add saveUserParameterTable
	*******************************************************/
	
	@RequestMapping(value={"/saveUserParameterTable"}, method = RequestMethod.POST)

	public @ResponseBody Map<String, Object> saveUserParameterTable(
			@RequestBody @Valid ParameterModel param, BindingResult bresult) {
		logger.info("Inside saveUserParameterTable Controller");
		Map<String, Object> resp = new HashMap<String, Object>();
		
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			System.out.println("Inside:: @@ hasNotErrors"); 
			String param_name = param.getParam_name();
			String tbl_name = param.getTbl_name();
		
			String type = param.getType();
			String id = param.getParam_id();
			boolean status = accessControlService.saveUserParameterTable(param_name,tbl_name,type,id);
			
			if(status){
				resp.put("success", true);
				resp.put("msg", "Record has been saved successfully.");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to add resource");
			}
		}
		
		return resp;
	}
	
	/*****************************************************
	 * Method to Get getDataParameterTable
	*******************************************************/
	@RequestMapping(value={"/getParameterTableData"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getParameterTableData(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		List <ParameterModel> lstUserDetails = accessControlService.getParameterTableData();
		
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", lstUserDetails);
		
		return resp;
	}
	/*****************************************************
	 * Method to Delete Parameter Details
	*******************************************************/
	@RequestMapping(value={"/deleteCreateParameter"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteCreateParameter(@RequestBody @Valid ParameterModel param, BindingResult bresult) {//@RequestParam("param_id") String param_id
		
		Map<String, Object> resp = new HashMap<String, Object>();
	
		System.out.println("param_id: " + param.getParam_id());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			System.out.println("Inside:: @@ hasNotErrors"); 
			boolean status = accessControlService.deleteCreateParameter(param.getParam_id());
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully deleted user details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to delete user details");
			}
		}
		
		
		return resp;
	}
	
	
	
	
	//File download code	
	@RequestMapping(value={"/download"}, method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> download(HttpServletRequest request, HttpServletResponse response) throws IOException {		
		Map<String, Object> resp = new HashMap<String, Object>();		
		File downloadFile = new File("C:/WebDataReportTamplate.xls");		
		if(downloadFile.exists()){
	        FileInputStream inputStream = new FileInputStream(downloadFile);	 
	        // set content attributes for the response
	        response.setContentType("application/octet-stream");
	        response.setContentLength((int) downloadFile.length());	 
	        // set headers for the response
	        String headerKey = "Content-Disposition";
	        String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
	        response.setHeader(headerKey, headerValue);	 
	        // get output stream of the response
	        OutputStream outStream = response.getOutputStream();	 
	        byte[] buffer = new byte[4096];
	        int bytesRead = -1;	 
	        // write bytes read from the input stream into the output stream
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }	 
	        inputStream.close();
	        outStream.close();	        
			return resp;		
		}else{
			resp.put("success", false);
			resp.put("msg", "File not exist");
			return resp;
		}
	}
	
	
	
	/*******************************************
	 * Method to get Parameter details
	 *******************************************/
	@RequestMapping(value = {"/getParameterDetails"},method = RequestMethod.POST) 
	public @ResponseBody Map<String, Object> getParamList(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
		List <Parameter> parameterList = accessControlService.getParameterList();
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", parameterList);
		
		return resp;
	}
	
	
	/***********************************************
	 * Method to save new parameter details
	 ***********************************************/
	
	@RequestMapping(value={"/saveParamDetails"},method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> saveParameter(@ModelAttribute Parameter param) {
		Map<String, Object> resp = new HashMap<String, Object>();
		logger.info(param.toString());
		
		boolean status = accessControlService.saveParameter(param);
		if(status){
			resp.put("success", true);
			resp.put("msg", "Successfully saved role details");
		}else{
			resp.put("success", false);
			resp.put("msg", "Failed to save Param details");
		}
		
		return resp;
	}
	
	/**************************************
	 * list of mapped parameter to group
	 *************************************/
	@RequestMapping(value={"/getMappedParametersToGroup"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getMappedParametersToGroup(@RequestBody @Valid GroupRoleMapModel grmap, BindingResult bresult) {//@RequestParam("groupId") long groupId
		
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug("groupId: " + grmap.getGroupId());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
			
		}else{
			System.out.println("Inside:: @@ hasNotErrors"); 
			
			List<UserModel> mappedParamList = accessControlService.getMappedParametersToGroup(grmap.getGroupId());
			
			resp.put("success", true);
			resp.put("msg", "Successfully received");
			resp.put("data", mappedParamList);
		}
		
		
		return resp;
	}
	
	/***************************************
	 * Dsecription Of Parameter From Table
	 * @throws JSONException 
	 * @throws IOException 
	 *****************************************/
//	@RequestMapping(value={"/getParameterValueTable"}, method = RequestMethod.POST)
//	public @ResponseBody void getParameterValueTable(@RequestParam("paramId") long paramId,@RequestParam("paramName") String paramName,
//			@RequestParam("tableName") String tableName,HttpServletRequest req,HttpServletResponse  res) throws JSONException, IOException {
//		
//		
//		PrintWriter out = res.getWriter();
//		res.setContentType("application/json");
//		logger.debug("tableName: " + tableName);
//		logger.debug("paramName: " + paramName);
//		logger.debug("paramId: " + paramId);
//		
//		
//		JSONObject mappedParamList = accessControlService.getParameterValueTable(tableName,paramName,paramId);
//		mappedParamList.put("success", true);
//		mappedParamList.put("msg", "Successfully received");
//		
//		
//		out.print(mappedParamList);
//	}
	
	@RequestMapping(value={"/getParameterValueTable"}, method = RequestMethod.POST)
	public @ResponseBody void getParameterValueTable(@RequestBody @Valid GroupRoleMapModel grmap, BindingResult bresult,HttpServletRequest req,HttpServletResponse  res) throws JSONException, IOException {
		System.out.println("inside getParameterValueTable method");
		PrintWriter out = res.getWriter();
		res.setContentType("application/json");
		JSONObject mappedParamList = new JSONObject();
		logger.debug("tableName: " + grmap.getTableName());
		logger.debug("paramName: " + grmap.getParamName());
		logger.debug("paramId: " + grmap.getParamId());
		System.out.println(grmap.getTableName() + "::" +  grmap.getParamName() + "::" + grmap.getParamId() );
		if(bresult.hasErrors()){
			System.out.println("Inside:: @### hasErrors"); 
			mappedParamList.put("success", false);
			mappedParamList.put("msg", "Invalid Request");
			
		}else{
			mappedParamList = accessControlService.getParameterValueTable(grmap.getTableName(),grmap.getParamName(),grmap.getParamId());
			mappedParamList.put("success", true);
			mappedParamList.put("msg", "Successfully received");
		}
		out.print(mappedParamList);
	}
	@RequestMapping(value={"/mapParamWithValues"},method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> mapParamvaluesToParam(HttpServletRequest req,HttpServletResponse res,@RequestParam("selectedGroupId") long selectedGroupId,
				@RequestParam ("paramName") String paramName) {
		
		String[] myJsonData = req.getParameterValues("selectedParamValues[]");
		System.out.println(myJsonData.length);
		List<String> paramList = new ArrayList<String>();
		for (int i=0; i < myJsonData.length; i++) {
			paramList.add(myJsonData[i]); 
		}
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		boolean status = accessControlService.mapParamvaluesToParam(paramList,selectedGroupId,paramName);
		if(status){
			resp.put("success", true);
			resp.put("msg", "Successfully mapped");
		}else{
			resp.put("success", false);
			resp.put("msg", "Failed to map");
		}
		return resp;
	}
	
	
	
	@RequestMapping (value={"/deleteMappedParam"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteMappedParam (@RequestBody @Valid GroupRoleMapModel grmap, BindingResult bresult) {
		Map<String, Object> resp = new HashMap<String, Object>();
		
		logger.debug("sl_no: " + grmap.getSl_no());
		
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
		}else{
			System.out.println("Inside:: @@ hasNotErrors"); 
			boolean status = accessControlService.deleteMappedParam( grmap.getSl_no());
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully deleted resource");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to delete resource");
			}
		}
		return resp;
	}
	
	
	/*******************************************
	 * method to get all mapped user datails
	 * @author sankar patra
	 * @Date 25-08-2018
	 *****************************************/
	@RequestMapping(value={"/getMappedUserDetails"}, method = RequestMethod.POST)
		public @ResponseBody Map<String, Object> getMappedUserDetails(HttpServletRequest req,HttpServletResponse res) {
		String methodName = "getMappedUserDetails";
		logger.info("Entry controller--->"+methodName);
		Map<String, Object> resp = new HashMap<String, Object>();
		List<MaappedUserDetailsResponseModel> lstMappedUserDetails = accessControlService.getListMappedUserDetails();
		resp.put("data",lstMappedUserDetails);
		logger.info("Exit controller--->"+methodName);
		return resp;
	}
	
	
	/**************************************************
	 * get all location details according to role
	 *******************************************************/
	
	@RequestMapping(value={"/{selectedGroupId}/getAllLocationAccordingToRole"}, method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getAllLocationAccordingToRole(@PathVariable("selectedGroupId") long selectedGroupId,HttpServletRequest req,
			HttpServletResponse res) {
		// @RequestParam("roleId") long roleId
		//long groupId = Long.parseLong(req.getParameter("selectedGroupId"));
		Map<String, Object> resp = new HashMap<String, Object>();
		logger.debug("groupId: " + selectedGroupId);
		
			System.out.println("Inside:: @@ hasNotErrors");
			List<AllLocationModel> lstLocation = accessControlService.getAllLocationAccordingToRole(selectedGroupId);
			resp.put("data",lstLocation);
		
		return resp;
	}
	
	/*************************************
	 * method to map location to user id
	 **************************************/
	@RequestMapping(value={"/{districtId}/{mapUserId}/mapLocationToUser"}, method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> mapLocationToUser(@PathVariable("districtId") long districtId,
			@PathVariable("mapUserId") String mapUserId,HttpServletRequest req,HttpServletResponse res) {
		// @RequestParam("roleId") long roleId
		//long groupId = Long.parseLong(req.getParameter("selectedGroupId"));
			Map<String, Object> resp = new HashMap<String, Object>();
			resp.put("status", "Failure");
			resp.put("msg", "Location Mapped successfully");
			logger.info("userid:->"+mapUserId);
			logger.info("location id:->"+districtId);
			boolean status = accessControlService.mapLocationToUser(mapUserId,districtId);
			if(status) {
				resp.put("status", "Success");
				resp.put("msg", "Location Mapped successfully");
			}
		
		return resp;
	}

	@RequestMapping(value={"/getAllDistrict"}, method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getAllDistrict(HttpServletRequest req,HttpServletResponse res) {
		// @RequestParam("roleId") long roleId
		//long groupId = Long.parseLong(req.getParameter("selectedGroupId"));
		Map<String, Object> resp = new HashMap<String, Object>();
		
			System.out.println("Inside:: @@ hasNotErrors");
			List<AllLocationModel> lstDistrict = accessControlService.getAllDistrict();
			resp.put("data",lstDistrict);
		return resp;
	}
	
	@RequestMapping(value={"/{districtcode}/getAllBlockLocationAccordingToRole"}, method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getAllBlockLocationAccordingToRole(HttpServletRequest req,
			HttpServletResponse res,@PathVariable("districtcode") long districtcode) {
		// @RequestParam("roleId") long roleId
		//long groupId = Long.parseLong(req.getParameter("selectedGroupId"));
		Map<String, Object> resp = new HashMap<String, Object>();
		
			System.out.println("Inside:: @@ hasNotErrors");
			List<AllLocationModel> lstDistrict = accessControlService.getAllBlockByDistrict(districtcode);
			resp.put("data",lstDistrict);
		return resp;
	}
	
	
	
	
}
