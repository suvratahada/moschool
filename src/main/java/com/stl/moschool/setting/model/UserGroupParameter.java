package com.stl.moschool.setting.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="group_param", schema="admin")
public class UserGroupParameter {
	@Id
	@Column(name="slno")
	private long slno;
	
	@Column(name="group_id")
	private long group_id;
	
	@Column(name="param_name")
	private String param_name;
	
	@Column(name="param_val")
	private String param_val;
	public long getSlno() {
		return slno;
	}

	public void setSlno(long slno) {
		this.slno = slno;
	}

	public long getGroup_id() {
		return group_id;
	}

	public void setGroup_id(long group_id) {
		this.group_id = group_id;
	}

	public String getParam_name() {
		return param_name;
	}

	public void setParam_name(String param_name) {
		this.param_name = param_name;
	}

	public String getParam_val() {
		return param_val;
	}

	public void setParam_val(String param_val) {
		this.param_val = param_val;
	}
	
	
	
	
	
	

}
