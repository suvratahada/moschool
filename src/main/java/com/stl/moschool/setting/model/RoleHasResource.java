package com.stl.moschool.setting.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="role_has_resource", schema="admin")
public class RoleHasResource {
	
	@Column(name="role_id")
	private long role_id;
	
	@Column(name="resource_id")
	private long resource_id;

	public long getRole_id() {
		return role_id;
	}

	public void setRole_id(long role_id) {
		this.role_id = role_id;
	}

	public long getResource_id() {
		return resource_id;
	}

	public void setResource_id(long resource_id) {
		this.resource_id = resource_id;
	}
}
