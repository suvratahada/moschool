/****************************************************************************
 * Used for Hybrid Seeded Excel Upload
 * @date 18-July-2018
 * @author Vamsi Krish
 ***************************************************************************/
package com.stl.moschool.setting.model;

import java.util.List;

public class HybridSeededSurveyDto {

	private long hybrid_user_id;
	private String seeded_tbl_name;
	private long survey_id;
	private List<Long> user_id;
	private List<String> remote_search_filter;
	private String userId;
	
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public long getHybrid_user_id() {
		return hybrid_user_id;
	}
	public void setHybrid_user_id(long hybrid_user_id) {
		this.hybrid_user_id = hybrid_user_id;
	}
	public String getSeeded_tbl_name() {
		return seeded_tbl_name;
	}
	public void setSeeded_tbl_name(String seeded_tbl_name) {
		this.seeded_tbl_name = seeded_tbl_name;
	}
	public long getSurvey_id() {
		return survey_id;
	}
	public void setSurvey_id(long survey_id) {
		this.survey_id = survey_id;
	}
	public List<Long> getUser_id() {
		return user_id;
	}
	public void setUser_id(List<Long> user_id) {
		this.user_id = user_id;
	}
	public List<String> getRemote_search_filter() {
		return remote_search_filter;
	}
	public void setRemote_search_filter(List<String> remote_search_filter) {
		this.remote_search_filter = remote_search_filter;
	}
}
