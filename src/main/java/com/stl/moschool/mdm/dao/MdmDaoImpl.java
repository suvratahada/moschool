package com.stl.moschool.mdm.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.stl.moschool.mdm.model.EmployeeModel;
import com.stl.moschool.mdm.model.SchoolDetailsModel;
import com.stl.moschool.mdm.model.StudentStrengthModel;
import com.stl.moschool.mdm.model.SubjectModel;

@Repository("mdmDao")
public class MdmDaoImpl implements MdmDao{
	
	@Autowired JdbcTemplate jdbcTemplate;
	
	@Autowired Properties mdmQueries;

	/*========================Employee Details Start========================*/
	@Override
	public boolean saveOrUpdateEmployeeDetails(EmployeeModel employee, String userId) {
		
		boolean isUpdate=employee.isUpdate();
		String sql=null;
		boolean resp=false;
		
		if(isUpdate) {
			sql=mdmQueries.getProperty("mdm.updateEmployeeDetails");
			jdbcTemplate.update(sql, new Object[] {
					employee.getEmpType(),
					userId,
					employee.getEmpId()					
			});
			resp=true;
		}else {
			sql=mdmQueries.getProperty("mdm.saveEmployeeDetails");
			jdbcTemplate.update(sql, new Object[] {
					employee.getEmpType(),
					employee.getEmpId(),
					userId,					
			});
			resp=true;
		}
		
		return resp;
	}

	@Override
	public List<EmployeeModel> getEmployeeDetails() {
		String sql = mdmQueries.getProperty("mdm.getEmployeeDetails");
		List<EmployeeModel> employee = jdbcTemplate.query(sql, new RowMapper<EmployeeModel>() {
			@Override
			public EmployeeModel mapRow(ResultSet rs, int rownumber) throws SQLException {
				EmployeeModel employee = new EmployeeModel();
				employee.setEmpId(rs.getInt("emp_id"));
				employee.setEmpType(rs.getString("emp_type"));
				employee.setRecordStatus(rs.getInt("record_status"));
				return employee;
			}
		});
		return employee;
	}

	@Override
	public boolean activateOrDeActiveEmployee(int empId, int recordStatus) {
		String sql = mdmQueries.getProperty("mdm.activateOrDeActivateEmployee");
		jdbcTemplate.update(sql, new Object[] {
				recordStatus, 
				empId
			});
		return true;
	}
	
	@Override
	public boolean deleteEmployee(int empId) {
		String sql = mdmQueries.getProperty("mdm.deleteEmployee");
		jdbcTemplate.update(sql, new Object[] {
					empId
			});
		return true;
	}
	
	/*========================Employee Details End========================*/
	
	/*========================Subject Details Start========================*/
	@Override
	public boolean saveOrUpdateSubjectDetails(SubjectModel subject, String userId) {
		boolean isUpdate=subject.isUpdate();
		String sql=null;
		boolean resp=false;
		
		if(isUpdate) {
			sql=mdmQueries.getProperty("mdm.updateSubjectDetails");
			jdbcTemplate.update(sql, new Object[] {
					subject.getSubjectDesc(),
					userId,
					subject.getSubjectId()		
			});
			resp=true;
		}else {
			sql=mdmQueries.getProperty("mdm.saveSubjectDetails");
			jdbcTemplate.update(sql, new Object[] {
					subject.getSubjectDesc(),
					subject.getSubjectId(),
					userId,					
			});
			resp=true;
		}
		
		return resp;
	}

	@Override
	public List<SubjectModel> getSubjectDetails() {
		String sql = mdmQueries.getProperty("mdm.getSubjectDetails");
		List<SubjectModel> subject = jdbcTemplate.query(sql, new RowMapper<SubjectModel>() {
			@Override
			public SubjectModel mapRow(ResultSet rs, int rownumber) throws SQLException {
				SubjectModel subject = new SubjectModel();
				subject.setSubjectId(rs.getInt("subject_id"));
				subject.setSubjectDesc(rs.getString("subject_desc"));
				subject.setRecordStatus(rs.getInt("record_status"));
				return subject;
			}
		});
		return subject;
	}

	@Override
	public boolean activateOrDeActiveSubject(int subjectId, int recordStatus) {
		String sql = mdmQueries.getProperty("mdm.activateOrDeActivateSubject");
		jdbcTemplate.update(sql, new Object[] {
					recordStatus, 
					subjectId
			});
		return true;
	}
	
	@Override
	public boolean deleteSubject(int subjectId) {
		String sql = mdmQueries.getProperty("mdm.deleteSubject");
		jdbcTemplate.update(sql, new Object[] {
				subjectId
			});
		return true;
	}
	
	/*========================Subject Details End========================*/
		
	/*========================School Details Start========================*/

	@Override
	public boolean saveOrUpdateSchoolDetails(SchoolDetailsModel schoolDetails, String userId) {
		boolean isUpdate=schoolDetails.isUpdate();
		String sql=null;
		boolean resp=false;
		
		if(isUpdate) {
			sql=mdmQueries.getProperty("mdm.updateSchoolDetails");
			jdbcTemplate.update(sql, new Object[] {
					schoolDetails.getUdiseCode(),
					schoolDetails.getAffiliationBoard(),
					schoolDetails.getSchoolAffiliatedCode(),
					schoolDetails.getMediumLanguageId(),
					userId,
					schoolDetails.getSchoolId()		
			});
			resp=true;
		}else {
			sql=mdmQueries.getProperty("mdm.saveSchoolDetails");
			jdbcTemplate.update(sql, new Object[] {
					schoolDetails.getUdiseCode(),
					schoolDetails.getSchoolId(),
					schoolDetails.getAffiliationBoard(),
					schoolDetails.getSchoolAffiliatedCode(),
					schoolDetails.getMediumLanguageId(),
					userId,					
			});
			resp=true;
		}
		
		return resp;
	}
	
	@Override
	public List<SchoolDetailsModel> getSchoolDetails() {
		String sql = mdmQueries.getProperty("mdm.getSchoolDetails");
		List<SchoolDetailsModel> schoolDetails = jdbcTemplate.query(sql, new RowMapper<SchoolDetailsModel>() {
			@Override
			public SchoolDetailsModel mapRow(ResultSet rs, int rownumber) throws SQLException {
				SchoolDetailsModel schoolDetails = new SchoolDetailsModel();
				schoolDetails.setUdiseCode(rs.getLong("udise_code"));
				schoolDetails.setSchoolId(rs.getLong("school_id"));
				schoolDetails.setAffiliationBoard(rs.getString("affiliation_board"));
				schoolDetails.setSchoolAffiliatedCode(rs.getLong("school_affiliated_code"));
				schoolDetails.setMediumLanguageId(rs.getInt("medium_language_id"));
				schoolDetails.setRecordStatus(rs.getInt("record_status"));
				return schoolDetails;
			}
		});
		return schoolDetails;
	}

	@Override
	public boolean activateOrDeActiveSchool(int schoolId, int recordStatus) {
		String sql = mdmQueries.getProperty("mdm.activateOrDeActivateSchoolDetails");
		jdbcTemplate.update(sql, new Object[] {
					recordStatus, 
					schoolId
			});
		return true;
	}
	
	@Override
	public boolean deleteSchool(int schoolId) {
		String sql = mdmQueries.getProperty("mdm.deleteSchoolDetails");
		jdbcTemplate.update(sql, new Object[] {
				schoolId
			});
		return true;
	}
	
	/*========================School Details End========================*/
	
	/*========================Student Strength Start========================*/
	@Override
	public boolean saveOrUpdateStudentStrength(StudentStrengthModel studentStrength, String userId) {
		boolean isUpdate=studentStrength.isUpdate();
		String sql=null;
		boolean resp=false;
		
		if(isUpdate) {
			sql=mdmQueries.getProperty("mdm.updateStudentStrength");
			jdbcTemplate.update(sql, new Object[] {
					studentStrength.getSection(),
					studentStrength.getMale(),
					studentStrength.getFeMale(),
					userId,
					studentStrength.getClassNo(),
					studentStrength.getSection()
			});
			resp=true;
		}else {
			sql=mdmQueries.getProperty("mdm.saveStudentStrength");
			jdbcTemplate.update(sql, new Object[] {
					studentStrength.getClassNo(),
					studentStrength.getSection(),
					studentStrength.getMale(),
					studentStrength.getFeMale(),
					userId,					
			});
			resp=true;
		}
		
		return resp;
	}

	@Override
	public List<StudentStrengthModel> getStudentStrength() {
		String sql = mdmQueries.getProperty("mdm.getStudentStrength");
		List<StudentStrengthModel> studentStrength = jdbcTemplate.query(sql, new RowMapper<StudentStrengthModel>() {
			@Override
			public StudentStrengthModel mapRow(ResultSet rs, int rownumber) throws SQLException {
				StudentStrengthModel studentStrength = new StudentStrengthModel();
				studentStrength.setClassNo(rs.getInt("class"));
				studentStrength.setSection(rs.getString("section"));
				studentStrength.setMale(rs.getInt("male"));
				studentStrength.setFeMale(rs.getInt("female"));
				return studentStrength;
			}
		});
		return studentStrength;
	}

	@Override
	public boolean deleteStudentStrength(int classNo, String section) {
		String sql = mdmQueries.getProperty("mdm.deleteStudentStrength");
		jdbcTemplate.update(sql, new Object[] {
				classNo, section
			});
		return true;
	}	
	/*========================Student Strength End========================*/
}
