package com.stl.moschool.mdm.dao;

import java.util.List;

import com.stl.moschool.mdm.model.EmployeeModel;
import com.stl.moschool.mdm.model.SchoolDetailsModel;
import com.stl.moschool.mdm.model.StudentStrengthModel;
import com.stl.moschool.mdm.model.SubjectModel;

public interface MdmDao {
	
	boolean saveOrUpdateEmployeeDetails(EmployeeModel employee, String userId);

	List<EmployeeModel> getEmployeeDetails();
	
	boolean activateOrDeActiveEmployee(int empId, int recordStatus);
	
	boolean deleteEmployee(int empId);
	
	boolean saveOrUpdateSubjectDetails(SubjectModel subject, String userId);

	List<SubjectModel> getSubjectDetails();
	
	boolean activateOrDeActiveSubject(int subjectId, int recordStatus);
	
	boolean deleteSubject(int subjectId);
	
	boolean saveOrUpdateSchoolDetails(SchoolDetailsModel schoolDetails, String userId);

	List<SchoolDetailsModel> getSchoolDetails();
	
	boolean activateOrDeActiveSchool(int schoolId, int recordStatus);
	
	boolean deleteSchool(int schoolId);
	
	boolean saveOrUpdateStudentStrength(StudentStrengthModel studentStrength, String userId);

	List<StudentStrengthModel> getStudentStrength();
	
	boolean deleteStudentStrength(int classNo, String section);
	
}
