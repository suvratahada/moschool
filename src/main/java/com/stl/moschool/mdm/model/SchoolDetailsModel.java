package com.stl.moschool.mdm.model;

public class SchoolDetailsModel {

	
	 private long udiseCode;
	 
	 private long schoolId;
	 
	 private String affiliationBoard;
	 
	 private long schoolAffiliatedCode;
	 
	 private int mediumLanguageId;
	 
	 private int recordStatus;
	 
	 private boolean isUpdate;

	public long getUdiseCode() {
		return udiseCode;
	}

	public void setUdiseCode(long udiseCode) {
		this.udiseCode = udiseCode;
	}

	public long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(long schoolId) {
		this.schoolId = schoolId;
	}

	public String getAffiliationBoard() {
		return affiliationBoard;
	}

	public void setAffiliationBoard(String affiliationBoard) {
		this.affiliationBoard = affiliationBoard;
	}

	public long getSchoolAffiliatedCode() {
		return schoolAffiliatedCode;
	}

	public void setSchoolAffiliatedCode(long schoolAffiliatedCode) {
		this.schoolAffiliatedCode = schoolAffiliatedCode;
	}

	public int getMediumLanguageId() {
		return mediumLanguageId;
	}

	public void setMediumLanguageId(int mediumLanguageId) {
		this.mediumLanguageId = mediumLanguageId;
	}

	public int getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}

	public boolean isUpdate() {
		return isUpdate;
	}

	public void setUpdate(boolean isUpdate) {
		this.isUpdate = isUpdate;
	}

	@Override
	public String toString() {
		return "SchoolDetailsModel [udiseCode=" + udiseCode + ", schoolId=" + schoolId + ", affiliationBoard="
				+ affiliationBoard + ", schoolAffiliatedCode=" + schoolAffiliatedCode + ", mediumLanguageId="
				+ mediumLanguageId + ", recordStatus=" + recordStatus + ", isUpdate=" + isUpdate + "]";
	}

		
}
