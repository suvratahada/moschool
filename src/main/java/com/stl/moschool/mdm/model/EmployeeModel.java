package com.stl.moschool.mdm.model;

public class EmployeeModel {

	 private String empType;
	 
	 private int empId;
	 
	 private int recordStatus;
	 
	 private boolean update;

	public String getEmpType() {
		return empType;
	}

	public void setEmpType(String empType) {
		this.empType = empType;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public int getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}

	public boolean isUpdate() {
		return update;
	}

	public void setUpdate(boolean update) {
		this.update = update;
	}

	@Override
	public String toString() {
		return "EmployeeModel [empType=" + empType + ", empId=" + empId + ", recordStatus=" + recordStatus + ", update="
				+ update + "]";
	}

	
	 
	 
	
}
