package com.stl.moschool.mdm.model;

public class CategoryMaster {
	private Integer catg_id;
	private String catg_desc;
	private boolean update;
	
	public Integer getCatg_id() {
		return catg_id;
	}
	public void setCatg_id(Integer catg_id) {
		this.catg_id = catg_id;
	}
	public String getCatg_desc() {
		return catg_desc;
	}
	public void setCatg_desc(String catg_desc) {
		this.catg_desc = catg_desc;
	}
	public boolean isUpdate() {
		return update;
	}
	public void setUpdate(boolean update) {
		this.update = update;
	}
	
	@Override
	public String toString() {
		return "CategoryMaster [catg_id=" + catg_id + ", catg_desc=" + catg_desc + ", update=" + update + "]";
	}
	
}
