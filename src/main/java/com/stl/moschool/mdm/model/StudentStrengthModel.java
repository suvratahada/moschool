package com.stl.moschool.mdm.model;

public class StudentStrengthModel {

	private int classNo;
	
	private String section;
	
	private int male;
	
	private int feMale;
	
	private int recordStatus;
	
	private boolean isUpdate;

	public int getClassNo() {
		return classNo;
	}

	public void setClassNo(int classNo) {
		this.classNo = classNo;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public int getMale() {
		return male;
	}

	public void setMale(int male) {
		this.male = male;
	}

	public int getFeMale() {
		return feMale;
	}

	public void setFeMale(int feMmale) {
		this.feMale = feMmale;
	}

	public int getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}

	public boolean isUpdate() {
		return isUpdate;
	}

	public void setUpdate(boolean isUpdate) {
		this.isUpdate = isUpdate;
	}

	@Override
	public String toString() {
		return "StudentStrengthModel [classNo=" + classNo + ", section=" + section + ", male=" + male + ", feMmale=" + feMale
				+ ", recordStatus=" + recordStatus + ", isUpdate=" + isUpdate + "]";
	}

		
}
