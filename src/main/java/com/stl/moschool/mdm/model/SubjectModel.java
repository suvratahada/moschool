package com.stl.moschool.mdm.model;

public class SubjectModel {

	private int subjectId;
	
	private String subjectDesc;
	
	private int recordStatus;
	
	private boolean update;

	public int getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}

	public String getSubjectDesc() {
		return subjectDesc;
	}

	public void setSubjectDesc(String subjectDesc) {
		this.subjectDesc = subjectDesc;
	}

	public int getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(int recordStatus) {
		this.recordStatus = recordStatus;
	}

	public boolean isUpdate() {
		return update;
	}

	public void setUpdate(boolean update) {
		this.update = update;
	}

	@Override
	public String toString() {
		return "SubjectModel [subjectId=" + subjectId + ", subjectDesc=" + subjectDesc + ", recordStatus="
				+ recordStatus + ", update=" + update + "]";
	}

		
}
