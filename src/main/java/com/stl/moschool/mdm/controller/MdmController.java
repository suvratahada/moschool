package com.stl.moschool.mdm.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stl.moschool.mdm.model.EmployeeModel;
import com.stl.moschool.mdm.model.SchoolDetailsModel;
import com.stl.moschool.mdm.model.StudentStrengthModel;
import com.stl.moschool.mdm.model.SubjectModel;
import com.stl.moschool.mdm.service.MdmService;


@Controller
@RequestMapping("/mdm")
public class MdmController {

	final static Logger logger = Logger.getLogger(MdmController.class);
	
	@Autowired 
	private MdmService mdmService;
	
	@RequestMapping(value = { "/dashboard" }, method = RequestMethod.GET)
	public String createDashboard(HttpSession session) {
		logger.debug("redirecting to  Category Setup");
		System.out.println("redirecting to  Category Setup");
		session.setAttribute("sessionDashboardTab", "styleActiveMST");
		return "createDashboard";
	}
	
	@RequestMapping(value = { "/category" }, method = RequestMethod.GET)
	public String createCategory(HttpSession session) {
		logger.debug("redirecting to  Category Setup");
		System.out.println("redirecting to  Category Setup");
		session.setAttribute("sessionDashboardTab", "styleActiveMST");
		return "createCategory";
	}
	
	@RequestMapping(value = { "/events_noti_ach" }, method = RequestMethod.GET)
	public String createEvents(HttpSession session) {
		logger.debug("redirecting to  Events Setup");
		System.out.println("redirecting to  Events Setup");
		session.setAttribute("sessionDashboardTab", "styleActiveMST");
		return "createEvents";
	}
	
	@RequestMapping(value = { "/language" }, method = RequestMethod.GET)
	public String createLanguage(HttpSession session) {
		logger.debug("redirecting to  Language Setup");
		System.out.println("redirecting to  Language Setup");
		session.setAttribute("sessionDashboardTab", "styleActiveMST");
		return "createLanguage";
	}
	
	@RequestMapping(value = { "/schoolcatg" }, method = RequestMethod.GET)
	public String createSchoolCategory(HttpSession session) {
		logger.debug("redirecting to  SchoolCategory Setup");
		System.out.println("redirecting to  SchoolCategory Setup");
		session.setAttribute("sessionDashboardTab", "styleActiveMST");
		return "createSchoolCategory";
	}
	
	@RequestMapping(value = { "/employeeSetup" }, method = RequestMethod.GET)
	public String createEmployee(HttpSession session) {
		logger.debug("redirecting to  Employee Setup");
		System.out.println("redirecting to  Employee Setup");
		//session.setAttribute("sessionDashboardTab", "styleActiveMST");
		return "employeeSetup";
	}
	
	@RequestMapping(value = { "/schoolDetailsSetup" }, method = RequestMethod.GET)
	public String schoolDetails(HttpSession session) {
		logger.debug("redirecting to  schoolDetails Setup");
		System.out.println("redirecting to  schoolDetails Setup");
		//session.setAttribute("sessionDashboardTab", "styleActiveMST");
		return "schoolDetailsSetup";
	}
	
	@RequestMapping(value = { "/schoolStrengthSetUp" }, method = RequestMethod.GET)
	public String schoolStrength(HttpSession session) {
		logger.debug("redirecting to  schoolStrength Setup");
		System.out.println("redirecting to  schoolStrength Setup");
		//session.setAttribute("sessionDashboardTab", "styleActiveMST");
		return "studentStrengthSetUp";
	}
	
	@RequestMapping(value = { "/subjectSetup" }, method = RequestMethod.GET)
	public String subject(HttpSession session) {
		logger.debug("redirecting to  subject Setup");
		System.out.println("redirecting to  subject Setup");
		//session.setAttribute("sessionDashboardTab", "styleActiveMST");
		return "subjectSetup";
	}

	@RequestMapping(value = { "/moSchoolAffYear" }, method = RequestMethod.GET)
	public String moSchoolAffYear(HttpSession session) {
		logger.debug("redirecting to  Block Setup");
		System.out.println("redirecting to  Location");
		session.setAttribute("sessionDashboardTab", "styleActiveMST");
		return "createBlock";
	}
	
	/*****************************************************
	 * Method to Save or update Employee Details
	 * @author suvrata
	 * @Date   04.01.2019
	*******************************************************/
	@RequestMapping(value={"/saveOrUpdateEmployeeDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> saveOrUpdateEmployeeDetails( @RequestBody /*@Valid */EmployeeModel employee, BindingResult bresult, HttpSession session) {
		System.out.println("Inside:: @@ saveEmployeeDetails Controller");
		String userId=(String) session.getAttribute("userId");
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug(employee.toString());
		
		System.out.println(employee.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ saveEmployeeDetails hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
		}else{
			System.out.println("Inside:: @@ saveEmployeeDetails NotErrors"); 
			boolean status = mdmService.saveOrUpdateEmployeeDetails(employee, userId);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully saved Employee details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to save Employee details");
			}
		}
		
		return resp;
	}
	
	/*****************************************************
	 * Method to Get Employee Details
	 * @author suvrata
	 * @Date   04.01.2019
	*******************************************************/
	@RequestMapping(value={"/getEmployeeDetails"}, method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getEmployeeDetails(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		List <EmployeeModel> lstEmployeeDetails = mdmService.getEmployeeDetails();
		
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", lstEmployeeDetails);
		
		return resp;
	}
	
	/*****************************************************
	 * Method to Activate Or Deactivate Employee Details
	 * @author suvrata
	 * @Date   04.01.2019
	*******************************************************/
	@RequestMapping(value={"/activateOrDeActiveEmployee/{empId}/{recordStatus}"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> activateOrDeActiveEmployee(@PathVariable("empId") int empId, @PathVariable("recordStatus") int recordStatus) {
		System.out.println("Inside:: @@ activateOrDeActiveEmployee Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		System.out.println("Id==========> "+empId);
		System.out.println("Employee id"+empId);
		logger.debug(empId);
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = mdmService.activateOrDeActiveEmployee(empId, recordStatus);
			
			String msg=null;
			if(recordStatus==1)
				msg="De Activated";
			else
				msg="Activated";
			if(status){
				resp.put("success", true);
				resp.put("msg", msg);
			}else{
				resp.put("success", false);
				resp.put("msg", msg);
			}		
		return resp;
	}
	
	/*****************************************************
	 * Method to Delete Employee Details
	 * @author suvrata
	 * @Date   07.01.2019
	*******************************************************/
	@RequestMapping(value={"/deleteEmployee/{empId}"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteEmployee(@PathVariable("empId") int empId) {
		System.out.println("Inside:: @@ Delete Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		System.out.println("Id==========> "+empId);
		System.out.println("Employee id"+empId);
		logger.debug(empId);
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = mdmService.deleteEmployee(empId);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully Delete Employee details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to Delete Employee details");
			}		
		return resp;
	}
	
	/*****************************************************
	 * Method to Save or update Subject Details
	 * @author suvrata
	 * @Date   04.01.2019
	*******************************************************/
	@RequestMapping(value={"/saveOrUpdateSubjectDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> saveOrUpdateSubjectDetails( @RequestBody /*@Valid */SubjectModel subject, BindingResult bresult, HttpSession session) {
		System.out.println("Inside:: @@ saveOrUpdateSubjectDetails Controller");
		String userId=(String) session.getAttribute("userId");
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug(subject.toString());
		
		System.out.println(subject.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ saveOrUpdateSubjectDetails hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
		}else{
			System.out.println("Inside:: @@ saveOrUpdateSubjectDetails NotErrors"); 
			boolean status = mdmService.saveOrUpdateSubjectDetails(subject, userId);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully saved Subject details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to save Subject details");
			}
		}
		
		return resp;
	}
	
	/*****************************************************
	 * Method to Get Subject Details
	 * @author suvrata
	 * @Date   07.01.2019
	*******************************************************/
	@RequestMapping(value={"/getSubjectDetails"}, method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getSubjectDetails(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		List <SubjectModel> lstSubjectDetails = mdmService.getSubjectDetails();
		
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", lstSubjectDetails);
		
		return resp;
	}
	
	/*****************************************************
	 * Method to Activate Or DeActivate Subject Details
	 * @author suvrata
	 * @Date   07.01.2019
	*******************************************************/
	@RequestMapping(value={"/activateOrDeActiveSubjectDetails/{subjectId}/{recordStatus}"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> activateOrDeActiveSubject(@PathVariable("subjectId") int subjectId, @PathVariable("recordStatus") int recordStatus) {
		System.out.println("Inside:: @@ Delete Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		System.out.println("Id==========> "+subjectId);
		System.out.println("Subject id"+subjectId);
		logger.debug(subjectId);
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = mdmService.activateOrDeActiveSubject(subjectId, recordStatus);
			
			String msg=null;
			if(recordStatus==1)
				msg="De Activated";
			else
				msg="Activated";
			if(status){
				resp.put("success", true);
				resp.put("msg", msg);
			}else{
				resp.put("success", false);
				resp.put("msg", msg);
			}		
		return resp;
	}
	
	/*****************************************************
	 * Method to Delete Subject Details
	 * @author suvrata
	 * @Date   07.01.2019
	*******************************************************/
	@RequestMapping(value={"/deleteSubject/{subjectId}"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteSubject(@PathVariable("subjectId") int subjectId) {
		System.out.println("Inside:: @@ Delete Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		System.out.println("Id==========> "+subjectId);
		System.out.println("Subject id"+subjectId);
		logger.debug(subjectId);
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = mdmService.deleteSubject(subjectId);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully Delete Subject details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to Delete Subject details");
			}		
		return resp;
	}
	
	/*****************************************************
	 * Method to Save or update School Details
	 * @author suvrata
	 * @Date   07.01.2019
	*******************************************************/
	@RequestMapping(value={"/saveOrUpdateSchoolDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> saveOrUpdateSchoolDetails( @RequestBody /*@Valid */SchoolDetailsModel schoolDetails, BindingResult bresult, HttpSession session) {
		System.out.println("Inside:: @@ saveOrUpdateSchoolDetails Controller");
		String userId=(String) session.getAttribute("userId");
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug(schoolDetails.toString());
		
		System.out.println(schoolDetails.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ saveOrUpdateSubjectDetails hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
		}else{
			System.out.println("Inside:: @@ saveOrUpdateSchoolDetails NotErrors"); 
			boolean status = mdmService.saveOrUpdateSchoolDetails(schoolDetails, userId);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully saved School details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to save School details");
			}
		}
		
		return resp;
	}
	
	/*****************************************************
	 * Method to Get School Details
	 * @author suvrata
	 * @Date   07.01.2019
	*******************************************************/
	@RequestMapping(value={"/getSchoolDetails"}, method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getSchoolDetails(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		List <SchoolDetailsModel> lstSchoolDetails = mdmService.getSchoolDetails();
		
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", lstSchoolDetails);
		
		return resp;
	}
	
	/*****************************************************
	 * Method to Activate Or DeActive School Details
	 * @author suvrata
	 * @Date   07.01.2019
	*******************************************************/
	@RequestMapping(value={"/activateOrDeActiveSchoolDetails/{schoolId}/{recordStatus}"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteSchoolDetails(@PathVariable("schoolId") int schoolId, @PathVariable("recordStatus") int recordStatus) {
		System.out.println("Inside:: @@ Delete Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		System.out.println("Id==========> "+schoolId);
		System.out.println("School id"+schoolId);
		logger.debug(schoolId);
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = mdmService.activateOrDeActiveSchool(schoolId, recordStatus);
			
			String msg=null;
			if(recordStatus==1)
				msg="De Activated";
			else
				msg="Activated";
			if(status){
				resp.put("success", true);
				resp.put("msg", msg);
			}else{
				resp.put("success", false);
				resp.put("msg", msg);
			}		
		return resp;
	}
	
	/*****************************************************
	 * Method to Delete Subject Details
	 * @author suvrata
	 * @Date   07.01.2019
	*******************************************************/
	@RequestMapping(value={"/deleteSchoolDetails/{schoolId}"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteSchoolDetails(@PathVariable("schoolId") int schoolId) {
		System.out.println("Inside:: @@ Delete Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		System.out.println("Id==========> "+schoolId);
		System.out.println("schoolId id"+schoolId);
		logger.debug(schoolId);
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = mdmService.deleteSchoolDetails(schoolId);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully Delete School details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to Delete School details");
			}		
		return resp;
	}
	
	/*****************************************************
	 * Method to Save or update Student Strength
	 * @author suvrata
	 * @Date   07.01.2019
	*******************************************************/
	@RequestMapping(value={"/saveOrUpdateStudentStrength"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> saveOrUpdateStudentStrength( @RequestBody /*@Valid */StudentStrengthModel studentStrength, BindingResult bresult, HttpSession session) {
		System.out.println("Inside:: @@ saveOrUpdateSchoolDetails Controller");
		String userId=(String) session.getAttribute("userId");
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug(studentStrength.toString());
		
		System.out.println(studentStrength.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ saveOrUpdateStudentStrength hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
		}else{
			System.out.println("Inside:: @@ saveOrUpdateStudentStrength NotErrors"); 
			boolean status = mdmService.saveOrUpdateStudentStrength(studentStrength, userId);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully saved Student Strength");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to save Student Strength");
			}
		}
		
		return resp;
	}
	
	/*****************************************************
	 * Method to Get Student Strength
	 * @author suvrata
	 * @Date   07.01.2019
	*******************************************************/
	@RequestMapping(value={"/getStudentStrength"}, method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getStudentStrength(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		List <StudentStrengthModel> lstStudentStrength = mdmService.getStudentStrength();
		
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", lstStudentStrength);
		
		return resp;
	}
	
	/*****************************************************
	 * Method to Delete Student Strength
	 * @author suvrata
	 * @Date   07.01.2019
	*******************************************************/
	@RequestMapping(value={"/deleteStudentStrength/{classNo}/{section}"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteStudentStrength(@PathVariable("classNo") int classNo, @PathVariable("section") String section) {
		System.out.println("Inside:: @@ Delete Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		System.out.println("classNo==========> "+classNo);
		System.out.println("classNo"+classNo);
		logger.debug(classNo);
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = mdmService.deleteStudentStrength(classNo, section);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully Deleted Student Strength");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to Delete Student Strength");
			}		
		return resp;
	}
	
}
