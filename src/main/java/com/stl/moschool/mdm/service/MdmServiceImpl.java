package com.stl.moschool.mdm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stl.moschool.mdm.dao.MdmDao;
import com.stl.moschool.mdm.model.EmployeeModel;
import com.stl.moschool.mdm.model.SchoolDetailsModel;
import com.stl.moschool.mdm.model.StudentStrengthModel;
import com.stl.moschool.mdm.model.SubjectModel;

@Service("mdmService")
public class MdmServiceImpl implements MdmService{

	@Autowired MdmDao mdmDao;
	
	@Override
	public boolean saveOrUpdateEmployeeDetails(EmployeeModel employee, String userId) {
		return mdmDao.saveOrUpdateEmployeeDetails(employee, userId);
	}
	
	@Override
	public List<EmployeeModel> getEmployeeDetails() {
		return mdmDao.getEmployeeDetails();
	}

	@Override
	public boolean activateOrDeActiveEmployee(int empId, int recordStatus) {
		if (recordStatus==1) 
			recordStatus=0;
		 else 
			 recordStatus=1;
		return mdmDao.activateOrDeActiveEmployee(empId, recordStatus);
	}
	
	@Override
	public boolean deleteEmployee(int empId) {
		return mdmDao.deleteEmployee(empId);
	}

	@Override
	public boolean saveOrUpdateSubjectDetails(SubjectModel subject, String userId) {
		return mdmDao.saveOrUpdateSubjectDetails(subject, userId);
	}

	@Override
	public List<SubjectModel> getSubjectDetails() {
		return mdmDao.getSubjectDetails();
	}

	@Override
	public boolean activateOrDeActiveSubject(int subjectId, int recordStatus) {
		if (recordStatus==1) 
			recordStatus=0;
		 else 
			 recordStatus=1;
		return mdmDao.activateOrDeActiveSubject(subjectId, recordStatus);
	}

	@Override
	public boolean deleteSubject(int subjectId) {
		return mdmDao.deleteSubject(subjectId);
	}

	@Override
	public boolean saveOrUpdateSchoolDetails(SchoolDetailsModel schoolDetails, String userId) {
		return mdmDao.saveOrUpdateSchoolDetails(schoolDetails, userId);
	}

	@Override
	public List<SchoolDetailsModel> getSchoolDetails() {
		return mdmDao.getSchoolDetails();
	}

	@Override
	public boolean activateOrDeActiveSchool(int schoolId, int recordStatus) {
		if (recordStatus==1) 
			recordStatus=0;
		 else 
			 recordStatus=1;
		return mdmDao.activateOrDeActiveSchool(schoolId, recordStatus);
	}

	@Override
	public boolean deleteSchoolDetails(int schoolId) {
		return mdmDao.deleteSchool(schoolId);
	}

	@Override
	public boolean saveOrUpdateStudentStrength(StudentStrengthModel studentStrength, String userId) {
		return mdmDao.saveOrUpdateStudentStrength(studentStrength, userId);
	}

	@Override
	public List<StudentStrengthModel> getStudentStrength() {
		return mdmDao.getStudentStrength();
	}

	@Override
	public boolean deleteStudentStrength(int classNo, String section) {
		return mdmDao.deleteStudentStrength(classNo, section);
	}
	
	
}
