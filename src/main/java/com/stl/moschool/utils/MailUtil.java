package com.stl.moschool.utils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;

public class MailUtil {
	private JavaMailSender mailSender;
	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}
	/*
	 * send simple text as mail text to single recipient
	 * @author Ranavir
	 */
	public void sendSimpleMail(String to, String subject, String msg) {
		// creating message
		SimpleMailMessage message = new SimpleMailMessage();
		//message.setFrom(from);
		message.setTo(to);
		message.setSubject(subject);
		message.setText(msg);
		// sending message
		mailSender.send(message);
	}
	/*
	 * send simple text as mail text to multiple users 
	 * @author Ranavir
	 */
	public void sendMultiSimpleMail(String[] to, String subject, String msg) {  
	       //creating message  
	    SimpleMailMessage message = new SimpleMailMessage();  
	    //message.setFrom(from);  
	    message.setTo(to);//passing array of recipients  
	    message.setSubject(subject);  
	    message.setText(msg);  
	       //sending message  
	    mailSender.send(message);     
	}   
	/*
	 * send mail with markup texts to single recipient
	 * @author Ranavir
	 */
	public void sendMimeMail(final String to, final String subject, final String msg) {
		mailSender.send(new MimeMessagePreparator() {
			  public void prepare(MimeMessage mimeMessage) throws MessagingException {
				    MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
				    message.setTo(to);
				    message.setSubject(subject);
				    message.setText(msg, true);
				    //message.addAttachment("CoolStuff.doc", new File("CoolStuff.doc"));
				  }
				});
	}
}