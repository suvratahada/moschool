package com.stl.moschool.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.lang3.StringUtils;

public class SendSMS {
	
	/*public static void main(String[] args) throws IOException{
	
		sendSMSInstant("7873949856","msg");
	}*/
	public static void sendSMSInstant(String toMobNo,String msg)throws IOException{
		final String USER_AGENT = "Mozilla/5.0";
		//http://bulksms.mysmsmantra.com:8080/WebSMS/SMSAPI.jsp?username=mantdemo&password=1951034277&sendername=SMDEMO&mobileno=9658602048&message=HiRanvir

		//http://msdgweb.mgov.gov.in/esms/sendsmsrequest?username=orms&password=orms@2014&smsservicetype=singlemsg&mobileno=7205388748&senderid=NALORS&content=Test
//		http://msdgweb.mgov.gov.in/esms/sendsmsrequest?username=orms&password=orms@2014&smsservicetype=singlemsg&mobileno=9658602048&senderid=NALCO&content=Message%20Testing
			
		
		String sms_url = "http://msdgweb.mgov.gov.in/esms/sendsmsrequest";
		String userName="orms";
		String password="orms@2014";
		String smsservicetype = "singlemsg";
		String senderId = "NALORS";
		
		msg = TextHelper.escapeJavascript(msg);
		if(StringUtils.isEmpty(msg) || StringUtils.isEmpty(toMobNo)){
			System.out.println("Invalid msg or mobile no--"+toMobNo+"--"+msg);
			return;
		}
		String strUrl = String.format("%s?username=%s&password=%s&smsservicetype=%s&mobileno=%s&senderid=%s&content=%s",
			    sms_url,userName,password,smsservicetype,toMobNo,senderId,msg);
		System.out.println("URL is " +strUrl);
		URL urlObj = new URL(strUrl);
		HttpURLConnection httpUrlconn = (HttpURLConnection) urlObj.openConnection();
		httpUrlconn.setRequestMethod("GET");
		httpUrlconn.setRequestProperty("User-Agent", USER_AGENT);

		int responseCode = httpUrlconn.getResponseCode();
		
		System.out.println("Sending 'GET' request to URL :" + strUrl);
		System.out.println("Response Code : " + responseCode);

		BufferedReader brIn = new BufferedReader(new InputStreamReader(
				httpUrlconn.getInputStream()));
		String strInputLine;
		StringBuffer strResponse = new StringBuffer();

		while ((strInputLine = brIn.readLine()) != null) {
			strResponse.append(strInputLine);
		}
		brIn.close();
		System.out.println("Response Received : "+strResponse);
		
	}
	public static void main(String[] args) throws IOException {
		sendSMSInstant("9658602048","Message Testing");
		
	}
}
