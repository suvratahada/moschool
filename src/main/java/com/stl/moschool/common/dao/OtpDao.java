package com.stl.moschool.common.dao;

import com.stl.moschool.common.model.OtpModel;

public interface OtpDao {
	boolean saveOrUpdateOtp(OtpModel otpObj);
	OtpModel getOtp(String key);
	boolean remove(String key);
}