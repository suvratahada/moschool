package com.stl.moschool.common;

import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.stl.moschool.setting.model.ResourceVO;
/**
 * 
 * @author SK Patra
 * @since  15-05-2018
 *
 */
public class AuthorizationInterceptor extends HandlerInterceptorAdapter{
	private static final Logger logger = Logger.getLogger(AuthorizationInterceptor.class);
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		logger.info("Entry ---> AuthorizationInterceptor");
		List<ResourceVO> availableResources = null ;
		List<ResourceVO> allowedResources = null ;
		boolean allowedflag = false ;
		boolean availableflag = false ;
		logger.warn("INTERCEPT URI: " + request.getRequestURI());
		String servPath = request.getServletPath() ;//login 
		System.out.println("Request Method@@##:  " + request.getMethod());
		if(request.getMethod().equalsIgnoreCase("POST") || request.getMethod().equalsIgnoreCase("GET")){
			if(servPath.endsWith(".do")){
				String resource= servPath.substring(1,servPath.indexOf('.'));
				logger.info("resource--->"+resource);
				availableResources =(List<ResourceVO>) request.getSession().getAttribute("availableResources");
				allowedResources =(List<ResourceVO>) request.getSession().getAttribute("allowedResources");
				ResourceVO resourceObj = new ResourceVO(resource);
				if(availableResources!=null && availableResources.contains(resourceObj)){//Check presence of resource in All Available Resources
					if(allowedResources!=null && !allowedResources.contains(resourceObj)){
						logger.info("Trying to access unauthorized resource...Redirecting...");
						response.sendRedirect(request.getContextPath() + "/"+"403.html");
						return false;//valid available resource but user not authorized 
					}//End Checking any allowedResources
				}//End checking any availableResources
				
			}//End if .do resource check
		}else{
			logger.info("Trying to access unauthorized resource...Redirecting...");
			response.sendRedirect(request.getContextPath() + "/"+"403.html");
			return false;//valid available resource but user not authorized 
		}
		logger.info("Exit ---> AuthorizationInterceptor");
		return true;
	}//end preHandle
	
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response,
			Object handler, ModelAndView modelAndView){
	}
	
	

}
