package com.stl.moschool.common.service;

import com.stl.moschool.common.model.OtpModel;

public interface OtpService{
	boolean saveOrUpdateOtp(OtpModel otpObj);
	OtpModel getOtp(String key);
	boolean remove(String key);
}