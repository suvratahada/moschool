package com.stl.moschool.common;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.Principal;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.stl.moschool.common.model.LoginUserDetailsModel;
import com.stl.moschool.common.model.LoginUserDetailsVO;
import com.stl.moschool.common.model.PasswordResetModel;
import com.stl.moschool.common.model.UserRegistrationDto;
import com.stl.moschool.setting.model.Resource;
import com.stl.moschool.setting.model.ResourceVO;
import com.stl.moschool.setting.service.AccessControlService;
import com.stl.moschool.utils.BCrypt;
import com.stl.moschool.utils.Utils;

/*import com.stl.ba.common.model.PasswordResetModel;
import com.stl.dpci.common.model.UserRegistrationDto;
import com.stl.dpci.setting.model.Resource;
import com.stl.dpci.setting.model.ResourceVO;
import com.stl.dpci.setting.service.AccessControlService;
import com.stl.dpci.utils.BCrypt;
//import com.stl.dpci.utils.PasswordEncryptionUtil;
import com.stl.dpci.utils.Utils;*/

@Controller
public class LoginController {
	
	static final Logger logger = Logger.getLogger(LoginController.class);
	@Autowired
	private AccessControlService accessControlService;
	
	private final static SecureRandom srandom = new SecureRandom();

	@RequestMapping(value = { "/login" }, method = RequestMethod.GET)
	public ModelAndView login(HttpServletRequest request, HttpSession session,
			@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout,
			@RequestParam(value = "sessExp", required = false) String sessExp) {
		
		logger.info("inside login page request with url /login");
		
		//session.invalidate();// Required to generate a new session id on logout
							 // or upon login page request so that any url rewriting not works
		
		//Added by Lusi on Date-26-Oct-2017
		session = request.getSession();
		String saltKey = BCrypt.generateSalt(srandom);
		session.setAttribute("saltKey",saltKey);
		
		ModelAndView model = new ModelAndView("login");
		model.addObject("saltKey",saltKey);
		if (error != null) {
			logger.info("error-->"+error);
			model.addObject("error", "Invalid username or password!");//Not required as we are giving spring customized message from Authenticator
		}

		if (logout != null) {
			logger.info("logout-->"+logout);
			model.addObject("msg", "You've been logged out successfully.");
		}
		
		if (sessExp != null) {
			logger.info("sessExp-->"+sessExp);
			model.addObject("msg", "Your current session expired!.");
			System.out.println("session coming");
		}
		//model.setViewName("login");
		return model;
	}
	
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ModelAndView accesssDenied(Principal user) {
		String methodName = "accesssDenied" ;
		logger.info("Entry---> "+this.getClass()+" method :  "+ methodName);
		
		ModelAndView model = new ModelAndView();

		if (user != null) {
			model.addObject("msg", "Hi " + user.getName() 
			+ ", you do not have permission to access this page!");
		} else {
			model.addObject("msg", 
			"You do not have permission to access this page!");
		}

		model.setViewName("403");
		
		logger.info("Exit---> "+this.getClass()+" method :  "+ methodName);
		return model;

	}//end of accessDenied
	/**
	 * This handler will build the session for the authenticated users
	 * - Here from the role ids first it will find out the allowed resources
	 * - Then  
	 * @param request
	 * @param response
	 * @param session
	 * @param principal
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value="/buildSession")
	public ModelAndView buildSession(HttpServletRequest request,HttpServletResponse response,HttpSession session,Principal principal)throws IOException{
		String methodName = "buildSession" ;
		logger.info("Entry---> "+this.getClass()+" method :  "+ methodName);
		
		
		Authentication authentication = null ;
		List<String> allowedRoles = new ArrayList<>() ;//This will store the allowed roles to the user/user_group(most cases it will hold a single role)
		List<ResourceVO> availableResources = new ArrayList<ResourceVO>() ;//This will store all the available resources with the system
        List<ResourceVO> allowedResources = null ;//This will store all the allowed resources to the user
        HashMap<String,List<String>> hmParamVal = null ;//This will store all the additional parameter values if available
        
        /*
         * Retrieving user information and then the allowed roles from Spring security context
         */
		String userId = principal.getName();
		logger.info("principal username/Id : "+userId);
		//Save user id/name in session scope
		session.setAttribute("userId", userId);
		
		// get first name and last name from adminr.user and set in session
		List<LoginUserDetailsVO> getFirstNameLastName = new ArrayList<LoginUserDetailsVO>() ;
		 List<LoginUserDetailsModel> firstNameLastName = accessControlService.getFirstNameLastName(userId) ;
		 LoginUserDetailsVO fnln = null;
	        if(firstNameLastName != null && firstNameLastName.size()>0){
	        	for(LoginUserDetailsModel res:firstNameLastName){
	        		fnln = new LoginUserDetailsVO();
	        		fnln.setFirst_name(res.getFirst_name());
	        		fnln.setLast_name(res.getLast_name());
	        		getFirstNameLastName.add(fnln);
	        	}
	        }//end if
	        logger.info("First Name Last Name--->"+getFirstNameLastName);
	        
	        String first_name = getFirstNameLastName.get(0).getFirst_name();
	        String last_name = getFirstNameLastName.get(0).getLast_name();
	        session.setAttribute("first_name", first_name);
	        session.setAttribute("last_name", last_name);
	        
	        
		session.setAttribute("showNameWelcomeSession", "true");
		SecurityContext context = SecurityContextHolder.getContext();
        if (context != null){
        	authentication = context.getAuthentication();
        }
        if (authentication!= null){
        	List<GrantedAuthority> grantedAuths = (List<GrantedAuthority>)authentication.getAuthorities() ;
        	for(GrantedAuthority ga : grantedAuths){
        		allowedRoles.add(ga.getAuthority()) ;
        	}
        }
        logger.info("allowedRoles-->"+allowedRoles);
        session.setAttribute("roleId",allowedRoles);
        
        //Used for Role Base Resource Controller by Vamsi Krish
        Map<String, Object> groupIdLocationId= accessControlService.getUserIdLocationId(userId);
        System.out.println("groupIdLocationId--->"+groupIdLocationId.toString());
        session.setAttribute("groupIdLocationId", groupIdLocationId);
        
        /*
         * Retrieve all allowed unique resources to this user and putting in session
         */
        allowedResources = accessControlService.getUniqueResourcesForRoles(allowedRoles);
        System.out.println("Total AllowedResources--->"+allowedResources);
        logger.info("Total AllowedResources--->"+allowedResources);
        session.setAttribute("allowedResources", allowedResources);
        
        /*
         * Retrieve all available resources in the system and putting in session
         */
        List<Resource> alAvailResources = accessControlService.getResourceList() ;
        ResourceVO resVo = null;
        if(alAvailResources != null && alAvailResources.size()>0){
        	for(Resource res:alAvailResources){
        		resVo = new ResourceVO();
        		resVo.setActive(res.isActive());
        		resVo.setDescription(res.getDescription());
        		resVo.setResourceId(res.getResourceId());
        		resVo.setResourceName(res.getResourceName());
        		resVo.setResourceType(res.getResourceType());
        		availableResources.add(resVo);
        	}
        }//end if
        logger.info("Total AvailableResources--->"+availableResources);
        session.setAttribute("availableResources", availableResources);
        
		/*
		 * Retrieve all the available additional parameters to the user/user_group
		 * with the provided values to these parameters for dynamic data control and 
		 * putting those values in session(with the key as parameter)
		 */
        hmParamVal = accessControlService.getDataControlDetails(userId);
        Set<String> keySet = hmParamVal.keySet();
        if(null != keySet && !keySet.isEmpty()){
        	Iterator<String> itr = keySet.iterator();
        	while(itr.hasNext()){
        		String key = itr.next();
        		List<String> values = (List<String>)hmParamVal.get(key);
        		logger.info("param key-->"+key+" param values--->"+values);
        		//put in session
        		session.setAttribute(key, values);
        	}//end while
        }//end if check exist additional params
        
        
		/*
		 * Finding out the initial redirect URL for the user
		 */
		String defaultResource = "login.html?error=true";
		if(allowedResources != null && allowedResources.size()>0){
			defaultResource = allowedResources.get(0).getResourceName()+".do";
		}//end if
		
		
		logger.info("Exit---> "+this.getClass()+" method :  "+ methodName);
		
		return new ModelAndView("redirect:/"+defaultResource);
	}//end of buildSession
	
	
	
	
	@RequestMapping(value = { "/ngoRegistration" }, method = RequestMethod.GET)
	public String register(Model model) {
		
		logger.info("register page request with url /ngoRegistration");
		
		return "ngoRegistration";
	}
	
	@RequestMapping(value = { "/govtRegistration" }, method = RequestMethod.GET)
	public String govtRegistration(Model model) {
		
		logger.info("register page request with url /ngoRegistration");
		
		return "govtRegistration";
	}
	
	@RequestMapping(value = { "/createPassword" }, method = RequestMethod.GET)
	public String createPassword(Model model) {
		
		logger.info("createPassword page request with url /createPassword");
		
		
		model.addAttribute("pwdAttr", new PasswordResetModel());
		return "createPassword";
	}
	/**********************************************
	 * Generating captcha code
	 ************************************************/
	@RequestMapping(value = {"/generateCaptcha"}, method = RequestMethod.POST )
	public @ResponseBody void getCaptchaCode(HttpServletRequest req,HttpServletResponse res) throws IOException {
		String methodName = "getCaptchaCode";
		res.setContentType("text/html");
		logger.info("Entry---> "+this.getClass()+" method :  "+ methodName);
		PrintWriter out = res.getWriter();
		String captchaCode = Utils.generateCaptchaTextMethod1();
		logger.info("captcha"+captchaCode);
		logger.info("Exit---> "+this.getClass()+" method :  "+ methodName);
		out.print(captchaCode);
	}
	/*@RequestMapping(value = "/getImage", method = RequestMethod.GET)
	  public void showImage(HttpServletResponse response) throws Exception {

	    ByteArrayOutputStream jpegOutputStream = new ByteArrayOutputStream();

	    try {
	      BufferedImage image = //CALL_OR_CREATE_YOUR_IMAGE_OBJECT;
	      ImageIO.write(image, "jpeg", jpegOutputStream);
	    } catch (IllegalArgumentException e) {
	      response.sendError(HttpServletResponse.SC_NOT_FOUND);
	    }

	    byte[] imgByte = jpegOutputStream.toByteArray();

	    response.setHeader("Cache-Control", "no-store");
	    response.setHeader("Pragma", "no-cache");
	    response.setDateHeader("Expires", 0);
	    response.setContentType("image/jpeg");
	    ServletOutputStream responseOutputStream = response.getOutputStream();
	    responseOutputStream.write(imgByte);
	    responseOutputStream.flush();
	    responseOutputStream.close();
	  }*/
	
	
	
	@RequestMapping(value = { "/reset" }, method = RequestMethod.GET)
	public String forgotPassword(Model model) {
		
		logger.debug("reset password page request with url /forgotPassword");
		
		model.addAttribute("regAttr", new UserRegistrationDto());
		return "reset";
	}
	
	
	/******************************************************************************************************
	 * this method will take input userid and email and check the userid and emailId is present or nt if present then
	 * it will take the parameter userid as input and generate a otp and save it to database table 
	 * and send the otp to corresponding Email id
	 * @throws IOException 
	 * @throws JSONException 
	 * @throws MessagingException 
	 **********************************************************************************************/
	@RequestMapping(value={"/GenerateOtpSendToMail"}, method = RequestMethod.POST)
	public @ResponseBody void GenerateOtpSendToMail(HttpServletRequest req,HttpServletResponse res) throws IOException, JSONException, MessagingException {
		String methodName = "GenerateOtpSendToMail";
		logger.info("Entry---> "+this.getClass()+" method :  "+ methodName);
		PrintWriter out = res.getWriter();
		JSONObject retObj = new JSONObject();
		String userid = req.getParameter("uid").trim();
		String emailId = req.getParameter("email").trim();
		String[] myEmailId = {emailId,"sankarkumarpatra@gmail.com","neelam.kaul@nic.in"};
		logger.info("Requested Email:"+emailId+"Requested userId :"+userid);
		JSONObject resObj = accessControlService.validateUidAndEmail(userid,emailId);
		logger.info("existance status of userid and emailId of a user:"+resObj.toString());
		if(resObj.get("status") == "success") {
			String captchaCode = Utils.generateCaptchaTextMethod1();//generating a captcha code/random number
			//EmailClient ec = new EmailClient();
			//ec.postMail(myEmailId,"OTP For Portal Login",captchaCode);//mail is send to the corresponding Email
			//save the randomcaptca in the data base
			boolean status = accessControlService.saveOtpInDb(captchaCode,userid);
			if(status == true) {
				retObj.put("message","Successfully OTP send to your Mail");
				retObj.put("status","Success");
			}else {
				retObj.put("message","Invalid Credentials");
				retObj.put("status","Failure");
			}
		}else {
			retObj.put("message","Invalid Credentials");
			retObj.put("status","Failure");
		}
		
		logger.info("Exit---> "+this.getClass()+" method :  "+ methodName);
		out.print(retObj);
	}
	
	/**********************************************
	 * Method that will take the otp and userid as input and 
	 * check in the table that wheather this otp is matching for this userid or not
	 * @throws JSONException 
	 * @throws IOException 
	 ***************************************************/
	@RequestMapping(value={"/validateOtp"}, method = RequestMethod.POST)
	public @ResponseBody void validateOtp(HttpServletRequest req,HttpServletResponse res) throws JSONException, IOException {
		String methodName = "validateOtp";
		logger.info("Entry---> "+this.getClass()+" method :  "+ methodName);
		PrintWriter out = res.getWriter();
		JSONObject retObj = new JSONObject();
		String uid = req.getParameter("uid").trim();
		String input_otp = req.getParameter("input_otp").trim();
		boolean status = accessControlService.validateOtp(uid,input_otp);
		if(status == true) {
			retObj.put("status", "Success");
		}else {
			retObj.put("status", "Failure");
		}
		logger.info("Exit---> "+this.getClass()+" method :  "+ methodName);
		out.print(retObj);
	}//end of validateOtp
	
	 /********************************************
	  * Method is taking the input UserId and it will create a one time password
	  * then it will check in otp table wheather the userid has 
	  * any otp previously if it found then it will update a new otp and send to the mail 
	  * if any otp is not found to this userid then it will insert a new row for this user in otp table and 
	  * send to the mail
	 * @throws MessagingException 
	  *******************************************/
	@RequestMapping(value={"/resendOtp"}, method = RequestMethod.POST)
	public @ResponseBody void resendOtp(HttpServletRequest req,HttpServletResponse res) throws JSONException, IOException, MessagingException {
		String methodName = "resendOtp";
		logger.info("Entry---> "+this.getClass()+" method :  "+ methodName);
		PrintWriter out = res.getWriter();
		JSONObject retObj = new JSONObject();
		String uid = req.getParameter("uid").trim();
		String emailId = req.getParameter("email").trim();
		String[] myEmailId = {emailId,"sankarkumarpatra@gmail.com","neelam.kaul@nic.in"};
		String captchaCode = Utils.generateCaptchaTextMethod1();//generating a captcha code/random number
		boolean status = accessControlService.resendOtp(uid,captchaCode);//method thaqt will check wheather any otp is present or not if present update or else insert
		if(status == true) {
			//EmailClient ec = new EmailClient();
			//ec.postMail(myEmailId,"OTP For Portal Login",captchaCode);//mail is send to the corresponding Email
			retObj.put("status","Success");
		}else {
			retObj.put("status","Failure");
		}
		logger.info("Exit---> "+this.getClass()+" method :  "+ methodName);
		out.print(retObj);
	}//End of resendOtp

	
	
	/***************************************************************************
	 * Method will set the new password of a corresponding UserId
	 * Input is password and userid
	 ***************************************************************************/
	
	@RequestMapping(value={"/savePassword"}, method = RequestMethod.POST)
	public @ResponseBody void savePassword(HttpServletRequest req,HttpServletResponse res) throws JSONException, IOException, MessagingException {
		String methodName = "savePassword";
		logger.info("Entry---> "+this.getClass()+" method :  "+ methodName);
		PrintWriter out = res.getWriter();
		JSONObject retObj = new JSONObject();
		String uid = req.getParameter("uid").trim();
		String password = req.getParameter("#new_password").trim();
		//commented by lusi on Dt-26-10-2017
		
		/*PasswordEncryptionUtil peu = new PasswordEncryptionUtil();
		String encryptPassword = peu.getEncriptedPassword(password);*/
		
		boolean status = accessControlService.savePassword(uid,password);
		if(status == true) {
			retObj.put("status", "Success");
		}else {
			retObj.put("status", "Failure");
		}
		logger.info("Exit---> "+this.getClass()+" method :  "+ methodName);
		out.print(retObj);
	}//End of savePassword
	
	/***************************************************
	 * Method to used to regester a new user
	 * 
	 * Please check this method USAGE
	 ****************************************************/
	@RequestMapping(value={"/newRegestration"}, method = RequestMethod.POST)
	public @ResponseBody void newUserRegestration(HttpServletRequest req,HttpServletResponse res) throws JSONException, IOException, MessagingException {
		String methodName = "newUserRegestration";
		logger.info("Entry---> "+this.getClass()+" method :  "+ methodName);
		JSONObject retObj  = new JSONObject();
		PrintWriter out = res.getWriter();
		String fName = req.getParameter("txtFirstName").toLowerCase();
		String mobNo = req.getParameter("txtCandidatePhone");
		String emailId = req.getParameter("txtEmail").trim();
		String uid = fName.concat(Utils.generateCaptchaTextMethod1().substring(0,3));//generating a random number and append to userId
		String password = Utils.generateCaptchaTextMethod1();
		/*PasswordEncryptionUtil peu = new PasswordEncryptionUtil();
		String encryptPassword = peu.getEncriptedPassword(password);*/
		logger.info("password is:"+password);
		HashMap<String,String> hm = new HashMap<String, String>();
		hm.put("uid", uid);
		hm.put("fName", fName);
		hm.put("mob", mobNo);
		hm.put("email", emailId);
		//commented by lusi on Dt-26-10-2017 
		//hm.put("epwd", encryptPassword);
		hm.put("epwd", password);
		String emaiMessage = "Your userId is:"+uid+"::"+"password is-"+password;
		boolean userRegestration = accessControlService.newUserRegestration(hm);
		String[] myEmailId = {emailId};
		if(userRegestration == true) {
			//EmailClient ec = new EmailClient();
			//ec.postMail(myEmailId,"Login Credentials",emaiMessage);//mail is send to the corresponding Email
			retObj.put("status","Success");
			retObj.put("message","Check Your Mail For Login Credentials");
		}else {
			retObj.put("status","Success");
			retObj.put("message","Failed");
		}
		logger.info("Exit---> "+this.getClass()+" method :  "+ methodName);
		out.print(retObj);
		
	}
	
	
	
}//end class
