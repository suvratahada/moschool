package com.stl.moschool.common.model;

public class MemberModel {
	private String memberName;
	private String memberDesignation;
	private String aadharNumber;
	private String aadharNumberVerifiedStatus;
	private String panNo;
	private String panVerifiedStatus;
	private String emailId;
	private String lastUpdate;
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getMemberDesignation() {
		return memberDesignation;
	}
	public void setMemberDesignation(String memberDesignation) {
		this.memberDesignation = memberDesignation;
	}
	public String getAadharNumber() {
		return aadharNumber;
	}
	public void setAadharNumber(String aadharNumber) {
		this.aadharNumber = aadharNumber;
	}
	public String getAadharNumberVerifiedStatus() {
		return aadharNumberVerifiedStatus;
	}
	public void setAadharNumberVerifiedStatus(String aadharNumberVerifiedStatus) {
		this.aadharNumberVerifiedStatus = aadharNumberVerifiedStatus;
	}
	public String getPanNo() {
		return panNo;
	}
	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}
	public String getPanVerifiedStatus() {
		return panVerifiedStatus;
	}
	public void setPanVerifiedStatus(String panVerifiedStatus) {
		this.panVerifiedStatus = panVerifiedStatus;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	@Override
	public String toString() {
		return "MemberModel [memberName=" + memberName + ", memberDesignation="
				+ memberDesignation + ", aadharNumber=" + aadharNumber
				+ ", aadharNumberVerifiedStatus=" + aadharNumberVerifiedStatus
				+ ", panNo=" + panNo + ", panVerifiedStatus="
				+ panVerifiedStatus + ", emailId=" + emailId + ", lastUpdate="
				+ lastUpdate + ", getMemberName()=" + getMemberName()
				+ ", getMemberDesignation()=" + getMemberDesignation()
				+ ", getAadharNumber()=" + getAadharNumber()
				+ ", getAadharNumberVerifiedStatus()="
				+ getAadharNumberVerifiedStatus() + ", getPanNo()="
				+ getPanNo() + ", getPanVerifiedStatus()="
				+ getPanVerifiedStatus() + ", getEmailId()=" + getEmailId()
				+ ", getLastUpdate()=" + getLastUpdate() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}
	
	

}
