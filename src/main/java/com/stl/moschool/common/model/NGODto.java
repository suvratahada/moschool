package com.stl.moschool.common.model;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class NGODto {
	
	@Pattern(regexp ="^[A-Z0-9/]*$")
    @Size(min = 10, max =25)
	private String ngoid;
	
	@Pattern(regexp ="^[A-Z0-9]*$")
    @Size(min = 10, max = 10)
	private String panNo;
	public String getNgoid() {
		return ngoid;
	}
	public void setNgoid(String ngoid) {
		this.ngoid = ngoid;
	}
	public String getPanNo() {
		return panNo;
	}
	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}
	@Override
	public String toString() {
		return "NGODto [ngoid=" + ngoid + ", panNo=" + panNo + "]";
	}
	
}
