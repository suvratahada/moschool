package com.stl.moschool.common.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name="Ngo")
@XmlAccessorType(XmlAccessType.NONE)
public class NgoModel implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nitiAayogId;
    private String name;
    private String registrationNumber;
    private String panNumber;
    private String panVerifyStatus;
    private String email;
    private String offPhone;
    private String regAddress;
    private String address;
    private String districtName;
	private String stateName;
	private String registeredStateName;
	private String pincode;
	private String cityReg;
	private String dateReg;
	private String keycontact_name;
	private String keycontact_designame;
	private String fcraNo;
	private String mobileNo;
	private String website;
	private String actName;
	private String bestPractices;
	private List<MemberModel> lstMemberModel;
	public String getNitiAayogId() {
		return nitiAayogId;
	}
	public void setNitiAayogId(String nitiAayogId) {
		this.nitiAayogId = nitiAayogId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRegistrationNumber() {
		return registrationNumber;
	}
	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}
	public String getPanNumber() {
		return panNumber;
	}
	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}
	public String getPanVerifyStatus() {
		return panVerifyStatus;
	}
	public void setPanVerifyStatus(String panVerifyStatus) {
		this.panVerifyStatus = panVerifyStatus;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getOffPhone() {
		return offPhone;
	}
	public void setOffPhone(String offPhone) {
		this.offPhone = offPhone;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDistrictName() {
		return districtName;
	}
	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getRegisteredStateName() {
		return registeredStateName;
	}
	public void setRegisteredStateName(String registeredStateName) {
		this.registeredStateName = registeredStateName;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getCityReg() {
		return cityReg;
	}
	public void setCityReg(String cityReg) {
		this.cityReg = cityReg;
	}
	public String getDateReg() {
		return dateReg;
	}
	public void setDateReg(String dateReg) {
		this.dateReg = dateReg;
	}
	public String getKeycontact_name() {
		return keycontact_name;
	}
	public void setKeycontact_name(String keycontact_name) {
		this.keycontact_name = keycontact_name;
	}
	
	
	public String getRegAddress() {
		return regAddress;
	}
	public void setRegAddress(String regAddress) {
		this.regAddress = regAddress;
	}
	public String getKeycontact_designame() {
		return keycontact_designame;
	}
	public void setKeycontact_designame(String keycontact_designame) {
		this.keycontact_designame = keycontact_designame;
	}
	public String getFcraNo() {
		return fcraNo;
	}
	public void setFcraNo(String fcraNo) {
		this.fcraNo = fcraNo;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getActName() {
		return actName;
	}
	public void setActName(String actName) {
		this.actName = actName;
	}
	public String getBestPractices() {
		return bestPractices;
	}
	public void setBestPractices(String bestPractices) {
		this.bestPractices = bestPractices;
	}
	public List<MemberModel> getLstMemberModel() {
		return lstMemberModel;
	}
	public void setLstMemberModel(List<MemberModel> lstMemberModel) {
		this.lstMemberModel = lstMemberModel;
	}
	@Override
	public String toString() {
		return "NgoModel [nitiAayogId=" + nitiAayogId + ", name=" + name + "]";
	}
	
    
    
    
}
