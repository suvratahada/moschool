package com.stl.moschool.common.model;


import org.hibernate.validator.constraints.NotEmpty;

public class PasswordResetModel {
	@NotEmpty
//	@Pattern (regexp = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}")
	private String newPassword;
	
	@NotEmpty
//	@Pattern (regexp = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}")
	private String confirmPassword;
	
	@NotEmpty 
	private String otp;
	private String token;
	@NotEmpty
	private String id;
	
	private boolean fileStatus;
	
	public boolean isFileStatus() {
		return fileStatus;
	}
	public void setFileStatus(boolean fileStatus) {
		this.fileStatus = fileStatus;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "PasswordResetModel [newPassword=" + newPassword
				+ ", confirmPassword=" + confirmPassword + ", otp=" + otp
				+ ", token=" + token + ", id=" + id + "]";
	}
	
}
