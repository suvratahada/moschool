package com.stl.moschool.common.model;

import org.hibernate.validator.constraints.NotEmpty;

public class UserRegistrationDto {
	@NotEmpty
	String userId;
	String email;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "UserRegistrationDto [userId=" + userId + ", email=" + email+ "]";
	}
	
}
