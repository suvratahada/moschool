package com.stl.moschool.common.model;

public class OtpModel {
	private long slno;
	private String otp_secret;
	private String otp;
	private String key;
	
	
	public OtpModel() {
		super();
		// TODO Auto-generated constructor stub
	}
	public OtpModel(String key, String otp_secret, String otp) {
		super();
		this.otp_secret = otp_secret;
		this.otp = otp;
		this.key = key;
	}
	public long getSlno() {
		return slno;
	}
	public void setSlno(long slno) {
		this.slno = slno;
	}
	public String getOtp_secret() {
		return otp_secret;
	}
	public void setOtp_secret(String otp_secret) {
		this.otp_secret = otp_secret;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	
}
