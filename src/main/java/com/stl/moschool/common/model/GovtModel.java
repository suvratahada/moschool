package com.stl.moschool.common.model;

import java.util.List;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.web.multipart.MultipartFile;



public class GovtModel {
	
	@Pattern(regexp ="^[A-Za-z ]*$")
	@Size(max = 500)
	private String txt_input_org_name;
	
	@Pattern(regexp ="^[A-Za-z _]*$")
	private String typeGovtOrganisation;
	
	@Pattern(regexp ="^[A-Z ]*$")
	private String differentDepartmentAutonomous;
	
	@Pattern(regexp ="^[A-Za-z0-9 _@.()}{%$!~+=^:\\;\"\',|?*/#&+\\s-]*$")
	private String txt_input_org_Address;
	
	@Pattern(regexp ="^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")
	private String txt_input_org_email;
	
	@Pattern(regexp ="^[0-9]*$")
    @Size(min = 10, max = 10)
	private String txt_input_org_mobNo;
	
	@Pattern(regexp ="^[a-zA-Z0-9  .@_/-]*$")
	private String txt_input_regd_no;
	
	@Pattern(regexp ="^[A-Za-z0-9 _@.()}{%$!~+=^:\\;\"\',|?*/#&+\\s-]*$")
	private String txt_input_reg_Address;
	
    @Pattern(regexp ="^[0-9-]*$")
	@Size(min =10, max = 10)
	private String txt_regd_date;
    
    @Pattern(regexp ="^[A-Za-z ./:]*$")
	private String txt_input_gov_url;
    
    @Pattern(regexp ="^[a-zA-Z \\s]*$")
	private String txt_input_chairman_name;
	
	@Pattern(regexp ="^[0-9A-Z]*$")
	@Size(min =10, max = 10)
	private String txt_input_chairman_panNo;
	
	@Pattern(regexp ="^[a-zA-Z \\s]*$")
	private String txt_input_city;
	
	@Pattern(regexp ="^[a-zA-Z \\s]*$")
	private String txt_input_district;
	
	@Pattern(regexp ="^[a-zA-Z \\s]*$")
	private String txt_input_state;
	
	@Pattern(regexp ="^[0-9]*$")
	@Size(min =6, max = 6)
	private String txt_input_pin_code;
	
	List<MultipartFile> file;
	
	private String roc_file;
	private String activity_file;
	private String memo;
	
	public String getRoc_file() {
		return roc_file;
	}
	public void setRoc_file(String roc_file) {
		this.roc_file = roc_file;
	}
	public String getActivity_file() {
		return activity_file;
	}
	public void setActivity_file(String activity_file) {
		this.activity_file = activity_file;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public List<MultipartFile> getFile() {
		return file;
	}
	public void setFile(List<MultipartFile> file) {
		this.file = file;
	}
	public String getTxt_input_org_name() {
		return txt_input_org_name;
	}
	public void setTxt_input_org_name(String txt_input_org_name) {
		this.txt_input_org_name = txt_input_org_name;
	}
	public String getTypeGovtOrganisation() {
		return typeGovtOrganisation;
	}
	public void setTypeGovtOrganisation(String typeGovtOrganisation) {
		this.typeGovtOrganisation = typeGovtOrganisation;
	}
	public String getDifferentDepartmentAutonomous() {
		return differentDepartmentAutonomous;
	}
	public void setDifferentDepartmentAutonomous(
			String differentDepartmentAutonomous) {
		this.differentDepartmentAutonomous = differentDepartmentAutonomous;
	}
	public String getTxt_input_org_Address() {
		return txt_input_org_Address;
	}
	public void setTxt_input_org_Address(String txt_input_org_Address) {
		this.txt_input_org_Address = txt_input_org_Address;
	}
	public String getTxt_input_org_email() {
		return txt_input_org_email;
	}
	public void setTxt_input_org_email(String txt_input_org_email) {
		this.txt_input_org_email = txt_input_org_email;
	}
	public String getTxt_input_org_mobNo() {
		return txt_input_org_mobNo;
	}
	public void setTxt_input_org_mobNo(String txt_input_org_mobNo) {
		this.txt_input_org_mobNo = txt_input_org_mobNo;
	}
	public String getTxt_input_regd_no() {
		return txt_input_regd_no;
	}
	public void setTxt_input_regd_no(String txt_input_regd_no) {
		this.txt_input_regd_no = txt_input_regd_no;
	}
	public String getTxt_input_reg_Address() {
		return txt_input_reg_Address;
	}
	public void setTxt_input_reg_Address(String txt_input_reg_Address) {
		this.txt_input_reg_Address = txt_input_reg_Address;
	}
	public String getTxt_regd_date() {
		return txt_regd_date;
	}
	public void setTxt_regd_date(String txt_regd_date) {
		this.txt_regd_date = txt_regd_date;
	}
	public String getTxt_input_gov_url() {
		return txt_input_gov_url;
	}
	public void setTxt_input_gov_url(String txt_input_gov_url) {
		this.txt_input_gov_url = txt_input_gov_url;
	}
	public String getTxt_input_chairman_name() {
		return txt_input_chairman_name;
	}
	public void setTxt_input_chairman_name(String txt_input_chairman_name) {
		this.txt_input_chairman_name = txt_input_chairman_name;
	}
	public String getTxt_input_chairman_panNo() {
		return txt_input_chairman_panNo;
	}
	public void setTxt_input_chairman_panNo(String txt_input_chairman_panNo) {
		this.txt_input_chairman_panNo = txt_input_chairman_panNo;
	}
	public String getTxt_input_city() {
		return txt_input_city;
	}
	public void setTxt_input_city(String txt_input_city) {
		this.txt_input_city = txt_input_city;
	}
	public String getTxt_input_district() {
		return txt_input_district;
	}
	public void setTxt_input_district(String txt_input_district) {
		this.txt_input_district = txt_input_district;
	}
	public String getTxt_input_state() {
		return txt_input_state;
	}
	public void setTxt_input_state(String txt_input_state) {
		this.txt_input_state = txt_input_state;
	}
	public String getTxt_input_pin_code() {
		return txt_input_pin_code;
	}
	public void setTxt_input_pin_code(String txt_input_pin_code) {
		this.txt_input_pin_code = txt_input_pin_code;
	}
	@Override
	public String toString() {
		return "GovtModel [txt_input_org_name=" + txt_input_org_name
				+ ", typeGovtOrganisation=" + typeGovtOrganisation
				+ ", differentDepartmentAutonomous="
				+ differentDepartmentAutonomous + ", txt_input_org_Address="
				+ txt_input_org_Address + ", txt_input_org_email="
				+ txt_input_org_email + ", txt_input_org_mobNo="
				+ txt_input_org_mobNo + ", txt_input_regd_no="
				+ txt_input_regd_no + ", txt_input_reg_Address="
				+ txt_input_reg_Address + ", txt_regd_date=" + txt_regd_date
				+ ", txt_input_gov_url=" + txt_input_gov_url
				+ ", txt_input_chairman_name=" + txt_input_chairman_name
				+ ", txt_input_chairman_panNo=" + txt_input_chairman_panNo
				+ ", txt_input_city=" + txt_input_city
				+ ", txt_input_district=" + txt_input_district
				+ ", txt_input_state=" + txt_input_state
				+ ", txt_input_pin_code=" + txt_input_pin_code + ", file="
				+ file + "]";
	}

}
