package com.stl.moschool.common.model;

import javax.validation.constraints.NotNull;

public class SimpleModel {
	@NotNull
	private Long id;
	private String value;
	private String uid;
	
	
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
}
