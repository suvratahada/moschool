package com.stl.moschool.masterSetup.dao;

import java.util.List;
import java.util.Map;

import com.stl.moschool.masterSetup.model.BlockModel;
import com.stl.moschool.masterSetup.model.CategoryModel;
import com.stl.moschool.masterSetup.model.ClusterModel;
import com.stl.moschool.masterSetup.model.DistrictModel;
import com.stl.moschool.masterSetup.model.MapModel;
import com.stl.moschool.masterSetup.model.MasterSchoolModel;
import com.stl.moschool.masterSetup.model.MasterSubcategoryModel;
import com.stl.moschool.masterSetup.model.ReAllocatedProjectModel;
import com.stl.moschool.masterSetup.model.SchemeModel;
import com.stl.moschool.masterSetup.model.SurveyModel;
import com.stl.moschool.masterSetup.model.YearModel;

public interface MasterSetupDao {
	
	boolean saveSchoolDetails(MasterSchoolModel school);
	
	List<MasterSchoolModel> getSchoolDetails();
	
	List<DistrictModel> getDistrictDetails(Map<String, Object> groupIdLocationId);

	List<BlockModel> getBlockDetails(Map<String, Object> groupIdLocationId);

	List<ClusterModel> getClusterDetails();

	boolean saveClusterDetails(ClusterModel user);

	boolean saveBlockDetails(BlockModel user);

	boolean saveDistrictDetails(DistrictModel user);

	List<MapModel> getBlockDetailsByDistrictID(long district, Map<String, Object> groupIdLocationId);

	List<SurveyModel> getSurveyDetails();

	boolean updateSchoolDetails(MasterSchoolModel school);

	boolean deleteSchool(long school_id);

	boolean updateDistrictDetails(DistrictModel district);

	boolean deleteDistrict(int district_id);

	boolean updateBlockDetails(BlockModel block);

	boolean deleteBlock(long block_id);

	List<YearModel> getYearDetails();

	boolean saveYearDetails(YearModel year);

	boolean updateYearDetails(YearModel year);

	boolean deleteYear(long year_id);

	List<SchemeModel> getSchemeDetails();

	boolean saveSchemeDetails(SchemeModel scheme);

	boolean updateSchemeDetails(SchemeModel scheme);

	boolean deleteScheme(long scheme_id);

	boolean deleteReleasedAmount(ReAllocatedProjectModel project);

	List<ReAllocatedProjectModel> getReAllocatedProjectDetails(String userId, String block, String finyear, String district, String subcategory);

	List<CategoryModel> getCategoryDetails();

	List<MasterSubcategoryModel> getAllSubcategoryFormMasterWorkDesc();

	List<MasterSubcategoryModel> getAllSubCategoryWorkDetailsFromMasterWork();

	boolean saveSubcategoryDetailsToMasterWork(MasterSubcategoryModel subcategory, String userId);

	boolean updateSubcategoryDetails(MasterSubcategoryModel subcategory);

	boolean deleteSubcategoryDetails(long work_id);

	boolean saveSubcategoryDescriptionToMasterWorkDesc(MasterSubcategoryModel subcategory);

	boolean deleteSubcategoryDescDetails(MasterSubcategoryModel subcategory);

}
