package com.stl.moschool.masterSetup.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.stl.moschool.masterSetup.dao.MasterSetupDao;
import com.stl.moschool.masterSetup.model.BlockModel;
import com.stl.moschool.masterSetup.model.CategoryModel;
import com.stl.moschool.masterSetup.model.ClusterModel;
import com.stl.moschool.masterSetup.model.DistrictModel;
import com.stl.moschool.masterSetup.model.MapModel;
import com.stl.moschool.masterSetup.model.MasterSchoolModel;
import com.stl.moschool.masterSetup.model.MasterSubcategoryModel;
import com.stl.moschool.masterSetup.model.ReAllocatedProjectModel;
import com.stl.moschool.masterSetup.model.SchemeModel;
import com.stl.moschool.masterSetup.model.SurveyModel;
import com.stl.moschool.masterSetup.model.YearModel;
import com.stl.moschool.utils.Utils;


@Repository("masterSetupDao")
public class MasterSetupDaoImpl implements MasterSetupDao{

	@SuppressWarnings("unused")
	@Autowired
	private SessionFactory sessionFactory ;
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	private Properties adminQueries;

	@Override
	public boolean saveSchoolDetails(MasterSchoolModel school) {
		String sql = adminQueries.getProperty("mst.saveSchoolDetails");		
		jdbcTemplate.update(sql, new Object[] { 
				school.getBlock_id(),
				school.getSchool_id(),
				school.getSchool_name(),
				school.getRecord_status(),
		});		
		return true;
	}

	@Override
	public List<MasterSchoolModel> getSchoolDetails() {
		
		String sql = adminQueries.getProperty("getSchoolListSQL");
		List<MasterSchoolModel> schools = jdbcTemplate.query(sql, new RowMapper<MasterSchoolModel>() {
			@Override
			public MasterSchoolModel mapRow(ResultSet rs, int rownumber) throws SQLException {
				MasterSchoolModel school = new MasterSchoolModel();
				school.setSchool_id(rs.getLong("school_id"));
				school.setSchool_name(rs.getString("school_name"));
				school.setBlock_id(rs.getLong("block_id"));
				school.setBlock_name(rs.getString("block_name"));
				return school;
			}
		});
		return schools;
	}

	@Override
	public List<DistrictModel> getDistrictDetails(Map<String, Object> groupIdLocationId) {
		String sql = adminQueries.getProperty("getDistrictListSQL");
		
		System.out.println(groupIdLocationId.toString());
		Long locId = Utils.getLocationId(groupIdLocationId, "stc"); //Long.parseLong((String) groupIdLocationId.get("districtId"));
		if(locId > 0){
			sql += "and district_id = "+locId+";";
		}
		
		List<DistrictModel> districts = jdbcTemplate.query(sql, new RowMapper<DistrictModel>(){

			@Override
			public DistrictModel mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				DistrictModel district=new DistrictModel();
				district.setDistrict_id(rs.getInt(1));
				district.setDistrict_name(rs.getString(2));
				district.setCreated_by(rs.getLong(3));
				district.setCreated_on(rs.getTimestamp(4));
				district.setModified_by(rs.getLong(5));
				district.setModified_on(rs.getTimestamp(6));
				district.setRecord_status(rs.getInt(7));
				district.setLastmodified(rs.getTimestamp(8));
				return district;
			}
			
		});
		return districts;
	}


	@Override
	public List<BlockModel> getBlockDetails(Map<String, Object> groupIdLocationId) {
		String sql = adminQueries.getProperty("getBlockListSQL");
		
		System.out.println(groupIdLocationId.toString());
		Long locId = Utils.getLocationId(groupIdLocationId, "tc"); //Long.parseLong((String) groupIdLocationId.get("blockId"));
		if(locId > 0){
			sql += "and block_id = "+locId+";";
		}
		
		List<BlockModel> blocks = jdbcTemplate.query(sql, new RowMapper<BlockModel>(){

			@Override
			public BlockModel mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				BlockModel block=new BlockModel();
				block.setBlock_id(rs.getInt("block_id"));
				block.setDistrict_id(rs.getLong("district_id"));
				block.setBlock_name(rs.getString("block_name"));
				block.setCreated_by(rs.getString("created_by"));
				block.setCreated_on(rs.getTimestamp("created_on"));
				block.setModified_by(rs.getString("modified_by"));
				block.setModified_on(rs.getTimestamp("modified_on"));
				return block;
			}
			
		});
		return blocks;
	}

	@Override
	public List<ClusterModel> getClusterDetails() {
		String sql = adminQueries.getProperty("getClusterListSQL");
		List<ClusterModel> clusters = jdbcTemplate.query(sql, new RowMapper<ClusterModel>(){

			@Override
			public ClusterModel mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				ClusterModel cluster=new ClusterModel();
				cluster.setCluster_id(rs.getLong(1));
				cluster.setBlock_id(rs.getLong(2));
				cluster.setCluster_name(rs.getString(3));
				cluster.setCreated_by(rs.getString(4));
				cluster.setCreated_on(rs.getTimestamp(5));
				cluster.setModified_by(rs.getString(6));
				cluster.setModified_on(rs.getTimestamp(7));
				cluster.setRecord_status(rs.getInt(8));
				cluster.setLastmodified(rs.getTimestamp(9));
				return cluster;
			}
			
		});
		return clusters;
	}

	@Override
	public boolean saveClusterDetails(ClusterModel cluster) {
		System.out.println(cluster.toString());
		String sql = adminQueries.getProperty("saveClusterDetailsSQL");	
		System.out.println(sql);
		jdbcTemplate.update(sql, new Object[] { 
				cluster.getCluster_id(),
				cluster.getBlock_id(),
				cluster.getCluster_name(),
				cluster.getRecord_status()
		});		
		return true;
	}

	@Override
	public boolean saveBlockDetails(BlockModel block) {
		System.out.println(block.getBlock_id());
		System.out.println("District ID:"+block.getDistrict_id());
		String sql = adminQueries.getProperty("saveBlockDetailsSQL");	
		System.out.println(sql);
		jdbcTemplate.update(sql, new Object[] { 
				block.getBlock_id(),
				block.getDistrict_id(),
				block.getBlock_name(),
				block.getRecord_status()
		});		
		return true;
	}


	@Override
	public boolean saveDistrictDetails(DistrictModel district) {
		String sql = adminQueries.getProperty("saveDistrictDetailsSQL");	
		System.out.println(sql);
		jdbcTemplate.update(sql, new Object[] { 
				district.getDistrict_id(),
				district.getDistrict_name(),
				district.getRecord_status()
		});		
		return true;
	}

	@Override
	public List<MapModel> getBlockDetailsByDistrictID(long district, Map<String, Object> groupIdLocationId) {
		String sql = adminQueries.getProperty("getBlockDetailsByDistrictIDSQL");

		System.out.println(groupIdLocationId.toString());
		Long locId = Utils.getLocationId(groupIdLocationId, "BLOCK");// Long.parseLong((String) groupIdLocationId.get("blockId"));
		if(locId > 0){
			sql += " and block_id = "+locId+";";
		}
		
		List<MapModel> blocks = jdbcTemplate.query(sql, new RowMapper<MapModel>(){

			@Override
			public MapModel mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				MapModel block=new MapModel();
				block.setId(rs.getLong(	1));
				block.setName(rs.getString(2));
				return block;
			}
			
		},district);
		return blocks;
	}


	@Override
	public List<SurveyModel> getSurveyDetails() {
		String sql = adminQueries.getProperty("getSurveyDetailsSQL");
		List<SurveyModel> Surveys = jdbcTemplate.query(sql, new RowMapper<SurveyModel>(){

			@Override
			public SurveyModel mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				SurveyModel Survey=new SurveyModel();
				Survey.setSurvey_id(rs.getLong(1));
				Survey.setSurvey_name(rs.getString(2));
				return Survey;
			}
			
		});
		return Surveys;
	}

	@Override
	public boolean updateSchoolDetails(MasterSchoolModel school) {
		String sql = adminQueries.getProperty("mst.updateSchoolDetailsSql");
		System.out.println("School Id"+school.getSchool_id());
		jdbcTemplate.update(sql, new Object[] { 
				school.getBlock_id(),
				school.getSchool_name(),
				school.getSchool_id()
		});		
		return true;
	}

	@Override
	public boolean deleteSchool(long school_id) {
		String sql = adminQueries.getProperty("mst.deleteSchoolSQL");
		jdbcTemplate.update(sql, new Object[] {	0, school_id });		
		return true;
	}

	@Override
	public boolean updateDistrictDetails(DistrictModel district) {
		String sql = adminQueries.getProperty("mst.updateDistrictDetailsSql");
		System.out.println("District Id"+district.getDistrict_id());
		jdbcTemplate.update(sql, new Object[] {
				district.getDistrict_name(),
				district.getDistrict_id()
		});		
		return true;
	}

	@Override
	public boolean deleteDistrict(int district_id) {
		String sql = adminQueries.getProperty("mst.deleteDistrictSQL");
		jdbcTemplate.update(sql, new Object[] {	0, district_id });		
		return true;
	}

	@Override
	public boolean updateBlockDetails(BlockModel block) {
		String sql = adminQueries.getProperty("mst.updateBlockDetailsSQL");
		System.out.println(sql);
		System.out.println("School Id"+block.getBlock_id());
		jdbcTemplate.update(sql, new Object[] { 
				block.getDistrict_id(),
				block.getBlock_name(),
				block.getCreated_by(),
				block.getCreated_on(),
				block.getModified_by(),
				block.getModified_on(),
				block.getLastmodified(),
				block.getBlock_id()
		});		
		return true;
	}

	@Override
	public boolean deleteBlock(long block_id) {
		String sql = adminQueries.getProperty("mst.deleteBlockSQL");
		jdbcTemplate.update(sql, new Object[] {	0, block_id });		
		return true;
	}

	@Override
	public List<YearModel> getYearDetails() {
		String sql = adminQueries.getProperty("pma.getFinYearSql");
		List<YearModel> years = jdbcTemplate.query(sql, new RowMapper<YearModel>() {
			@Override
			public YearModel mapRow(ResultSet rs, int rownumber) throws SQLException {
				YearModel year = new YearModel();
					year.setYear_id(rs.getLong("year_id"));
					year.setYear_desc(rs.getString("year_desc"));
				return year;
			}
		});
		return years;
	}

	@Override
	public boolean saveYearDetails(YearModel year) {
		System.out.println("Year: "+year.getYear_desc());
		String sql = adminQueries.getProperty("mst.saveYearDetails");		
		jdbcTemplate.update(sql, new Object[] { 
				year.getYear_desc()
		});		
		return true;
	}

	@Override
	public boolean updateYearDetails(YearModel year) {
		String sql = adminQueries.getProperty("mst.updateYearDetailsSQL");
		System.out.println(sql);
		System.out.println("Year Id"+year.getYear_id());
		jdbcTemplate.update(sql, new Object[] { 
				year.getYear_desc(),
				year.getYear_id()
		});		
		return true;
	}

	@Override
	public boolean deleteYear(long year_id) {
		String sql = adminQueries.getProperty("mst.deleteYearSQL");
		jdbcTemplate.update(sql, new Object[] {	0, year_id });		
		return true;
	}

	@Override
	public List<SchemeModel> getSchemeDetails() {
		
		String sql = adminQueries.getProperty("pma.getSchemeDetails");
		List<SchemeModel> schemes = jdbcTemplate.query(sql, new RowMapper<SchemeModel>() {
			@Override
			public SchemeModel mapRow(ResultSet rs, int rownumber) throws SQLException {
				SchemeModel scheme = new SchemeModel();
					scheme.setScheme_id(rs.getLong("scheme_id"));
					scheme.setScheme(rs.getString("scheme"));
					scheme.setScheme_desc(rs.getString("scheme_desc"));
				return scheme;
			}
		});
		return schemes;
	}

	@Override
	public boolean saveSchemeDetails(SchemeModel scheme) {
		String sql = adminQueries.getProperty("mst.saveSchemeDetails");		
		jdbcTemplate.update(sql, new Object[] { 
				scheme.getScheme(),
				scheme.getScheme_desc()
		});		
		return true;
	}

	@Override
	public boolean updateSchemeDetails(SchemeModel scheme) {
		String sql = adminQueries.getProperty("mst.updateSchemeDetailsSQL");
		System.out.println(sql);
		System.out.println("Scheme Id"+scheme.getScheme_id());
		jdbcTemplate.update(sql, new Object[] { 
				scheme.getScheme(),
				scheme.getScheme_desc(),
				scheme.getScheme_id()
		});		
		return true;
	}

	@Override
	public boolean deleteScheme(long scheme_id) {
		String sql = adminQueries.getProperty("mst.deleteSchemeSQL");
		jdbcTemplate.update(sql, new Object[] {	0, scheme_id });		
		return true;
	}

	@Override
	public boolean deleteReleasedAmount(ReAllocatedProjectModel project) {
		String sql = adminQueries.getProperty("mst.checkForReleasedAmountSQL");
		String sql1 = adminQueries.getProperty("mst.deleteReleasedAmountSQL");
		String sql2 = adminQueries.getProperty("mst.deleteReAllocationSQL");
		System.out.println("Project:: "+project.getProject_id());
		int record_status = 0;
		long count = jdbcTemplate.queryForObject(sql, Long.class, new Object[]{project.getProject_id()});
		System.out.println("count:: "+count);
		if(count != 0){
			jdbcTemplate.update(sql1, new Object[] {record_status, project.getProject_id()});
			jdbcTemplate.update(sql2, new Object[] {record_status, project.getProject_id()});
			return true;
		}else{
			jdbcTemplate.update(sql2, new Object[] {record_status, project.getProject_id()});
			return true;
		}
		
	}

	@Override
	public List<ReAllocatedProjectModel> getReAllocatedProjectDetails(
			String userId, String block, String finyear,
			String district,
			String subcategory) {
		StringBuilder sql = new StringBuilder(adminQueries.getProperty("mst.getAllReAllocatedProjectDetails"));
		
		if(userId.equalsIgnoreCase("admin") &&  !district.equalsIgnoreCase("All")) {
			sql.append(" AND ramp.district_id = "+district+"");
		}
		if(userId.equalsIgnoreCase("admin") &&  !block.equalsIgnoreCase("All")) {
			sql.append(" AND ramp.block_id = "+block+"");
		}
		if(userId.equalsIgnoreCase("admin") &&  !finyear.equalsIgnoreCase("All")) {
			sql.append(" AND ramp.finyr_id = "+finyear+"");
		}
		if(userId.equalsIgnoreCase("admin") && !subcategory.equalsIgnoreCase("All")){
			sql.append(" AND mw.work_desc_id = "+subcategory+"");
		}
		System.out.println(sql.toString());
		List<ReAllocatedProjectModel> Projects = jdbcTemplate.query(sql.toString(), new Object[]{ }, new RowMapper<ReAllocatedProjectModel>(){
			@Override
			public ReAllocatedProjectModel mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				ReAllocatedProjectModel project=new ReAllocatedProjectModel();
					project.setProject_id(rs.getLong("project_id"));
					project.setFrom_school_id(rs.getLong("from_school_id"));
					project.setFrom_school(rs.getString("from_school"));
					project.setTo_school_id(rs.getLong("to_school_id"));
					project.setTo_school(rs.getString("to_school"));
					project.setYear_desc(rs.getString("year_desc"));
					project.setDistrict_name(rs.getString("district_name"));
					project.setBlock_name(rs.getString("block_name"));
					project.setWork_desc(rs.getString("work_desc"));
					
					return project;
			}
			
		});
		return Projects;
	}

	@Override
	public List<CategoryModel> getCategoryDetails() {
		
		String sql = adminQueries.getProperty("mst.getCategoryListSQL");
		List<CategoryModel> categories = jdbcTemplate.query(sql, new RowMapper<CategoryModel>() {
			@Override
			public CategoryModel mapRow(ResultSet rs, int rownumber) throws SQLException {
				CategoryModel cat = new CategoryModel();
				cat.setWorkcatg_id(rs.getLong("workcatg_id"));
				cat.setCatg_desc(rs.getString("catg_desc"));
				return cat;
			}
		});
		return categories;
	}

	@Override
	public List<MasterSubcategoryModel> getAllSubcategoryFormMasterWorkDesc() {
		
		String sql = adminQueries.getProperty("mst.getAllSubcategoryListSQL");
		List<MasterSubcategoryModel> subcategories = jdbcTemplate.query(sql, new RowMapper<MasterSubcategoryModel>() {
			@Override
			public MasterSubcategoryModel mapRow(ResultSet rs, int rownumber) throws SQLException {
				MasterSubcategoryModel subcat = new MasterSubcategoryModel();
				subcat.setWorkcatg_id(rs.getLong("workcatg_id"));
				subcat.setWork_desc_id(rs.getLong("work_desc_id"));
				subcat.setWork_desc(rs.getString("work_desc"));
				return subcat;
			}
		});
		return subcategories;
	}

	@Override
	public List<MasterSubcategoryModel> getAllSubCategoryWorkDetailsFromMasterWork() {
		
		String sql = adminQueries.getProperty("mst.getAllSubcategoryListFromMasterWorkSQL");
		List<MasterSubcategoryModel> subcategories = jdbcTemplate.query(sql, new RowMapper<MasterSubcategoryModel>() {
			@Override
			public MasterSubcategoryModel mapRow(ResultSet rs, int rownumber) throws SQLException {
				MasterSubcategoryModel subcat = new MasterSubcategoryModel();
				subcat.setWork_id(rs.getLong("work_id"));
				subcat.setCatg_desc(rs.getString("catg_desc"));
				subcat.setWork_desc_id(rs.getLong("work_desc_id"));
				subcat.setWork_desc(rs.getString("work_desc"));
				subcat.setFinyr_id(rs.getLong("year_id"));
				subcat.setCost_pu(rs.getDouble("cost_pu"));
				subcat.setWorkcatg_id(rs.getLong("workcatg_id"));
				subcat.setYear_desc(rs.getString("year_desc"));
				return subcat;
			}
		});
		return subcategories;
	}

	@Override
	public boolean saveSubcategoryDetailsToMasterWork(MasterSubcategoryModel subcategory, String userId) {
		int record_status = 1;
		String sql = adminQueries.getProperty("mst.saveSubCategoryDetailsSQL");		
		jdbcTemplate.update(sql, new Object[] {
				subcategory.getWorkcatg_id(),
				subcategory.getWork_desc(),
				subcategory.getCost_pu(),
				userId,
				record_status,
				subcategory.getWork_desc_id(),
				subcategory.getFinyr_id(),
		});		
		return true;
	}

	@Override
	public boolean updateSubcategoryDetails(MasterSubcategoryModel subcategory) {
		String sql = adminQueries.getProperty("mst.updateSubcategoryDetailsSql");
		jdbcTemplate.update(sql, new Object[] { 
				subcategory.getWorkcatg_id(),
				subcategory.getWork_desc(),
				subcategory.getCost_pu(),
				subcategory.getWork_desc_id(),
				subcategory.getFinyr_id(),
				subcategory.getWork_id()
		});		
		return true;
	}

	@Override
	public boolean deleteSubcategoryDetails(long work_id) {
		String sql = adminQueries.getProperty("mst.deleteSubcategoryDetailsSQL");
		jdbcTemplate.update(sql, new Object[] {	0, work_id });		
		return true;
	}

	@Override
	public boolean saveSubcategoryDescriptionToMasterWorkDesc(MasterSubcategoryModel subcategory) {
		String sql = adminQueries.getProperty("mst.saveSubCategoryDescriptionDetailsSQL");		
		jdbcTemplate.update(sql, new Object[] {
				subcategory.getWorkcatg_id(),
				subcategory.getWork_desc(),
		});		
		return true;
	}

	@Override
	public boolean deleteSubcategoryDescDetails(MasterSubcategoryModel subcategory) {
		String sql = adminQueries.getProperty("mst.deleteSubcategoryDescDetailsSql");
		jdbcTemplate.update(sql, new Object[] { 
				subcategory.getWork_desc_id(),
		});		
		return true;
	}
	
}
