package com.stl.moschool.masterSetup.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.stl.moschool.masterSetup.dao.MasterSetupDao;
import com.stl.moschool.masterSetup.model.BlockModel;
import com.stl.moschool.masterSetup.model.CategoryModel;
import com.stl.moschool.masterSetup.model.ClusterModel;
import com.stl.moschool.masterSetup.model.DistrictModel;
import com.stl.moschool.masterSetup.model.MapModel;
import com.stl.moschool.masterSetup.model.MasterSchoolModel;
import com.stl.moschool.masterSetup.model.MasterSubcategoryModel;
import com.stl.moschool.masterSetup.model.ReAllocatedProjectModel;
import com.stl.moschool.masterSetup.model.SchemeModel;
import com.stl.moschool.masterSetup.model.SurveyModel;
import com.stl.moschool.masterSetup.model.YearModel;
import com.stl.moschool.masterSetup.service.MasterSetupService;

@Service("masterSetupService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)	
public class MasterSetupServiceImpl implements MasterSetupService{
	@Autowired
	private MasterSetupDao masterSetupDo;

	@Override
	public boolean saveSchoolDetails(MasterSchoolModel school) {
		return masterSetupDo.saveSchoolDetails(school);
	}

	@Override
	public List<MasterSchoolModel> getSchoolDetails() {
		return masterSetupDo.getSchoolDetails();
	}

	@Override
	public List<DistrictModel> getDistrictDetails(Map<String, Object> groupIdLocationId) {
		return masterSetupDo.getDistrictDetails(groupIdLocationId);
	}


	@Override
	public List<BlockModel> getBlockDetails(Map<String, Object> groupIdLocationId) {
		return masterSetupDo.getBlockDetails(groupIdLocationId);
	}

	@Override
	public List<ClusterModel> getClusterDetails() {
		return masterSetupDo.getClusterDetails();
	}

	@Override
	public boolean saveClusterDetails(ClusterModel user) {
		user.setRecord_status(1);
		return masterSetupDo.saveClusterDetails(user);
	}

	@Override
	public boolean saveBlockDetails(BlockModel user) {
		user.setRecord_status(1);
		return masterSetupDo.saveBlockDetails(user);
	}


	@Override
	public boolean saveDistrictDetails(DistrictModel user) {
		user.setRecord_status(1);
		return masterSetupDo.saveDistrictDetails(user);
	}

	@Override
	public List<MapModel> getBlockDetailsByDistrictID(long district,Map<String, Object> groupIdLocationId) {
		return masterSetupDo.getBlockDetailsByDistrictID(district, groupIdLocationId);
	}

	@Override
	public List<SurveyModel> getSurveyDetails() {
		return masterSetupDo.getSurveyDetails();
	}

	@Override
	public boolean updateSchoolDetails(MasterSchoolModel school) {
		return masterSetupDo.updateSchoolDetails(school);
	}

	@Override
	public boolean deleteSchool(long school_id) {
		return masterSetupDo.deleteSchool(school_id);
	}

	@Override
	public boolean updateDistrictDetails(DistrictModel district) {
		return masterSetupDo.updateDistrictDetails(district);
	}

	@Override
	public boolean deleteDistrict(int district_id) {
		return masterSetupDo.deleteDistrict(district_id);
	}

	@Override
	public boolean updateBlockDetails(BlockModel block) {
		return masterSetupDo.updateBlockDetails(block);
	}

	@Override
	public boolean deleteBlock(long block_id) {
		return masterSetupDo.deleteBlock(block_id);
	}

	@Override
	public List<YearModel> getYearDetails() {
		return masterSetupDo.getYearDetails();
	}

	@Override
	public boolean saveYearDetails(YearModel year) {
		return masterSetupDo.saveYearDetails(year);
	}

	@Override
	public boolean updateYearDetails(YearModel year) {
		return masterSetupDo.updateYearDetails(year);
	}

	@Override
	public boolean deleteYear(long year_id) {
		return masterSetupDo.deleteYear(year_id);
	}

	@Override
	public List<SchemeModel> getSchemeDetails() {
		return masterSetupDo.getSchemeDetails();
	}

	@Override
	public boolean saveSchemeDetails(SchemeModel scheme) {
		return masterSetupDo.saveSchemeDetails(scheme);
	}

	@Override
	public boolean updateSchemeDetails(SchemeModel scheme) {
		return masterSetupDo.updateSchemeDetails(scheme);
	}

	@Override
	public boolean deleteScheme(long scheme_id) {
		return masterSetupDo.deleteScheme(scheme_id);
	}

	@Override
	public boolean deleteReleasedAmount(ReAllocatedProjectModel project) {
		return masterSetupDo.deleteReleasedAmount(project);
	}

	@Override
	public List<ReAllocatedProjectModel> getReAllocatedProjectDetails(String userId, String block, String finyear, String district, String subcategory) {
		return masterSetupDo.getReAllocatedProjectDetails(userId, block, finyear, district, subcategory);
	}

	@Override
	public List<CategoryModel> getCategoryDetails() {
		return masterSetupDo.getCategoryDetails();
	}

	@Override
	public List<MasterSubcategoryModel> getAllSubcategoryFormMasterWorkDesc() {
		return masterSetupDo.getAllSubcategoryFormMasterWorkDesc();
	}

	@Override
	public List<MasterSubcategoryModel> getAllSubCategoryWorkDetailsFromMasterWork() {
		return masterSetupDo.getAllSubCategoryWorkDetailsFromMasterWork();
	}

	@Override
	public boolean saveSubcategoryDetailsToMasterWork(MasterSubcategoryModel subcategory, String userId) {
		return masterSetupDo.saveSubcategoryDetailsToMasterWork(subcategory, userId);
	}

	@Override
	public boolean updateSubcategoryDetails(MasterSubcategoryModel subcategory) {
		return masterSetupDo.updateSubcategoryDetails(subcategory);
	}

	@Override
	public boolean deleteSubcategoryDetails(long work_id) {
		return masterSetupDo.deleteSubcategoryDetails(work_id);
	}

	@Override
	public boolean saveSubcategoryDescriptionToMasterWorkDesc(MasterSubcategoryModel subcategory) {
		return masterSetupDo.saveSubcategoryDescriptionToMasterWorkDesc(subcategory);
	}

	@Override
	public boolean deleteSubcategoryDescDetails(MasterSubcategoryModel subcategory) {
		return masterSetupDo.deleteSubcategoryDescDetails(subcategory);
		}

}


	


