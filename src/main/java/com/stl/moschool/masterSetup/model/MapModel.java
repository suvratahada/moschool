package com.stl.moschool.masterSetup.model;

public class MapModel {
	private long id;
	
	private String name;
	
	private String officer;
	
	private long officerId;
	
	private String officerLogin;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public String getOfficer() {
		return officer;
	}

	public void setOfficer(String officer) {
		this.officer = officer;
	}

	public long getOfficerId() {
		return officerId;
	}

	public void setOfficerId(long officerId) {
		this.officerId = officerId;
	}

	public String getOfficerLogin() {
		return officerLogin;
	}

	public void setOfficerLogin(String officerLogin) {
		this.officerLogin = officerLogin;
	}

	@Override
	public String toString() {
		return "MapModel [id=" + id + ", name=" + name + ", officer=" + officer
				+ ", officerId=" + officerId + ", officerLogin=" + officerLogin
				+ "]";
	}


	

}
