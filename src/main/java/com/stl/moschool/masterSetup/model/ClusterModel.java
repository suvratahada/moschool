package com.stl.moschool.masterSetup.model;

import java.sql.Timestamp;


public class ClusterModel {

	private long cluster_id;
	
	private long block_id;
	
	private String cluster_name;
	
	private String created_by;
	
	private Timestamp created_on;
	
	private String modified_by;
	
	private Timestamp modified_on;
	
	private int record_status;
	
	private Timestamp lastmodified;

	public long getCluster_id() {
		return cluster_id;
	}

	public void setCluster_id(long cluster_id) {
		this.cluster_id = cluster_id;
	}

	public long getBlock_id() {
		return block_id;
	}

	public void setBlock_id(long block_id) {
		this.block_id = block_id;
	}

	public String getCluster_name() {
		return cluster_name;
	}

	public void setCluster_name(String cluster_name) {
		this.cluster_name = cluster_name;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public Timestamp getCreated_on() {
		return created_on;
	}

	public void setCreated_on(Timestamp created_on) {
		this.created_on = created_on;
	}

	public String getModified_by() {
		return modified_by;
	}

	public void setModified_by(String modified_by) {
		this.modified_by = modified_by;
	}

	public Timestamp getModified_on() {
		return modified_on;
	}

	public void setModified_on(Timestamp modified_on) {
		this.modified_on = modified_on;
	}

	public int getRecord_status() {
		return record_status;
	}

	public void setRecord_status(int record_status) {
		this.record_status = record_status;
	}

	public Timestamp getLastmodified() {
		return lastmodified;
	}

	public void setLastmodified(Timestamp lastmodified) {
		this.lastmodified = lastmodified;
	}

	@Override
	public String toString() {
		return "ClusterModel [cluster_id=" + cluster_id + ", block_id="
				+ block_id + ", cluster_name=" + cluster_name + ", created_by="
				+ created_by + ", created_on=" + created_on + ", modified_by="
				+ modified_by + ", modified_on=" + modified_on
				+ ", record_status=" + record_status + ", lastmodified="
				+ lastmodified + "]";
	}
	

	
	
}
