package com.stl.moschool.masterSetup.model;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name="block_master", schema="mst")
public class BlockModel {
	

	 private long block_id;
	 
	 private long district_id;
	 
	 private String block_name;
	 
	 private String created_by;
	 
	 private Timestamp created_on;
	 
	 private String modified_by;
	 
	 private Timestamp modified_on;
	 
	 private int record_status;
	 
	 private Timestamp lastmodified;

	public long getBlock_id() {
		return block_id;
	}

	public void setBlock_id(long block_id) {
		this.block_id = block_id;
	}

	public long getDistrict_id() {
		return district_id;
	}

	public void setDistrict_id(long district_id) {
		this.district_id = district_id;
	}

	public String getBlock_name() {
		return block_name;
	}

	public void setBlock_name(String block_name) {
		this.block_name = block_name;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public Timestamp getCreated_on() {
		return created_on;
	}

	public void setCreated_on(Timestamp created_on) {
		this.created_on = created_on;
	}

	public String getModified_by() {
		return modified_by;
	}

	public void setModified_by(String modified_by) {
		this.modified_by = modified_by;
	}

	public Timestamp getModified_on() {
		return modified_on;
	}

	public void setModified_on(Timestamp modified_on) {
		this.modified_on = modified_on;
	}

	public int getRecord_status() {
		return record_status;
	}

	public void setRecord_status(int record_status) {
		this.record_status = record_status;
	}

	public Timestamp getLastmodified() {
		return lastmodified;
	}

	public void setLastmodified(Timestamp lastmodified) {
		this.lastmodified = lastmodified;
	}

	@Override
	public String toString() {
		return "BlockModel [block_id=" + block_id + ", district_id="
				+ district_id + ", block_name=" + block_name + ", created_by="
				+ created_by + ", created_on=" + created_on + ", modified_by="
				+ modified_by + ", modified_on=" + modified_on
				+ ", record_status=" + record_status + ", lastmodified="
				+ lastmodified + "]";
	}

	 
	 
}
