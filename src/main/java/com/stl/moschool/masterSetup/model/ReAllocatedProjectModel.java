package com.stl.moschool.masterSetup.model;

public class ReAllocatedProjectModel {
	
	private long project_id;
	private long from_school_id;
	private String from_school;
	private long to_school_id;
	private String to_school;
	private String year_desc;
	private String district_name;
	private String block_name;
	private String work_desc;
	public long getProject_id() {
		return project_id;
	}
	public void setProject_id(long project_id) {
		this.project_id = project_id;
	}
	public long getFrom_school_id() {
		return from_school_id;
	}
	public void setFrom_school_id(long from_school_id) {
		this.from_school_id = from_school_id;
	}
	public String getFrom_school() {
		return from_school;
	}
	public void setFrom_school(String from_school) {
		this.from_school = from_school;
	}
	public long getTo_school_id() {
		return to_school_id;
	}
	public void setTo_school_id(long to_school_id) {
		this.to_school_id = to_school_id;
	}
	public String getTo_school() {
		return to_school;
	}
	public void setTo_school(String to_school) {
		this.to_school = to_school;
	}
	public String getYear_desc() {
		return year_desc;
	}
	public void setYear_desc(String year_desc) {
		this.year_desc = year_desc;
	}
	public String getDistrict_name() {
		return district_name;
	}
	public void setDistrict_name(String district_name) {
		this.district_name = district_name;
	}
	public String getBlock_name() {
		return block_name;
	}
	public void setBlock_name(String block_name) {
		this.block_name = block_name;
	}
	public String getWork_desc() {
		return work_desc;
	}
	public void setWork_desc(String work_desc) {
		this.work_desc = work_desc;
	}
	@Override
	public String toString() {
		return "ReAllocatedProjectModel [project_id=" + project_id
				+ ", from_school_id=" + from_school_id + ", from_school="
				+ from_school + ", to_school_id=" + to_school_id
				+ ", to_school=" + to_school + ", year_desc=" + year_desc
				+ ", district_name=" + district_name + ", block_name="
				+ block_name + ", work_desc=" + work_desc + "]";
	}
	
}
