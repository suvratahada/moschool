package com.stl.moschool.masterSetup.model;

public class SchemeModel {
	private long scheme_id;
	private String scheme;
	private String scheme_desc;
	
	public long getScheme_id() {
		return scheme_id;
	}
	public void setScheme_id(long scheme_id) {
		this.scheme_id = scheme_id;
	}
	public String getScheme() {
		return scheme;
	}
	public void setScheme(String scheme) {
		this.scheme = scheme;
	}
	public String getScheme_desc() {
		return scheme_desc;
	}
	public void setScheme_desc(String scheme_desc) {
		this.scheme_desc = scheme_desc;
	}
	@Override
	public String toString() {
		return "SchemeModel [scheme_id=" + scheme_id + ", scheme=" + scheme
				+ ", scheme_desc=" + scheme_desc + "]";
	}
	
	
}
