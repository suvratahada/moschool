package com.stl.moschool.masterSetup.model;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name="master_district", schema="public")
public class DistrictModel {

	private int district_id;
	
	private String  district_name;
	
	private int  record_status;
	
	private long  created_by;
	
	private Timestamp created_on;
	
	private long modified_by;
	
	private Timestamp modified_on;

	private Timestamp lastmodified;
	
	public int getDistrict_id() {
		return district_id;
	}

	public void setDistrict_id(int district_id) {
		this.district_id = district_id;
	}

	public String getDistrict_name() {
		return district_name;
	}

	public void setDistrict_name(String district_name) {
		this.district_name = district_name;
	}
	
	public int getRecord_status() {
		return record_status;
	}

	public void setRecord_status(int record_status) {
		this.record_status = record_status;
	}

	public long getCreated_by() {
		return created_by;
	}

	public void setCreated_by(long created_by) {
		this.created_by = created_by;
	}

	public Timestamp getCreated_on() {
		return created_on;
	}

	public void setCreated_on(Timestamp created_on) {
		this.created_on = created_on;
	}

	public long getModified_by() {
		return modified_by;
	}

	public void setModified_by(long modified_by) {
		this.modified_by = modified_by;
	}

	public Timestamp getModified_on() {
		return modified_on;
	}

	public void setModified_on(Timestamp modified_on) {
		this.modified_on = modified_on;
	}

	public Timestamp getLastmodified() {
		return lastmodified;
	}

	public void setLastmodified(Timestamp lastmodified) {
		this.lastmodified = lastmodified;
	}

	@Override
	public String toString() {
		return "DistrictModel [district_id=" + district_id + ", district_name="
				+ district_name + ", record_status=" + record_status
				+ ", created_by=" + created_by + ", created_on=" + created_on
				+ ", modified_by=" + modified_by + ", modified_on="
				+ modified_on + ", lastmodified=" + lastmodified + "]";
	}

	
	
	
}
