package com.stl.moschool.masterSetup.model;

public class MasterSubcategoryModel {

	  private long work_id; 
	  private long workcatg_id;
	  private long work_desc_id;
	  private String work_desc;
	  private Double cost_pu;
	  private long finyr_id;
	  private String catg_desc;
	  private String year_desc;
	public long getWork_id() {
		return work_id;
	}
	public void setWork_id(long work_id) {
		this.work_id = work_id;
	}
	public long getWorkcatg_id() {
		return workcatg_id;
	}
	public void setWorkcatg_id(long workcatg_id) {
		this.workcatg_id = workcatg_id;
	}
	public long getWork_desc_id() {
		return work_desc_id;
	}
	public void setWork_desc_id(long work_desc_id) {
		this.work_desc_id = work_desc_id;
	}
	public String getWork_desc() {
		return work_desc;
	}
	public void setWork_desc(String work_desc) {
		this.work_desc = work_desc;
	}
	public Double getCost_pu() {
		return cost_pu;
	}
	public void setCost_pu(Double cost_pu) {
		this.cost_pu = cost_pu;
	}
	public long getFinyr_id() {
		return finyr_id;
	}
	public void setFinyr_id(long finyr_id) {
		this.finyr_id = finyr_id;
	}
	public String getCatg_desc() {
		return catg_desc;
	}
	public void setCatg_desc(String catg_desc) {
		this.catg_desc = catg_desc;
	}
	public String getYear_desc() {
		return year_desc;
	}
	public void setYear_desc(String year_desc) {
		this.year_desc = year_desc;
	}
	@Override
	public String toString() {
		return "MasterSubcategoryModel [work_id=" + work_id + ", workcatg_id="
				+ workcatg_id + ", work_desc_id=" + work_desc_id
				+ ", work_desc=" + work_desc + ", cost_pu=" + cost_pu
				+ ", finyr_id=" + finyr_id + ", catg_desc=" + catg_desc
				+ ", year_desc=" + year_desc + "]";
	}
	  
}
