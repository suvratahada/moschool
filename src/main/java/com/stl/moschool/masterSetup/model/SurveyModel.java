package com.stl.moschool.masterSetup.model;

public class SurveyModel {
	
	private long survey_id;
	
	private String survey_name;

	public long getSurvey_id() {
		return survey_id;
	}

	public void setSurvey_id(long survey_id) {
		this.survey_id = survey_id;
	}

	public String getSurvey_name() {
		return survey_name;
	}

	public void setSurvey_name(String survey_name) {
		this.survey_name = survey_name;
	}

	@Override
	public String toString() {
		return "SurveyModel [survey_id=" + survey_id + ", survey_name="
				+ survey_name + "]";
	}
	
	

}
 