package com.stl.moschool.masterSetup.model;

public class SchoolMapModel {

	private long schoolId;
	
	private String schoolName;
	
	private String schoolCat;

	public long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(long schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getSchoolCat() {
		return schoolCat;
	}

	public void setSchoolCat(String schoolCat) {
		this.schoolCat = schoolCat;
	}

	@Override
	public String toString() {
		return "SchoolMapModel [schoolId=" + schoolId + ", schoolName="
				+ schoolName + ", schoolCat=" + schoolCat + "]";
	}
	
	
}
