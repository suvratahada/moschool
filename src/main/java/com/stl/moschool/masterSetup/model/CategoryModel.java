package com.stl.moschool.masterSetup.model;

public class CategoryModel {

	private long workcatg_id;
	private String catg_desc;
	
	public long getWorkcatg_id() {
		return workcatg_id;
	}
	public void setWorkcatg_id(long workcatg_id) {
		this.workcatg_id = workcatg_id;
	}
	public String getCatg_desc() {
		return catg_desc;
	}
	public void setCatg_desc(String catg_desc) {
		this.catg_desc = catg_desc;
	}
	@Override
	public String toString() {
		return "CategoryModel [workcatg_id=" + workcatg_id + ", catg_desc="
				+ catg_desc + "]";
	}

}
