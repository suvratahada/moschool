package com.stl.moschool.masterSetup.model;

public class YearModel {
	private long year_id;
	private String year_desc;
	public long getYear_id() {
		return year_id;
	}
	public void setYear_id(long year_id) {
		this.year_id = year_id;
	}
	public String getYear_desc() {
		return year_desc;
	}
	public void setYear_desc(String year_desc) {
		this.year_desc = year_desc;
	}
	@Override
	public String toString() {
		return "YearModel [year_id=" + year_id + ", year_desc=" + year_desc
				+ "]";
	}
	
}
