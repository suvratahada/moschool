package com.stl.moschool.masterSetup.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name="officer_master", schema="mst")
public class OfficerModel implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1762483492342690971L;
	
	@Pattern(regexp ="[0-9A-Za-z _]+")
	@Column(name="officer_id")
	private int officer_id;
	
	@Pattern(regexp="[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*")
	@Column(name="officer_name")
	private String officer_name;
	
	@Column(name="officer_designation")
	private String officer_designation;
	
	@Column(name="designation_group")
	private String designation_group;
	
	@Column(name="Officer_login")
	private String officer_loin;
	
	@Pattern(regexp="[0-9]")
	@Size(min=10, max=10)
	@Column(name="officer_mobile")
	private String officer_mobile;
	
	@Pattern(regexp="^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")
	@Column(name="officer_email")
	private String officer_email;

	public int getOfficer_id() {
		return officer_id;
	}

	public void setOfficer_id(int officer_id) {
		this.officer_id = officer_id;
	}

	public String getOfficer_name() {
		return officer_name;
	}

	public void setOfficer_name(String officer_name) {
		this.officer_name = officer_name;
	}

	public String getOfficer_designation() {
		return officer_designation;
	}

	public void setOfficer_designation(String officer_designation) {
		this.officer_designation = officer_designation;
	}

	public String getDesignation_group() {
		return designation_group;
	}

	public void setDesignation_group(String designation_group) {
		this.designation_group = designation_group;
	}

	public String getOfficer_loin() {
		return officer_loin;
	}

	public void setOfficer_loin(String officer_loin) {
		this.officer_loin = officer_loin;
	}

	public String getOfficer_mobile() {
		return officer_mobile;
	}

	public void setOfficer_mobile(String officer_mobile) {
		this.officer_mobile = officer_mobile;
	}

	public String getOfficer_email() {
		return officer_email;
	}

	public void setOfficer_email(String officer_email) {
		this.officer_email = officer_email;
	}

	@Override
	public String toString() {
		return "OfficerModel [officer_id=" + officer_id + ", officer_name=" + officer_name + ", officer_designation="
				+ officer_designation + ", designation_group=" + designation_group + ", officer_loin=" + officer_loin
				+ ", officer_mobile=" + officer_mobile + ", officer_email=" + officer_email + "]";
	}
	
	
	
}
