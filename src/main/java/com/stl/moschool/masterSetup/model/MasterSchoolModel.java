package com.stl.moschool.masterSetup.model;

public class MasterSchoolModel {

	private long school_id;
	private String school_name;
	private long block_id;
	private String block_name;
	private long record_status;
	public long getSchool_id() {
		return school_id;
	}
	public void setSchool_id(long school_id) {
		this.school_id = school_id;
	}
	public String getSchool_name() {
		return school_name;
	}
	public void setSchool_name(String school_name) {
		this.school_name = school_name;
	}
	public long getBlock_id() {
		return block_id;
	}
	public void setBlock_id(long block_id) {
		this.block_id = block_id;
	}
	public String getBlock_name() {
		return block_name;
	}
	public void setBlock_name(String block_name) {
		this.block_name = block_name;
	}
	public long getRecord_status() {
		return record_status;
	}
	public void setRecord_status(long record_status) {
		this.record_status = record_status;
	}
	@Override
	public String toString() {
		return "MasterSchoolModel [school_id=" + school_id + ", school_name="
				+ school_name + ", block_id=" + block_id + ", block_name="
				+ block_name + ", record_status=" + record_status + "]";
	}
	
	
}
