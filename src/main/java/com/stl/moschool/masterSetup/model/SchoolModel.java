package com.stl.moschool.masterSetup.model;

//import java.util.Date;

import javax.persistence.Column;
import javax.validation.constraints.Pattern;
//import javax.validation.constraints.Size;

public class SchoolModel implements java.io.Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1762483492342690971L;

	
	@Pattern(regexp ="[0-9A-Za-z _]+")
	@Column(name="id")
	private int id;
	
	@Column(name="lower_class")
	private int lower_class;
	
	@Column(name="higher_class")
	private int higher_class;
	
	@Column(name="rur_urb")
	private int rur_urb;
	
	@Column(name="rururb_1")
	private String rururb_1;
	
	@Column(name="school_type")
	private String school_type;
	
	@Column(name="pincd")
	private int pincd;
	
	@Column(name="est_year")
	private int est_year;
	
	@Column(name="contact_no")
	private String contact_no;
	
	@Column(name="init_year")
	private int init_year;

	public int getid() {
		return id;
	}

	public void setid(int id) {
		this.id = id;
	}

	public int getlower_class() {
		return lower_class;
	}

	public void setlower_class(int lower_class) {
		this.lower_class = lower_class;
	}

	public int gethigher_class() {
		return higher_class;
	}

	public void sethigher_class(int higher_class) {
		this.higher_class = higher_class;
	}

	public int getRur_urb() {
		return rur_urb;
	}

	public void setRur_urb(int rur_urb) {
		this.rur_urb = rur_urb;
	}

	public String getrururb_1() {
		return rururb_1;
	}

	public void setrururb_1(String rururb_1) {
		this.rururb_1 = rururb_1;
	}

	public String getschool_type() {
		return school_type;
	}

	public void setschool_type(String school_type) {
		this.school_type = school_type;
	}

	public int getPincd() {
		return pincd;
	}

	public void setPincd(int pincd) {
		this.pincd = pincd;
	}

	public int getEst_year() {
		return est_year;
	}

	public void setEst_year(int est_year) {
		this.est_year = est_year;
	}

	public String getContact_no() {
		return contact_no;
	}

	public void setContact_no(String contact_no) {
		this.contact_no = contact_no;
	}

	public int getInit_year() {
		return init_year;
	}

	public void setInit_year(int init_year) {
		this.init_year = init_year;
	}

	@Override
	public String toString() {
		return "SchoolModel [id=" + id + ", lower_class=" + lower_class + ", higher_class=" + higher_class
				+ ", rur_urb=" + rur_urb + ", rururb_1=" + rururb_1 + ", school_type=" + school_type + ", pincd="
				+ pincd + ", est_year=" + est_year + ", contact_no=" + contact_no + ", init_year=" + init_year + "]";
	}	
	
}
