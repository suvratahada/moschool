package com.stl.moschool.masterSetup.model;

import java.sql.Date;
import java.util.List;

public class OfficerSchoolMappingModel {

	private String officerType;
	private long districtId;
	private long blockId;
	private long officeId;
	private long surveyId;
	private List<Long> schoolIds;
	private String startDate;
	private String endDate;
	
	public String getOfficerType() {
		return officerType;
	}
	public void setOfficerType(String officerType) {
		this.officerType = officerType;
	}
	public long getDistrictId() {
		return districtId;
	}
	public void setDistrictId(long districtId) {
		this.districtId = districtId;
	}
	public long getBlockId() {
		return blockId;
	}
	public void setBlockId(long blockId) {
		this.blockId = blockId;
	}
	public long getOfficeId() {
		return officeId;
	}
	public void setOfficeId(long officeId) {
		this.officeId = officeId;
	}
	public long getSurveyId() {
		return surveyId;
	}
	public void setSurveyId(long surveyId) {
		this.surveyId = surveyId;
	}
	public List<Long> getSchoolIds() {
		return schoolIds;
	}
	public void setSchoolIds(List<Long> schoolIds) {
		this.schoolIds = schoolIds;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	@Override
	public String toString() {
		return "OfficerSchoolMappingModel [officerType=" + officerType
				+ ", districtId=" + districtId + ", blockId=" + blockId
				+ ", officeId=" + officeId + ", surveyId=" + surveyId
				+ ", schoolIds=" + schoolIds + ", startDate=" + startDate
				+ ", endDate=" + endDate + "]";
	}
	
}
