package com.stl.moschool.masterSetup.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
//import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stl.moschool.masterSetup.model.BlockModel;
import com.stl.moschool.masterSetup.model.CategoryModel;
import com.stl.moschool.masterSetup.model.ClusterModel;
import com.stl.moschool.masterSetup.model.DistrictModel;
import com.stl.moschool.masterSetup.model.MapModel;
import com.stl.moschool.masterSetup.model.MasterSchoolModel;
import com.stl.moschool.masterSetup.model.MasterSubcategoryModel;
import com.stl.moschool.masterSetup.model.ReAllocatedProjectModel;
import com.stl.moschool.masterSetup.model.SchemeModel;
import com.stl.moschool.masterSetup.model.SurveyModel;
import com.stl.moschool.masterSetup.model.YearModel;
import com.stl.moschool.masterSetup.service.MasterSetupService;


@Controller
@RequestMapping("/mastersetup")
public class MasterSetupController {
	
	@Autowired
	MasterSetupService masterSetupService;
	
	final static Logger logger = Logger.getLogger(MasterSetupController.class);
	
	@RequestMapping(value = { "/block" }, method = RequestMethod.GET)
	public String createBlock(HttpSession session) {
		logger.debug("redirecting to  Block Setup");
		System.out.println("redirecting to  Location");
		session.setAttribute("sessionDashboardTab", "styleActiveMST");
		return "createBlock";
	}

	@RequestMapping(value = { "/district" }, method = RequestMethod.GET)
	public String createDistrict(HttpSession session) {
		logger.debug("redirecting to  district Setup");
		System.out.println("redirecting to  district");
		session.setAttribute("sessionDashboardTab", "styleActiveMST");
		return "createDistrict";
	}
	
	@RequestMapping(value = { "/school" }, method = RequestMethod.GET)
	public String createSchool(HttpSession session) {
		logger.debug("redirecting to  school Setup");
		System.out.println("redirecting to  school Setup");
		session.setAttribute("sessionDashboardTab", "styleActiveMST");
		return "createSchool";
	}
	
	@RequestMapping(value = { "/finyear" }, method = RequestMethod.GET)
	public String createFinancialYear(HttpSession session) {
		logger.debug("redirecting to  Finyear Setup");
		System.out.println("redirecting to  Finyear Mapping");
		session.setAttribute("sessionDashboardTab", "styleActiveMST");
		return "createFinYear";
	}
	
	@RequestMapping(value = { "/scheme" }, method = RequestMethod.GET)
	public String createSchemeView(HttpSession session) {
		logger.debug("redirecting to  Scheme Setup");
		System.out.println("redirecting to  scheme Mapping");
		session.setAttribute("sessionDashboardTab", "styleActiveMST");
		return "createScheme";
	}
	
	
	@RequestMapping(value = { "/subcategory" }, method = RequestMethod.GET)
	public String createSubCategoryView(HttpSession session) {
		logger.debug("redirecting to  Subcategory Setup");
		System.out.println("redirecting to  Subcategory Mapping");
		session.setAttribute("sessionDashboardTab", "styleActiveMST");
		return "createSubcategorySetup";
	}
	
	@RequestMapping(value = { "/workdesc" }, method = RequestMethod.GET)
	public String createWorkdescView(HttpSession session) {
		logger.debug("redirecting to  Subcategory Setup");
		System.out.println("redirecting to  Work Desc Mapping");
		session.setAttribute("sessionDashboardTab", "styleActiveMST");
		return "createWorkdescSetup";
	}
	
	/*****************************************************
	 * Method to Save SchoolDetails
	*******************************************************/
	@RequestMapping(value={"/saveSchoolDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> saveSchoolDetails( @RequestBody /*@Valid */MasterSchoolModel school, BindingResult bresult) {
		System.out.println("Inside:: @@ Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
	
		logger.debug(school.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
		}else{
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = masterSetupService.saveSchoolDetails(school);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully saved school details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to save school details");
			}
		}
		
		return resp;
	}
	
	/*****************************************************
	 * Method to Get School Details
	*******************************************************/
	@RequestMapping(value={"/getSchoolDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getSchoolDetails(HttpServletRequest request, HttpServletResponse response) {
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		List <MasterSchoolModel> lstSchoolDetails = masterSetupService.getSchoolDetails();
		
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", lstSchoolDetails);
		
		return resp;
	}
	/*****************************************************
	 * Method to Update SchoolDetails
	*******************************************************/
	@RequestMapping(value={"/updateSchoolDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> updateSchoolDetails(@RequestBody MasterSchoolModel school, BindingResult bresult) {
		System.out.println("Inside:: @@ Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		System.out.println(school.getSchool_id());
		System.out.println(school.getBlock_id());
		System.out.println(school.getSchool_name());
		logger.debug(school.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
			
		}else{
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = masterSetupService.updateSchoolDetails(school);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully Updated school details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to Update school details");
			}
		}
		
		return resp;
	}

	/*****************************************************
	 * Method to Delete SchoolDetails
	*******************************************************/
	@RequestMapping(value={"/deleteSchool"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteSchool(@RequestBody MasterSchoolModel school, BindingResult bresult) {
		System.out.println("Inside:: @@ Delete Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		System.out.println("School id"+school.getSchool_id());
		logger.debug(school.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
			
		}else{
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = masterSetupService.deleteSchool(school.getSchool_id());
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully Deleted school details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to Delete school details");
			}
		}
		
		return resp;
	}
	
	@RequestMapping(value={"/getDistrictDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getDistrictDetails(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		
		System.out.println("Inside District Controller");
		Map<String, Object> resp = new HashMap<String, Object>();
		
		//Used for Role Base Resource Controller by Vamsi Krish
		Map<String, Object> groupIdLocationId = (Map<String, Object>) session.getAttribute("groupIdLocationId");
				
		List <DistrictModel> lstDistrictDetails = masterSetupService.getDistrictDetails(groupIdLocationId);
		
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", lstDistrictDetails);
		
		return resp;
	}
	
	
	
	@RequestMapping(value={"/saveDistrictDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> saveUserDetails( @RequestBody DistrictModel user, BindingResult bresult) { //@ModelAttribute UserModel user,
		System.out.println("Inside:: @@ Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		System.out.println(user.toString());
	
		logger.debug(user.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = masterSetupService.saveDistrictDetails(user);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully Saved District details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to Save District details!!");
			}
		}
		
		return resp;
	}
	
	@RequestMapping(value={"/getBlockDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getBlockDetails(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		
		System.out.println("Inside Block Controller");
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		//Used for Role Base Resource Controller by Vamsi Krish
		Map<String, Object> groupIdLocationId = (Map<String, Object>) session.getAttribute("groupIdLocationId");
				
		List <BlockModel> lstBlockDetails = masterSetupService.getBlockDetails(groupIdLocationId);
		
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", lstBlockDetails);
		
		return resp;
	}
	
	@RequestMapping(value = { "/getBlockDetailsByDistrictID/{district}" }, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getBlockDetailsByDistrictID(@PathVariable("district") long district, HttpSession session,HttpServletRequest request, HttpServletResponse response) {
		
		System.out.println("Inside Map Block Controller");
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		//Used for Role Base Resource Controller by Vamsi Krish
		Map<String, Object> groupIdLocationId = (Map<String, Object>) session.getAttribute("groupIdLocationId");
				
		List <MapModel> lstBlockDetails = masterSetupService.getBlockDetailsByDistrictID(district,groupIdLocationId);
		
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", lstBlockDetails);
		
		return resp;
		
	}
	
	@RequestMapping(value={"/getSurveyDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getSurveyDetails(HttpServletRequest request, HttpServletResponse response) {
		
		System.out.println("Inside District Controller");
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		List <SurveyModel> lstSurveyDetails = masterSetupService.getSurveyDetails();
		
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", lstSurveyDetails);
		
		return resp;
	}
	
	
	@RequestMapping(value={"/saveBlockDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> saveBlockDetails( @RequestBody BlockModel user, BindingResult bresult) { //@ModelAttribute UserModel user,
		System.out.println("Inside:: @@ Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		System.out.println(user.toString());
		logger.debug(user.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
			
		}else{
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = masterSetupService.saveBlockDetails(user);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully saved Block details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to save Block details");
			}
		}
		
		return resp;
	}
	
	@RequestMapping(value={"/getClusterDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getClusterDetails(HttpServletRequest request, HttpServletResponse response) {
		
		System.out.println("Inside Cluster Controller");
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		List <ClusterModel> lstClusterDetails = masterSetupService.getClusterDetails();
		
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", lstClusterDetails);
		
		return resp;
	}
	
	
	@RequestMapping(value={"/saveClusterDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> saveClusterDetails( @RequestBody ClusterModel user, BindingResult bresult) { //@ModelAttribute UserModel user,
		System.out.println("Inside:: @@ Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		System.out.println(user.toString());
		logger.debug(user.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
			
		}else{
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = masterSetupService.saveClusterDetails(user);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully saved user details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to save user details");
			}
		}
		
		return resp;
	}
	
	
	/*****************************************************
	 * Method to Update District Details
	*******************************************************/
	@RequestMapping(value={"/updateDistrictDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object>updateDistrictDetails(@RequestBody DistrictModel district, BindingResult bresult) {
		System.out.println("Inside:: @@ Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		logger.debug(district.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = masterSetupService.updateDistrictDetails(district);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully Updated District details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to Update District details");
			}
		}
		
		return resp;
	}
	

	/*****************************************************
	 * Method to Delete SchoolDetails
	*******************************************************/
	@RequestMapping(value={"/deleteDistrict"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteDistrict(@RequestBody DistrictModel district, BindingResult bresult) {
		System.out.println("Inside:: @@ Delete Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		System.out.println("District id"+district.getDistrict_id());
		logger.debug(district.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = masterSetupService.deleteDistrict(district.getDistrict_id());
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully Deleted District details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to Delete District details");
			}
		}
		
		return resp;
	}
	
	/*****************************************************
	 * Method to Update Block Details
	*******************************************************/
	@RequestMapping(value={"/updateBlockDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> updateBlockDetails(@RequestBody BlockModel block, BindingResult bresult) {
		System.out.println("Inside:: @@ Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		logger.debug(block.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = masterSetupService.updateBlockDetails(block);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully Updated block details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to Update block details");
			}
		}
		
		return resp;
	}
	

	/*****************************************************
	 * Method to Delete Block Details
	*******************************************************/
	@RequestMapping(value={"/deleteBlock"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteBlock(@RequestBody BlockModel block, BindingResult bresult) {
		System.out.println("Inside:: @@ Delete Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		System.out.println("Block id"+block.getBlock_id());
		logger.debug(block.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = masterSetupService.deleteBlock(block.getBlock_id());
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully Deleted Block details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to Delete Block details");
			}
		}
		
		return resp;
	}
	
	
	@RequestMapping(value={"/getYearDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getYearDetails(HttpServletRequest request, HttpServletResponse response) {
		
		System.out.println("Inside Year Controller");
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		List <YearModel> lstYearDetails = masterSetupService.getYearDetails();
		
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", lstYearDetails);
		
		return resp;
	}
	

	@RequestMapping(value={"/saveYearDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> saveYearDetails( @RequestBody YearModel year, BindingResult bresult) {
		System.out.println("Inside:: @@ Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		System.out.println(year.toString());
		logger.debug(year.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
			
		}else{
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = masterSetupService.saveYearDetails(year);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully saved Financial Year");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to save Financial Year details");
			}
		}
		
		return resp;
	}
	
	
	/*****************************************************
	 * Method to Update Year Details
	*******************************************************/
	@RequestMapping(value={"/updateYearDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> updateYearDetails(@RequestBody YearModel year, BindingResult bresult) {
		System.out.println("Inside:: @@ Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		logger.debug(year.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = masterSetupService.updateYearDetails(year);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully Updated Financial Year details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to Update Financial Year details");
			}
		}
		
		return resp;
	}
	
	
	/*****************************************************
	 * Method to Delete Year Details
	*******************************************************/
	@RequestMapping(value={"/deleteYear"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteYear(@RequestBody YearModel year, BindingResult bresult) {
		System.out.println("Inside:: @@ Delete Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		System.out.println("Block id"+year.getYear_id());
		logger.debug(year.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
//			return result.toString();
			
		}else{
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = masterSetupService.deleteYear(year.getYear_id());
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully Deleted Financial Year details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to Delete Financial Year details");
			}
		}
		
		return resp;
	}
	
	@RequestMapping(value={"/getSchemeDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getSchemeDetails(HttpServletRequest request, HttpServletResponse response) {
		
		System.out.println("Inside Scheme Controller");
		
		Map<String, Object> resp = new HashMap<String, Object>();
		
		List <SchemeModel> lstSchemeDetails = masterSetupService.getSchemeDetails();
		
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", lstSchemeDetails);
		
		return resp;
	}
	
	
	@RequestMapping(value={"/saveSchemeDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> saveSchemeDetails( @RequestBody SchemeModel scheme, BindingResult bresult) {
		System.out.println("Inside:: @@ Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		System.out.println(scheme.toString());
		logger.debug(scheme.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
			
		}else{
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = masterSetupService.saveSchemeDetails(scheme);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Scheme details successfully saved.");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to save Scheme details");
			}
		}
		
		return resp;
	}
	
	/*****************************************************
	 * Method to Update Scheme Details
	*******************************************************/
	@RequestMapping(value={"/updateSchemeDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> updateSchemeDetails(@RequestBody SchemeModel scheme, BindingResult bresult) {
		System.out.println("Inside:: @@ Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		logger.debug(scheme.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
			
		}else{
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = masterSetupService.updateSchemeDetails(scheme);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully Updated Scheme details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to Update Scheme details");
			}
		}
		
		return resp;
	}
	

	/*****************************************************
	 * Method to Delete Scheme Details
	*******************************************************/
	@RequestMapping(value={"/deleteScheme"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteScheme(@RequestBody SchemeModel scheme, BindingResult bresult) {
		System.out.println("Inside:: @@ Delete Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		logger.debug(scheme.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
			
		}else{
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = masterSetupService.deleteScheme(scheme.getScheme_id());
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully Deleted Scheme details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to Delete Scheme details");
			}
		}
		
		return resp;
	}
	
	
	/*Method for getting all Category*/
	@RequestMapping(value={"/getCategoryDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getCategoryDetails(HttpServletRequest request, HttpServletResponse response) {
		
		System.out.println("Inside Category Controller");
		Map<String, Object> resp = new HashMap<String, Object>();
		
		List <CategoryModel> lstCategoryDetails = masterSetupService.getCategoryDetails();
		System.out.println("Categories are:: "+lstCategoryDetails.toString());
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", lstCategoryDetails);
		
		return resp;
	}
	
	/*Method for Subcategory Master Setup*/
	@RequestMapping(value={"/getAllSubcategoryFormMasterWorkDesc"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getAllSubcategoryFormMasterWorkDesc(HttpServletRequest request, HttpServletResponse response) {
		
		System.out.println("Inside Category Controller");
		Map<String, Object> resp = new HashMap<String, Object>();
		
		List <MasterSubcategoryModel> lstCategoryDetails = masterSetupService.getAllSubcategoryFormMasterWorkDesc();
		System.out.println("Subcategories are:: "+lstCategoryDetails.toString());
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", lstCategoryDetails);
		
		return resp;
	}
	
	/*Method to get All data from master_work table*/
	
	@RequestMapping(value={"/getAllSubCategoryWorkDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> getAllSubCategoryWorkDetailsFromMasterWork(HttpServletRequest request, HttpServletResponse response) {
		
		System.out.println("Inside Category Controller");
		Map<String, Object> resp = new HashMap<String, Object>();
		
		List <MasterSubcategoryModel> lstCategoryDetails = masterSetupService.getAllSubCategoryWorkDetailsFromMasterWork();
		System.out.println("Subcategories are:: "+lstCategoryDetails.toString());
		resp.put("success", true);
		resp.put("msg", "Successfully received");
		resp.put("data", lstCategoryDetails);
		
		return resp;
	}
	
	/*Method to save subcategory details*/
	
	@RequestMapping(value={"/saveSubcategoryDetailsToMasterWork"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> saveSubcategoryDetailsToMasterWork( @RequestBody MasterSubcategoryModel subcategory, BindingResult bresult,HttpSession session) {
		System.out.println("Inside:: @@ Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		String userId=(String) session.getAttribute("userId");
		System.out.println("Save Data:: "+subcategory.toString());
		logger.debug(subcategory.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
			
		}else{
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = masterSetupService.saveSubcategoryDetailsToMasterWork(subcategory, userId);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully saved Subcategory details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to save Subcategory details");
			}
		}
		
		return resp;
	}
	
	@RequestMapping(value={"/updateSubcategoryDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> updateSubcategoryDetails( @RequestBody MasterSubcategoryModel subcategory, BindingResult bresult,HttpSession session) {
		System.out.println("Inside:: @@ Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		String userId=(String) session.getAttribute("userId");
		
		System.out.println("Update Model:: "+subcategory.toString());
		logger.debug(subcategory.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
			
		}else{
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = masterSetupService.updateSubcategoryDetails(subcategory);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully Updated Subcategory details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to Update Subcategory details");
			}
		}
		
		return resp;
	}
	
	@RequestMapping(value={"/deleteSubcategoryDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteSubcategoryDetails( @RequestBody MasterSubcategoryModel subcategory, BindingResult bresult,HttpSession session) {
		System.out.println("Inside:: @@ Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		String userId=(String) session.getAttribute("userId");
		long work_id = subcategory.getWork_id();
		logger.debug(subcategory.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
			
		}else{
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = masterSetupService.deleteSubcategoryDetails(work_id);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully Deleted Subcategory details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to Delete Subcategory details");
			}
		}
		
		return resp;
	}
		
	@RequestMapping(value={"/saveSubcategoryDescriptionToMasterWorkDesc"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> saveSubcategoryDescriptionToMasterWorkDesc( @RequestBody MasterSubcategoryModel subcategory, BindingResult bresult,HttpSession session) {
		System.out.println("Inside:: @@ Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		System.out.println("Save Data:: "+subcategory.toString());
		logger.debug(subcategory.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
			
		}else{
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = masterSetupService.saveSubcategoryDescriptionToMasterWorkDesc(subcategory);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully saved subcategory name details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to save subcategory name details");
			}
		}
		
		return resp;
	}
	
	
	@RequestMapping(value={"/deleteSubcategoryDescDetails"}, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> updateSubcategoryDescDetails( @RequestBody MasterSubcategoryModel subcategory, BindingResult bresult,HttpSession session) {
		System.out.println("Inside:: @@ Controller"); 
		Map<String, Object> resp = new HashMap<String, Object>();
		System.out.println("Save Data:: "+subcategory.toString());
		logger.debug(subcategory.toString());
		if(bresult.hasErrors()){
			System.out.println("Inside:: @@ hasErrors"); 
			resp.put("success", false);
			resp.put("status", "failure");
			resp.put("msg", "Invalid Request.");
			
		}else{
			System.out.println("Inside:: @@ NotErrors"); 
			boolean status = masterSetupService.deleteSubcategoryDescDetails(subcategory);
			if(status){
				resp.put("success", true);
				resp.put("msg", "Successfully deleted subcategory name details");
			}else{
				resp.put("success", false);
				resp.put("msg", "Failed to delete subcategory name details");
			}
		}
		
		return resp;
	}
}
