$(document).ready(function(){
	$('#frmApplyNew').bootstrapValidator({
		fields: {
			txtFirstName: {
				validators: {
					notEmpty: {
					message: 'The name is required and can\'t be empty'
					},
					stringLength: {
					min: 6,
					max: 30,
					message: 'The name must be more than 6 and less than 30 characters long'
					},
				}

			},
			txtEmail: {
				validators: {
					notEmpty: {
					message: 'The email address is required and can\'t be empty'
					},
					emailAddress: {
					message: 'The input is not a valid email address'
					}
				}
			},
			txtCandidatePhone: {
				 validators: {
					 notEmpty: {
					 message: 'Mobile number is required and cannot be empty.'
					 },
					 digits: {
					 message: 'Only digits are allowed in mobile number.'
					 },
					 regexp: {
					 regexp: /^[9 8 7]\d{9}$/,
					 message: 'Mobile number should be of 10 digits only.'
					 }
				 
				 }
			 }
		}
	})
})