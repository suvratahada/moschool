<html>
<meta charset="utf-8">
		<meta name = "format-detection" content = "telephone=no" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<meta name="theme-color" content="#042048" />

		<link rel="shortcut icon" href="./assets/images/favicon.png" type="image/x-icon">
		<link rel="icon" href="./assets/images/favicon.png" type="image/x-icon">
		
		<!-- Bootstrap -->
		<link href="./plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="./build/css/plugins/dataTables/css/dataTables.bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="./plugins/chosen/chosen.css" rel="stylesheet" media="screen">
		<link href="./plugins/chosen/prism.css" rel="stylesheet" media="screen">
		<link href="./build/css/plugins/dataTables/css/rowReorder.dataTables.min.css" rel="stylesheet" media="screen">
		<link href="./build/css/plugins/dataTables/css/responsive.dataTables.min.css" rel="stylesheet" media="screen">
		<link href="./assets/css/custom.css" rel="stylesheet" media="screen">
		
		<link href="./plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" media="screen">
		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
		<script src="./customJS/common/sha512.js"></script>
		<script href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
		
		<!-- Sweet Alert  -->
		<script src="./js/user/application/sweetalert.min.js"></script>
		<link rel="stylesheet" type="text/css" href="./css/user/application/sweetalert.css">
<style>
li{
border-bottom: none;
}
.base{
border-bottom: 1px solid #012b72;
}
.nav>li>a:hover {
    text-decoration: none;
    background-color: #0f3e12 !important;
    color: white !important;
    font-weight: bold;
}
.cusNav.navbar-default .navbar-nav > li > a{
	color: gold !important;
    font-size: 14px;
}
.cusNav.navbar-default .navbar-nav > .open > a{
	 background-color: #0f3e12 !important;
	 color: white !important;
}
.cusNav.navbar-default .navbar-nav > li.active > a,
.cusNav.navbar-default .navbar-nav > li.active > a:focus,
.cusNav.navbar-default .navbar-nav > li.active > a:hover {
    text-decoration: none;
    background-color: #0f3e12 !important;
    color: white !important;
    font-weight: bold;
 }
@media screen and (max-width: 768px) {
		.nav>li>a:hover {
		    text-decoration: none;
		    background-color: #ffffff !important;
		    color: #0f3e12 !important;
		    font-weight: bold;
		    border-top: 3px solid #0f3e12 !important ;
		}
		.cusNav.navbar-default .navbar-nav > li > a{
			color: #fff;
		    font-size: 14px;
		}
		.cusNav.navbar-default .navbar-nav > .open > a{
			 background-color: #0f3e12 !important;
			 color: #0f3e12 !important;
		}
		.cusNav.navbar-default .navbar-nav > li.active > a,
		.cusNav.navbar-default .navbar-nav > li.active > a:focus,
		.cusNav.navbar-default .navbar-nav > li.active > a:hover {
		    text-decoration: none;
		    background-color: #ffffff !important;
		    color: #0f3e12 !important;
		    font-weight: bold;
		    border-top: 3px solid #0f3e12 !important;
		 }
		 .logout-btn2{
			display:none;
			}
		 .logout-btn1{
			display:block;
			float: right;
			font-size: 24px;
			margin-top: 2%;
			margin-left: 2%;
			}
		.logout-btn1-color{
		color:#fff;
		}
		.logout-btn1-color:hover{
		color:#ff0000;
		}
}
.dropdown-menu{
    /* padding: 5px 3px !important;
    margin: 0px 0px !important;
    min-width: 0px !important;   */
}
.dropdown-menu>li>a{
	border-bottom: 2px solid #0f3e12 !important;
	color:gold !important;
}

.dropdown-menu>li>a:hover{
    color: #fff !important;
    text-decoration: none;
    background-color: #0f3e12 !important ;
}
@media screen and (min-width: 768px) {
	.logout-btn1{
		display:none;
	}
	.logout-btn2{
		display:block;
		float: right;
		font-size: 24px;
		margin-top: 1%;
		margin-left: 2%;
	}
	.logout-btn2-color{
		color:#fff;
	}
	.logout-btn2-color:hover{
		color:#ff0000;
	}
}
.glyphicon-off:before {
	color: gold !important;
}
.dropdown-menu {
	background-color: #0f3e12 !important;
}
.dash-style{
	color: gold;
	margin-right: 30px;
	font-size: 15px;
}
.dash-style:hover{
	color: white !important;
	background-color: #0f3e12 !important;
	text-decoration:none;
	font-weight: 600;
}

.dash-style:focus{
	color:white;
}

.dropdown-menu-style{
	width: 150px;
	height: 100px;
	background-color: #fff !important;
	border-radius: 0px !important;
}
.btn-logout{
	position: absolute;
	left: 45px;
	top: 35px;
	border-color: transparent;
	color: #0f3e12;
}

</style>
<script>
    $(function () {
        $('#datetimepicker1').datetimepicker();
        $('#datetimepicker2').datetimepicker({
            format: 'LT'
        });
    });
</script>

<nav class="navbar navbar-default base cusNav" style="margin-top: 0px">
		<div class="container-fluid" style="background-color: #0f3e12 !important;">
			
			
			<div style="float: left; margin-top: 10px; margin-bottom: 10px">
				<img style="height: 85px;width: 450px; " src="./images/opepa_logo_new.png">
			</div>
			<div style="float: right;">
				<h3 style="float: left; color: #d8d700;font-style: italic;">Project Monitoring App</h3>
				<div class="dropdown" style="float:right;font-size:17px;margin-top: 10px;">
				  <button type="button" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color: transparent; border: transparent;">
					<i style="float:right;" class="fa fa-user-circle fa-2x" aria-hidden="true"></i>
					<span style="float:right;font-size:18px;margin-top:9px;margin-left:5px;font-weight:900;letter-spacing:0px;">Welcome&nbsp;stcgajapati&nbsp; &nbsp;</span>
				  </button>
				  
				     <div class="dropdown-menu dropdown-menu-style">
				      	
							<form id="command" action="/OPEPA/perform_logout" method="POST">
							    <button class="btn-logout" title="Logout" type="submit"><b>Logout</b></button>
							    <input type="hidden" name="_csrf" value="a0e9b568-9db5-4f77-8827-fe15d1307300">
							<div>
<input type="hidden" name="_csrf" value="a0e9b568-9db5-4f77-8827-fe15d1307300">
</div></form>
	   			     </div>
				</div>
			</div>
			
			
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="logout-btn1">
					<!-- /OPEPA/j_spring_security_logout -->
					
				</div>
				<!-- <a class="navbar-brand" href="#" style="margin-left: 30px;"><img src="./assets/images/LOGO_SPFO-Blue.png"/></a>  -->
			</div>
			<div id="navbar" class="navbar-collapse collapse">
			
			</div><!--/.nav-collapse -->
		</div>
		<div style="width: 100%; height: 2px; background-color: white"></div>
		
		<div class="container-fluid" style="background-color: #0f3e12 !important; color: white; padding-top: 5px; padding-bottom: 4px;">
					<!-- Menu Hiding in JSTL Tag -->
		<div class="col-sm-8" style="margin-top:10px">
			
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
				   
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
			
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
				
			
								
		</div>
		
		
	</div>
		
    </nav>
<body>
<iframe width="1024" height="760" src="https://app.powerbi.com/view?r=eyJrIjoiNjJmYjA2ZmEtM2U2Yy00YzUwLTllYTMtMjY5MDk0Zjk4ZTI1IiwidCI6ImRiMmYxOTNiLTFmNjYtNGEyNC05YTMwLWU2Y2E4MGZmY2M2MyJ9" frameborder="0" allowFullScreen="true"></iframe>
</body>
</html>