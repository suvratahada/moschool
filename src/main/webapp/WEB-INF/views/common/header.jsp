<!-- SVN MasterSetup menu-->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%! 
String styleActiveSettings = "";
String styleActiveMST="";
String styleActiveMDM="";
String styleActiveDashboard="";


public void deactiveStyleTab() {
	styleActiveSettings="";
	styleActiveMST="";
	styleActiveMDM="";
	styleActiveDashboard="";
}
%>
<%

String activeTab = (String)session.getAttribute("sessionDashboardTab");

System.out.println("=============+++++++++==============++++++++++"+activeTab);

if(activeTab == "styleActiveSettings"){
	deactiveStyleTab();
	styleActiveSettings="active";
}else if(activeTab == "styleActiveMST"){
	deactiveStyleTab();
	styleActiveMST="active";
}else if(activeTab == "styleActiveMDM"){
	deactiveStyleTab();
	styleActiveMDM="";
}else if(activeTab == "styleActiveDashboard"){
	deactiveStyleTab();
	styleActiveDashboard="";
}
%>

<style>
li{
border-bottom: none;
}
.base{
border-bottom: 1px solid #012b72;
}
.nav>li>a:hover {
    text-decoration: none;
    background-color: #0f3e12 !important;
    color: white !important;
}
.cusNav.navbar-default .navbar-nav > li > a{
	color: gold !important;
    font-size: 14px;
}
.cusNav.navbar-default .navbar-nav > .open > a{
	 background-color: #0f3e12 !important;
	 color: white !important;
}
.cusNav.navbar-default .navbar-nav > li.active > a,
.cusNav.navbar-default .navbar-nav > li.active > a:focus,
.cusNav.navbar-default .navbar-nav > li.active > a:hover {
    text-decoration: none;
    background-color: #0f3e12 !important;
    color: white !important;
 }
 
 #shortpma{
 	display: none
 }
 
@media screen and (max-width: 768px) {

	 #shortpma{
	 	display: inline;
	 	margin-left: 45%;
	 	font-size: 15px !important
	 }
	 
	 #longpma{
	 	display: none
	 }
	 
	 #header-logo{
	 	width: 100%;
	 	height: 10%;
	 }
	 
	 .firstlastName{
	 	left: 27% !important;
	 	font-size: 15px !important
	 }
	 
	 #login-user{
	    font-size: 12px !important;
	    margin-top: 10px !important;
	    font-weight: 200 !important;
	 }
	 #login-menu{
	 	font-size: 14px !important;
	 	margin-top: 3% !important;
	 }
	 #navnbarbtn{
	 	float: left	
	 }
	 
	 .dropdown-menu-style{
	 	left: 0 !important
	 }
	.nav>li>a:hover {
	    text-decoration: none;
	    background-color: #ffffff !important;
	    color: #0f3e12 !important;
	    border-top: 3px solid #0f3e12 !important ;
	}
	.cusNav.navbar-default .navbar-nav > li > a{
		color: #fff;
	    font-size: 14px;
	}
	.cusNav.navbar-default .navbar-nav > .open > a{
		 background-color: #0f3e12 !important;
		 color: #0f3e12 !important;
	}
	.cusNav.navbar-default .navbar-nav > li.active > a,
	.cusNav.navbar-default .navbar-nav > li.active > a:focus,
	.cusNav.navbar-default .navbar-nav > li.active > a:hover {
	    text-decoration: none;
	    background-color: #ffffff !important;
	    color: #0f3e12 !important;
	    border-top: 3px solid #0f3e12 !important;
	 }
}
.dropdown-menu>li>a{
	border-bottom: 2px solid #0f3e12 !important;
	color:gold !important;
}

.dropdown-menu>li>a:hover{
    color: #fff !important;
    text-decoration: none;
    background-color: #0f3e12 !important ;
}
.glyphicon-off:before {
	color: gold !important;
}
.dropdown-menu {
	background-color: #0f3e12 !important;
	top: 50px	
}
.dash-style{
	color: gold;
	margin-right: 30px;
	font-size: 15px;
}
.dash-style:hover{
	color: white !important;
	background-color: #0f3e12 !important;
	text-decoration:none;
}

.dash-style:focus{
	color:white;
}

.dropdown-menu-style{
	width: 150px;
	left: 50px;
	background-color: #fff !important;
	border-radius: 5px 5px 5px 5px;
}
.dropdown-menu.dropdown-menu-style::after {
    content: '';
    position: absolute;
    top: -15px;
    left: 89.5%;
    width: 16px;
    height: 20px;
    border-bottom: solid 20px white;
    border-left: solid 15px transparent;
}
.btn-logout{
	position: absolute;
	left: 40px;
	top: 35px;
	border-color: #999;
	color: #0f3e12;
}

.btn-logout:hover{
	box-shadow: 1px 1px 5px grey;
	color: #0f3e12;
	transform: scale(1.01);
}

.firstlastName{
    position: absolute;
    top: 100px;
    left: 46%;
    font-variant: small-caps;
    font-weight: 900;
}
.pad-down{
	padding: 5px;
	text-align: center;
}
.btn-dropdown-item{
	width: 100%;
	color: #0f3e12;
	border-radius: 0 !important;
	border: 1px solid #ada2a2;
}
.btn-dropdown-item:hover{
	background-color: #0f3e12;
	color: #fff;
	transform: scale(1.03);
	transition-duration: 0.3s;
	box-shadow: 2px 2px 5px grey;
	border: 1px solid #0f3e12;
}
</style>
<script src = "./customJS/common/changePassword.js"></script>
<body>
<nav class="navbar navbar-default base cusNav" style="margin-top: 0px">
		<div class="container-fluid" style="background-color: #0f3e12 !important;">
			<div style="float: left; margin-top: 15px; margin-bottom: 10px">
				<img id="header-logo" src = "./images/opepa_logo_new.png"/>
			</div>
			<h4><span class="firstlastName"><%=session.getAttribute("first_name")%>&nbsp;<%=session.getAttribute("last_name")%></span></h4>
			<div style="margin-top: 15px"><h3 style="color: yellow;font-variant: small-caps"><b id="longpma" style="border:1px dashed;font-variant:small-caps">&nbsp;Project Monitoring Application&nbsp;</b><b id="shortpma" style="border:1px dashed;font-variant:small-caps" data-text = "">&nbsp;PMA&nbsp;</b></h3></div>
			
			<div id="login-menu" class="dropdown" style="float:right;font-size:17px;margin-top: -35px;" >
			  <button type="button" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color: transparent; border: transparent;">
				<i style="float:right;" class="fa fa-user-circle fa-2x" aria-hidden="true"></i>
				<div id="login-user" style="float:right;font-size:18px;margin-top:9px;margin-left:10px;font-weight:900;letter-spacing:0px;">Welcome&nbsp;<%=session.getAttribute("userId")%>&nbsp; &nbsp;</div><br/>
			  </button>
			  
			     <div class="dropdown-menu dropdown-menu-style">
			      	<c:url value="/perform_logout" var="logoutUrl" />
							<div class="pad-down" style="border-bottom: solid 1px #adadad">
						    	<button class="btn btn-dropdown-item" title="Change Password" data-toggle = "modal" data-target="#changePasswordModal"><b>Change Password</b></button>
						    </div>
						    <div class="pad-down">
					    	<form:form action="${logoutUrl}" method="POST">
						    	<button class="btn btn-dropdown-item" title="Logout" type="submit"><b>Logout</b></button>
						    	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
							</form:form>
						    </div>
   			     </div>
			</div>
			
			<div class="navbar-header">
				<button id="navnbarbtn" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="logout-btn1">
					<%-- <a class="glyphicon glyphicon-off logout-btn1-color" href="${logoutUrl}" role="button" ></a> --%><!-- <c:url value='/j_spring_security_logout' /> -->
					
				</div>
				<!-- <a class="navbar-brand" href="#" style="margin-left: 30px;"><img src="./assets/images/LOGO_SPFO-Blue.png"/></a>  -->
			</div>
			<div id="navbar" class="navbar-collapse collapse"><br/><br/>
				<ul class="nav navbar-nav navbar-right">
				
				<li class="dropdown <%=styleActiveDashboard%>" >
					<!-- Menu Hiding in JSTL Tag -->
						<c:set var="isDshboardVisible" value="false" />
						<c:forEach var="resource" items="${sessionScope.allowedResources}">
							<c:if test="${resource.resourceName == 'mdm/dashboard'}">
							   <c:set var="isDshboardVisible" value="true" />
							   <li><a href="./mdm/dashboard.do">Dashboard</a></li>
							</c:if>
						</c:forEach>
					</li>
				
				
					<!-- Admin Master Setup MENU -->
						
					<li class="dropdown <%=styleActiveMST%>" >
					<!-- Menu Hiding in JSTL Tag -->
						<c:set var="isMSTVisible" value="false" />
						<c:forEach var="resource" items="${sessionScope.allowedResources}">
							<c:if test="${resource.resourceName == 'mastersetup/'}">
							   <c:set var="isMSTVisible" value="true" />
							</c:if>
						</c:forEach>
						<c:if test = "${isMSTVisible == 'true'}">
							<a href="#" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" >Master Setup<span class="caret"></span></a>
						</c:if>	
							<ul class="dropdown-menu">
								<c:forEach var="resource" items="${sessionScope.allowedResources}">
									<c:if test="${resource.resourceName == 'mastersetup/district'}">
										<li><a href="./mastersetup/district.do">District Setup</a></li>
									</c:if>
								</c:forEach>
								<c:forEach var="resource" items="${sessionScope.allowedResources}">
									<c:if test="${resource.resourceName == 'mastersetup/block'}">
										<li><a href="./mastersetup/block.do">Block Setup</a></li>
									</c:if>
								</c:forEach>
								<c:forEach var="resource" items="${sessionScope.allowedResources}">
									<c:if test="${resource.resourceName == 'mastersetup/school'}">
										<li><a href="./mastersetup/school.do">School Setup</a></li>
									</c:if>
								</c:forEach>
								
								<c:forEach var="resource" items="${sessionScope.allowedResources}">
									<c:if test="${resource.resourceName == 'mastersetup/finyear'}">
										<li><a href="./mastersetup/finyear.do">Financial Year</a></li>
									</c:if>
								</c:forEach>
								
								<c:forEach var="resource" items="${sessionScope.allowedResources}">
									<c:if test="${resource.resourceName == 'mastersetup/scheme'}">
										<li><a href="./mastersetup/scheme.do">Scheme Setup</a></li>
									</c:if>
								</c:forEach>
								
								<c:forEach var="resource" items="${sessionScope.allowedResources}">
									<c:if test="${resource.resourceName == 'mastersetup/workdesc'}">
										<li><a href="./mastersetup/workdesc.do">Subcategory Master</a></li>
									</c:if>
								</c:forEach>
								
								<c:forEach var="resource" items="${sessionScope.allowedResources}">
									<c:if test="${resource.resourceName == 'mastersetup/subcategory'}">
										<li><a href="./mastersetup/subcategory.do">Subcategory Setup</a></li>
									</c:if>
								</c:forEach>
								
							</ul>
					</li>
					
					<!-- Mdm Setup MENU -->
						
					<li class="dropdown <%=styleActiveMDM%>" >
					<!-- Menu Hiding in JSTL Tag -->
						<c:set var="isMSTVisible" value="false" />
						<c:forEach var="resource" items="${sessionScope.allowedResources}">
							<c:if test="${resource.resourceName == 'mdm/'}">
							   <c:set var="isMSTVisible" value="true" />
							</c:if>
						</c:forEach>
						<c:if test = "${isMSTVisible == 'true'}">
							<a href="#" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" >MDM<span class="caret"></span></a>
						</c:if>	
							<ul class="dropdown-menu">
								<!-- moschool category -->
								<c:forEach var="resource" items="${sessionScope.allowedResources}">
									<c:if test="${resource.resourceName == 'mdm/category'}">
										<li><a href="./mdm/category.do">Category Setup</a></li>
									</c:if>
								</c:forEach>
								
								<!-- moschool events -->
								<c:forEach var="resource" items="${sessionScope.allowedResources}">
									<c:if test="${resource.resourceName == 'mdm/events_noti_ach'}">
										<li><a href="./mdm/events_noti_ach.do">E/N/A Setup</a></li>
									</c:if>
								</c:forEach>
								
								<!-- moschool language -->
								<c:forEach var="resource" items="${sessionScope.allowedResources}">
									<c:if test="${resource.resourceName == 'mdm/language'}">
										<li><a href="./mdm/language.do">Language Setup</a></li>
									</c:if>
								</c:forEach>
								
								<!-- moschool school category -->
								<c:forEach var="resource" items="${sessionScope.allowedResources}">
									<c:if test="${resource.resourceName == 'mdm/schoolcatg'}">
										<li><a href="./mdm/schoolcatg.do">School Category Setup</a></li>
									</c:if>
								</c:forEach>

								<c:forEach var="resource" items="${sessionScope.allowedResources}">
									<c:if test="${resource.resourceName == 'mdm/school_aff_year'}">
										<li><a href="./mdm/school_aff_year.do">School Affiliation Year</a></li>
									</c:if>
								</c:forEach>
								
								<c:forEach var="resource" items="${sessionScope.allowedResources}">
									<c:if test="${resource.resourceName == 'mdm/school_contact_details'}">
										<li><a href="./mdm/school_contact_details.do">School Contact Details</a></li>
									</c:if>
								</c:forEach>
								
								<c:forEach var="resource" items="${sessionScope.allowedResources}">
									<c:if test="${resource.resourceName == 'mdm/school_details'}">
										<li><a href="./mdm/school_details.do">School Location Setup</a></li>
									</c:if>
								</c:forEach>

								<c:forEach var="resource" items="${sessionScope.allowedResources}">
									<c:if test="${resource.resourceName == 'mdm/employeeSetup'}">
										<li><a href="./mdm/employeeSetup.do">Employee SetUp</a></li>
									</c:if>
								</c:forEach>
								
								<c:forEach var="resource" items="${sessionScope.allowedResources}">
									<c:if test="${resource.resourceName == 'mdm/schoolDetailsSetup'}">
										<li><a href="./mdm/schoolDetailsSetup.do">School Details SetUp</a></li>
									</c:if>
								</c:forEach>
								
								<c:forEach var="resource" items="${sessionScope.allowedResources}">
									<c:if test="${resource.resourceName == 'mdm/schoolStrengthSetUp'}">
										<li><a href="./mdm/schoolStrengthSetUp.do">Student Strength SetUp</a></li>
									</c:if>
								</c:forEach>
								
								<c:forEach var="resource" items="${sessionScope.allowedResources}">
									<c:if test="${resource.resourceName == 'mdm/subjectSetup'}">
										<li><a href="./mdm/subjectSetup.do">Subject SetUp</a></li>
									</c:if>
								</c:forEach>
							</ul>
					</li>
					
					<!-- User Access Control Menus -->						
					<li class="dropdown <%=styleActiveSettings%>" >
						<!-- Menu Hiding in JSTL Tag -->
						<c:set var="isSettingVisible" value="false" />
						<c:forEach var="resource" items="${sessionScope.allowedResources}">
							<c:if test="${fn:startsWith(resource.resourceName, 'setting/')}">
							   <c:set var="isSettingVisible" value="true" />
							</c:if>
						</c:forEach>
						<c:if test = "${isSettingVisible == 'true'}">
						 <a href="#" class="dropdown-toggle tab-active" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" >Setting<span class="caret"></span></a>
						</c:if>
						<!-- Menu Hiding in JSTL Tag -->
						<ul class="dropdown-menu">
							<c:forEach var="resource" items="${sessionScope.allowedResources}">
								<c:if test="${resource.resourceName == 'setting/createUser'}">
									<li><a href="./setting/createUser.do">Create User</a></li>
								</c:if>
			  				</c:forEach>
			  				<c:forEach var="resource" items="${sessionScope.allowedResources}">
								<c:if test="${resource.resourceName == 'setting/userGroups'}">
									<li><a href="./setting/userGroups.do">Create Group</a></li>
								</c:if>
			  				</c:forEach> 
			  				<c:forEach var="resource" items="${sessionScope.allowedResources}">
								<c:if test="${resource.resourceName == 'setting/createResources'}">
									<li><a href="./setting/createResources.do">Create Resources</a></li>
								</c:if>
			  				</c:forEach> 
			  				<c:forEach var="resource" items="${sessionScope.allowedResources}">
								<c:if test="${resource.resourceName == 'setting/createRole'}">
									<li><a href="./setting/createRole.do">Create Role</a></li>
								</c:if>
			  				</c:forEach> 
			  				<c:forEach var="resource" items="${sessionScope.allowedResources}">
								<c:if test="${resource.resourceName == 'setting/createParameter'}">
									<li><a href="./setting/createParameter.do">Create Parameter</a></li>
								</c:if>
			  				</c:forEach> 
			  				<c:forEach var="resource" items="${sessionScope.allowedResources}">
								<c:if test="${resource.resourceName == 'setting/groupRoleMap'}">
									<li><a href="./setting/groupRoleMap.do">Group Role Map</a></li>
								</c:if>
			  				</c:forEach> 
						</ul>		
					</li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
		<div style="height: 1px; background-color: #fff"></div>
		<div class="container-fluid" style="height: 0.5px; background-color: #0f3e12 !important"></div>
    </nav>
    
    <!-- Change Password Modal -->
    <div class="modal fade" id="changePasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <div class="modal-header" style="text-align: center; background-color:#0f3e12; color: #fff">
	        <h5 class="modal-title" id="exampleModalLongTitle">Change Password</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <form action="" name="formPassword" id="formPassword">
		
				<div class="row">
				      <div class="col-sm-4">Current Password</div>
				      <div class="col-sm-8"><input type="password" class="form-control" id="old_psw"></div>
				</div><br>
				<div class="row">
				      <div class="col-sm-4">New Password</div>
				      <div class="col-sm-8"><input type="password" class="form-control" id="new_psw"></div>
				</div><br>
				<div class="row">
				      <div class="col-sm-4">Confirm Password</div>
				      <div class="col-sm-8"><input type="password" class="form-control" id="con_psw"></div>
				</div>
			</form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="button"  class="btn btn-primary" id="submit_changePassword">Change Password</button>
	      </div>
	    </div>
	  </div>
	</div>
    
 </body>   