<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %> 
 <%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta name="_csrf" content="${_csrf.token}"/>
	<meta name="_csrf_header" content="${_csrf.headerName}"/>
<link href="./images/Emblem_of_India.png" rel="shortcut icon" title="Indian Emblem" />
<title>OPEPA PMA</title>
<c:set var="url">${pageContext.request.requestURL}</c:set>
<base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />

		<meta charset="utf-8">
		<meta name = "format-detection" content = "telephone=no" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<meta name="theme-color" content="#042048" />
		<!-- for session timeout -->
		<meta http-equiv="refresh" content="<%=session.getMaxInactiveInterval()%>;url=login.html"/>

		<link rel="stylesheet" href= "./plugins/jquery/jquery-confirm.min.css">

		<link rel="shortcut icon" href="./assets/images/favicon.png" type="image/x-icon">
		<link rel="icon" href="./assets/images/favicon.png" type="image/x-icon">
		
		<!-- Bootstrap -->
		<link href="./plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="./build/css/plugins/dataTables/css/dataTables.bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="./plugins/chosen/chosen.css" rel="stylesheet" media="screen">
		<link href="./plugins/chosen/prism.css" rel="stylesheet" media="screen">
		<link href="./build/css/plugins/dataTables/css/rowReorder.dataTables.min.css" rel="stylesheet" media="screen">
		<link href="./build/css/plugins/dataTables/css/responsive.dataTables.min.css" rel="stylesheet" media="screen">
		<link href="./assets/css/custom.css" rel="stylesheet" media="screen">
		
		<link href="./plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" media="screen">
		<!-- For jquery  -->
		<script src="./plugins/jquery/jquery.min.js"></script>
		
		<!-- Bootstrap Datepicker -->
		<link href="./plugins/boootstrap-datepicker/bootstrap-datepicker3.min.css" rel="stylesheet">
		
		<!-- Sweet Alert Import -->
		<link href="./plugins/sweet-alert/sweetalert.css" rel="stylesheet">
		
		<script type="text/javascript">
			var token = $("meta[name='_csrf']").attr("content");
			var header = $("meta[name='_csrf_header']").attr("content");
		</script>

</head>
<style>
#loading {
	width: 100%;
	height: 100%;
	top: 0px;
	left: 0px;
	position: fixed;
	display: block;
    opacity: 0.7;
	background-color: #fff;
	z-index: 99;
	text-align: center;
}
	
#loading-image {
	position: absolute;
	top:50%;
	left:50%;
	z-index: 100;
}
</style>
<body>
	<!-- Page Loader -->
    <div id="loading" class="wrapper">
            <img id="loading-image" src="./images/ajax-loader.gif" alt="Loading..." />
    </div>
    <!-- Page code starts -->
	<div><tiles:insertAttribute name="header"></tiles:insertAttribute></div>
	<div style="min-height: 463px;"><tiles:insertAttribute name="body"></tiles:insertAttribute></div>
	<div><%-- <tiles:insertAttribute name="footer"></tiles:insertAttribute> --%></div>
	
	<script src="./plugins/jquery/jquery-confirm.min.js"></script>
	
	<script src="./plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="./assets/datatable/jquery.dataTables.min.js"></script>
	<script src="./assets/datatable/dataTables.bootstrap.min.js"> </script>
	<script src="./assets/datatable/dataTables.rowReorder.min.js"> </script>
	<script src="./assets/datatable/dataTables.responsive.min.js"> </script>
	<script src="./plugins/chosen/prism.js"></script>
	<script src="./plugins/chosen/chosen.jquery.min.js"></script>
<!-- for datatable -->
<script src="./assets/datatable/dataTables.buttons.js" type="text/javascript"></script>
<script src="./assets/datatable/buttons.flash.min.js" type="text/javascript"></script>
<!-- For datatable export -->
<script src="./plugins/datatable/jszip.min.js"></script>
<script src="./plugins/datatable/pdfmake.min.js"></script>
<script src="./plugins/datatable/vfs_fonts.js"></script>
<script src="./assets/datatable/buttons.html5.min.js" type="text/javascript"></script>
<script src="./assets/datatable/buttons.print.min.js" type="text/javascript"></script>
<!-- Jquery Validation  -->
<script src="./plugins/jquery-validation/jquery.validate.js"></script>
<!-- Bootstrap Datepicker -->
<script src="./plugins/boootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<!-- Sweet Alert  Import -->
<script src="./plugins/sweet-alert/sweetalert.min.js"></script>
<!-- Overlay Loader -->
<script src="./plugins/overlay-loader/overlay-loader.js"></script>
<!-- sha512 conversion  -->
<script src="./customJS/common/sha512.js"></script>
	<script type="text/javascript">
		   var config = {
			 '.chosen-select'           : {},
			 '.chosen-select-deselect'  : {allow_single_deselect:true},
			 '.chosen-select-no-single' : {disable_search_threshold:10},
			 '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
			 '.chosen-select-width'     : {width:"95%"}
		   }
		   for (var selector in config) {
			 $(selector).chosen(config[selector]);
		   }
		   
		   $(window).on("load", function() {
	     		$('#loading').hide();
	  		});
   </script>
	<c:if test="${sessionScope.showNameWelcomeSession == true}">
	</c:if>
</body>
</html>