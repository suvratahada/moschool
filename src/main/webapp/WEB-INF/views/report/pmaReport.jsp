<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
<link rel="stylesheet" href="./css/w3css.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="./customCSS/button.css">
<link rel="stylesheet" href="./customCSS/tickets.css">
<link rel="stylesheet" href="./customCSS/top_lebel.css">
<link rel="stylesheet" href="./customCSS/opacity_custom.css">

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
	
<script type="text/javascript" src="./plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script src="./plugins/datatable/dataTables.fixedColumns.min.js"></script>
<script src="./plugins/datatable/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="./plugins/datatable/buttons.flash.min.js" type="text/javascript"></script>
<script src="./plugins/datatable/jszip.min.js" type="text/javascript"></script>
<script src="./plugins/datatable/pdfmake.min.js" type="text/javascript"></script>
<script src="./plugins/datatable/vfs_fonts.js" type="text/javascript"></script>
<script src="./plugins/datatable/buttons.html5.min.js" type="text/javascript"></script>
<script src="./plugins/datatable/buttons.print.min.js" type="text/javascript"></script>


<script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript" src="./plugins/fusionchart/fusioncharts.js"></script>
<script type="text/javascript" src="https://static.fusioncharts.com/code/latest/themes/fusioncharts.theme.fint.js"></script>
<script type="text/javascript" src="./plugins/sweet-alert/sweetalert.min.js"></script>

<script type="text/javascript" src="./plugins/d3/d3.v3.min.js"></script>
<script type="text/javascript" src="./customJS/dashboard/liquidFillGauge.js"></script>
<script type="text/javascript" src="./customJS/dashboard/script.js"></script>
<script type="text/javascript" src="./customJS/report/report.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<style>
    /* .dataTables_scrollHeadInner {
        width:100% !important;
        padding: 0 !important;
    }
    td {white-space: nowrap;}  */
    .r-click{
    	/* background-color: aqua; */
    	border: thick;
    	text-align: center;
    	cursor: pointer;
    }
    
    .cls{
    height: 20px; 
    padding: 20px
    }
    
    .textcard{
    	width: 100%;
	    height: 60px;
	    background-color: #cc6600;
	    color: #fff;
	    box-shadow: 5px 5px #585f6d;
	    border-radius: 3px;
    }
    
    .textcard:hover{
    	transform: scale(1.05);
    	box-shadow: 10px 10px #585f6d;
    	transition-duration: .4s
    }
    
    .cardhead{
        display: block;
	    padding-top: 10px;
	    text-align: center;
	    font-weight: 900
    }
    
    .cardbody{
    	line-height: 30px;
    }
    
</style>

</head>
<body>

	<div class="container-fluid col-sm-12 col-lg-12 col-md-12">
		<div class="container-fluid col-sm-12 col-lg-12 col-md-12">
			<h5 class="container-fluid" style="text-align: center;">
				<b
					style="font-style: regular; font-family: Gill Sans MT; font-size: 22px; font-variant: inherit;">Infrastructure Report</b>
			</h5>
		</div>
		<div class="container-fluid col-sm-12 col-lg-12 col-md-12">
			<div class="container-fluid col-sm-12 col-lg-12 col-md-12">
				<div class="container-fluid col-sm-5 col-lg-5 col-md-5">
					<div class="container-fluid col-sm-12 col-lg-12 col-md-12">
						<div class="container-fluid col-sm-6 col-lg-6 col-md-6">
							<div class="card TICKETS_DPX">
								<div class="card-body" style="text-align: center;">
									<h5 class="card-title">Scheme</h5>
									<select id="scheme_dropdown" style="width: 70%" class=""></select>
								</div>
							</div>
						</div>
						<div class="container-fluid col-sm-6 col-lg-6 col-md-6">
							<div class="card TICKETS_DPX">
								<div class="card-body" style="text-align: center;">
									<h5 class="card-title">Year Of Approval</h5>
									<select id="appr_year" style="width: 70%" class=""></select>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="container-fluid col-sm-7 col-lg-7 col-md-7">
					<div class="container-fluid col-sm-12 col-lg-12 col-md-12">
						<div class="container-fluid col-sm-4 col-lg-4 col-md-4">
							<div class="card TICKETS_DPX">
								<div class="card-body" style="text-align: center;">
									<h5 class="card-title">District Name</h5>
									<select id="district_name" style="width: 70%" class=""></select>
								</div>
							</div>
						</div>
						<div class="container-fluid col-sm-4 col-lg-4 col-md-4">
							<div class="card TICKETS_DPX">
								<div class="card-body" style="text-align: center;">
									<h5 class="card-title">Block Name</h5>
									<select id="block_name" style="width: 70%" class=""></select>
								</div>
							</div>
						</div>
						<div class="container-fluid col-sm-4 col-lg-4 col-md-4">
							<div class="card TICKETS_DPX">
								<div class="card-body" style="text-align: center;">
									<h5 class="card-title">School Name</h5>
									<select id="school_name" style="width: 70%" class=""></select>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container-fluid col-sm-12 col-lg-12 col-md-12">
				<div class="container-fluid col-sm-12 col-lg-12 col-md-12">
					<div class="container-fluid col-sm-6 col-lg-6 col-md-6"
						style="margin-top: 2%;" align="center">
						<div class="card TICKETS_DPX"
							style="width: 50%; margin-right: initial">
							<div class="card-body">
								<h5 class="card-title">No. Of Works Approved</h5>
								<label id="work_no" style="width: 70%; font-size: 22px"></label>
							</div>
						</div>
					</div>
					<div class="container-fluid col-sm-6 col-lg-6 col-md-6"
						style="margin-top: 2%;" align="center">
						<div class="card TICKETS_DPX"
							style="width: 50%; margin-right: initial">
							<div class="card-body">
								<h5 class="card-title">Amount Approved(In Lakhs)</h5>
								<label id="sanc_amount" style="width: 70%; font-size: 22px"></label>
							</div>
						</div>
					</div>
				</div>
				<div class="container-fluid col-sm-12 col-lg-12 col-md-12">
					<div class="container-fluid col-sm-6 col-lg-6 col-md-6"
						id="bar_area" style="margin-top: 3%"></div>

					<div class="container-fluid col-sm-6 col-lg-6 col-md-6"
						id="bar_area_2nd" style="margin-top: 3%"></div>
				</div>
			</div>
		</div>
		</div>

	<div class="container-fluid">
		<div class="row">
		<div class="cls col-sm-12 col-lg-12 col-md-12 clearfix" style="padding-left: 2%; padding-bottom: 85px; display: flex; text-align: center;">
			<div class="col-sm-2"><div class="textcard"><span class="cardhead">Scheme</span><span class="cardbody" id="scheme_text"></span></div></div>
			<div class="col-sm-2"><div class="textcard"><span class="cardhead">Year Of Approval</span><span class="cardbody" id="year_text"></span></div></div>
			<div class="col-sm-2"><div class="textcard"><span class="cardhead">District</span><span class="cardbody" id="district_text"></span></div></div>
			<div class="col-sm-2"><div class=textcard><span class="cardhead">Block</span><span class="cardbody" id="block_text"></span></div></div>
			<div class="col-sm-4"><div class="textcard"><span class="cardhead">School</span><span class="cardbody" id="school_text"></span></div></div>
			</div>
		</div>
	</div>

	<div class="container-fluid col-sm-12 col-lg-12 col-md-12">
			<div class="container-fluid col-sm-12 col-lg-12 col-md-12">
				<div style="width: 100%">
					<table class="display w3-table w3-striped table-bordered"
						id='table1' width="100%" >
						<thead style="font-weight: bold;">
							<tr class="w3-panel w3-border-left w3-border-blue">
								<td><b>Sl No.</b></td>
								<td><b>Catagory</b></td>
								<td><b>Name Of Activity</b></td>
								<td><b>No. of Target School</b></td>
								<td><b>No. of Work Approved</b></td>
								<td><b>No. of Work Sanctioned</b></td>
								<td><b>Approved Amount(In lakhs)</b></td>
								<td><b>Sanctioned Amount(In lakhs)</b></td>
							</tr>
						</thead>
						<tbody>

						</tbody>
						<tfoot>
							<tr>
								<!-- <th colspan="3" >Total:</th> -->
								<th style="border-right: 0;"></th>
								<th style="border-left: 0; border-right: 0">Total:</th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		<br>
		<br>
		<br>
		<br>
		<div width="95%" style="margin-top: 10px" class="containeer-fluid">

		</div>
		
		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog" style="width: 95%">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h2 class="modal-title" style="text-align: center;" id='modal_heading'>Details Of School</h2>
					</div>
					<div class="modal-body">
						<!-- <p>Some text in the modal.</p> -->
						<table class="display w3-table w3-striped" id="details_table" width="100%">
							<thead>
								<tr>
									<td><b>Sl No.</b></td>
									<td><b>Catagory</b></td>
									<td><b>Name Of Activity</b></td>
									<td><b>School Name</b></td>
									<td><b>No. of Work Approved</b></td>
									<td><b>No. of Work Sanctioned</b></td>
									<td><b>Approved Amount(In lakhs)</b></td>
									<td><b>Sanctioned Amount(In lakhs)</b></td>
								</tr>
							</thead>
							<tbody>

							</tbody>
					</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>

			</div>

		</div>

</body>
</html>