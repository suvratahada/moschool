<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
<link rel="stylesheet" href="./css/w3css.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="./customCSS/button.css">
<link rel="stylesheet" href="./customCSS/tickets.css">
<link rel="stylesheet" href="./customCSS/top_lebel.css">
<link rel="stylesheet" href="./customCSS/opacity_custom.css">

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
	
<script type="text/javascript" src="./plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script src="./plugins/datatable/dataTables.fixedColumns.min.js"></script>
<script src="./plugins/datatable/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="./plugins/datatable/buttons.flash.min.js" type="text/javascript"></script>
<script src="./plugins/datatable/jszip.min.js" type="text/javascript"></script>
<script src="./plugins/datatable/pdfmake.min.js" type="text/javascript"></script>
<script src="./plugins/datatable/vfs_fonts.js" type="text/javascript"></script>
<script src="./plugins/datatable/buttons.html5.min.js" type="text/javascript"></script>
<script src="./plugins/datatable/buttons.print.min.js" type="text/javascript"></script>


<script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript" src="./plugins/fusionchart/fusioncharts.js"></script>
<script type="text/javascript" src="https://static.fusioncharts.com/code/latest/themes/fusioncharts.theme.fint.js"></script>
<script type="text/javascript" src="./plugins/sweet-alert/sweetalert.min.js"></script>
<link href="./customCSS/pma/blockInspection.css" rel="stylesheet" type="text/css" />
<script src="./customJS/report/pmaInspectionReport.js"></script>
	<style>
	.r-click{
		border: thick;
		text-align: center;
		cursor: pointer;
	}
	</style>
</head>
<body>
	<div class="container-fluid col-sm-12 col-lg-12 col-md-12">
		<div class="container-fluid col-sm-12 col-lg-12 col-md-12">
			<h5 class="container-fluid" style="text-align: center;">
				<b
					style="font-style: regular; font-family: Gill Sans MT; font-size: 22px; font-variant: inherit;">Inspection Report</b>
			</h5>
		</div>
		<div class="container-fluid col-sm-12 col-lg-12 col-md-12">
					<div class="container-fluid col-sm-12 col-lg-12 col-md-12">
						<div class="container-fluid col-sm-3 col-lg-3 col-md-3">
							<div class="card TICKETS_DPX">
								<div class="card-body" style="text-align: center;">
									<h5 class="card-title">District Name</h5>
									<select id="district_name" style="width: 70%" class=""></select>
								</div>
							</div>
						</div>
						
						<div class="container-fluid col-sm-3 col-lg-3 col-md-3">
							<div class="card TICKETS_DPX">
								<div class="card-body" style="text-align: center;">
									<h5 class="card-title">Block Name</h5>
									<select id="block_name" style="width: 70%" class=""></select>
								</div>
							</div>
						</div>
						
						<div class="container-fluid col-sm-3 col-lg-3 col-md-3">
							<div class="card TICKETS_DPX">
								<div class="card-body" style="text-align: center;">
									<h5 class="card-title">School Name</h5>
									<select id="school_name" style="width: 70%" class=""></select>
								</div>
							</div>
						</div>
						
						<div class="container-fluid col-sm-3 col-lg-3 col-md-3">
							<div class="card TICKETS_DPX">
								<div class="card-body" style="text-align: center;">
									<h5 class="card-title">Activity Name</h5>
									<select id="subCatgId" style="width: 70%" class=""></select>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				
				
		<div width="95%" style="margin-top: 10px;" class="containeer-fluid">
			<div class="container-fluid col-sm-12 col-lg-12 col-md-12" style="margin-top: 2%">
					<div style="width: 100%">
						<table class="display w3-table w3-striped table-bordered" 
						id='inspectionTable' width="100%" style="margin-top: 2%">
							<thead>
								<tr>
									<th>Sl No.</th>
									<th id="code">District Code</th>
									<th id="name">District Name</th>
									<th id="name1">Activity Name</th>
									<th>No. of Activities</th>
									<th>No. of visits till last month</th>
									<th>No. of visits during current month</th>
									<th>No. of Activity monitored till last month</th>
									<th>No. of Activity monitored during current month</th>
									
									
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
		</div>
				
				
	</div>
</html>