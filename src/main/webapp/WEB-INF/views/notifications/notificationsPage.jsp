<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="./js/notification/ckeditor.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Notifications</title>


<script type="text/javascript">
$(document).ready(function() {
    var text_max = 500;
    $('#lblcount').html(text_max + ' characters remaining');

    $('#msg_notification').keyup(function() {
        var text_length = $('#msg_notification').val().length;
        var text_remaining = text_max - text_length;

        $('#lblcount').html(text_remaining + ' Characters Remaining');
    });
});

CKEDITOR.editorConfig = function (config) {
    config.language = 'es';
    config.uiColor = '#F7B42C';
    config.height = 300;
    config.toolbarCanCollapse = true;

};
CKEDITOR.replace('msg_notification');
</script>





<style type="text/css">
.noti-header{
	background-color: #012b72;
	color:#fff;
	padding: 0.5%;
	border-radius:8px;
}
label{
	font-size: 16px;
	letter-spacing: 0.3px;
}
.stybtn:hover {
    box-shadow: 5px 5px 15px #333333;
    -webkit-box-shadow: 5px 5px 15px #333333;
    -moz-box-shadow: 5px 5px 15px #333333;
}
sub{
	font-size: 14px;
}
.stybtn {
	width:100%;
    border: none;
    font-size:18px;
    display: inline-block;
    outline: 0;
    padding: 2%;
    vertical-align: middle;
    overflow: hidden;
    text-decoration: none !important;
    text-align: center;
    border-radius:8px;
    cursor: pointer;
    white-space: nowrap;
    color: #FFFFFF;
    background-color:#012b72;
    line-height: 18px;
    height: 36px;
    margin-bottom: 16px !important;
    -webkit-transition: background-color .25s,color .15s,box-shadow .25s,opacity 0.25s,filter 0.25s,border 0.15s;
    transition: background-color .25s,color .15s,box-shadow .15s,opacity .25s,filter .25s,border .15s;
}
.panel-default, .panel-default > .panel-heading {
    border-color: #012b72;
}
.panel-default>.panel-heading {
    color: #fff;
    background-color: #012b72;
    font-size:22px;
    letter-spacing:0.2px;
    border-color: #012b72;
    font-weight: 600;
    font-style: italic;
}
.table > thead > tr > th{
	background:#00b9f5;
	color:#fff;
}

/* This only works with JavaScript, 
if it's not present, don't show loader */
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url(images/Preloader_8.gif) center no-repeat #fff;
}
</style>
</head>
<body>
<div class="se-pre-con"></div> <!-- Image Loading -->

<div class="container-fluid">
  <div class="panel panel-default">
    <div class="panel-body">
	    <div class="panel panel-default">
		  <div class="panel-heading">
		  	<div><i class="fa fa-bell"></i><sub><i class="fa fa-envelope"></i></sub> Send Notifications</div>
		  </div>
		  <div class="panel-body">
			<form class="form-horizontal">
			    <div class="form-group">
			      <label class="control-label col-sm-4" for="recipientAddr">Select Recipients dress:</label>
			      <div class="col-sm-6">
			      	<div class="input-group">
			    		<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
			    		<select class="form-control" id="recipientAddr">
			    			<option>Select Any Recipient</option>
			             	<option>john@example.com</option>
			              	<option>mary@example.com</option>
			              	<option>july@example.com</option>
			            </select>
			  		</div>
			      </div>
			    </div>
			    <div class="form-group">
			      <label class="control-label col-sm-4" for="msg_notification">Write Your Messages:</label>
			      <div class="col-sm-6">          
			        <textarea class="form-control" rows="4" id="msg_notification"></textarea>
			        <span class="help-block" id="lblcount"></span>
			      </div>
			    </div>
			    <div class="form-group">        
			      <div class="col-sm-offset-8 col-sm-2">
			        <button type="submit" class="stybtn"><i class="fa fa-rocket"></i>&nbsp;&nbsp;Send</button>
			      </div>
			    </div>
		  </form>
		</div>
		</div>
		  <div class="panel panel-default">
			  <div class="panel-heading">
			  	<div><i class="fa fa-bell"></i><sub><i class="fa fa-eye"></i></sub> View Notifications</div>
			  </div>
			  <div class="panel-body">
			  	<table class="table table-bordered data-table display nowrap">
				    <thead>
				      <tr>
				        <th>Application Id</th>
				        <th>Received Date</th>
				        <th>Sent/Received</th>
				        <th>Description</th>
				      </tr>
				    </thead>
				    <tbody>
				      <tr>
				        <td>ABDFG4533ER</td>
				        <td>12-05-2017</td>
				        <td>Sent</td>
				        <td>Hi How are you</td>
				      </tr>
				      <tr>
				        <td>ABDGR573ER</td>
				        <td>08-06-2017</td>
				        <td>Received</td>
				        <td>call me, when your work is done</td>
				      </tr>
				      <tr>
				        <td>AGHTG483ER</td>
				        <td>07-07-2017</td>
				        <td>Sent</td>
				        <td>i'm busy write now please call me later</td>
				      </tr>
				    </tbody>
			  	</table>
			  </div>
		</div>
	</div>
  </div>
</div>

<script>


$(window).load(function() {
			// Animate loader off screen
			$(".se-pre-con").fadeOut("slow");;
		});	

</script>

</body>
</html>