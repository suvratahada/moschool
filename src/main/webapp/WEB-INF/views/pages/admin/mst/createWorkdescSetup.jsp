<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script src="./customJS/MasterSetup/createSubcategoryDescription.js"></script>
</head>
<body>
<div class="container-fluid entry-header">
	<h1 class="entry-title"><span class="glyphicon glyphicon-plus" style="color:#444;"></span>Subcategory Name Setup</h1>
</div>

<div class="container-fluid vh100">
	<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading"><div id="user_panel_heading" class="panel-title"><a href="#"><span class="glyphicon glyphicon-plus"></span></a> Subcategory Description Setup </div></div>
			
				<div class="panel-body">
					
					<form action="" theme="simple" name="formWorkDescDetails" id="formWorkDescDetails">
					
						<div class="form-group">
							<label>Category :</label>
							<select name="workcatg_id"  id = "workcatg_id" class="form-control"></select>
						</div>
						<div class="form-group">
							<label>Subcategory Name:</label>
							<input name="work_desc"  id = "work_desc" class="form-control">
						</div>
						
						<div class="form-action">
							<div class="row">
								<div class="col-xs-6 col-sm-6"><input type="reset" value="Reset" class="btn btn-default btn-block" onclick="resetForm_SubcatDesc()"></div>
								<div class="col-xs-6 col-sm-6"><input type="button" value="Add" class="btn btn-primary btn-block submit" id="subcatdesc_form_button"></div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<div class="col-xs-12 col-sm-12 col-md-9">
			<div class="panel panel-default">
				<div class="panel-heading"><div class="panel-title">Subcategory Details</div></div>
				<div class="panel-body">
					<table class="table table-condensed table-striped table-bordered display nowrap" id="tblSubcategoryDescDetails" width="100%">
						<thead>
							<tr>
								<th class="datatable-nosort text-center">Category</th>
								<th class="datatable-nosort text-center">Subcategory Name</th>
								<th class="datatable-nosort text-center">Work Desc. Id</th>
							    <th class="datatable-nosort text-center">Action</th>
							</tr>
						</thead>
					 <tbody></tbody>
					</table>
				</div>
			</div>
		</div>
		
	</div>
	
</div>
</body>
</html>