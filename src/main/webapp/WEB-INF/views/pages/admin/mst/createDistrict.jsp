<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" src="./customJS/MasterSetup/district.js"></script>
<title>Create District</title>
</head>
<body>
<div class="container-fluid entry-header">
	<h1 class="entry-title"><span class="glyphicon glyphicon-plus" style="color:#444;"></span> District</h1>
</div>

<div class="container-fluid vh100">
	<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading"><div id="user_panel_heading" class="panel-title"><a href="#"><span class="glyphicon glyphicon-plus"></span></a> District </div></div>
			
				<div class="panel-body">
					
					<form:form action="" theme="simple" name="formDistrictDetails" id="formDistrictDetails">
					
						<div class="form-group">
							<label>District Code:</label>
							<input type="text" name="district_id"  id = "district_id" class="form-control" autocomplete="off">
						</div>
						
						<div class="form-group">
							<label>District Name:</label>
							<input type="text" name="district_name"  id = "district_name" class="form-control" autocomplete="off">
						</div>	
						

						<div class="form-action">
							<div class="row">
								<div class="col-xs-6 col-sm-6"><input type="reset" value="Reset" class="btn btn-default btn-block" onclick="resetForm_CreateUser()"></div>
								<div class="col-xs-6 col-sm-6"><input type="button" value="Add" class="btn btn-primary btn-block submit" id="district_form_button"></button></div>
							</div>
						</div>
					</form:form>
				</div>
			</div>
		</div>
		
		
		
	
		<div class="col-xs-12 col-sm-12 col-md-9">
			<div class="panel panel-default">
				<div class="panel-heading"><div class="panel-title">District Details</div></div>
				<div class="panel-body">
					<table class="table table-condensed table-striped table-bordered display nowrap" id="tblDistrictDetails" width="100%">
						<thead>
							<tr>
								<!-- <th class="datatable-nosort text-center">District ID</th> -->
								<th class="datatable-nosort text-center">District Code</th>
								<th class="datatable-nosort text-center">District Name</th>
							    <th class="datatable-nosort text-center">Action</th>
							</tr>
						</thead>
					 <tbody></tbody>
					</table>
				</div>
			</div>
		</div>
		
	</div>
	 <div class="modal fade" id="editDistrictModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	        <h5 class="modal-title" id="exampleModalLabel">Update District</h5>
	      </div>
	      <div class="modal-body">
	      <div class="panel panel-default">
			<div class="panel-heading"><div id="user_panel_heading" class="panel-title"><a href="#"><span class="glyphicon glyphicon-plus"></span></a> District </div></div>
		
			<div class="panel-body">
		        <form:form action="" theme="simple" name="formDistrictUpdateDetails" id="formDistrictUpdateDetails">
					<div class="row">
						<div class="form-group">
							<div class="col-sm-12">
								<label>District Id:</label>
								<input type="text" name="district_id" id="district_id1" class="form-control">
							</div>
						</div>
					</div>
					<br>
					<br>
					<div class="row">	
						<div class="form-group">
							<div class="col-sm-12">
								<label>District Name:</label>
								<input type="text" name="district_name" id = "district_name1" class="form-control" autocomplete="off">
							</div>
						</div>
					</div>
				</form:form>
			</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="button" id="update-district" class="btn btn-primary">Save changes</button>
	      </div>
	    </div>
	  </div>
	</div>
</div>
</div>

</body>
</html>