<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script src="./customJS/MasterSetup/createSubcategory.js"></script>
</head>
<body>
<div class="container-fluid entry-header">
	<h1 class="entry-title"><span class="glyphicon glyphicon-plus" style="color:#444;"></span>Subcategory Setup</h1>
</div>

<div class="container-fluid vh100">
	<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading"><div id="user_panel_heading" class="panel-title"><a href="#"><span class="glyphicon glyphicon-plus"></span></a> Subcategory Setup </div></div>
			
				<div class="panel-body">
					
					<form action="" theme="simple" name="formSubcategoryDetails" id="formSubcategoryDetails">
					
						<div class="form-group">
							<select name="finyr_id"  id = "finyr_id" class="form-control"></select>
						</div>
						<div class="form-group">
							<label>Category :</label>
							<select name="workcatg_id"  id = "workcatg_id" class="form-control"></select>
						</div>
						<div class="form-group">
							<label>Subcategory:</label>
							<select name="sub_work_desc"  id = "sub_work_desc" class="form-control"></select>
						</div>
						<div class="form-group">
							<label>Cost Per Unit:</label>
							<input type="number" name="cost_pu"  id = "cost_pu" class="form-control">
						</div>
						<input type="hidden" id="work_desc_id" name="work_desc_id">
						<input type="hidden" id="work_desc" name="work_desc">
						<div class="form-action">
							<div class="row">
								<div class="col-xs-6 col-sm-6"><input type="reset" value="Reset" class="btn btn-default btn-block" onclick="resetForm_Subcategory()"></div>
								<div class="col-xs-6 col-sm-6"><input type="button" value="Add" class="btn btn-primary btn-block submit" id="subcategory_form_button"></div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		
		
	
		<div class="col-xs-12 col-sm-12 col-md-9">
			<div class="panel panel-default">
				<div class="panel-heading"><div class="panel-title">Subcategory Details</div></div>
				<div class="panel-body">
					<table class="table table-condensed table-striped table-bordered display nowrap" id="tblSubcategoryDetails" width="100%">
						<thead>
							<tr>
								<th class="datatable-nosort text-center">Work Desc ID</th>
								<th class="datatable-nosort text-center">Work ID</th>
								<th class="datatable-nosort text-center">Fin. Year</th>
								<th class="datatable-nosort text-center">Category</th>
								<th class="datatable-nosort text-center">Subcategory Name</th>
								<th class="datatable-nosort text-center">Cost Per Unit</th>
							    <th class="datatable-nosort text-center">Action</th>
							</tr>
						</thead>
					 <tbody></tbody>
					</table>
				</div>
			</div>
		</div>
		
	</div>
	 <div class="modal fade" id="editSubcategoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	        <h5 class="modal-title" id="exampleModalLabel">Update Subcategory</h5>
	      </div>
	      <div class="modal-body">
	      <div class="panel panel-default">
			<div class="panel-heading"><div id="user_panel_heading" class="panel-title"><a href="#"><span class="glyphicon glyphicon-plus"></span></a> Subcategory </div></div>
		
			<div class="panel-body">
				<form action="" theme="simple" name="formUpdateSubcategoryDetails" id="formUpdateSubcategoryDetails">
						<input type="hidden" id="update_work_id_hidden" name = "work_id">
						<div class="form-group">
							<select name="finyr_id"  id = "finyear" class="form-control"></select>
						</div>
						<div class="form-group">
							<label>Category :</label>
							<select name="workcatg_id"  id = "category" class="form-control"></select>
						</div>
						<div class="form-group">
							<label>Subcategory:</label>
							<select name="work_desc_id"  id = "subcategory_dd" class="form-control"></select>
						</div>
						<div class="form-group">
							<label>Cost Per Unit:</label>
							<input type="number" name="cost_pu"  id = "cost_per_unit" class="form-control">
						</div>
						<input type="hidden" id="work_desc_hidden" name="work_desc">
					</form>
					
			</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="button" id="update_subcategory" class="btn btn-primary">Update</button>
	      </div>
	    </div>
	  </div>
	</div>
</div>
</div>
</body>
</html>