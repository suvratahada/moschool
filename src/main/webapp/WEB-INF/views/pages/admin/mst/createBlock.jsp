<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" src="./customJS/MasterSetup/block.js"></script>
<title>Create Block</title>
</head>
<body>
<div class="container-fluid entry-header">
	<h1 class="entry-title"><span class="glyphicon glyphicon-plus" style="color:#444;"></span> Block</h1>
</div>

<div class="container-fluid vh100">
	<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading"><div id="user_panel_heading" class="panel-title"><a href="#"><span class="glyphicon glyphicon-plus"></span></a> Block </div></div>
			
				<div class="panel-body">
					
					<form:form action="" theme="simple" name="formBlockDetails" id="formBlockDetails">
					
						<div class="form-group">
							<label>Dist ID:</label>
							<select name="district_id"  id = "district_id" class="form-control">
							</select>
						</div>
						
						<div class="form-group">
							<label>Block Code:</label>
							<input type="text" name="block_id"  id = "block_id" class="form-control" autocomplete="off">
						</div>
						
						<div class="form-group">
							<label>Block Name:</label>
							<input type="text" name="block_name"  id = "block_name" class="form-control" autocomplete="off">
						</div>	
						
						<!-- <div class="col-md-6">
						<div class="form-group">
							<label>Rural Urban:</label>
							<input type="text" name="rural_urban"  id = "rural_urban" class="form-control" autocomplete="off">
						</div>
						</div> --><!-- end of class="col" -->

						<div class="form-action">
							<div class="row">
								<div class="col-xs-6 col-sm-6"><input type="reset" value="Reset" class="btn btn-default btn-block" onclick="resetForm_CreateUser()"></div>
								<div class="col-xs-6 col-sm-6"><input type="button" value="Add" class="btn btn-primary btn-block submit" id="block_form_button"></button></div>
							</div>
						</div>
					</form:form>
				</div>
			</div>
		</div>
		
		
		
	
		<div class="col-xs-12 col-sm-12 col-md-9">
			<div class="panel panel-default">
				<div class="panel-heading"><div class="panel-title">Block Details</div></div>
				<div class="panel-body">
					<table class="table table-condensed table-striped table-bordered display nowrap" id="tblBlockDetails" width="100%">
						<thead>
							<tr>
								<!-- <th class="datatable-nosort text-center">Block ID</th> -->
								<th class="datatable-nosort text-center">Dist ID</th>
								<th class="datatable-nosort text-center">Block Code</th>
								<th class="datatable-nosort text-center">Block Name</th>
								<!-- <th class="datatable-nosort text-center">Rural Urban</th> -->
							    <th class="datatable-nosort text-center">Action</th>
							</tr>
						</thead>
					 <tbody></tbody>
					</table>
				</div>
			</div>
		</div>
		
	</div>
	 <div class="modal fade" id="editBlockModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	        <h5 class="modal-title" id="exampleModalLabel">Update Block</h5>
	      </div>
	      <div class="modal-body">
	      <div class="panel panel-default">
			<div class="panel-heading"><div id="user_panel_heading" class="panel-title"><a href="#"><span class="glyphicon glyphicon-plus"></span></a> Block </div></div>
		
			<div class="panel-body">
					
					<form:form action="" theme="simple" name="formUpdateBlockDetails" id="formUpdateBlockDetails">
					
						<div class="form-group">
							<label>District ID:</label>
							<input name="district_id"  id = "district_id1" class="form-control">
						</div>
						
						<div class="form-group">
							<label>Block Code:</label>
							<input type="text" name="block_id"  id = "block_id1" class="form-control" autocomplete="off">
						</div>
						
						<div class="form-group">
							<label>Block Name:</label>
							<input type="text" name="block_name"  id = "block_name1" class="form-control" autocomplete="off">
						</div>	
					</form:form>
			</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="button" id="update-block" class="btn btn-primary">Save changes</button>
	      </div>
	    </div>
	  </div>
	</div>
</div>
</div>

</body>
</html>