<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script src="./customJS/MasterSetup/createFinyear.js"></script>
</head>
<body>
<div class="container-fluid entry-header">
	<h1 class="entry-title"><span class="glyphicon glyphicon-plus" style="color:#444;"></span>Financial Year</h1>
</div>

<div class="container-fluid vh100">
	<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading"><div id="user_panel_heading" class="panel-title"><a href="#"><span class="glyphicon glyphicon-plus"></span></a> Financial Year </div></div>
			
				<div class="panel-body">
					
					<form action="" theme="simple" name="formFinYearDetails" id="formFinYearDetails">
					
						<input type="hidden" name="year_id"  id = "year_id" class="form-control"> 
						
						<div class="form-group">
							<label>Year Desc:</label>
							<input type="text" name="year_desc"  id = "year_desc" class="form-control" autocomplete="off">
						</div>
						
						<div class="form-action">
							<div class="row">
								<div class="col-xs-6 col-sm-6"><input type="reset" value="Reset" class="btn btn-default btn-block" onclick="resetForm_CreateYear()"></div>
								<div class="col-xs-6 col-sm-6"><input type="button" value="Add" class="btn btn-primary btn-block submit" id="year_form_button"></div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		
		
	
		<div class="col-xs-12 col-sm-12 col-md-9">
			<div class="panel panel-default">
				<div class="panel-heading"><div class="panel-title">Year Details</div></div>
				<div class="panel-body">
					<table class="table table-condensed table-striped table-bordered display nowrap" id="tblYearDetails" width="100%">
						<thead>
							<tr>
								<th class="datatable-nosort text-center">Year ID</th>
								<th class="datatable-nosort text-center">Year Desc</th>
							    <th class="datatable-nosort text-center">Action</th>
							</tr>
						</thead>
					 <tbody></tbody>
					</table>
				</div>
			</div>
		</div>
		
	</div>
	 <div class="modal fade" id="editYearModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	        <h5 class="modal-title" id="exampleModalLabel">Update Year</h5>
	      </div>
	      <div class="modal-body">
	      <div class="panel panel-default">
			<div class="panel-heading"><div id="user_panel_heading" class="panel-title"><a href="#"><span class="glyphicon glyphicon-plus"></span></a> Financial Year </div></div>
		
			<div class="panel-body">
					
					<form action="" theme="simple" name="formUpdateYearDetails" id="formUpdateYearDetails">
							<input type="hidden" name="year_id"  id = "year_id_hidden">
						
						<div class="form-group">
							<label>Year Desc:</label>
							<input type="text" name="year_desc"  id = "year_desc1" class="form-control" autocomplete="off">
						</div>
					</form>
			</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="button" id="update-year" class="btn btn-primary">Save changes</button>
	      </div>
	    </div>
	  </div>
	</div>
</div>
</div>
</body>
</html>