<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" src="./customJS/MasterSetup/createSchool.js"></script>    
<!--   <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> -->
<!-- <script src="./customJS/iCheck/icheck.min.js" type="text/javascript"></script> 

<link rel="stylesheet" type="text/css" href="./customCSS/iCheck/all.css" /> -->

<title>createSchool</title>
<body>
<div class="container-fluid entry-header">
	<h1 class="entry-title"> <a href="#"><span class="glyphicon glyphicon-user"></span></a> School</h1>
</div>

<div class="container-fluid vh100">
	<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading"><div id="user_panel_heading" class="panel-title"><a href="#"><span class="glyphicon glyphicon-plus"></span></a> School </div></div>
			
				<div class="panel-body">
					
					<form:form action="" theme="simple" name="formSchoolDetails" id="formSchoolDetails">
						<div class="row">
							<div class="form-group">
								<div class="col-sm-12">
									<select id="block_id" name="block_id" class="form-control"></select>
								</div>
							</div>
						</div>
						<br>
						<div class="row">		
							<div class="form-group">
								<div class="col-sm-12">
									<label>School Id:</label>
									<input type="text" name="school_id"  id = "id" class="form-control" autocomplete="off">
								</div>
							</div>
						</div>
						<br>
						<div class="row">	
							<div class="form-group">
								<div class="col-sm-12">
									<label>School Name:</label>
									<input type="text" name="school_name"  id = "lower_class" class="form-control" autocomplete="off">
									<input type="hidden" name="record_status" id = "record_status" value=1>
								</div>
							</div>
						</div>
						<br>
						<div class="form-action">
							<div class="row">
								<div class="col-xs-6 col-sm-6"><input type="reset" value="Reset" class="btn btn-default btn-block" onclick="resetForm_CreateSchool()"></div>
								<div class="col-xs-6 col-sm-6"><input type="button" value="Add" class="btn btn-primary btn-block submit" id="school_form_button" ></div>
							</div>
						</div>
					</form:form>
				</div>
			</div>
		</div>
		
		
		
	
		<div class="col-xs-12 col-sm-12 col-md-9">
			<div class="panel panel-default">
				<div class="panel-heading"><div class="panel-title">School</div></div>
				<div class="panel-body">
					<table class="table table-condensed table-striped table-bordered display nowrap" id="tblSchoolDetails" width="100%">
						<thead>
							<tr>
								<th class="datatable-nosort text-center">School Id</th>
								<th class="datatable-nosort text-center">School Name</th>
								<th class="datatable-nosort text-center">Block Id</th>
								<th class="datatable-nosort text-center">Block Name</th>
								<th class="datatable-nosort text-center">Actions</th>
							</tr>
						</thead>
					 <tbody></tbody>
					</table>
				</div>
			</div>
		</div>
		
	</div>
	 <div class="modal fade" id="editSchoolModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	        <h5 class="modal-title" id="exampleModalLabel">Update School</h5>
	      </div>
	      <div class="modal-body">
	      <div class="panel panel-default">
			<div class="panel-heading"><div id="user_panel_heading" class="panel-title"><a href="#"><span class="glyphicon glyphicon-plus"></span></a> School </div></div>
		
			<div class="panel-body">
		        <form:form action="" theme="simple" name="formSchoolUpdateDetails" id="formSchoolUpdateDetails">
		        <input type="hidden" id="school_id" name="school_id">
					<div class="row">
						<div class="form-group">
							<div class="col-sm-12">
								<label>Block Id:</label>
								<input type="text" name="block_id" id="block_id1" class="form-control">
							</div>
						</div>
					</div>
					<br>
					<br>
					<div class="row">	
						<div class="form-group">
							<div class="col-sm-12">
								<label>School Name:</label>
								<input type="text" name="school_name" id = "school_name1" class="form-control" autocomplete="off">
							</div>
						</div>
					</div>
				</form:form>
			</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="button" id="update-school" class="btn btn-primary">Save changes</button>
	      </div>
	    </div>
	  </div>
	</div>
</div>
</div>
</body>
</html>