<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" src="./customJS/mdm/categorySetup.js"></script>
<title>Category Master Setup</title>
</head>
<body>
<div class="container-fluid entry-header">
	<h1 class="entry-title"><span class="glyphicon glyphicon-plus" style="color:#444;"></span> Category </h1>
</div>

<div class="container-fluid vh100">
	<div class="row col-sm-4" style="margin-bottom: 10px">
		<button class="btn btn-success btn-md" data-toggle="modal" data-target="#categoryModal"><i class="fa fa-plus"></i></button>
	</div>
	
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading"><div class="panel-title">Category Details</div></div>
				<div class="panel-body">
					<table class="table table-condensed table-striped table-bordered display nowrap" id="tblCategoryDetails" width="100%">
						<thead>
							<tr>
								<!-- <th class="datatable-nosort text-center">Block ID</th> -->
								<th class="datatable-nosort text-center">Category ID</th>
								<th class="datatable-nosort text-center">Category Desc</th>
							    <th class="datatable-nosort text-center">Action</th>
							</tr>
						</thead>
					 <tbody></tbody>
					</table>
				</div>
			</div>
		</div>
		
	</div>
	 <div class="modal fade" id="categoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleCategoryLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	        <h5 class="modal-title" id="exampleModalLabel">Update Category</h5>
	      </div>
	      <div class="modal-body">
			<div class="panel-body">
					<div class="panel panel-default">
				<div class="panel-heading"><div id="user_panel_heading" class="panel-title"><a href="#"><span class="glyphicon glyphicon-plus"></span></a> Category </div></div>
			
				<div class="panel-body">
					
					<form:form action="" theme="simple" name="formCategoryDetails" id="formCategoryDetails">
					
						<div class="form-group">
							<label>Category ID:</label>
							<input type="text" name="catg_id"  id = "catg_id" class="form-control" autocomplete="off">
						</div>
						
						<div class="form-group">
							<label>Category Desc:</label>
							<input type="text" name="catg_desc"  id = "catg_desc" class="form-control" autocomplete="off">
						</div>

						<div class="form-action">
							<div class="row">
								<div class="col-xs-6 col-sm-6"><input type="reset" value="Reset" class="btn btn-default btn-block" onclick="resetForm_Category()"></div>
								<div class="col-xs-6 col-sm-6"><input type="button" value="Add" class="btn btn-primary btn-block submit" id="submit_category"></button></div>
							</div>
						</div>
					</form:form>
				</div>
			</div>
					
			</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>
</div>

</body>
</html>