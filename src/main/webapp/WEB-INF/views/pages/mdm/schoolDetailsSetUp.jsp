<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" src="./customJS/mdm/schoolDetailsSetup.js"></script>
<title>School Other Details</title>
</head>
<body>
<div class="container-fluid entry-header">
	<h1 class="entry-title" ><span class="glyphicon glyphicon-plus" style="color:#444;"></span>School Other Details</h1>
</div>

<div class="container-fluid vh100">
<button id="addSchoolDetails" class="btn btn-success" style="margin-bottom: 1%;"><span class="glyphicon glyphicon-plus" style="color:white; "></span> Add</button><br>
	<div class="row">
	
			<!-- Modal -->
			<div class="modal fade" id="schoolDetailsSetupModal" role="dialog" style="color: black;">
				<div class="modal-dialog modal-lg">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h3 class="modal-title">School Other Details</h3>
						</div>
						<div>
							<div class="col-xs-12 col-sm-12 col-md-3 col-lg-12">
										
								<form:form action="" theme="simple" name="formSchoolDetails" id="formSchoolDetails" style="margin-top: 2%;">

									
									<div class="form-group">
										<label>UDISE Code:</label>
										<input type="text" name="udiseCode"  id = "udiseCode" class="form-control" autocomplete="off">
									</div>
									
														
									<div class="form-group">
										<label>School Id:</label>
										<input type="text" name="schoolId"  id = "schoolId" class="form-control" />
									</div>
									
									<div class="form-group">
										<label>Affiliation Board:</label>
										<input type="text" name="affiliationBoard"  id = "affiliationBoard" class="form-control" />
									</div>
									
									<div class="form-group">
										<label>School Affiliation Code:</label>
										<input type="text" name="schoolAffiliatedCode"  id = "schoolAffiliatedCode" class="form-control" />
									</div>
									
									<div class="form-group">
										<label>Medium Language(Id):</label>
										<input type="text" name="mediumLanguageId"  id = "mediumLanguageId" class="form-control" />
									</div>
			
									<div class="form-action" style="text-align: center;">
									
										<div class="col-xs-6 col-sm-6 col-lg-6">
											<div class="col-xs-6 col-sm-6 col-lg-6">
												<input style="margin-left: 50%" type="button" value="Reset" class="btn btn-default btn-block" id="reset">
											</div>
										</div>
										
										<div class="col-xs-6 col-sm-6 col-lg-6">
											<div class="col-xs-6 col-sm-6 col-lg-6">
												<input style="margin-left: 50%" type="button" value="Add" class="btn btn-primary btn-block submit" id="schoolDetailsFormButton">
											</div>
										</div>
										
									</div>
								</form:form>							
						</div>
						<div class="modal-footer">
							<!-- <button type="button" class="btn btn-default" >Close</button> -->
						</div>
					</div>

				</div>
			</div>
	</div>

			<div class="col-xs-12 col-sm-12 col-md-9 col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"><div class="panel-title">School Other Details</div></div>
				<div class="panel-body">
					<table class="table table-condensed table-striped table-bordered display nowrap" id="tblSchoolDetails" width="100%">
						<thead>
							<tr>
								<th class="datatable-nosort text-center">UDISE Code</th>
								<th class="datatable-nosort text-center">School Id</th>
							    <th class="datatable-nosort text-center">Affiliation Board</th>
								<th class="datatable-nosort text-center">School Affiliation Code</th>
								<th class="datatable-nosort text-center">Medium Language(Id)</th>
							    <th class="datatable-nosort text-center">Action</th>
							</tr>
						</thead>
					 <tbody></tbody>
					</table>
				</div>
			</div>
		</div>
		
	</div>
</div>

</body>
</html>