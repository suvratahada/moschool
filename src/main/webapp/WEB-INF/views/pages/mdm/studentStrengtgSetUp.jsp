<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" src="./customJS/mdm/studentStrengthSetup.js"></script>
<title>Student Strength</title>
</head>
<body>
<div class="container-fluid entry-header">
	<h1 class="entry-title" ><span class="glyphicon glyphicon-plus" style="color:#444;"></span>Student Strength</h1>
</div>

<div class="container-fluid vh100">
<button id="addStudentStrength" class="btn btn-success" style="margin-bottom: 1%;"><span class="glyphicon glyphicon-plus" style="color:white; "></span> Add</button><br>
	<div class="row">
	
			<!-- Modal -->
			<div class="modal fade" id="studentStrengthSetupModal" role="dialog" style="color: black;">
				<div class="modal-dialog modal-lg">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h3 class="modal-title">Student Strength</h3>
						</div>
						<div>
							<div class="col-xs-12 col-sm-12 col-md-3 col-lg-12">
										
								<form:form action="" theme="simple" name="formStudentStrength" id="formStudentStrength" style="margin-top: 2%;">

									
									<div class="form-group">
										<label>Class:</label>
										<input type="text" name="classNo"  id = "classNo" class="form-control" autocomplete="off">
									</div>
									
														
									<div class="form-group">
										<label>Section:</label>
										<input type="text" name="section"  id = "section" class="form-control" />
									</div>
									
									<div class="form-group">
										<label>Male:</label>
										<input type="text" name="male"  id = "male" class="form-control" />
									</div>
									
									<div class="form-group">
										<label>Female:</label>
										<input type="text" name="feMale"  id = "feMale" class="form-control" />
									</div>
			
									<div class="form-action" style="text-align: center;">
									
										<div class="col-xs-6 col-sm-6 col-lg-6">
											<div class="col-xs-6 col-sm-6 col-lg-6">
												<input style="margin-left: 50%" type="button" value="Reset" class="btn btn-default btn-block" id="reset">
											</div>
										</div>
										
										<div class="col-xs-6 col-sm-6 col-lg-6">
											<div class="col-xs-6 col-sm-6 col-lg-6">
												<input style="margin-left: 50%" type="button" value="Add" class="btn btn-primary btn-block submit" id="studentStrengthFormButton">
											</div>
										</div>
										
									</div>
								</form:form>							
						</div>
						<div class="modal-footer">
							<!-- <button type="button" class="btn btn-default" >Close</button> -->
						</div>
					</div>

				</div>
			</div>
		</div>

			<div class="col-xs-12 col-sm-12 col-md-9 col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading"><div class="panel-title">School Other Details</div></div>
				<div class="panel-body">
					<table class="table table-condensed table-striped table-bordered display nowrap" id="tblStudentStrength" width="100%">
						<thead>
							<tr>
								<th class="datatable-nosort text-center">Class</th>
								<th class="datatable-nosort text-center">Section</th>
							    <th class="datatable-nosort text-center">Male</th>
								<th class="datatable-nosort text-center">Female</th>
								<th class="datatable-nosort text-center">Action</th>
							</tr>
						</thead>
					 <tbody></tbody>
					</table>
				</div>
			</div>
		</div>
		
	</div>
</div>

</body>
</html>