<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="./customCSS/pma/districtpma.css">
<link href="./customCSS/blockLevelTrack.css" rel="stylesheet" type="text/css" />
<script src="./customJS/pma/blockReAllocation.js"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-heading panel-opepa">
					<div class="panel-title">Budget Allocation Details</div>
				</div>
				<div class="panel-body">
				<div class="row">
					<!-- <div class="pull-right">
						<button class="btn btn-success btn-sm btn-padding" data-toggle="modal" data-target="#districtMonitoringModal" id="new_btn"><i class="fa fa-plus"></i>&nbsp;New</button>
					</div> -->
				</div>
				<div class="filter-wrap">
					<div class="row">
					<div class="col-sm-3">	
						<select id="finyear" class="form-control">
						</select>
					</div>
					<div class="col-sm-3">	
						<select id="subcategoryfilter" class="form-control">
						</select>
					</div>
					
					</div>
				</div>
				<div class="overflow-opepa">
					<table class="table table-bordered table-striped table-responsive table-hover" id="tblBlockSchoolReAllocationDetails" width="100%">
						<thead>
							<tr>
								<th>TC Actions</th>
								<th>Project Id</th>
								<th>Scheme Name</th>
								<th>School Code</th>
								<th>School</th>
								<th>Category</th>
								<th>Sub Category</th>
								<th>Financial Year</th>
								<th>Allocated Units</th>
								<th>Unit Cost</th>
								<th>Approved Amount</th>
								<th>Balance Amount</th>
								<th>Released Amount</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				</div> <!-- end of panel body -->
			</div>
		</div>
	</div>
		<!-- View Tracking School Model START-->
	   <div class="modal fade" id="releaseAmountTrackingModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
	       <div class="modal-dialog modal-lg">
	           <div class="modal-content">
	               <div class="modal-header modal-opepa-header">
	                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	                   <h4 class="modal-title" id="">Fund Allocation Phase Wise</h4>
	               </div>
	               <div class="modal-body">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									 <div class="card-inner">
	                                    <div class="timeline-block">
		                                        <!--Timeline-->
		                                        <div class="time-bar" id="timeBar"></div>
		                                        <div id="timelineDynamic"></div>
	                                    </div>
	                                </div>
								</div>
							</div>
		                </div>
		           </div>
		       </div>
		   </div>
		   
</body>
</html>