<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="./customCSS/pma/districtpma.css">
<script src="./customJS/pma/adminLevelProjectMonitoring.js"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-heading panel-opepa">
					<div class="panel-title">Budget Allocation Details</div>
				</div>
				<div class="panel-body">
				<div class="filter-wrap">
					<div class="row">
					<div class="col-sm-3">
						<select id="schemeFilter"  class="form-control" title="Scheme">
						</select>
					</div>
					<div class="col-sm-3">
						<select id="districtfilter"  class="form-control" title="District">
						</select>
					</div>
					<div class="col-sm-3">	
						<select id="blockfilter" class="form-control" title="Block" disabled>
						</select>
					</div>
					<div class="col-sm-3">	
						<select id="finyear" class="form-control" title="Financial Year">
						</select>
					</div>
					</div>
				</div>
				<div class="excel-btn-container">
					<a href="javascript:void(0)" class="btn btn-success btn-sm" id="admin-allocation-excel"><i class="fa fa-file-excel-o"></i></a>
				</div>
				<div class="overflow-opepa">
					<table class="table table-bordered table-striped table-responsive table-hover" id="tblDistrictSchoolMonitoringDetails" width="100%">
						<thead>
							<tr>
								<th>District</th>
								<th>Block</th>
								<th>School Name</th>
								<th>School Code</th>
								<th>Financial Year</th>
								<th>Sub Category</th>
								<th>Total Units</th>
								<th>Unit Cost</th>
								<th>Approved Amount</th>
								<th>Balance Amount</th>
								<th>Project ID</th>
								<th>Scheme Name</th>
								<th>Category</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				</div> <!-- end of panel body -->
			</div>
		</div>
	</div>
</body>
</html>