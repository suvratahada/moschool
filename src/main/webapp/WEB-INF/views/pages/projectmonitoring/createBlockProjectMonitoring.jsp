<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="./customCSS/blockLevelTrack.css" rel="stylesheet" type="text/css" />
<link href="./customCSS/pma/projectMonitoringBlockCustom.css" rel="stylesheet" type="text/css" />
<script src="./customJS/pma/blockMonitoring.js"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-heading" style="background-color: #0f3e12;color: #ffffff;"><div class="panel-title">Budget Allocation Details</div></div>
				<div class="panel-body">
				<div class="filter-wrap">
					<div class="row">
					<div class="col-sm-3">	
						<select id="finyearblockfilter" class="form-control"></select>
					</div>
					<div class="col-sm-3">
						<select id="subcategoryfilter"  class="form-control"></select>
					</div>
					</div>
				</div>
				<br/>
				<div class="row" style="padding-left: 2em; padding-right: 2em">
					<table class="table table-bordered table-striped table-responsive table-hover" id="tblBlockSchoolMonitoringDetails" width="100%">
						<thead>
							<tr>
								<th>TC Actions</th>
								<th>Project Id</th>
								<th>Scheme Name</th>
								<th>School Name</th>
								<th>Category</th>
								<th>Sub Category</th>
								<th>Financial Year</th>
								<th>Total Units</th>
								<th>Unit Cost</th>
								<th>Approved Amount</th>
								<th>Balance Amount</th>
								<th>Release Amount</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				</div> <!-- end of panel body -->
			</div>
		</div>
		
	</div>
	<!-- View Tracking School Model START-->
	   <div class="modal fade" id="releaseAmountTrackingModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
	       <div class="modal-dialog modal-lg">
	           <div class="modal-content">
	               <div class="modal-header modal-opepa-header">
	                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	                   <h4 class="modal-title" id="">Fund Allocation Phase Wise</h4>
	               </div>
	               <div class="modal-body">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									 <div class="card-inner">
	                                    <div class="timeline-block">
		                                        <!--Timeline-->
		                                        <div class="time-bar" id="timeBar"></div>
		                                        <div id="timelineDynamic"></div>
	                                    </div>
	                                </div>
								</div>
							</div>
		                </div>
		           </div>
		       </div>
		   </div>
</body>
</html>