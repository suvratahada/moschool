<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="./customCSS/pma/districtpma.css">
<script src="./customJS/pma/adminReallocationView.js"></script>
</head>
<body>
<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-heading panel-opepa">
					<div class="panel-title">Budget Re-Allocation Details</div>
				</div>
				<div class="panel-body">
				<div class="filter-wrap">
					<div class="row">
					<div class="col-sm-3">
						<select id="schemeFilter"  class="form-control" title="Scheme">
						</select>
					</div>
					<div class="col-sm-3">
						<select id="districtFilter"  class="form-control" title="District">
						</select>
					</div>
					<div class="col-sm-3">	
						<select id="blockFilter" class="form-control" title="Block" disabled>
						</select>
					</div>
					<div class="col-sm-3">	
						<select id="finyearFilter" class="form-control" title="Financial Year">
						</select>
					</div>
					</div>
				</div>
				<div class="excel-btn-container">
					<a href="javascript:void(0)" class="btn btn-success btn-sm" id="admin-reallocation-excel"><i class="fa fa-file-excel-o"></i></a>
				</div>
				<div class="overflow-opepa">
					<table class="table table-bordered table-striped table-responsive table-hover" id="adminLevelTotalReallocationViewTable" width="100%">
						<thead>
							<tr>
								<th>Block Name</th>
								<th>School Code</th>
								<th>From School</th>
								<th>To School Code</th>
								<th>To School</th>
								<th>Financial Year</th>
								<th>Sub Category</th>
								<th>Allocated Units</th>
								<th>Unit Cost</th>
								<th>Approved Amount</th>
								<th>Balance Amount</th>
								<th>Released Amount</th>
								<th>Project Id</th>
								<th>Scheme Name</th>
								<th>Category</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				</div> <!-- end of panel body -->
			</div>
		</div>
	</div>
</body>
</html>