<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<script src="./customJS/pma/blockInspection.js"></script>
<script src = "./customJS/pma/inspectionModal.js"></script>
<link rel="stylesheet" href="./customCSS/pma/blockInspection.css">
</head>
<body>
<div class="container">
<div class="row">
	<div class="panel panel-default">
		<div class="panel-heading panel-opepa">
			<div class="panel-title">Monitoring of civil construction</div>
		</div>
		<div class="panel-body">
			
		<div class="row filter-wrap">
			<label for="school_filter">Please select a school: </label>
			<select id="school_filter" class="form-control"></select>
		</div>
		<div id="btn-desc">
			<div class="btn btn-danger btn-sm"><i class="fa fa-send"></i></div><label>&nbsp;: Start Project</label>&nbsp;&nbsp;
			<div class="btn btn-info btn-sm"><i class="fa fa-info"></i></div><label>&nbsp;: Inspect Project</label>
			<br/>
			<div style="margin-top: 20px">
				<!-- <label class="gpscheckbox" for="gpstoggle">&nbsp;
					<input type="checkbox" id="gpstoggle" class="btn btn-success btn-sm">
					:Disable Geo-location
				</label> -->
				<label class="gps-chk-container">
				  <input type="checkbox" id="gpstoggle">
				  <span class="checkmark"></span>
				  <span class="chk-label">:Disable Geo-location</span>
				</label>
			</div>
		</div>
		<br/>
		
		<div class="append-cards"></div>
		</div>
		<!-- Project Start Date Modal-->
	   <div class="modal fade" id="projectStartDateModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true" data-keyboard="false" data-backdrop="static">
	       <div class="modal-dialog">
	           <div class="modal-content">
	               <div class="modal-header modal-opepa-header">
	                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	                   <h4 class="modal-title modal-opepa-header" id="">Start Project</h4>
	               </div>
	               <div class="modal-body">
	               <input type="hidden" id="longitude">
	               <input type="hidden" id="latitude">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<form id="projectStartDateForm" name="projectStartDateForm">
							<!-- For setting hidden fields  -->
							<input type="hidden" name="finyr_id" id="hidden_fin_year_id">
							<input type="hidden" name="work_id" id="hidden_work_id">
							<input type="hidden" name="scheme_id" id="hidden_scheme_id">
							<input type="hidden" name="work_desc_id" id="hidden_work_desc_id">
							<input type="hidden" name="total_amount_approved" id="hidden_amount_approved">
							<input type="hidden" name="total_amount_released" id="hidden_amount_released">
							<input type="hidden" id="hidden_school_id">
							<fieldset>
							<legend class="pma-header">Project Details</legend>
							<div class="row">
								<div class="form-group col-sm-6" style="display: flex">
									 <div><label for="project_id">Project ID: </label></div>
									 <input id="project_id" name="project_id" class="project-border" readonly>
								</div>
								<div class="form-group col-sm-6 flex">
									 <div><label for="school_id">School ID: </label></div>
									 <div id="school_id" class="ml-5"></div>
								</div>
							</div>
								<div class="form-group flex">
									 <div><label for="school_name">School Name: </label></div>
									 <div id="school_name" class="ml-5"></div>
								</div>
								<div class="form-group flex">
									 <div><label for="finyear">Financial Year: </label></div>
									 <div class="col-sm-6" id="finyear" class="ml-5"></div>
								</div>
								<div class="form-group flex">
									 <div><label for="subcategory">Activity: </label></div>
									 <div id="subcategory" class="ml-5"></div>
								</div>
								<div class="form-group flex">
									 <div><label for="released_amount">Released Amount: </label></div>
									 <div id="released_amount" class="ml-5"></div>
								</div>
								<hr/>
							<div class="row">
								<div class="form-group col-sm-6">
								<p>Is the released amount received?</p>
									  <p>
									    <input type="radio" id="test1" name="yesno"  class="onselection" value="1">
									    <label for="test1">Yes</label>
									  </p>
									  <p>
									    <input type="radio" id="test2" value="0" name="yesno">
									    <label for="test2">No</label>
									  </p>
								</div>
							</div>
								<div class="dynamic_start_date_field"></div>
								</fieldset>
							</form>
							</div>
		                </div>
		           </div>
		           <div class="modal-footer">
		           	<button type="button" class="btn btn-primary" id="projectStartDateModalSubmit">Start</button>
		           	<button class="btn btn-default" id="closeModal" data-dismiss="modal">Close</button>
		           </div>
		       </div>
		   </div>
		</div>
		
		<!-- Inspection Modal Type Building-->
	   <div class="modal fade" id="projectInspectionModalBuilding" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true" data-keyboard="false" data-backdrop="static">
	       <div class="modal-dialog modal-lg">
	           <div class="modal-content">
	               <div class="modal-header modal-opepa-header">
	                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color: #fff">x</span></button>
	                   <h4 class="modal-title modal-opepa-header" id="">Inspection</h4>
	               </div>
	               <div class="modal-body">
	               		<div id="myProgress" class="progress-bar active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
						  <div id="myBar" class="progress-bar progress-bar-striped active">
						  	<label id="progressLabel">10%</label>
						  </div>
						</div>
		               <div class="detailsOfProject">
		               	<div>Project Id: <span id="projectId"></span></div>
		               	<div>School Id: <span id="schoolId"></span></div>
		               	<div>School Name: <span id="schoolName"></span></div>
		               	<div>Financial Year: <span id="financialYear"></span></div>
		               	<div>Sub-category: <span id="subcategoryName"></span></div>
		               </div>
						<div class="containerr">
		                <div class="tab-head">
		                <!-- <div id="myProgress" class="progress-bar active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
						  <div id="myBar" class="progress-bar progress-bar-striped active">
						  	<label id="progressLabel">10%</label>
						  </div>
						</div> -->
		                    <div class="list-group">
		                        <button  class="list-group-item active text-center" title="Fund&nbsp;Withdrawn"><span>1</span>Fund Withdrawn</button>
				                <button  class="list-group-item text-center" title="Layout"><span>2</span>Layout</button>
				                <button  class="list-group-item text-center" title="Foundation"><span>3</span>Foundation</button>
				                <button  class="list-group-item text-center" title="Plinth"><span>4</span><p>Plinth</p></button>
				                <button  class="list-group-item text-center" title="Lintel"><span>5</span><p>Lintel</p></button>
				                <button  class="list-group-item text-center" title="Roof"><span>6</span><p>Roof</p></button>
				                <button  class="list-group-item text-center" title="Roof&nbsp;Cast"><span>7</span>Roof&nbsp;Cast</button>
				                <button  class="list-group-item text-center" title="Finishing"><span>8</span>Finishing</button>
				                <button  class="list-group-item text-center" title="Completed"><span>9</span>Completed</button>
				                <button  class="list-group-item text-center" title="Checks & Measure"><span>10</span>Checks</button>
				                <button  class="list-group-item text-center" title="Handed Over"><span>11</span>Handed</button>
				                <button  class="list-group-item text-center" title="UC Submitted"><span>12</span>Submitted</button>
				                <button  class="list-group-item text-center" title="UC Approved"><span>13</span>Approved</button>
		                    </div>
		                </div>
		                <div class="tab-content">
		                	<div style="float: right; font-size: 10px; font-weight: bold">(*Amount in lakhs.)</div>
		                    <div class="tab-pane active" role="tabpanel" id="step1">
		                        <div class="content-text">
		                        <form class="bform" id="blockInspectionform1" name="blockInspectionform1">
		                        <input type="hidden" id="construction_level_id1" name="construction_level_id" value=1>
		                        <input type="hidden" name="finyr_id" id="hidden_inspection_finyear_id_step1">
		                        <input type="hidden" name="project_id" id="hidden_inspection_project_id_step1">
		                        <input type="hidden" name="work_id" id="hidden_inspection_work_id_step1">
								<input type="hidden" name="approved_amount" id="hidden_approved_amount_step1">
								<input type="hidden" name="released_amount" id="hidden_released_amount_step1">
								
								<div class = "amount_div col-sm-6">
					        		<div class="form-group">
						        		<div>Total Amount Withdrawn Till Date:<div class="text-background" id="hidden_expended_amount_step1">0.0</div></div>
						        		
					        		</div>
					        		<div class="form-group">
						        		<div>Balance Amount: <div class="text-background" id="hidden_balance_amount_step1">0.0</div></div>
					        		</div>
					        		<div class="form-group">
						        		<div>Last Inspected Date: <div class="text-background" id="hidden_last_inspection_date_step1"></div></div>
					        		</div>
								</div>
								
								<div class="option_div col-sm-6">
					                <div class="form-group">
						                <p>Is Fund withdrawn?</p>
						                <p>
						                <input type="radio" class="fundrb" id="test01" name="fund" value="1">
						                <label for="test01">Yes</label>
						                </p>
						                <p>
						                <input type="radio" class="fundrb" id="test02" value="0" name="fund">
						                <label for="test02">No</label>
						                </p>
					                </div>
					             </div>
					                <div id="fundreleaseyes"  class="onselection">
						                <hr/>
										<div class="col-sm-6">
							        		<div class="form-group">
								        		<div>Level Completion Date: </div>
								        		<input type="text" name="inspection_date" class="form-control recieved_date" readonly>
							        		</div>
						        		</div>
						        		<div class="form-group col-sm-6">
					        			<div class="upload-btn-wrapper"> Take Photograph:
										  <button class="btn-file btn-sm"><i class="fa fa-camera"></i></button>
										  <img id="blah1" src="" alt=""  style="width: 25px; height: 25px;"/>
										  <input type="file" id="imgInp1" name="inspection_img1" accept="image/*" capture="camera"/>
										</div>
					        				<!-- <input type="file" name="inspection_img1" id="inspection_img1_step1" accept="image/*" capture="camera"> -->
					        			</div>
					                </div>
					                
					                <div id="fundreleaseno"  class="onselection" style="display: none">
					                	<hr/>
							        		<div class="form-group col-sm-6">
								        		<div>Reason for delay: </div>
								        		<select id="fund_delay_reason" name="delay_reason_id" class="form-control delay_reason"></select>
							        		</div>
					                </div>
					                
					                <div id="fundremarks" class="onselection">
						        		<div class="form-group col-sm-6">
							        		<div>Amount withdrawn: </div>
							        		<input id="amount_expended1" class="form-control" value=0>
							        		<input type="hidden" name="amount_expended" id="input_amount_expended1">
						        		</div>
					        			
							        		<div class="form-group col-sm-6">
							        		<div>Remarks: </div>
							        		<textarea  name="remarks" class="form-control"></textarea>
							        		</div>
						        		<div class="row"><div class="pos-btn"><button type="button" class="nextTab btn btn-success btn-md" id="bfundreleaseSubmit">Save</button></div></div>
					                </div>
				                
				                </form>
		                        </div>
		                    </div>
		                    <div class="tab-pane" role="tabpanel" id="step2">
		                        <div class="content-text">
		                        <form class="bform" id="blockInspectionform2" name="blockInspectionform2">
		                        <input type="hidden" id="construction_level_id2" name="construction_level_id" value=2>
		                        <input type="hidden" name="finyr_id" id="hidden_inspection_finyear_id_step2">
		                        <input type="hidden" name="project_id" id="hidden_inspection_project_id_step2">
		                        <input type="hidden" name="work_id" id="hidden_inspection_work_id_step2">
								<input type="hidden" name="approved_amount" id="hidden_approved_amount_step2">
								<input type="hidden" name="released_amount" id="hidden_released_amount_step2">
								
								<div class = "amount_div col-sm-6">
					        		<div class="form-group">
						        		<div>Total Amount Withdrawn Till Date:<div class="text-background" id="hidden_expended_amount_step2">0.0</div></div>
						        		
					        		</div>
					        		<div class="form-group">
						        		<div>Balance Amount: <div class="text-background" id="hidden_balance_amount_step2">0.0</div></div>
					        		</div>
					        		<div class="form-group">
						        		<div>Last Inspected Date: <div class="text-background" id="hidden_last_inspection_date_step2"></div></div>
					        		</div>
								</div>
								<div class="option_div col-sm-6">
			                         <div class="form-group">
							                <p>Is Layout completed?</p>
							                <p>
							                <input type="radio" class="layoutrb" id="test3" name="layout" value="1">
							                <label for="test3">Yes</label>
							                </p>
							                <p>
							                <input type="radio" class="layoutrb" id="test4" value="0" name="layout">
							                <label for="test4">No</label>
							                </p>
						                </div>
					              </div>
					                <div id="layoutyes"  class="onselection" style="display: none">
						                <hr/>
						        		<div class="col-sm-6">
								        		<div class="form-group">
								        		<div>Level Completion Date: </div>
								        		<input type="text" name="inspection_date" class="form-control recieved_date" readonly>
								        		</div>
						        		</div>
						        		<div class="form-group col-sm-6">
					        			<div class="upload-btn-wrapper"> Take Photograph:
										  <button class="btn-file btn-sm"><i class="fa fa-camera"></i></button>
										  <img id="blah2" src="" alt=""  style="width: 25px; height: 25px;"/>
										  <input type="file" id="imgInp2" name="inspection_img1" accept="image/*" capture="camera"/>
										</div>
					        				<!-- <input type="file" name="inspection_img1" id="inspection_img1_step1" accept="image/*" capture="camera"> -->
					        			</div>
					                </div>
					                
					                <div id="layoutno"  class="onselection" style="display: none">
					                	<hr/>
							        		<div class="form-group col-sm-6">
							        		<div>Reason for delay: </div>
							        		<select id="layout_delay_reason" name="delay_reason_id" class="form-control delay_reason"></select>
							        		</div>
					                </div>
					                
					                <div id="layoutremarks" class="onselection">
							        		<div class="form-group col-sm-6">
							        		<div>Amount withdrawn: </div>
							        		<input id="amount_expended2" class="form-control" value=0>
							        		<input type="hidden" name="amount_expended" id="input_amount_expended2">
							        		</div>
							        		<div class="form-group col-sm-6">
							        		<div>Remarks: </div>
							        		<textarea   name="remarks" class="form-control"></textarea>
							        		</div>
						        		<div class="row"><div class="pos-btn"><button type="button" class="nextTab btn btn-success btn-md" id="blayoutSubmit">Save</button></div></div>
					                </div>
				                
				                </form>
		                        </div>
		                    </div>
		                    <div class="tab-pane" role="tabpanel" id="step3">
		                        <div class="content-text">
		                        <form class="bform" id="blockInspectionform3" name="blockInspectionform3">
		                        <input type="hidden" id="construction_level_id3" name="construction_level_id" value=3>
		                        <input type="hidden" name="finyr_id" id="hidden_inspection_finyear_id_step3">
		                        <input type="hidden" name="project_id" id="hidden_inspection_project_id_step3">
		                        <input type="hidden" name="work_id" id="hidden_inspection_work_id_step3">
								<input type="hidden" name="approved_amount" id="hidden_approved_amount_step3">
								<input type="hidden" name="released_amount" id="hidden_released_amount_step3">
								
								<div class = "amount_div col-sm-6">
					        		<div class="form-group">
						        		<div>Total Amount Withdrawn Till Date:<div class="text-background" id="hidden_expended_amount_step3">0.0</div></div>
						        		
					        		</div>
					        		<div class="form-group">
						        		<div>Balance Amount: <div class="text-background" id="hidden_balance_amount_step3">0.0</div></div>
					        		</div>
					        		<div class="form-group">
						        		<div>Last Inspected Date: <div class="text-background" id="hidden_last_inspection_date_step3"></div></div>
					        		</div>
								</div>
								
								<div class="option_div col-sm-6">
		                         <div class="form-group">
						                <p>Is Foundation completed?</p>
						                <p>
						                <input type="radio" class="foundationrb" id="test5" name="foundation" value="1">
						                <label for="test5">Yes</label>
						                </p>
						                <p>
						                <input type="radio" class="foundationrb" id="test6" value="0" name="foundation">
						                <label for="test6">No</label>
						                </p>
					                </div>
					              </div>
					                <div id="foundationyes"  class="onselection" style="display: none">
						                <hr/>
						        		<div class="col-sm-6">
								        		<div class="form-group">
								        		<div>Level Completion Date: </div>
								        		<input type="text" name="inspection_date" class="form-control recieved_date" readonly>
								        		</div>
						        		</div>
						        		<div class="form-group col-sm-6">
					        			<div class="upload-btn-wrapper"> Take Photograph:
										  <button class="btn-file btn-sm"><i class="fa fa-camera"></i></button>
										  <img id="blah3" src="" alt=""  style="width: 25px; height: 25px;"/>
										  <input type="file" id="imgInp3" name="inspection_img1" accept="image/*" capture="camera"/>
										</div>
					        				<!-- <input type="file" name="inspection_img1" id="inspection_img1_step1" accept="image/*" capture="camera"> -->
					        			</div>
					                </div>
					                
					                <div id="foundationno"  class="onselection" style="display: none">
					                	<hr/>
							        		<div class="form-group col-sm-6">
							        		<div>Reason for delay: </div>
							        		<select id="foundation_delay_reason" name="delay_reason_id" class="form-control delay_reason"></select>
							        		</div>
					                </div>
					                
					                <div id="foundationremarks" class="onselection">
							        		<div class="form-group col-sm-6">
								        		<div>Amount withdrawn: </div>
								        		<input id="amount_expended3" class="form-control" value=0>
								        		<input type="hidden" name="amount_expended" id="input_amount_expended3">
							        		</div>
							        		<div class="form-group col-sm-6">
							        		<div>Remarks: </div>
							        		<textarea   name="remarks" class="form-control"></textarea>
							        		</div>
						        		<div class="row"><div class="pos-btn"><button type="button" class="nextTab btn btn-success btn-md" id="bfoundationSubmit">Save</button></div></div>
					                </div>
				                
				                </form>
		                        </div>
		                    </div>
		                    <div class="tab-pane" role="tabpanel" id="step4">
		                        <div class="content-text">
		                        <form class="bform" id="blockInspectionform4" name="blockInspectionform4">
		                        <input type="hidden" id="construction_level_id4" name="construction_level_id" value=4>
		                        <input type="hidden" name="finyr_id" id="hidden_inspection_finyear_id_step4">
		                        <input type="hidden" name="project_id" id="hidden_inspection_project_id_step4">
		                        <input type="hidden" name="work_id" id="hidden_inspection_work_id_step4">
								<input type="hidden" name="approved_amount" id="hidden_approved_amount_step4">
								<input type="hidden" name="released_amount" id="hidden_released_amount_step4">
								
								<div class = "amount_div col-sm-6">
					        		<div class="form-group">
						        		<div>Total Amount Withdrawn Till Date:<div class="text-background" id="hidden_expended_amount_step4">0.0</div></div>
						        		
					        		</div>
					        		<div class="form-group">
						        		<div>Balance Amount: <div class="text-background" id="hidden_balance_amount_step4">0.0</div></div>
					        		</div>
					        		<div class="form-group">
						        		<div>Last Inspected Date: <div class="text-background" id="hidden_last_inspection_date_step4"></div></div>
					        		</div>
								</div>
								<div class="option_div col-sm-6">
		                         <div class="form-group">
						                <p>Is plinth completed?</p>
						                <p>
						                <input type="radio" class="plinthrb" id="test7" name="plinth" value="1">
						                <label for="test7">Yes</label>
						                </p>
						                <p>
						                <input type="radio" class="plinthrb" id="test8" value="0" name="plinth">
						                <label for="test8">No</label>
						                </p>
					                </div>
				               	</div>
					                <div id="plinthyes"  class="onselection" style="display: none">
						                <hr/>
						        		<div class="col-sm-6">
								        		<div class="form-group">
								        		<div>Level Completion Date: </div>
								        		<input type="text" name="inspection_date" class="form-control recieved_date" readonly>
								        		</div>
						        		</div>
						        		<div class="form-group col-sm-6">
					        			<div class="upload-btn-wrapper"> Take Photograph:
										  <button class="btn-file btn-sm"><i class="fa fa-camera"></i></button>
										  <img id="blah4" src="" alt=""  style="width: 25px; height: 25px;"/>
										  <input type="file" id="imgInp4" name="inspection_img1" accept="image/*" capture="camera"/>
										</div>
					        				<!-- <input type="file" name="inspection_img1" id="inspection_img1_step1" accept="image/*" capture="camera"> -->
					        			</div>
					                </div>
					                
					                <div id="plinthno"  class="onselection" style="display: none">
					                	<hr/>
							        		<div class="form-group col-sm-6">
							        		<div>Reason for delay: </div>
							        		<select id="plinth_delay_reason" name="delay_reason_id" class="form-control delay_reason"></select>
							        		</div>
					                </div>
					                
					                <div id="plinthremarks" class="onselection">
						        		
							        		<div class="form-group col-sm-6">
							        		<div>Amount withdrawn: </div>
							        		<input id="amount_expended4" class="form-control" value=0>
							        		<input type="hidden" name="amount_expended" id="input_amount_expended4">
							        		</div>
							        		<div class="form-group col-sm-6">
							        		<div>Remarks: </div>
							        		<textarea   name="remarks" class="form-control"></textarea>
							        		</div>
						        		<div class="row"><div class="pos-btn"><button type="button" class="nextTab btn btn-success btn-md" id="bplinthSubmit">Save</button></div></div>
					                </div>
				                
				                </form>
		                        </div>
		                    </div>
		                    <div class="tab-pane" role="tabpanel" id="step5">
		                        <div class="content-text">
		                        <form class="bform" id="blockInspectionform5" name="blockInspectionform5">
		                        <input type="hidden" id="construction_level_id5" name="construction_level_id" value=5>
		                        <input type="hidden" name="finyr_id" id="hidden_inspection_finyear_id_step5">
		                        <input type="hidden" name="project_id" id="hidden_inspection_project_id_step5">
		                        <input type="hidden" name="work_id" id="hidden_inspection_work_id_step5">
								<input type="hidden" name="approved_amount" id="hidden_approved_amount_step5">
								<input type="hidden" name="released_amount" id="hidden_released_amount_step5">
								
								<div class = "amount_div col-sm-6">
					        		<div class="form-group">
						        		<div>Total Amount Withdrawn Till Date:<div class="text-background" id="hidden_expended_amount_step5">0.0</div></div>
						        		
					        		</div>
					        		<div class="form-group">
						        		<div>Balance Amount: <div class="text-background" id="hidden_balance_amount_step5">0.0</div></div>
					        		</div>
					        		<div class="form-group">
						        		<div>Last Inspected Date: <div class="text-background" id="hidden_last_inspection_date_step5"></div></div>
					        		</div>
								</div>
								<div class="option_div col-sm-6">
		                          <div class="form-group">
						                <p>Is lintel completed?</p>
						                <p>
						                <input type="radio" class="lintelrb" id="test9" name="lintel" value="1">
						                <label for="test9">Yes</label>
						                </p>
						                <p>
						                <input type="radio" class="lintelrb" id="test10" value="0" name="lintel">
						                <label for="test10">No</label>
						                </p>
					                </div>
					               </div>
					                <div id="lintelyes" class="onselection" style="display: none">
						                <hr/>
						        		<div class="col-sm-6">
								        		<div class="form-group">
								        		<div>Level Completion Date: </div>
								        		<input type="text" name="inspection_date" class="form-control recieved_date" readonly>
								        		</div>
						        		</div>
						        		<div class="form-group col-sm-6">
					        			<div class="upload-btn-wrapper"> Take Photograph:
										  <button class="btn-file btn-sm"><i class="fa fa-camera"></i></button>
										  <img id="blah5" src="" alt=""  style="width: 25px; height: 25px;"/>
										  <input type="file" id="imgInp5" name="inspection_img1" accept="image/*" capture="camera"/>
										</div>
					        				<!-- <input type="file" name="inspection_img1" id="inspection_img1_step1" accept="image/*" capture="camera"> -->
					        			</div>
					                </div>
					                
					                <div id="lintelno"  class="onselection" class="onselection" style="display: none">
					                	<hr/>
							        		<div class="form-group col-sm-6">
							        		<div>Reason for delay: </div>
							        		<select id="lintel_delay_reason" name="delay_reason_id" class="form-control delay_reason"></select>
							        		</div>
					                </div>
					                
					                <div id="lintelremarks" class="onselection">
							        		<div class="form-group col-sm-6">
							        		<div>Amount withdrawn: </div>
							        		<input id="amount_expended5" class="form-control" value=0>
							        		<input type="hidden" name="amount_expended" id="input_amount_expended5">
							        		</div>
							        		<div class="form-group col-sm-6">
							        		<div>Remarks: </div>
							        		<textarea id="lintel_remarks" name="remarks" class="form-control"></textarea>
							        		</div>
						        		<div class="row"><div class="pos-btn"><button type="button" class="nextTab btn btn-success btn-md" id="blintelSubmit">Save</button></div></div>
					                </div>
				                
				                </form>
		                        </div>
		                    </div>
		                    <div class="tab-pane" role="tabpanel" id="step6">
		                        <div class="content-text">
		                        <form class="bform" id="blockInspectionform6" name="blockInspectionform6">
		                        <input type="hidden" id="construction_level_id6" name="construction_level_id" value=6>
		                        <input type="hidden" name="finyr_id" id="hidden_inspection_finyear_id_step6">
		                        <input type="hidden" name="project_id" id="hidden_inspection_project_id_step6">
		                        <input type="hidden" name="work_id" id="hidden_inspection_work_id_step6">
								<input type="hidden" name="approved_amount" id="hidden_approved_amount_step6">
								<input type="hidden" name="released_amount" id="hidden_released_amount_step6">
								
								<div class = "amount_div col-sm-6">
					        		<div class="form-group">
						        		<div>Total Amount Withdrawn Till Date:<div class="text-background" id="hidden_expended_amount_step6">0.0</div></div>
						        		
					        		</div>
					        		<div class="form-group">
						        		<div>Balance Amount: <div class="text-background" id="hidden_balance_amount_step6">0.0</div></div>
					        		</div>
					        		<div class="form-group">
						        		<div>Last Inspected Date: <div class="text-background" id="hidden_last_inspection_date_step6"></div></div>
					        		</div>
								</div>
								<div class="option_div col-sm-6">
		                         <div class="form-group">
						                <p>Is Roof completed?</p>
						                <p>
						                <input type="radio" class="roofrb" id="test11" name="roof" value="1">
						                <label for="test11">Yes</label>
						                </p>
						                <p>
						                <input type="radio" class="roofrb" id="test12" value="0" name="roof">
						                <label for="test12">No</label>
						                </p>
					                </div>
				               	</div>
					                <div id="roofyes"  class="onselection" style="display: none">
						                <hr/>
						        		<div class="col-sm-6">
								        		<div class="form-group">
								        		<div>Level Completion Date: </div>
								        		<input type="text" name="inspection_date" class="form-control recieved_date" readonly>
								        		</div>
						        		</div>
						        		<div class="form-group col-sm-6">
					        			<div class="upload-btn-wrapper"> Take Photograph:
										  <button class="btn-file btn-sm"><i class="fa fa-camera"></i></button>
										  <img id="blah6" src="" alt=""  style="width: 25px; height: 25px;"/>
										  <input type="file" id="imgInp6" name="inspection_img1" accept="image/*" capture="camera"/>
										</div>
					        				<!-- <input type="file" name="inspection_img1" id="inspection_img1_step1" accept="image/*" capture="camera"> -->
					        			</div>
					                </div>
					                
					                <div id="roofno" class="onselection" style="display: none">
					                	<hr/>
							        		<div class="form-group col-sm-6">
							        		<div>Reason for delay: </div>
							        		<select id="roof_delay_reason" name="delay_reason_id" class="form-control delay_reason"></select>
							        		</div>
					                </div>
					                
					                <div id="roofremarks" class="onselection">
							        		<div class="form-group col-sm-6">
							        		<div>Amount withdrawn: </div>
							        		<input id="amount_expended6" class="form-control" value=0>
							        		<input type="hidden" name="amount_expended" id="input_amount_expended6">
							        		</div>
							        		<div class="form-group col-sm-6">
							        		<div>Remarks: </div>
							        		<textarea id="roof_no_remarks" name="remarks" class="form-control"></textarea>
							        		</div>
						        		<div class="row"><div class="pos-btn"><button type="button" class="nextTab btn btn-success btn-md" id="broofSubmit">Save</button></div></div>
					                </div>
				                
				                </form>
		                        </div>
		                    </div>
		                    <div class="tab-pane" role="tabpanel" id="step7">
		                        <div class="content-text">
		                        <form class="bform" id="blockInspectionform7" name="blockInspectionform7">
		                        <input type="hidden" id="construction_level_id7" name="construction_level_id" value=7>
		                        <input type="hidden" name="finyr_id" id="hidden_inspection_finyear_id_step7">
		                        <input type="hidden" name="project_id" id="hidden_inspection_project_id_step7">
		                        <input type="hidden" name="work_id" id="hidden_inspection_work_id_step7">
								<input type="hidden" name="approved_amount" id="hidden_approved_amount_step7">
								<input type="hidden" name="released_amount" id="hidden_released_amount_step7">
								
								<div class = "amount_div col-sm-6">
					        		<div class="form-group">
						        		<div>Total Amount Withdrawn Till Date:<div class="text-background" id="hidden_expended_amount_step7">0.0</div></div>
						        		
					        		</div>
					        		<div class="form-group">
						        		<div>Balance Amount: <div class="text-background" id="hidden_balance_amount_step7">0.0</div></div>
					        		</div>
					        		<div class="form-group">
						        		<div>Last Inspected Date: <div class="text-background" id="hidden_last_inspection_date_step7"></div></div>
					        		</div>
								</div>
								<div class="option_div col-sm-6">
		                         <div class="form-group">
						                <p>Is Roof Cast completed?</p>
						                <p>
						                <input type="radio" class="roofcastrb" id="test13" name="roofcast" value="1">
						                <label for="test13">Yes</label>
						                </p>
						                <p>
						                <input type="radio" class="roofcastrb" id="test14" value="0" name="roofcast">
						                <label for="test14">No</label>
						                </p>
					                </div>
				               	</div>
					                <div id="roofcastyes"  class="onselection" style="display: none">
						                <hr/>
						        		<div class="col-sm-6">
								        		<div class="form-group">
								        		<div>Level Completion Date: </div>
								        		<input type="text" name="inspection_date" class="form-control recieved_date" readonly>
								        		</div>
						        		</div>
						        		<div class="form-group col-sm-6">
					        			<div class="upload-btn-wrapper"> Take Photograph:
										  <button class="btn-file btn-sm"><i class="fa fa-camera"></i></button>
										  <img id="blah7" src="" alt=""  style="width: 25px; height: 25px;"/>
										  <input type="file" id="imgInp7" name="inspection_img1" accept="image/*" capture="camera"/>
										</div>
					        				<!-- <input type="file" name="inspection_img1" id="inspection_img1_step1" accept="image/*" capture="camera"> -->
					        			</div>
					                </div>
					                
					                <div id="roofcastno"  class="onselection" style="display: none">
					                	<hr/>
							        		<div class="form-group col-sm-6">
							        		<div>Reason for delay: </div>
							        		<select id="roofcast_delay_reason" name="delay_reason_id" class="form-control delay_reason"></select>
							        		</div>
					                </div>
					                
					                <div id="roofcastremarks" class="onselection">
							        		<div class="form-group col-sm-6">
							        		<div>Amount withdrawn: </div>
							        		<input id="amount_expended7" class="form-control" value=0>
							        		<input type="hidden" name="amount_expended" id="input_amount_expended7">
							        		</div>
							        		<div class="form-group col-sm-6">
							        		<div>Remarks: </div>
							        		<textarea id="roofcast_remarks" name="remarks" class="form-control"></textarea>
							        		</div>
						        		<div class="row"><div class="pos-btn"><button type="button" class="nextTab btn btn-success btn-md" id="broofcastSubmit">Save</button></div></div>
					                </div>
				                
				                </form>
		                        </div>
		                    </div>
		                    <div class="tab-pane" role="tabpanel" id="step8">
		                        <div class="content-text">
		                        <form class="bform" id="blockInspectionform8" name="blockInspectionform8">
		                        <input type="hidden" id="construction_level_id8" name="construction_level_id" value=8>
		                        <input type="hidden" name="finyr_id" id="hidden_inspection_finyear_id_step8">
		                        <input type="hidden" name="project_id" id="hidden_inspection_project_id_step8">
		                        <input type="hidden" name="work_id" id="hidden_inspection_work_id_step8">
								<input type="hidden" name="approved_amount" id="hidden_approved_amount_step8">
								<input type="hidden" name="released_amount" id="hidden_released_amount_step8">
								
								<div class = "amount_div col-sm-6">
					        		<div class="form-group">
						        		<div>Total Amount Withdrawn Till Date:<div class="text-background" id="hidden_expended_amount_step8">0.0</div></div>
						        		
					        		</div>
					        		<div class="form-group">
						        		<div>Balance Amount: <div class="text-background" id="hidden_balance_amount_step8">0.0</div></div>
					        		</div>
					        		<div class="form-group">
						        		<div>Last Inspected Date: <div class="text-background" id="hidden_last_inspection_date_step8"></div></div>
					        		</div>
								</div>
								<div class="option_div col-sm-6">
		                         <div class="form-group">
						                <p>Is Finishing completed?</p>
						                <p>
						                <input type="radio" class="finishingrb" id="test15" name="finishing" value="1">
						                <label for="test15">Yes</label>
						                </p>
						                <p>
						                <input type="radio" class="finishingrb" id="test16" value="0" name="finishing">
						                <label for="test16">No</label>
						                </p>
					                </div>
					             </div>
					                <div id="finishingyes"  class="onselection" style="display: none">
						                <hr/>
						        		<div class="col-sm-6">
								        		<div class="form-group">
								        		<div>Level Completion Date: </div>
								        		<input type="text" name="inspection_date" class="form-control recieved_date" readonly>
								        		</div>
						        		</div>
						        		<div class="form-group col-sm-6">
					        			<div class="upload-btn-wrapper"> Take Photograph:
										  <button class="btn-file btn-sm"><i class="fa fa-camera"></i></button>
										  <img id="blah8" src="" alt=""  style="width: 25px; height: 25px;"/>
										  <input type="file" id="imgInp8" name="inspection_img1" accept="image/*" capture="camera"/>
										</div>
					        				<!-- <input type="file" name="inspection_img1" id="inspection_img1_step1" accept="image/*" capture="camera"> -->
					        			</div>
					                </div>
					                
					                <div id="finishingno"  class="onselection" style="display: none">
					                	<hr/>
							        		<div class="form-group col-sm-6">
							        		<div>Reason for delay: </div>
							        		<select id="finishing_delay_reason" name="delay_reason_id" class="form-control delay_reason"></select>
							        		</div>
					                </div>
					                
					                <div id="finishingremarks" class="onselection">
							        		<div class="form-group col-sm-6">
							        		<div>Amount withdrawn: </div>
							        		<input id="amount_expended8" class="form-control" value=0>
							        		<input type="hidden" name="amount_expended" id="input_amount_expended8">
							        		</div>
							        		<div class="form-group col-sm-6">
							        		<div>Remarks: </div>
							        		<textarea   name="remarks" class="form-control"></textarea>
							        		</div>
						        		<div class="row"><div class="pos-btn"><button type="button" class="nextTab btn btn-success btn-md" id="bfinishingSubmit">Save</button></div></div>
					                </div>
				                
				                </form>
		                        </div>
		                    </div>
		                    <div class="tab-pane" role="tabpanel" id="step9">
		                        <div class="content-text">
		                    <form class="bform" id="blockInspectionform9" name="blockInspectionform9">
		                        <input type="hidden" id="construction_level_id9" name="construction_level_id" value=9>
		                        <input type="hidden" name="finyr_id" id="hidden_inspection_finyear_id_step9">
		                        <input type="hidden" name="project_id" id="hidden_inspection_project_id_step9">
		                        <input type="hidden" name="work_id" id="hidden_inspection_work_id_step9">
								<input type="hidden" name="approved_amount" id="hidden_approved_amount_step9">
								<input type="hidden" name="released_amount" id="hidden_released_amount_step9">
								
								<div class = "amount_div col-sm-6">
					        		<div class="form-group">
						        		<div>Total Amount Withdrawn Till Date:<div class="text-background" id="hidden_expended_amount_step9">0.0</div></div>
						        		
					        		</div>
					        		<div class="form-group">
						        		<div>Balance Amount: <div class="text-background" id="hidden_balance_amount_step9">0.0</div></div>
					        		</div>
					        		<div class="form-group">
						        		<div>Last Inspected Date: <div class="text-background" id="hidden_last_inspection_date_step9"></div></div>
					        		</div>
								</div>
								<div class="option_div col-sm-6">
		                         <div class="form-group">
						                <p>Is this level completed?</p>
						                <p>
						                <input type="radio" class="completedrb" id="test17" name="completed" value="1">
						                <label for="test17">Yes</label>
						                </p>
						                <p>
						                <input type="radio" class="completedrb" id="test18" value="0" name="completed">
						                <label for="test18">No</label>
						                </p>
					                </div>
					              </div>
					                <div id="completedyes"  class="onselection" style="display: none">
						                <hr/>
						        		<div class="col-sm-6">
								        		<div class="form-group">
								        		<div>Level Completion Date: </div>
								        		<input type="text" name="inspection_date" class="form-control recieved_date" readonly>
								        		</div>
						        		</div>
						        		<div class="form-group col-sm-6">
					        			<div class="upload-btn-wrapper"> Take Photograph:
										  <button class="btn-file btn-sm"><i class="fa fa-camera"></i></button>
										  <img id="blah9" src="" alt=""  style="width: 25px; height: 25px;"/>
										  <input type="file" id="imgInp9" name="inspection_img1" accept="image/*" capture="camera"/>
										</div>
					        				<!-- <input type="file" name="inspection_img1" id="inspection_img1_step1" accept="image/*" capture="camera"> -->
					        			</div>
					                </div>
					                
					                <div id="completedno"  class="onselection" style="display: none">
					                	<hr/>
							        		<div class="form-group col-sm-6">
							        		<div>Reason for delay: </div>
							        		<select id="completed_delay_reason" name="delay_reason_id" class="form-control delay_reason"></select>
							        		</div>
					                </div>
					                
					                <div id="completedremarks" class="onselection">
							        		<div class="form-group col-sm-6">
							        		<div>Amount withdrawn: </div>
							        		<input id="amount_expended9" class="form-control" value=0>
							        		<input type="hidden" name="amount_expended" id="input_amount_expended9">
							        		</div>
							        		<div class="form-group col-sm-6">
							        		<div>Remarks: </div>
							        		<textarea   name="remarks" class="form-control"></textarea>
							        		</div>
						        		<div class="row"><div class="pos-btn"><button type="button" class="nextTab btn btn-success btn-md" id="bcompletedSubmit">Save</button></div></div>
					                </div>
				                
				                </form>
		                        </div>
		                    </div>
		                    <div class="tab-pane" role="tabpanel" id="step10">
		                        <div class="content-text">
		                        <form class="bform" id="blockInspectionform10" name="blockInspectionform10">
		                        <input type="hidden" id="construction_level_id10" name="construction_level_id" value=10>
		                        <input type="hidden" name="finyr_id" id="hidden_inspection_finyear_id_step10">
		                        <input type="hidden" name="project_id" id="hidden_inspection_project_id_step10">
		                        <input type="hidden" name="work_id" id="hidden_inspection_work_id_step10">
								<input type="hidden" name="approved_amount" id="hidden_approved_amount_step10">
								<input type="hidden" name="released_amount" id="hidden_released_amount_step10">
								
								<div class = "amount_div col-sm-6">
					        		<div class="form-group">
						        		<div>Total Amount Withdrawn Till Date:<div class="text-background" id="hidden_expended_amount_step10">0.0</div></div>
						        		
					        		</div>
					        		<div class="form-group">
						        		<div>Balance Amount: <div class="text-background" id="hidden_balance_amount_step10">0.0</div></div>
					        		</div>
					        		<div class="form-group">
						        		<div>Last Inspected Date: <div class="text-background" id="hidden_last_inspection_date_step10"></div></div>
					        		</div>
								</div>
								<div class="option_div col-sm-6">
		                         <div class="form-group">
						                <p>Is Check and Measure completed?</p>
						                <p>
						                <input type="radio" class="checkrb" id="test19" name="check" value="1">
						                <label for="test19">Yes</label>
						                </p>
						                <p>
						                <input type="radio" class="checkrb" id="test20" value="0" name="check">
						                <label for="test20">No</label>
						                </p>
					                </div>
					              </div>
					                <div id="checkyes"  class="onselection" style="display: none">
						                <hr/>
						        		<div class="col-sm-6">
								        		<div class="form-group">
								        		<div>Level Completion Date: </div>
								        		<input type="text" name="inspection_date" class="form-control recieved_date" readonly>
								        		</div>
						        		</div>
						        		<div class="form-group col-sm-6">
					        			<div class="upload-btn-wrapper"> Take Photograph:
										  <button class="btn-file btn-sm"><i class="fa fa-camera"></i></button>
										  <img id="blah10" src="" alt=""  style="width: 25px; height: 25px;"/>
										  <input type="file" id="imgInp10" name="inspection_img1" accept="image/*" capture="camera"/>
										</div>
					        				<!-- <input type="file" name="inspection_img1" id="inspection_img1_step1" accept="image/*" capture="camera"> -->
					        			</div>
					                </div>
					                
					                <div id="checkno"  class="onselection" style="display: none">
					                	<hr/>
							        		<div class="form-group col-sm-6">
							        		<div>Reason for delay: </div>
							        		<select id="check_delay_reason" name="delay_reason_id" class="form-control delay_reason"></select>
							        		</div>
					                </div>
					                
					                <div id="checkremarks" class="onselection">
						        		<div class="form-group">
							        		<div>Amount withdrawn: </div>
							        		<input id="amount_expended10" class="form-control" value=0>
							        		<input type="hidden" name="amount_expended" id="input_amount_expended10">
						        		</div>
							        		<div class="form-group col-sm-6">
							        		<div>Remarks: </div>
							        		<textarea   name="remarks" class="form-control"></textarea>
							        		</div>
						        		<div class="row"><div class="pos-btn"><button type="button" class="nextTab btn btn-success btn-md" id="bcheckSubmit">Save</button></div></div>
					                </div>
				                
				                </form>
		                        </div>
		                    </div>
		                    <div class="tab-pane" role="tabpanel" id="step11">
		                        <div class="content-text">
		                        <form class="bform" id="blockInspectionform11" name="blockInspectionform11">
		                        <input type="hidden" id="construction_level_id11" name="construction_level_id" value=11>
		                        <input type="hidden" name="finyr_id" id="hidden_inspection_finyear_id_step11">
		                        <input type="hidden" name="project_id" id="hidden_inspection_project_id_step11">
		                        <input type="hidden" name="work_id" id="hidden_inspection_work_id_step11">
								<input type="hidden" name="approved_amount" id="hidden_approved_amount_step11">
								<input type="hidden" name="released_amount" id="hidden_released_amount_step11">
								
								<div class = "amount_div col-sm-6">
					        		<div class="form-group">
						        		<div>Total Amount Withdrawn Till Date:<div class="text-background" id="hidden_expended_amount_step11">0.0</div></div>
						        		
					        		</div>
					        		<div class="form-group">
						        		<div>Balance Amount: <div class="text-background" id="hidden_balance_amount_step11">0.0</div></div>
					        		</div>
					        		<div class="form-group">
						        		<div>Last Inspected Date: <div class="text-background" id="hidden_last_inspection_date_step11"></div></div>
					        		</div>
								</div>
								<div class="option_div col-sm-6">
		                         <div class="form-group">
						                <p>Is Handed Over completed?</p>
						                <p>
						                <input type="radio" class="handedrb" id="test21" name="handed" value="1">
						                <label for="test21">Yes</label>
						                </p>
						                <p>
						                <input type="radio" class="handedrb" id="test22" value="0" name="handed">
						                <label for="test22">No</label>
						                </p>
					                </div>
					              </div>
					                <div id="handedyes"  class="onselection" style="display: none">
						                <hr/>
						        		<div class="col-sm-6">
								        		<div class="form-group">
								        		<div>Level Completion Date: </div>
								        		<input type="text" name="inspection_date" class="form-control recieved_date" readonly>
								        		</div>
						        		</div>
						        		<div class="form-group col-sm-6">
					        			<div class="upload-btn-wrapper"> Take Photograph:
										  <button class="btn-file btn-sm"><i class="fa fa-camera"></i></button>
										  <img id="blah11" src="" alt=""  style="width: 25px; height: 25px;"/>
										  <input type="file" id="imgInp11" name="inspection_img1" accept="image/*" capture="camera"/>
										</div>
					        				<!-- <input type="file" name="inspection_img1" id="inspection_img1_step1" accept="image/*" capture="camera"> -->
					        			</div>
					                </div>
					                
					                <div id="handedno"  class="onselection" style="display: none">
					                	<hr/>
							        		<div class="form-group col-sm-6">
							        		<div>Reason for delay: </div>
							        		<select id="handed_delay_reason" name="delay_reason_id" class="form-control delay_reason"></select>
							        		</div>
					                </div>
					                
					                <div id="handedremarks" class="onselection">
						        		<div class="form-group col-sm-6">
							        		<div>Amount withdrawn: </div>
							        		<input id="amount_expended11" class="form-control" value=0>
							        		<input type="hidden" name="amount_expended" id="input_amount_expended11">
						        		</div>
							        		<div class="form-group col-sm-6">
							        		<div>Remarks: </div>
							        		<textarea   name="remarks" class="form-control"></textarea>
							        		</div>
						        		<div class="row"><div class="pos-btn"><button type="button" class="nextTab btn btn-success btn-md" id="bhandedSubmit">Save</button></div></div>
					                </div>
				                
				                </form>
		                        </div>
		                    </div>
		                    <div class="tab-pane" role="tabpanel" id="step12">
		                        <div class="content-text">
		                        <form class="bform" id="blockInspectionform12" name="blockInspectionform12">
		                        <input type="hidden" id="construction_level_id12" name="construction_level_id" value=12>
		                        <input type="hidden" name="finyr_id" id="hidden_inspection_finyear_id_step12">
		                        <input type="hidden" name="project_id" id="hidden_inspection_project_id_step12">
		                        <input type="hidden" name="work_id" id="hidden_inspection_work_id_step12">
								<input type="hidden" name="approved_amount" id="hidden_approved_amount_step12">
								<input type="hidden" name="released_amount" id="hidden_released_amount_step12">
								
								<div class = "amount_div col-sm-6">
					        		<div class="form-group">
						        		<div>Total Amount Withdrawn Till Date:<div class="text-background" id="hidden_expended_amount_step12">0.0</div></div>
						        		
					        		</div>
					        		<div class="form-group">
						        		<div>Balance Amount: <div class="text-background" id="hidden_balance_amount_step12">0.0</div></div>
					        		</div>
					        		<div class="form-group">
						        		<div>Last Inspected Date: <div class="text-background" id="hidden_last_inspection_date_step12"></div></div>
					        		</div>
								</div>
								<div class="option_div col-sm-6">
		                         <div class="form-group">
						                <p>Is Submitted completed?</p>
						                <p>
						                <input type="radio" class="submittedrb" id="test23" name="submitted" value="1">
						                <label for="test23">Yes</label>
						                </p>
						                <p>
						                <input type="radio" class="submittedrb" id="test24" value="0" name="submitted">
						                <label for="test24">No</label>
						                </p>
					                </div>
					              </div>
					                <div id="submittedyes"  class="onselection" style="display: none">
						                <hr/>
						        		<div class="col-sm-6">
								        		<div class="form-group">
								        		<div>Level Completion Date: </div>
								        		<input type="text" name="inspection_date" class="form-control recieved_date" readonly>
								        		</div>
						        		</div>
						        		<div class="form-group col-sm-6">
					        			<div class="upload-btn-wrapper"> Take Photograph:
										  <button class="btn-file btn-sm"><i class="fa fa-camera"></i></button>
										  <img id="blah12" src="" alt=""  style="width: 25px; height: 25px;"/>
										  <input type="file" id="imgInp12" name="inspection_img1" accept="image/*" capture="camera"/>
										</div>
					        				<!-- <input type="file" name="inspection_img1" id="inspection_img1_step1" accept="image/*" capture="camera"> -->
					        			</div>
					                </div>
					                
					                <div id="submittedno"  class="onselection" style="display: none">
					                	<hr/>
							        		<div class="form-group col-sm-6">
							        		<div>Reason for delay: </div>
							        		<select id="submitted_delay_reason" name="delay_reason_id" class="form-control delay_reason"></select>
							        		</div>
					                </div>
					                
					                <div id="submittedremarks" class="onselection">
						        		<div class="form-group col-sm-6">
							        		<div>Amount withdrawn: </div>
							        		<input id="amount_expended12" class="form-control" value=0>
							        		<input type="hidden" name="amount_expended" id="input_amount_expended12">
						        		</div>
							        		<div class="form-group col-sm-6">
							        		<div>Remarks: </div>
							        		<textarea   name="remarks" class="form-control"></textarea>
							        		</div>
						        		<div class="row"><div class="pos-btn"><button type="button" class="nextTab btn btn-success btn-md" id="bsubmittedSubmit">Save</button></div></div>
					                </div>
				                
				                </form>
		                        </div>
		                    </div>
		                    <div class="tab-pane" role="tabpanel" id="step13">
		                        <div class="content-text">
		                        <form class="bform" id="blockInspectionform13" name="blockInspectionform13">
		                        <input type="hidden" id="construction_level_id13" name="construction_level_id" value=13>
		                        <input type="hidden" name="finyr_id" id="hidden_inspection_finyear_id_step13">
		                        <input type="hidden" name="project_id" id="hidden_inspection_project_id_step13">
		                        <input type="hidden" name="work_id" id="hidden_inspection_work_id_step13">
								<input type="hidden" name="approved_amount" id="hidden_approved_amount_step13">
								<input type="hidden" name="released_amount" id="hidden_released_amount_step13">
								
								<div class = "amount_div col-sm-6">
					        		<div class="form-group">
						        		<div>Total Amount Withdrawn Till Date:<div class="text-background" id="hidden_expended_amount_step13">0.0</div></div>
						        		
					        		</div>
					        		<div class="form-group">
						        		<div>Balance Amount: <div class="text-background" id="hidden_balance_amount_step13">0.0</div></div>
					        		</div>
					        		<div class="form-group">
						        		<div>Last Inspected Date: <div class="text-background" id="hidden_last_inspection_date_step13"></div></div>
					        		</div>
								</div>
								<div class="option_div col-sm-6">
		                         <div class="form-group">
						                <p>Is UC Approved completed?</p>
						                <p>
						                <input type="radio" class="approvedrb" id="test25" name="approved" value="1">
						                <label for="test25">Yes</label>
						                </p>
						                <p>
						                <input type="radio" class="approvedrb" id="test26" value="0" name="approved">
						                <label for="test26">No</label>
						                </p>
					                </div>
					              </div>
					                <div id="approvedyes"  class="onselection" style="display: none">
						                <hr/>
						        		<div class="col-sm-6">
								        		<div class="form-group">
								        		<div>Level Completion Date: </div>
								        		<input type="text" name="inspection_date" class="form-control recieved_date" readonly>
								        		</div>
						        		</div>
						        		<div class="form-group col-sm-6">
					        			<div class="upload-btn-wrapper"> Take Photograph:
										  <button class="btn-file btn-sm"><i class="fa fa-camera"></i></button>
										  <img id="blah13" src="" alt=""  style="width: 25px; height: 25px;"/>
										  <input type="file" id="imgInp13" name="inspection_img1" accept="image/*" capture="camera"/>
										</div>
					        				<!-- <input type="file" name="inspection_img1" id="inspection_img1_step1" accept="image/*" capture="camera"> -->
					        			</div>
					                </div>
					                
					                <div id="approvedno"  class="onselection" style="display: none">
					                	<hr/>
							        		<div class="form-group col-sm-6">
							        		<div>Reason for delay: </div>
							        		<select id="approved_delay_reason" name="delay_reason_id" class="form-control delay_reason"></select>
							        		</div>
					                </div>
					                
					                <div id="approvedremarks" class="onselection">
						        		<div class="form-group col-sm-6">
							        		<div>Amount withdrawn: </div>
							        		<input id="amount_expended13" class="form-control" value=0>
							        		<input type="hidden" name="amount_expended" id="input_amount_expended13">
						        		</div>
							        		<div class="form-group col-sm-6">
							        		<div>Remarks: </div>
							        		<textarea   name="remarks" class="form-control"></textarea>
							        		</div>
						        		<div class="row"><div class="pos-btn"><button type="button" class="nextTab btn btn-success btn-md" id="bapprovedSubmit">Save</button></div></div>
					                </div>
				                
		                        </form>
		                        </div>
		                    </div>
		                </div>
			            </div>
			        </div>
		           </div>
		           <div class="modal-footer">
		           		<!-- <button class="btn btn-primary" id="saveInspection">Save</button>
		           		<button class="btn btn-default" id="close" data-dismiss="modal">Close</button> -->
		           </div>
		       </div>
		   </div>
		   
		   
		   
		<!-- Inspection Modal Type NonBuilding-->
	   <div class="modal fade" id="projectInspectionModalNonBuilding" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true" data-keyboard="false" data-backdrop="static">
	       <div class="modal-dialog modal-lg">
	           <div class="modal-content">
	               <div class="modal-header modal-opepa-header">
	                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color: #fff">x</span></button>
	                   <h4 class="modal-title modal-opepa-header" id="">Inspection</h4>
	               </div>
	               <div class="modal-body">
	               		<div id="myProgressNonBuilding" class="progress-bar active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
						  <div id="myBarNonBuilding" class="progress-bar progress-bar-striped active">
						  	<label id="progressLabelNonBuilding"></label>
						  </div>
						</div>
	               		<div class="detailsOfProject">
			               	<div>Project Id: <span id="nb-projectId"></span></div>
			               	<div>School Id: <span id="nb-schoolId"></span></div>
			               	<div>School Name: <span id="nb-schoolName"></span></div>
			               	<div>Financial Year: <span id="nb-financialYear"></span></div>
			               	<div>Sub-category: <span id="nb-subcategoryName"></span></div>
		               	</div>
						<div class="containerr">
		                <div class="tab-head-nb">
		                
		                    <div class="list-group">
		                        <button  class="list-group-item active text-center" title="Fund&nbsp;Withdrawn"><span>1</span>Fund Withdrawn</button>
				                <button  class="list-group-item text-center" title="Start"><span>2</span>Start</button>
				                <button  class="list-group-item text-center" title="Start"><span>3</span>InProgress</button>
				                <button  class="list-group-item text-center" title="Completed"><span>4</span>Completed</button>
				                <button  class="list-group-item text-center" title="Handed Over"><span>5</span>Handed</button>
				                <button  class="list-group-item text-center" title="UC Submitted"><span>6</span>Submitted</button>
				                <button  class="list-group-item text-center" title="UC Approved"><span>7</span>Approved</button>
		                    </div>
		                </div>
		                <div class="tab-content-nb">
		                <div style="float: right; font-size: 10px; font-weight: bold">(*Amount in lakhs.)</div>
		                    <div class="tab-pane-nb active" role="tabpanel" id="nstep1">
		                        <div class="content-text">
		                        <form class="nbform" id="blockInspectionNonBuildingform1" name="blockInspectionNonBuildingform1">
		                        <input type="hidden" name="construction_level_id" value=1>
		                        <input type="hidden" name="finyr_id" id="hidden_inspection_nb_finyear_id_step1">
		                        <input type="hidden" name="project_id" id="hidden_inspection_nb_project_id_step1">
		                        <input type="hidden" name="work_id" id="hidden_inspection_nb_work_id_step1">
								<input type="hidden" name="approved_amount" id="hidden_inspection_nb_approved_amount_step1">
								<input type="hidden" name="released_amount" id="hidden_inspection_nb_released_amount_step1">
								
								<div class = "amount_div col-sm-6">
					        		<div class="form-group">
						        		<div>Total Amount Withdrawn Till Date:<div class="text-background" id="hidden_inspection_nb_expended_amount_step1">0.0</div></div>
						        		
					        		</div>
					        		<div class="form-group">
						        		<div>Balance Amount: <div class="text-background" id="hidden_inspection_nb_balance_amount_step1">0.0</div></div>
					        		</div>
					        		<div class="form-group">
						        		<div>Last Inspected Date: <div class="text-background" id="hidden_inspection_nb_inspected_date_step1"></div></div>
					        		</div>
								</div>
								<div class="option_div col-sm-6">
					                <div class="form-group">
						                <p>Is Fund withdrawn?</p>
						                <p>
						                <input type="radio" class="nonbuildingrb" id="ntest1" name="nbfund" value="1">
						                <label for="ntest1">Yes</label>
						                </p>
						                <p>
						                <input type="radio" class="nonbuildingrb" id="ntest2" value="0" name="nbfund">
						                <label for="ntest2">No</label>
						                </p>
					                </div>
					             </div>
					                
					                <div id="nbfundreleaseyes" class="onselection">
						                <hr/>
						        		<div class="col-sm-6">
								        		<div class="form-group">
								        		<div>Level Completion Date: </div>
								        		<input type="text" name="inspection_date" class="form-control recieved_date" readonly>
								        		</div>
						        		</div>
						        		<div class="form-group col-sm-6">
					        			<div class="upload-btn-wrapper"> Take Photograph:
										  <button class="btn-file btn-sm"><i class="fa fa-camera"></i></button>
										  <img id="nb-blah1" src="" alt=""  style="width: 25px; height: 25px;"/>
										  <input type="file" id="nb-imgInp1" name="inspection_img1" accept="image/*" capture="camera"/>
										</div>
					        				<!-- <input type="file" name="inspection_img1" id="inspection_img1_step1" accept="image/*" capture="camera"> -->
					        			</div>
					                </div>
					                
					                <div id="nbfundreleaseno" class="onselection">
					                	<hr/>
							        		<div class="form-group col-sm-6">
							        		<div>Reason for delay: </div>
							        		<select id="nbfund_delay_reason" name="delay_reason_id" class="form-control delay_reason"></select>
							        		</div>
					                </div>
					                
					                <div id="nbfundremarks" class="onselection">
						        		<div class="form-group col-sm-6">
							        		<div>Amount withdrawn: </div>
							        		<input id="nb-amount_expended1" class="form-control" value=0>
							        		<input type="hidden" name="amount_expended" id="nb_input_amount_expended1">
							        		
						        		</div>
							        		<div class="form-group col-sm-6">
							        		<div>Remarks: </div>
							        		<textarea  name="remarks" class="form-control"></textarea>
							        		</div>
						        		<div class="row"><div class="pos-btn"><button type="button" class="nextTab btn btn-success btn-md" id="nbfundreleaseSubmit">Save</button></div></div>
					                </div>
				                
				                </form>
		                        </div>
		                    </div>
		                    <div class="tab-pane-nb" role="tabpanel" id="nstep2">
		                        <div class="content-text">
		                        <form class="nbform" id="blockInspectionNonBuildingform2" name="blockInspectionNonBuildingform2">
		                        <input type="hidden" name="construction_level_id" value=2>
		                        <input type="hidden" name="finyr_id" id="hidden_inspection_nb_finyear_id_step2">
		                        <input type="hidden" name="project_id" id="hidden_inspection_nb_project_id_step2">
		                        <input type="hidden" name="work_id" id="hidden_inspection_nb_work_id_step2">
								<input type="hidden" name="approved_amount" id="hidden_inspection_nb_approved_amount_step2">
								<input type="hidden" name="released_amount" id="hidden_inspection_nb_released_amount_step2">
								
								<div class = "amount_div col-sm-6">
					        		<div class="form-group">
						        		<div>Total Amount Withdrawn Till Date:<div class="text-background" id="hidden_inspection_nb_expended_amount_step2">0.0</div></div>
						        		
					        		</div>
					        		<div class="form-group">
						        		<div>Balance Amount: <div class="text-background" id="hidden_inspection_nb_balance_amount_step2">0.0</div></div>
					        		</div>
					        		<div class="form-group">
						        		<div>Last Inspected Date: <div class="text-background" id="hidden_inspection_nb_inspected_date_step2"></div></div>
					        		</div>
								</div>
								<div class="option_div col-sm-6">
		                         <div class="form-group">
						                <p>Is Started?</p>
						                <p>
						                <input type="radio" class="nonbuildingrb" id="ntest3" name="nbstart" value="1">
						                <label for="ntest3">Yes</label>
						                </p>
						                <p>
						                <input type="radio" class="nonbuildingrb" id="ntest4" value="0" name="nbstart">
						                <label for="ntest4">No</label>
						                </p>
					                </div>
					              </div>
					                <div id="nbstartyes" class="onselection">
						                <hr/>
						        		<div class="col-sm-6">
								        		<div class="form-group">
								        		<div>Level Completion Date: </div>
								        		<input type="text" name="inspection_date" class="form-control recieved_date" readonly>
								        		</div>
						        		</div>
						        		<div class="form-group col-sm-6">
					        			<div class="upload-btn-wrapper"> Take Photograph:
										  <button class="btn-file btn-sm"><i class="fa fa-camera"></i></button>
										  <img id="nb-blah2" src="" alt=""  style="width: 25px; height: 25px;"/>
										  <input type="file" id="nb-imgInp2" name="inspection_img1" accept="image/*" capture="camera"/>
										</div>
					        				<!-- <input type="file" name="inspection_img1" id="inspection_img1_step1" accept="image/*" capture="camera"> -->
					        			</div>
					                </div>
					                
					                <div id="nbstartno" class="onselection">
					                	<hr/>
							        		<div class="form-group col-sm-6">
							        		<div>Reason for delay: </div>
							        		<select id="nbstart_delay_reason" name="delay_reason_id" class="form-control delay_reason"></select>
							        		</div>
					                </div>
					                
					                <div id="nbstartremarks" class="onselection">
						        		<div class="form-group col-sm-6">
							        		<div>Amount withdrawn: </div>
							        		<input id="nb-amount_expended2" class="form-control" value=0>
							        		<input type="hidden" name="amount_expended" id="nb_input_amount_expended2">
			                        	</div>
							        		<div class="form-group col-sm-6">
							        		<div>Remarks: </div>
							        		<textarea   name="remarks" class="form-control"></textarea>
							        		</div>
						        		<div class="row"><div class="pos-btn"><button type="button" class="nextTab btn btn-success btn-md" id="nbstartSubmit">Save</button></div></div>
					                </div>
				                
				                </form>
		                        </div>
		                    </div>
		                    <div class="tab-pane-nb" role="tabpanel" id="nstep3">
		                        <div class="content-text">
		                        <form class="nbform" id="blockInspectionNonBuildingform3" name="blockInspectionNonBuildingform3">
		                        <input type="hidden" name="construction_level_id" value=3>
		                        <input type="hidden" name="finyr_id" id="hidden_inspection_nb_finyear_id_step3">
		                        <input type="hidden" name="project_id" id="hidden_inspection_nb_project_id_step3">
		                        <input type="hidden" name="work_id" id="hidden_inspection_nb_work_id_step3">
								<input type="hidden" name="approved_amount" id="hidden_inspection_nb_approved_amount_step3">
								<input type="hidden" name="released_amount" id="hidden_inspection_nb_released_amount_step3">
								
								<div class = "amount_div col-sm-6">
					        		<div class="form-group">
						        		<div>Total Amount Withdrawn Till Date:<div class="text-background" id="hidden_inspection_nb_expended_amount_step3">0.0</div></div>
						        		
					        		</div>
					        		<div class="form-group">
						        		<div>Balance Amount: <div class="text-background" id="hidden_inspection_nb_balance_amount_step3">0.0</div></div>
					        		</div>
					        		<div class="form-group">
						        		<div>Last Inspected Date: <div class="text-background" id="hidden_inspection_nb_inspected_date_step3"></div></div>
					        		</div>
								</div>
								<div class="option_div col-sm-6">
		                         <div class="form-group">
						                <p>Is in progress?</p>
						                <p>
						                <input type="radio" class="nonbuildingrb" id="ntest13" name="nbprogress" value="1">
						                <label for="ntest13">Yes</label>
						                </p>
						                <p>
						                <input type="radio" class="nonbuildingrb" id="ntest14" value="0" name="nbprogress">
						                <label for="ntest14">No</label>
						                </p>
				                 </div>
					            </div>
					                <div id="nbprogressyes" class="onselection">
						                <hr/>
						        		<div class="col-sm-6">
								        		<div class="form-group">
								        		<div>Level Completion Date: </div>
								        		<input type="text" name="inspection_date" class="form-control recieved_date" readonly>
								        		</div>
						        		</div>
						        		<div class="form-group col-sm-6">
					        			<div class="upload-btn-wrapper"> Take Photograph:
										  <button class="btn-file btn-sm"><i class="fa fa-camera"></i></button>
										  <img id="nb-blah3" src="" alt=""  style="width: 25px; height: 25px;"/>
										  <input type="file" id="nb-imgInp3" name="inspection_img1" accept="image/*" capture="camera"/>
										</div>
					        				<!-- <input type="file" name="inspection_img1" id="inspection_img1_step1" accept="image/*" capture="camera"> -->
					        			</div>
					                </div>
					                
					                <div id="nbprogressno" class="onselection">
					                	<hr/>
							        		<div class="form-group col-sm-6">
							        		<div>Reason for delay: </div>
							        		<select id="nbprogress_delay_reason" name="delay_reason_id" class="form-control delay_reason"></select>
						        			</div>
					                </div>
					                
					                <div id="nbprogressremarks" class="onselection">
						        		<div class="form-group col-sm-6">
							        		<div>Amount withdrawn: </div>
							        		<input id="nb-amount_expended3" class="form-control" value=0>
							        		<input type="hidden" name="amount_expended" id="nb_input_amount_expended3">
				                        </div>
							        		<div class="form-group col-sm-6">
							        		<div>Remarks: </div>
							        		<textarea   name="remarks" class="form-control"></textarea>
							        		</div>
						        		<div class="row"><div class="pos-btn"><button type="button" class="nextTab btn btn-success btn-md" id="nbprogressSubmit">Save</button></div></div>
					                </div>
				                
				                </form>
		                        </div>
		                    </div>
		                    <div class="tab-pane-nb" role="tabpanel" id="nstep4">
		                        <div class="content-text">
		                        <form class="nbform" id="blockInspectionNonBuildingform4" name="blockInspectionNonBuildingform4">
		                        <input type="hidden" name="construction_level_id" value=4>
		                        <input type="hidden" name="finyr_id" id="hidden_inspection_nb_finyear_id_step4">
		                        <input type="hidden" name="project_id" id="hidden_inspection_nb_project_id_step4">
		                        <input type="hidden" name="work_id" id="hidden_inspection_nb_work_id_step4">
								<input type="hidden" name="approved_amount" id="hidden_inspection_nb_approved_amount_step4">
								<input type="hidden" name="released_amount" id="hidden_inspection_nb_released_amount_step4">
								
								<div class = "amount_div col-sm-6">
					        		<div class="form-group">
						        		<div>Total Amount Withdrawn Till Date:<div class="text-background" id="hidden_inspection_nb_expended_amount_step4">0.0</div></div>
						        		
					        		</div>
					        		<div class="form-group">
						        		<div>Balance Amount: <div class="text-background" id="hidden_inspection_nb_balance_amount_step4">0.0</div></div>
					        		</div>
					        		<div class="form-group">
						        		<div>Last Inspected Date: <div class="text-background" id="hidden_inspection_nb_inspected_date_step4"></div></div>
					        		</div>
								</div>
								<div class="option_div col-sm-6">
		                         <div class="form-group">
						                <p>Is completed?</p>
						                <p>
						                <input type="radio" class="nonbuildingrb" id="ntest5" name="nbcompleted" value="1">
						                <label for="ntest5">Yes</label>
						                </p>
						                <p>
						                <input type="radio" class="nonbuildingrb" id="ntest6" value="0" name="nbcompleted">
						                <label for="ntest6">No</label>
						                </p>
					                </div>
					              </div>
					                <div id="nbcompletedyes" class="onselection">
						                <hr/>
						        		<div class="col-sm-6">
								        		<div class="form-group">
								        		<div>Level Completion Date: </div>
								        		<input type="text" name="inspection_date" class="form-control recieved_date" readonly>
								        		</div>
						        		</div>
						        		<div class="form-group col-sm-6">
					        			<div class="upload-btn-wrapper"> Take Photograph:
										  <button class="btn-file btn-sm"><i class="fa fa-camera"></i></button>
										  <img id="nb-blah4" src="" alt=""  style="width: 25px; height: 25px;"/>
										  <input type="file" id="nb-imgInp4" name="inspection_img1" accept="image/*" capture="camera"/>
										</div>
					        				<!-- <input type="file" name="inspection_img1" id="inspection_img1_step1" accept="image/*" capture="camera"> -->
					        			</div>
					                </div>
					                
					                <div id="nbcompletedno" class="onselection">
					                	<hr/>
							        		<div class="form-group col-sm-6">
							        		<div>Reason for delay: </div>
							        		<select id="nbcompleted_delay_reason" name="delay_reason_id" class="form-control delay_reason"></select>
							        		</div>
					                </div>
					                
					                <div id="nbcompletedremarks" class="onselection">
						        		<div class="form-group col-sm-6">
							        		<div>Amount withdrawn: </div>
							        		<input id="nb-amount_expended4" class="form-control" value=0>
							        		<input type="hidden" name="amount_expended" id="nb_input_amount_expended4">
				                        </div>
							        		<div class="form-group col-sm-6">
							        		<div>Remarks: </div>
							        		<textarea   name="remarks" class="form-control"></textarea>
							        		</div>
						        		<div class="row"><div class="pos-btn"><button type="button" class="nextTab btn btn-success btn-md" id="nbcompletedSubmit">Save</button></div></div>
					                </div>
				                
				                </form>
		                        </div>
		                    </div>
		                    <div class="tab-pane-nb" role="tabpanel" id="nstep5">
		                        <div class="content-text">
		                        <form class="nbform" id="blockInspectionNonBuildingform5" name="blockInspectionNonBuildingform5">
		                        <input type="hidden" name="construction_level_id" value=5>
		                        <input type="hidden" name="finyr_id" id="hidden_inspection_nb_finyear_id_step5">
		                        <input type="hidden" name="project_id" id="hidden_inspection_nb_project_id_step5">
		                        <input type="hidden" name="work_id" id="hidden_inspection_nb_work_id_step5">
								<input type="hidden" name="approved_amount" id="hidden_inspection_nb_approved_amount_step5">
								<input type="hidden" name="released_amount" id="hidden_inspection_nb_released_amount_step5">
								
								<div class = "amount_div col-sm-6">
					        		<div class="form-group">
						        		<div>Total Amount Withdrawn Till Date:<div class="text-background" id="hidden_inspection_nb_expended_amount_step5">0.0</div></div>
						        		
					        		</div>
					        		<div class="form-group">
						        		<div>Balance Amount: <div class="text-background" id="hidden_inspection_nb_balance_amount_step5">0.0</div></div>
					        		</div>
					        		<div class="form-group">
						        		<div>Last Inspected Date: <div class="text-background" id="hidden_inspection_nb_inspected_date_step5"></div></div>
					        		</div>
								</div>
								<div class="option_div col-sm-6">
		                          <div class="form-group">
						                <p>Is Handed Over completed?</p>
						                <p>
						                <input type="radio" class="nonbuildingrb" id="ntest7" name="nbhanded" value="1">
						                <label for="ntest7">Yes</label>
						                </p>
						                <p>
						                <input type="radio" class="nonbuildingrb" id="ntest8" value="0" name="nbhanded">
						                <label for="ntest8">No</label>
						                </p>
					                </div>
					              </div>
					                <div id="nbhandedyes" class="onselection">
						                <hr/>
						        		<div class="col-sm-6">
								        		<div class="form-group">
								        		<div>Level Completion Date: </div>
								        		<input type="text" name="inspection_date" class="form-control recieved_date" readonly>
								        		</div>
							        	</div>
						        		<div class="form-group col-sm-6">
					        			<div class="upload-btn-wrapper"> Take Photograph:
										  <button class="btn-file btn-sm"><i class="fa fa-camera"></i></button>
										  <img id="nb-blah5" src="" alt=""  style="width: 25px; height: 25px;"/>
										  <input type="file" id="nb-imgInp5" name="inspection_img1" accept="image/*" capture="camera"/>
										</div>
					        				<!-- <input type="file" name="inspection_img1" id="inspection_img1_step1" accept="image/*" capture="camera"> -->
					        			</div>
					                </div>
					                
					                <div id="nbhandedno" class="onselection">
					                	<hr/>
							        		<div class="form-group col-sm-6">
							        		<div>Reason for delay: </div>
							        		<select id="nbhanded_delay_reason" name="delay_reason_id" class="form-control delay_reason"></select>
							        		</div>
					                </div>
					                
					                <div id="nbhandedremarks" class="onselection">
						        		<div class="form-group col-sm-6">
							        		<div>Amount withdrawn: </div>
							        		<input id="nb-amount_expended5" class="form-control" value=0>
							        		<input type="hidden" name="amount_expended" id="nb_input_amount_expended5">
				                        </div>
							        		<div class="form-group col-sm-6">
							        		<div>Remarks: </div>
							        		<textarea   name="remarks" class="form-control"></textarea>
							        		</div>
						        		<div class="row"><div class="pos-btn"><button type="button" class="nextTab btn btn-success btn-md" id="nbhandedSubmit">Save</button></div></div>
					                </div>
				                
				                </form>
		                        </div>
		                    </div>
		                    <div class="tab-pane-nb" role="tabpanel" id="nstep6">
		                        <div class="content-text">
		                        <form class="nbform" id="blockInspectionNonBuildingform6" name="blockInspectionNonBuildingform6">
		                        <input type="hidden" name="construction_level_id" value=6>
		                        <input type="hidden" name="finyr_id" id="hidden_inspection_nb_finyear_id_step6">
		                        <input type="hidden" name="project_id" id="hidden_inspection_nb_project_id_step6">
		                        <input type="hidden" name="work_id" id="hidden_inspection_nb_work_id_step6">
								<input type="hidden" name="approved_amount" id="hidden_inspection_nb_approved_amount_step6">
								<input type="hidden" name="released_amount" id="hidden_inspection_nb_released_amount_step6">
								
								<div class = "amount_div col-sm-6">
					        		<div class="form-group">
						        		<div>Total Amount Withdrawn Till Date:<div class="text-background" id="hidden_inspection_nb_expended_amount_step6">0.0</div></div>
						        		
					        		</div>
					        		<div class="form-group">
						        		<div>Balance Amount: <div class="text-background" id="hidden_inspection_nb_balance_amount_step6">0.0</div></div>
					        		</div>
					        		<div class="form-group">
						        		<div>Last Inspected Date: <div class="text-background" id="hidden_inspection_nb_inspected_date_step6"></div></div>
					        		</div>
								</div>
								<div class="option_div col-sm-6">
		                         <div class="form-group">
						                <p>Is Submitted completed?</p>
						                <p>
						                <input type="radio" class="nonbuildingrb" id="ntest9" name="nbsubmitted" value="1">
						                <label for="ntest9">Yes</label>
						                </p>
						                <p>
						                <input type="radio" class="nonbuildingrb" id="ntest10" value="0" name="nbsubmitted">
						                <label for="ntest10">No</label>
						                </p>
				                 </div>
					             </div>
					                <div id="nbsubmittedyes" class="onselection">
						                <hr/>
						        		<div class="col-sm-6">	
								        		<div class="form-group">
								        		<div>Level Completion Date: </div>
								        		<input type="text" name="inspection_date" class="form-control recieved_date" readonly>
								        		</div>
							        	</div>
						        		<div class="form-group col-sm-6">
					        			<div class="upload-btn-wrapper"> Take Photograph:
										  <button class="btn-file btn-sm"><i class="fa fa-camera"></i></button>
										  <img id="nb-blah6" src="" alt=""  style="width: 25px; height: 25px;"/>
										  <input type="file" id="nb-imgInp6" name="inspection_img1" accept="image/*" capture="camera"/>
										</div>
					        				<!-- <input type="file" name="inspection_img1" id="inspection_img1_step1" accept="image/*" capture="camera"> -->
					        			</div>
					                </div>
					                
					                <div id="nbsubmittedno" class="onselection">
					                	<hr/>
							        		<div class="form-group col-sm-6">
							        		<div>Reason for delay: </div>
							        		<select id="nbsubmitted_delay_reason" name="delay_reason_id" class="form-control delay_reason"></select>
							        		</div>
					                </div>
					                
					                <div id="nbsubmittedremarks" class="onselection">
						        		<div class="form-group col-sm-6">
							        		<div>Amount withdrawn: </div>
							        		<input id="nb-amount_expended6" class="form-control" value=0>
							        		<input type="hidden" name="amount_expended" id="nb_input_amount_expended6">
				                        </div>
							        		<div class="form-group col-sm-6">
							        		<div>Remarks: </div>
							        		<textarea   name="remarks" class="form-control"></textarea>
							        		</div>
						        		<div class="row"><div class="pos-btn"><button type="button" class="nextTab btn btn-success btn-md" id="nbsubmittedSubmit">Save</button></div></div>
					                </div>
				                
				                </form>
		                        </div>
		                    </div>
		                    <div class="tab-pane-nb" role="tabpanel" id="nstep7">
		                        <div class="content-text">
		                        <form class="nbform" id="blockInspectionNonBuildingform7" name="blockInspectionNonBuildingform7">
		                        <input type="hidden" name="construction_level_id" value=7>
		                        <input type="hidden" name="finyr_id" id="hidden_inspection_nb_finyear_id_step7">
		                        <input type="hidden" name="project_id" id="hidden_inspection_nb_project_id_step7">
		                        <input type="hidden" name="work_id" id="hidden_inspection_nb_work_id_step7">
								<input type="hidden" name="approved_amount" id="hidden_inspection_nb_approved_amount_step7">
								<input type="hidden" name="released_amount" id="hidden_inspection_nb_released_amount_step7">
								
								<div class = "amount_div col-sm-6">
					        		<div class="form-group">
						        		<div>Total Amount Withdrawn Till Date:<div class="text-background" id="hidden_inspection_nb_expended_amount_step7">0.0</div></div>
						        		
					        		</div>
					        		<div class="form-group">
						        		<div>Balance Amount: <div class="text-background" id="hidden_inspection_nb_balance_amount_step7">0.0</div></div>
					        		</div>
					        		<div class="form-group">
						        		<div>Last Inspected Date: <div class="text-background" id="hidden_inspection_nb_inspected_date_step7"></div></div>
					        		</div>
								</div>
								<div class="option_div col-sm-6">
		                         <div class="form-group">
						                <p>Is Approved?</p>
						                <p>
						                <input type="radio" class="nonbuildingrb" id="ntest11" name="nbapproved" value="1">
						                <label for="ntest11">Yes</label>
						                </p>
						                <p>
						                <input type="radio" class="nonbuildingrb" id="ntest12" value="0" name="nbapproved">
						                <label for="ntest12">No</label>
						                </p>
					                </div>
					              </div>
					                <div id="nbapprovedyes" class="onselection">
						                <hr/>
						        		<div class="col-sm-6">
								        		<div class="form-group">
								        		<div>Level Completion Date: </div>
								        		<input type="text" name="inspection_date" class="form-control recieved_date" readonly>
								                </div>
							            </div>
						        		<div class="form-group col-sm-6">
					        			<div class="upload-btn-wrapper"> Take Photograph:
										  <button class="btn-file btn-sm"><i class="fa fa-camera"></i></button>
										  <img id="nb-blah7" src="" alt=""  style="width: 25px; height: 25px;"/>
										  <input type="file" id="nb-imgInp7" name="inspection_img1" accept="image/*" capture="camera"/>
										</div>
					        				<!-- <input type="file" name="inspection_img1" id="inspection_img1_step1" accept="image/*" capture="camera"> -->
					        			</div>
					                </div>
					                
					                <div id="nbapprovedno" class="onselection">
					                	<hr/>
							        		<div class="form-group col-sm-6">
							        		<div>Reason for delay: </div>
							        		<select id="nbapproved_delay_reason" name="delay_reason_id" class="form-control delay_reason"></select>
							        		</div>
					                </div>
					                
					                <div id="nbapprovedremarks" class="onselection">
						        		<div class="form-group col-sm-6">
							        		<div>Amount withdrawn: </div>
							        		<input id="nb-amount_expended7" class="form-control" value=0>
							        		<input type="hidden" name="amount_expended" id="nb_input_amount_expended7">
				                        </div>
							        		<div class="form-group col-sm-6">
							        		<div>Remarks: </div>
							        		<textarea   name="remarks" class="form-control"></textarea>
							        		</div>
						        		<div class="row"><div class="row"><div class="pos-btn"><button type="button" class="nextTab btn btn-success btn-md" id="nbapprovedSubmit">Save</button></div></div></div>
					                </div>
				                
				                </form>
		                        </div>	
		                    </div>
		                    
		                </div>
			            </div>
			        </div>
		           </div>
		           <div class="modal-footer">
		           		<!-- <button class="btn btn-primary" id="saveInspection">Save</button>
		           		<button class="btn btn-default" id="close" data-dismiss="modal">Close</button> -->
		           </div>
		       </div>
		   </div>
		</div>
	</div>
</div>
</body>
</html>