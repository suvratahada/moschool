<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="./customCSS/pma/districtpma.css">
<link href="./customCSS/blockLevelTrack.css" rel="stylesheet" type="text/css" />
<script src="./customJS/pma/districtProjectMonitoring.js"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-heading panel-opepa">
					<div class="panel-title">Budget Allocation Details</div>
				</div>
				<div class="panel-body">
				<div class="excel-btn-container">
					<a href="javascript:void(0)" class="btn btn-success btn-sm" id="district-allocation-excel"><i class="fa fa-file-excel-o"></i></a>
				</div>
				<div class="row">
					<!-- <div class="pull-right">
						<button class="btn btn-success btn-sm btn-padding" data-toggle="modal" data-target="#districtMonitoringModal" id="new_btn"><i class="fa fa-plus"></i>&nbsp;New</button>
					</div> -->
				</div>
				<div class="filter-wrap">
					<div class="row">
					<div class="col-sm-3">	
						<select id="finyear" class="form-control">
						</select>
					</div>
					<div class="col-sm-3">
						<select id="schemeFilter"  class="form-control">
						</select>
					</div>
					 <div class="col-sm-3">	
						<select id="blockfilter" class="form-control">
						</select>
					</div> 
					<div class="col-sm-3">
						<select id="subcategoryfilter"  class="form-control">
						</select>
					</div>
					
					</div>
				</div>
				<div class="row instruction">
					<div class="btn btn-sm btn-primary btn-reallocation" action="budgetReallocation" title="Budget Re-Allocation Details"><i class="fa fa-repeat"></i></div><b> : Re-Allocation</b> &nbsp;
					<div class="btn btn-sm btn-success" action="releaseAmount" title="Release Amount"><i class="fa fa-rupee" ></i></div><b> : Release Amount</b> &nbsp;
	        		<div class="btn btn-sm btn-warning" action="trackReleaseAmount"  title="Track Release Amount"><i class="fa fa-sitemap"></i></div><b> : View Release Amount</b> &nbsp;
				</div>
				<div class="overflow-opepa">
					<table class="table table-bordered table-striped table-responsive table-hover stripe row-border order-column" id="tblDistrictSchoolMonitoringDetails" width="100%">
						<thead>
							<tr>
								<th style="width: 150px !important;">STC Actions</th>
								<th>Block</th>
								<th>School Name</th>
								<th>School Code</th>
								<th>Financial Year</th>
								<th>Sub Category</th>
								<th>Total Units</th>
								<th>Balance Units</th>
								<th>Unit Cost</th>
								<th>Approved Amount</th>
								<th>Balance Amount</th>
								<th>Released Amount</th>
								<th>Project ID</th>
								<th>Scheme Name</th>
								<th>Category</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				</div> <!-- end of panel body -->
			</div>
		</div>
		     
	<!-- View Tracking School Model START-->
	   <div class="modal fade" id="releaseAmountTrackingModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
	       <div class="modal-dialog modal-lg">
	           <div class="modal-content">
	               <div class="modal-header modal-opepa-header">
	                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	                   <h4 class="modal-title" id="">Fund Allocation Phase Wise</h4>
	               </div>
	               <div class="modal-body">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								 <div class="card-inner">
                                    <div class="timeline-block">
                                        <!--Timeline-->
                                        <div class="time-bar" id="timeBar"></div>
                                        <div id="timelineDynamic"></div>
                                    </div>
                                </div>
							</div>
						</div>
		                </div>
		           </div>
		       </div>
		   </div>
		   
		 <!-- Re-Allocation fund Modal -->
		<div class="modal fade" id="reAllocationFundsModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
		  <div class="modal-dialog modal-lg">
		    <div class="modal-content modal-border">
		      <div class="modal-header modal-opepa-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		        <h4 class="modal-title" id="budget_name">Budget Re-Allocation Details</h4>
		      </div>
		      <form class="form-horizontal" id="reallocateFundsForm" name="reallocateFundsForm">
   			  <div class="modal-body modal-content-border">
   			  <div class="amount_msg">*All amount in lakhs.</div>
   			  <input type="hidden" class="form-control" name="${_csrf.parameterName}" value="${_csrf.token}"/>
   			  <input type="hidden" name="project_id" id="hidden_project_id">
		      		<div class="row">
		      		<div class="col-sm-12 col-md-12">
		      		 <fieldset>
               		 <legend class="pacs-header">Budget Allocation</legend>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label col-sm-4" for="">Scheme Name:</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="allocate_scheme_name" readonly>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label col-sm-4" for="">Financial Year:</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="allocate_financial_year" readonly>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label col-sm-4" for="">Block Name: </label>
								<div class="col-sm-8">
									<input type="text" class="form-control"  readonly="true" id="allocated_block" readonly>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label col-sm-4" for="">Category:</label>
								<div class="col-sm-8">
										<input type="text" class="form-control" id="allocated_category" readonly>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label col-sm-4" for="" >Sub-Category: </label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="allocated_sub_category" readonly>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label col-sm-4" for="">School Code:</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="mapping_school_id0" name="mapping_school0" readonly>
									<small class="validation_error_msg"></small>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label col-sm-4" for="">School Name:</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" readonly="true" id="allocated_school_name">
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label col-sm-4" for="">Allocated Units:</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="allocated_total_units0" name="" value = 0 name="balanced_units0">
									<small class="validation_error_msg"></small>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label col-sm-4" for="">Unit Cost:</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="allocated_cost" name="unit_cost" readonly>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label col-sm-4" for="">Approved Amount:</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="total_approved_amount0" name="" readonly value = 0>
								</div>
							</div>
						</div>
						
						<input type="hidden" id="block_id_hidden0" readonly>
						<input type="hidden" id="totalunits_hidden" readonly>
						<input type="hidden" id="school_id_hidden" readonly>
						<input type="hidden" id="work_id_hidden" readonly>
						<input type="hidden" id="allocated_block0" readonly>
						<input type="hidden" id="balanced_unit" readonly>
						<input type="hidden" id="scheme_id_hidden" readonly>
						<input type="hidden" id="finyr_id_hidden" readonly>
						<input type="hidden" id="project_id_hidden" readonly>
						<input type="hidden" id="fixed_units" readonly>
						
					</div>
					<div style="float:right">
					<span class="tot-bal mute-text">Total Balance Units: <label id="tot_bal0">0</label></span><br/>
					<span class="mute-text rem-bal">Remaining Balance Units: <label id="rem_bal0">0</label></span>
					</div>
					</fieldset>
					<div class="row">
						<div align="center" style="margin-top: 5px">
							<button type="button" class="btn btn-success btn-get" id="get_allocation" title="Re-Allocate To Another School"><b>+</b></button>
							<button type="button" class="btn btn-danger btn-get delete" id="remove" title="Remove School" disabled><b>-</b></button>
						</div>
					</div>
					<div id="budget_allocation_wrap"></div>
		        	</div>
		      	</div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-primary" id="reAllocationFundsSubmit" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Re-Allocating...">Submit</button>
		      </div>
		      </form>
		    </div>
		  </div>
		</div>
		
		
		<!--Amount Release Modal -->
		<div class="modal fade" id="releasedAmountModal" role="dialog"  tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-lg">
		    <div class="modal-content modal-border">
		      <div class="modal-header modal-opepa-header">
		        <button type="button" class="close" data-dismiss="modal">
		          <span>&times;</span>
		        </button>
		        <h4 class="modal-title">Release Amount</h4>
		      </div>
		      <form class="form-horizontal" id="releaseAmountForm">
		      <div class="modal-body modal-content-border">
		      <div class="amount_msg">*All amount in lakhs.</div>
		      <input type="hidden" name="project_id" id="rel_hidden_project_id">
			      <div class="row">
	                <div class="col-sm-12 col-md-12">
	                    <fieldset>
	                        <legend class="pma-header">Budget Allocation Details</legend>
	                        <div class="row">
	                            <div class="col-sm-4 pma-details-header">Total Sanctioned Amount</div>
	                            <div class="col-sm-8" id="total_sanc_amt"></div>
	                        </div>
	                        <div class="row">
	                            <div class="col-sm-4 pma-details-header">Balance Amount</div>
	                            <div class="col-sm-8"><span id="total_balance_amt"></span></div>
	                        </div>
	                        <div class="row">
	                            <div class="col-sm-4 pma-details-header">Released Amount</div>
	                            <div class="col-sm-8" id="total_released_amt"></div>
	                        </div>
	                    </fieldset>
	                </div>
	           	  </div>
           	  	<div class="row margin-10"></div>
		      	<div class="row">
		      		<div class="col-sm-12 col-md-12">
			      		 <fieldset>
		              		<legend class="pma-header">Release Amount</legend>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label class="control-label col-sm-4" for="">Release Amount:</label>
										<div class="col-sm-8">
											<input class="form-control" id="release_amount" name="amount_released">
											<small class="validation_error_msg"></small>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
											<label class="control-label col-sm-4" for="">Balance Amount:</label>
											<div class="col-sm-8">
												<input class="form-control" id="rel_app_amount" name="balance_amount" readonly="readonly">
												<small class="validation_error_msg"></small>
											</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label class="control-label col-sm-4" for="">Release Date:</label>
										<div class="col-sm-8">
											<input class="form-control"  id="release_date" name="date_of_rel" autocomplete="off" readonly>
											<small class="validation_error_msg"></small>
										</div>
									</div>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal" >Close</button>
		        <button type="button" class="btn btn-primary" id="releaseAmountModalSubmit" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Saving...">Submit</button>
		      </div>
		      </form>
		     </div>
		     </div>
		     </div>
		     
		<!-- Modal Page Loader  --> 
	<!--	<div class="modal modal-child" id="pleaseWaitDialog" role="dialog"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="pleaseWaitDialogLabel" aria-hidden="true">
 			<div class="modal-dialog modal-sm">
            <div class="modal-content">
		        <div class="modal-header">
		            <h5>Please wait until it finished...!!!</h5>
		        </div>
		        <div class="modal-body">
		            <div class="progress">
		                <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
		  				</div>
		            </div>
	            </div>
	        </div>
	        </div>
		</div> -->
</div>
</body>
</html>