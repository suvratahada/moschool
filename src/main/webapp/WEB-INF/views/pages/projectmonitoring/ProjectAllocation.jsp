<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="./customJS/pma/projectAllocation.js"></script>
<link href="./customCSS/pma/projectAllocation.css" rel="stylesheet">
</head>
<body>
<div class="container">
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading panel-opepa">
				<div class="panel-title">Project Allocation Details</div>
			</div>
			<div class="panel-body">
				<div class="row pull-right">
					<a href="./projectmonitoring/downloadProjectAllocationTemplate.do">
					<button type="button" class="btn btn-success pull-right mr-10"  id="downloadTempleteBtn">
							<i class="fa fa-download"></i> Download Template
					</button></a>
					
				</div>
				<div class="card">
					<div class="card-header">
						<h4>Allocation Project For</h4>
					</div>
					<div class="card-block">
						<form id="createProjectAllocationFileForm">
							<div class="row">
								<div class="form-group">
									<label class="control-label col-sm-4 label-right" for="">Select Financial Year:</label>
									<div class="col-sm-4">
										<select id="project_fin_year" name="finyr_id" class="form-control">
										</select>
									</div>
								<div class="col-sm-4">
									<button type="button" class="btn btn-primary" id="createProjectAllocationFileLogbtn">Create File Logger</button>
								</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="row mb-10"></div>
				<div class="row col-sm-12 col-md-12">
				<div class="overflow-opepa">
					<table class="table table-bordered table-striped table-responsive table-hover stripe row-border order-column" id="projectAllocationFileLoggerTable" width="100%">
						<thead>
							<tr>
								<th>Sl No</th>
								<th>Upload Projects</th>
								<th>Process Projects</th>
								<th>Financial Year</th>
								<th>Download File</th>
								<th>View Log File</th>
								<th>Uploaded By</th>
								<th>Uploaded Date</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				</div>
			</div>
		</div>
	</div>
		<!--Project Allocation Upload Modal -->
		<div class="modal fade" id="projectAllocationUploadModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
		  <div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content modal-border">
		      <div class="modal-header modal-opepa-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		        <h4 class="modal-title">Upload Project Allocation Template</h4>
		      </div>
		      <form class="form-horizontal" id="projectAllocationUploadForm">
		      <div class="modal-body modal-content-border">
		      <input type="hidden"  id="hidden_slno" value="">
		      <input type="hidden"  id="hidden_fin_year" value="">
           	  	<div class="row mb-10"></div>
		      	<div class="row">
		      		<div class="col-sm-12 col-md-12">
			      		 <fieldset>
		              		<legend class="pma-header">Upload Project Allocation Template</legend>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group">
										<label class="control-label col-sm-6" for="">Upload Allocation Template:</label>
										<div class="col-sm-6">
											<input type="file" class="form-control" id="project_upload_file" name="project_template">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
										
								</div>
							</div>
						</fieldset>
					</div>
				</div>
		      </div>
		      <div class="modal-footer">
		      	<button type="button" class="btn btn-primary" id="uploadProjectAllocationTemplateButton">Upload</button>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      </div>
		      </form>
		     </div>
		     </div>
		     </div>
		     
		 <!--View Log Modal -->
		<div class="modal fade" id="viewProjectAllocationLoggerDetailsModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
		  <div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content modal-border">
		      <div class="modal-header modal-opepa-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		        <h4 class="modal-title">Project Allocation Logger Details</h4>
		      </div>
		      <form class="form-horizontal" id="viewLoggerDetailsform">
		      <div class="modal-body modal-content-border">
			      <div class="row">
	                <div class="col-sm-12 col-md-12">
	                    <fieldset>
	                        <legend class="pma-header">View Logger Details</legend>
	                        <div class="row">
	                            <div class="col-sm-4 pma-details-header">Upload Start Time</div>
	                            <div class="col-sm-8" id="log_upload_start_time"></div>
	                        </div>
	                        <div class="row">
	                            <div class="col-sm-4 pma-details-header">Upload Completion Time</div>
	                            <div class="col-sm-8" id="log_upload_comp_time"></div>
	                        </div>
	                        <div class="row">
	                            <div class="col-sm-4 pma-details-header">Total Data Processed</div>
	                            <div class="col-sm-8" id="log_success_count"></div>
	                        </div>
	                        <div class="row">
	                            <div class="col-sm-4 pma-details-header">Total Data Failed</div>
	                            <div class="col-sm-8" id="log_failed_count"></div>
	                        </div>
	                        <div class="row" id="duplicate_data_hidden">
	                            <div class="col-sm-4 pma-details-header">Total Duplicated Data Found</div>
	                            <div class="col-sm-8" id="log_duplicated_count"></div>
	                        </div>
	                    </fieldset>
	                </div>
	           	  </div>
           	  	<div class="row margin-10"></div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      </div>
		      </form>
		     </div>
		  </div>
	 </div>
</div>
</body>
</html>