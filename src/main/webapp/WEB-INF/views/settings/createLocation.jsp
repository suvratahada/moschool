<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
   <script type="text/javascript" src="./customJS/setting/createlocation.js"></script>
   <script type="text/javascript" src="./customJS/setting/createCountry.js"></script>
   <script type="text/javascript" src="./customJS/setting/createCity.js"></script>   
<!--   <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> -->
  <script src="./customJS/iCheck/icheck.min.js" type="text/javascript"></script> 
  
  <link rel="stylesheet" type="text/css" href="./customCSS/iCheck/all.css" />
  <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css"/>
  <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"> </script>

<title>createLocation</title>

<style type="text/css">
	.panel-default{
		border: 0.2px solid #012b72;
	}
	.panel-default>.panel-heading{
		background-color: #012b72;
		color : white;
	}
	.table-bordered>thead>tr>th {
	    background-color: #012b72;
	    color: #ffffff;
	}
</style>
</head>
<body>

<div class="container-fluid entry-header">
	<h1 class="entry-title"> <a href="#"><span class="glyphicon glyphicon-user"></span></a> Locations</h1>
	<button value="AddCountry" class="btn btn-primary" id="addCountry" style="float: right;"data-toggle="modal" data-target="#adduserModal">Add Country</button>
	<button value="AddCountry" class="btn btn-primary" id="addCountry" style="float: right; margin-right:2%;" data-toggle="modal" data-target="#addCityModal">Add City</button>
</div>

<div class="container-fluid vh100">
	<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading"><div id="location_panel_heading" class="panel-title"><a href="#"><span class="glyphicon glyphicon-plus"></span></a> Location </div></div>
			
				<div class="panel-body">
					
					<form:form action="" theme="simple" name="formLocationDetails" id="formLocationDetails">
					
						<div class="form-group">
							<label>Location Name:</label>
							<input type="text" name="loc_name"  id = "loc_name" class="form-control" autocomplete="off">
						</div>
						
						<div class="form-group">
							<label>Address:</label>
							<input type="text" name="address"  id = "address" class="form-control" autocomplete="off">
						</div>
						
						<div class="form-group">
							<label class="control-label" for="">Country Name:</label>
					        <select class="form-control" id = "country" name="country_id">
					        </select>
						</div>
						<div class="form-group">
							<label>State:</label>
							<input type="text" name="state" id = "state"   class="form-control" autocomplete="off">
						</div>
						<div class="form-group dropdown">
							<label class="control-label" for="">City Name:</label>
					        <select class="form-control" id = "city" name="city_id">
					        <option>Select</option>
					        </select>
						</div>
						<div class="row">		
						<div class="col-md-6">
						<div class="form-group">
							<label>PO Box</label>
							<input type="text" name="po_box"  id = "po_box" class="form-control" autocomplete="off">
						</div>	
						</div><!-- end of class="col" -->
						
						<div class="col-md-6">
						
						</div><!-- end of class="col" -->
						
						
						<div class="col-md-6">
						<div class="form-group">
							<label>Postal Code:</label>
							<input type="text" name="postal_code" id = "postal_code"  class="form-control" autocomplete="off">
						</div>
						</div><!-- end of class="col" -->
						
						</div><!-- end of class="row" -->		

						<div class="form-action">
							<div class="row">
								<div class="col-xs-6 col-sm-6"><input type="reset" value="Reset" class="btn btn-default btn-block" onclick="resetForm_CreateLocation()"></div>
								<div class="col-xs-6 col-sm-6"><input type="button" value="Add" class="btn btn-primary btn-block submit" id="location_form_button"></button></div>
							</div>
						</div>
					</form:form>
				</div>
			</div>
		</div>
		
		
		
	
		<div class="col-xs-12 col-sm-12 col-md-9">
			<div class="panel panel-default">
				<div class="panel-heading"><div class="panel-title">Location Details</div></div>
				<div class="panel-body">
					<table class="table table-condensed table-striped table-bordered display nowrap" id="tblLocationDetails" width="100%">
						<thead>
							<tr>
								<th class="datatable-nosort text-center">Location Name</th>
								<th class="datatable-nosort text-center">Address</th>
								<th class="datatable-nosort text-center">Post Ofiice Box</th>
								<th class="datatable-nosort text-center">City ID</th>
								<th class="datatable-nosort text-center">Postal Code</th>
							    <th class="datatable-nosort text-center">State</th>
							  	<th class="datatable-nosort text-center">Country ID</th>
							  	<th class="datatable-nosort text-center">Action</th>								
							</tr>
						</thead>
					 <tbody></tbody>
					</table>
				</div>
			</div>
		</div>
		
	</div>
</div>

<div class="modal fade" id="adduserModal" role="dialog">
    <div class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="background-color:#3c8dbc;color:#fff;">
          <button type="button" class="close" data-dismiss="modal" id="reset">&times;</button>
          <h4 class="modal-title">Add Country</h4>
        </div>
        <div class="modal-body">
      <form:form action="" theme="simple" name="formCountryDetails" id="formCountryDetails" class="form-horizontal">
						
						<div class="form-group">
					      <label class="control-label col-sm-4" for="">Country Name:</label>
					      <div class="col-sm-8">
					        <input type="text" class="form-control" name="country_name"  id ="country_name">
					      </div>
					    </div>
					    
					    <div class="form-group">
					      <label class="control-label col-sm-4" for="">Country code:</label>
					      <div class="col-sm-8">
					        <input type="text" class="form-control" name="country_code"  id = "country_code">
					      </div>
					      </div>
					      <div class="form-group">
					      <label class="control-label col-sm-4" for="">Country Dial Code:</label>
					      <div class="col-sm-8">
					        <input type="text" class="form-control" name="country_dial_code"  id = "country_dial_code">
					      </div>
					      </div>
					    
					    <div class="form-group">
					    <div style="text-align:center;">
					     	 <input type="button" value="Add" class="btn btn-primary submit"  id="country_form_button">
					    </div>
					    </div>
					   </form:form>
        <div class="modal-footer">
			<div>
				
					<div style="text-align:center;"><h4><b>Country Details</b></h4></div>
					<div>
						<table class="table table-condensed table-striped table-bordered display nowrap" id="tblCountryDetails" width="100%">
							<thead>
								<tr>
									<th class="datatable-nosort text-center">Country Name</th>
									<th class="datatable-nosort text-center">Country Code</th>
									<th class="datatable-nosort text-center">Country Dial Code</th>
								  	<th class="datatable-nosort text-center">Action</th>								
								</tr>
							</thead>
						 <tbody></tbody>
						</table>
					</div>
				</div>
			</div>
        </div>
        
        
        </div>
      </div>
      
    </div>
    <div class="modal fade" id="addCityModal" role="dialog">
    <div class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="background-color:#3c8dbc;color:#fff;">
          <button type="button" class="close" data-dismiss="modal" id="resetc">&times;</button>
          <h4 class="modal-title">Add City</h4>
        </div>
        <div class="modal-body">
      <form:form action="" theme="simple" name="formCityDetails" id="formCityDetails" class="form-horizontal">
						<div class="form-group">
						<label class="control-label col-sm-4" for="">Country Name:</label>
					        <!-- <input type="text" class="form-control" name="city_country_name"  id = "city_country_name"> -->
					        <div class="col-sm-8">
					        <select class="form-control" id = "country_id" name="country_id">
					        </select>
					        </div>
						</div>
						<div class="form-group">
					      <label class="control-label col-sm-4" for="">City Name:</label>
					      <div class="col-sm-8" style="float: left;">
					        <input type="text" class="form-control" name="city_name"  id ="city_name">
					      </div>
					    </div>
					    
					    <div class="form-group">
					      <label class="control-label col-sm-4" for="">City code:</label>
					      <div class="col-sm-4">
					        <input type="text" class="form-control" name="city_code"  id = "city_code">
					      </div>
					      
					    </div>
					    
					    <div class="form-group">
					    <div style="text-align:center;">
					     	 <input type="button" value="Add" class="btn btn-primary submit"  id="city_form_button">
					    </div>
					    </div>
					   </form:form>
        <div class="modal-footer">
			<div>
				
					<div style="text-align:center;"><h4><b>City Details</b></h4></div>
					<div>
						<table class="table table-condensed table-striped table-bordered display nowrap" id="tblCityDetails" width="100%">
							<thead>
								<tr>
									<th class="datatable-nosort text-center">City Name</th>
									<th class="datatable-nosort text-center">City Code</th>
									<th class="datatable-nosort text-center">Country Name</th>
								  	<th class="datatable-nosort text-center">Action</th>								
								</tr>
							</thead>
						 <tbody></tbody>
						</table>
					</div>
				</div>
			</div>
        </div>
        
        
        </div>
      </div>
      
    </div>
</body>
</html>