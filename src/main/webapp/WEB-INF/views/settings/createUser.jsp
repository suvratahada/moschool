<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" src="./customJS/setting/createuser.js"></script>    
<title>createUser</title>
</head>
<body>

<div class="container-fluid entry-header">
	<h1 class="entry-title"> <a href="#"><span class="glyphicon glyphicon-user"></span></a> Users</h1>
</div>

<div class="container-fluid vh100">
	<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading"><div id="user_panel_heading" class="panel-title"><a href="#"><span class="glyphicon glyphicon-plus"></span></a> User </div></div>
			
				<div class="panel-body">
					
					<form:form action="" theme="simple" name="formUserDetails" id="formUserDetails">
					
						<div class="form-group">
							<label>User ID:</label>
							<input type="text" name="userId"  id ="user_id" class="form-control" autocomplete="off">
						</div>
						
						<div class="form-group">
							<label>Email ID:</label>
							<input type="text" name="email"  id="email_id" class="form-control" autocomplete="off">
						</div>
						
						<div class="row">		
						<div class="col-md-6">
						<div class="form-group">
							<label>Block/District:</label>
							<input type="text" name="firstName"  id="first_name" class="form-control" placeholder="Name" autocomplete="off">
						</div>	
						</div><!-- end of class="col" -->
						
						<div class="col-md-6">
						<div class="form-group">
							<label>Type</label>
							<select name="lastName"  id="last_name" class="form-control">
								<option>--Select--</option>
								<option value="District">District</option>
								<option value="Block">Block</option>
							</select>
						</div>
						</div><!-- end of class="col" -->
						
						
						<div class="col-md-6" id="div_password">
						<div class="form-group">
							<label>Password:</label>
							<input type="password" name="password"  id="password"  class="form-control" autocomplete="off">
						</div>
						</div><!-- end of class="col" -->
						
						<div class="col-md-6" id="div_conf_password">
						<div class="form-group">
							<label>Confirm Pwd:</label>
							<input type="password" name="confirm_password" id="confirm_password"   class="form-control" autocomplete="off">
						</div>
						</div>
						
						<div class="col-md-6">
						<div class="form-group">
							<label>Contact No:</label>
							<input type="text" name="contactNo"  id="contact_no"  class="form-control" autocomplete="off">
						</div>
						</div><!-- end of class="col" -->	
						
						<div class="col-md-6">
						<div class="form-group dropdown">
							<label class="control-label" id="user_type_label" for="">User Type:</label>
					        <select class="form-control" id = "user_type" name="userType">
					        <option>----Select----</option>
					        <option value="admin">Admin</option>
					        <option value="stc">STC</option>
					        <option value="tc">TC</option>
					        <option value="pma">PMA View</option>
					        </select>
						</div>
						</div><!-- end of class="col" -->
						
						</div><!-- end of class="row" -->		

						<div class="form-action">
							<div class="row">
								<div class="col-xs-6 col-sm-6"><input type="reset" value="Reset" class="btn btn-default btn-block" onclick="resetForm_CreateUser()"></div>
								<div class="col-xs-6 col-sm-6"><input type="button" value="Add" class="btn btn-primary btn-block submit" id="user_form_button"></div>
							</div>
						</div>
					</form:form>
				</div>
			</div>
		</div>
		
		
		
	
		<div class="col-xs-12 col-sm-12 col-md-9">
			<div class="panel panel-default">
				<div class="panel-heading"><div class="panel-title">User Details</div></div>
				<div class="panel-body">
					<table class="table table-condensed table-striped table-bordered display nowrap" id="tblUserDetails" width="100%">
						<thead>
							<tr>
								<th class="datatable-nosort text-center">User ID</th>
								<th class="datatable-nosort text-center">First Name</th>
								<th class="datatable-nosort text-center">Last Name</th>
								<th class="datatable-nosort text-center">Email</th>
								<th class="datatable-nosort text-center">Contact Number</th>
							    <th class="datatable-nosort text-center">Action</th>
							</tr>
						</thead>
					 <tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
    $('input[type="checkbox"].flat-orange').iCheck({
        checkboxClass: 'icheckbox_flat-orange',       
    });
	</script>
</body>
</html>