<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
   <script type="text/javascript" src="./customJS/setting/createmail.js"></script>    
<!--   <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> -->
  <script src="./customJS/iCheck/icheck.min.js" type="text/javascript"></script> 
  
  <link rel="stylesheet" type="text/css" href="./customCSS/iCheck/all.css" />

<title>Add Email</title>

<style type="text/css">
	.panel-default{
		border: 0.2px solid #012b72;
	}
	.panel-default>.panel-heading{
		background-color: #012b72;
		color : white;
	}
	.table-bordered>thead>tr>th {
	    background-color: #012b72;
	    color: #ffffff;
	}
</style>
</head>
<body>

<div class="container-fluid entry-header">
	<h1 class="entry-title"> <a href="#"><span class="glyphicon glyphicon-user"></span></a> Email</h1>
</div>

<div class="container-fluid vh100">
	<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading"><div id="user_panel_heading" class="panel-title"><a href="#"><span class="glyphicon glyphicon-plus"></span></a> Email </div></div>
			
				<div class="panel-body">
					
					<form:form action="" theme="simple" name="formUserDetails" id="formUserDetails">
					
						
						<div class="form-group">
							<label>Email ID:</label>
							<input type="text" name="email_name"  id = "email_name" class="form-control" autocomplete="off">
						</div>
						
						<!-- <div class="row">		
						<div class="col-md-6">
						<div class="form-group">
							<label>First Name:</label>
							<input type="text" name="firstName"  id = "first_name" class="form-control" autocomplete="off">
						</div>	
						</div>end of class="col"
						
						<div class="col-md-6">
						<div class="form-group">
							<label>Last Name:</label>
							<input type="text" name="lastName"  id = "last_name" class="form-control" autocomplete="off">
						</div>
						</div> --><!-- end of class="col" -->
						
						
						<div class="form-group">
							<label>Password:</label>
							<input type="password" name="email_pwd"  id = "email_pwd"  class="form-control" autocomplete="off">
						</div>
						
						<div class="form-group">
							<label>Port:</label>
							<input type="text" name="port"  id = "port"  class="form-control" autocomplete="off">
						</div>
						
						<div class="form-group">
							<label>Description:</label>
							<input type="text" name="description"  id = "description" class="form-control" autocomplete="off">
						</div><!-- end of class="col" -->
						

						<div class="form-action">
							<div class="row">
								<div class="col-xs-6 col-sm-6"><input type="reset" value="Reset" class="btn btn-default btn-block" onclick="resetForm_CreateUser()"></div>
								<div class="col-xs-6 col-sm-6"><input type="button" value="Add" class="btn btn-primary btn-block submit" id="user_form_button" onclick="return changePwdUser()"></button></div>
							</div>
						</div>
					</form:form>
				</div>
			</div>
		</div>
		
		
		
	
		<div class="col-xs-12 col-sm-12 col-md-9">
			<div class="panel panel-default">
				<div class="panel-heading"><div class="panel-title">Email Details</div></div>
				<div class="panel-body">
					<table class="table table-condensed table-striped table-bordered display nowrap" id="tblUserDetails" width="100%">
						<thead>
							<tr>
								<th class="datatable-nosort text-center">Email ID</th>
								<th class="datatable-nosort text-center">Password</th>
								<th class="datatable-nosort text-center">Port Number</th>
								<th class="datatable-nosort text-center">Description</th>
							    <th class="datatable-nosort text-center">Action</th>
							    <th class="datatable-nosort text-center">Active</th>
								
								
							</tr>
						</thead>
					 <tbody></tbody>
					</table>
				</div>
			</div>
		</div>
		
	</div>
</div>

<script>
    $('input[type="checkbox"].flat-orange').iCheck({
        checkboxClass: 'icheckbox_flat-orange',       
    });
	</script>
</body>
</html>