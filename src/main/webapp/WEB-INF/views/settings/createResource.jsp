<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript" src="./customJS/setting/createresource.js?v=1"></script> 
</head>
<body>
<div class="container-fluid entry-header">
	<h1 class="entry-title" style="font-weight: bold;"> <a href="#"><span class="glyphicon glyphicon-user"></span></a> Resource</h1>
</div>


<div class="container-fluid vh100">
	<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading">
				<div id="user_panel_heading" class="panel-title"><a href="#"><span class="glyphicon glyphicon-plus"></span></a> Resource</div></div>
				<div class="panel-body">
					
					<form action="#" class="user_resource" id="new_user_form" method="post" accept-charset="utf-8">
						
						<div class="form-group">
							<input type="hidden" name="resourceId" id="resourceId" value="123" class="form-control">
						</div>		
						
						<div class="form-group">
							<label>Resource Name:</label>
							<input type="text" name="resource_name" id = "resource_name" class="form-control" placeholder="@EX:-MenuName/ResourceName" autocomplete="off">
						</div>
						
						<div class="form-group">
							<label>Resource Type:<span id="resource"></span> </label>
							<select name="resource_type" id="resource_type" class="form-control">
								<option value="Application">Application</option>
								<option value="Record">Record</option>
							</select>
						</div>
						
						<div class="form-group">
							<label>Description:</label>
							<textarea class="form-control" name="descriptaion" id="descriptaion" autocomplete="off"></textarea>
						</div>
						
						<div class="form-action">
							<div class="row">
								<div class="col-xs-6 col-sm-6"><input type="reset" value="Reset" class="btn btn-default btn-block" onClick="resetFormCreateResource()"></div>
								<div class="col-xs-6 col-sm-6"><button type="button" style="width:96px;" class="btn btn-primary" id="addResource">Add</button></div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	
		<div class="col-xs-12 col-sm-12 col-md-9">
			<div class="panel panel-default">
				<div class="panel-heading">
				<div class="panel-title">Resource Details</div></div>
				<div class="panel-body">
					<table class="table table-condensed table-striped table-bordered display nowrap" id="UserResourceDetails" width="100%">
						<thead>
							<tr>
								<th>Resource Id</th>
								<th>Resource Name</th>
								<th>Description</th>
								<th>Type</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
	</div>
</div>



</body>
</html>