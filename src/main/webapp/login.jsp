<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link href="./images/Emblem_of_India.png" rel="shortcut icon" title="Indian Emblem" />
<title>Login - OPEPA - PMA  </title>

<!-- Bootstrap core CSS -->
<link href="./assets/css/bootstrap.css" rel="stylesheet">
<link href="./plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" media="screen">
<!-- Custom styles for this template -->
<link href="./assets/css/main.css" rel="stylesheet">
<link href="./assets/css/extra.css" rel="stylesheet">
<link href="./assets/css/bootstrapValidator.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="./build/css/plugins/dataTables/css/dataTables.bootstrap.css" />
<link rel="stylesheet" type="text/css" href="./build/css/plugins/dataTables/css/jquery.dataTables.min.css" />
<link rel="stylesheet" type="text/css" href="./build/css/plugins/dataTables/css/dataTables.responsive.css" />
<link rel="stylesheet" id="parent-style-css" href="./include/docs/style(1).css" type="text/css" media="all">
<link rel="stylesheet" id="child-style-css" href="./include/docs/style(2).css" type="text/css" media="all">
<link rel="stylesheet" type="text/css" href="./assets/css/chemical.css" />
<script src="./plugins/jquery/jquery.min.js"></script>
<script src="./plugins/jquery/jquery.flip.min.js"></script>
<script src="./plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="./assets/js/bootstrapValidator.js"></script>
<script src="./customJS/common/md5_5034.js"></script>
<script src="./customJS/common/profile_sha.js"></script>
<script src="./customJS/common/sha512.js"></script>
<script src="./customJS/chemical/login.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<meta name="_csrf" content="${_csrf.token}"/>
<meta name="_csrf_header" content="${_csrf.headerName}"/>
</head>
<style>
li {
  position: relative;
  list-style: none;
}
a{
	color: #0f3e12;
}
a:hover {
    color: #0f3e12;
    /* font-size:17px; */
    font-weight: 700;
    border-bottom: 2px solid white;
    /* text-decoration: underline white; */
}
li.str:before {
  content: "*";
  font-weight: 900;
}
.btn{
	color:#fff;
	font-weight: bold;
}
.btn:HOVER{
	color:#ffff80;
	font-family: cursive;
	font-stretch: ultra-condensed;
	font-weight: bold;
}
.btn1{
	background-color:white;
	color:black;
}
.login-margin{
margin-bottom:20px;
}

@media (max-width: 500px){
	.container{
		margin-top: 20px !important;
	}
	
	footer{
		display: none;
	}
	
	#sb-logo{
		margin-left: 10%;
	}
}
</style>
<script type="text/javascript">
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
</script>

<body class="page-template-default page page-id-483 _masterslider _ms_version_2.9.5 parallax-on columns-3;" style="overflow:hidden">
		 <!-- Modal -->

	<div id="page" class="hfeed site">

		<header id="masthead" class="logo-side">
			<!-- <div
				style="width: 100%; height: 5px; margin: 0px 0px 5px 0px; border-bottom: 1px solid #00b9f5; background: #00b9f5; float: left;"></div> -->
			<div class="middleblock">
				<div class="mid-content clearfix">
					<div id="site-logo">
						<a href="" rel="home" class = "sw-logo"> <img src="./images/opepa_logo.png"
							alt="OPEPA" >
						</a>
					</div>
					<div class="toprightmenu">
						<!-- <a class="sw-logo"
							href=""> <img
							src="./images/helplini.png" alt="Make In India">
						</a> -->
						<a class="sw-logo" id="sb-logo"
							target="_blank"> <img
							src="./images/ssa.png" alt="Swach Bharat">
						</a>
					</div>
				</div>
			</div>
			<div style="width: 100%; height: 30px; margin: 0px 0px -5px 0px; border-bottom: 1px solid #0f3e12; background: #0f3e12; float: left; color: #fff;">
				<marquee direction = "right" width="65%" behavior="alternate" style="margin-left:15%;" >Odisha Primary Education Programme Authority (Project Monitoring App)</marquee>
			</div>
				
			<div class="social-icons" style="margin-top: 0px;"></div>
		</header>
	</div>
	<div class="container" style="margin-top: 126px; background:#fafafa url(./images/background_new_pma.jpg); width:100%;">
		<div class="row">
			<div class="col-lg-offset-4 col-lg-4" style="padding-top: 0px;">
				<div class="panel panel-primary" style="background-color:0f3e12;  background: transparent; border-color: transparent;">
					<div style="width: 100%; background: #fff; margin-bottom: 10%;">
						<div class="panel-heading step-heading"
							style="font-size: 10px; color: #fff; margin-top: 20px">
							<b><i class="fa fa-user"></i> Login</b>
						</div>
						<div class="alert alert-danger alert-dismissible" role="alert"
							style="display: none;">
							<!-- Alert for error -->
							<div id="alertmessage"></div>
						</div>
						<div style="border-radius: 0px; border:1px solid #0f3e12; padding-left: 5px; padding-right: 5px; padding-top: 10px;">
							<!--Added by Lusi on date-26-Oct-2017  -->
							 <input type="hidden" id="saltKey" name="saltKey" value="${saltKey}">
							<form class="form-horizontal"
								action="<c:url value='authentication' />" method="post" id="frmLogin" name="frmLogin">

								<table class="table table-responsive" style="" align="center">
									<tr>
										<td>
											<div class="col-sm-12">
												<label class="control-label col-sm-12" style="text-align: left;"><i
													style="color: red; font-size: 18px;">*</i> User Id :</label>
												<div class="col-sm-offset-1 col-sm-10">
												<div class="form-group">
													<input type="text" class="form-control" id="j_username"
														name="j_username" placeholder="Enter User Id " autocomplete="off">
												</div>
												</div>
												<span id="spanPhone" style="color: red; font-size: 12px"></span>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="col-sm-12">
												<label class="control-label col-sm-12" style="text-align: left;"><i
													style="color: red; font-size: 18px;">*</i> Password :</label>
												<div class="col-sm-offset-1 col-sm-10">
												<div class="form-group">
													<input type="password" class="form-control" id="password"
														name="password" placeholder="Enter Password"
														autocomplete="off">
													<input type="hidden" class="form-control" id="j_password" name="j_password">
												</div>
												</div>
												<input type="hidden" class="form-control" name="${_csrf.parameterName}" value="${_csrf.token}"/>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div class="row col-md-offset-1 col-md-10">
					                        <div  id ="div_captcha"  style = "text-align: -webkit-center;margin-bottom: 2px">
									               <img id="img_captcha" src='./captcha/getCaptcha.do'> 
									               <!-- <button type="button" value="" name="btnReload" id="refreshImageId"  
									               style="background-color: rgb(240, 143, 33);height: 50px; float: right;"><i class="fa fa-refresh" style="   font-size: 23px;"></i></button> -->
									               <button class="btn waves-effect btn-recpps" 
									               style = "margin-left: -3px;font-size: 7px;height: 20%;width: 17%;padding: 0px;text-align: center;background: #00b9f5;border-radius: 0px;"id="btnCaptchaReload" name="btnCaptchaReload" type="button">
									                <i class="fa fa-refresh" style="   font-size: 23px;"></i>
									               </button>
									           </div>
									           <input type="text" style = "text-align: -webkit-center;margin-left: 16%;width: 52%;height: 19%;" id="captcha" name="captcha" required autofocus autocomplete="off">
								            </div>
											<c:if test="${not empty error}">
												<h5 id="regmsg" style="color: red; text-align: center;">&nbsp;&nbsp;${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}</h5>
												
											</c:if>
											<c:if test="${not empty msg}">
												<h5 id="regmsg" style="color: red; text-align: center;" id="err">&nbsp;&nbsp;${msg}</h5>
											</c:if>
											<c:if test="${isNgoPresent == true}">
												<label id="regmsg" class="control-label col-sm-12" style="text-align: center;color:red; font-size:20px;"><spring:message code="message.user.registered" /></label>
											</c:if>
											<c:if test = "${errOtp == true}">
									    		<label id="regmsg" class="control-label col-sm-12" style="text-align: center;color:red; font-size:20px;"><spring:message code="error.otp.invalid" /></label>
											</c:if>
											
											<c:if test = "${pwdstatus == true}">
									    		<label id="regmsg" class="control-label col-sm-12" style="text-align: center;color:green; font-size:20px;"><spring:message code="message.pwd.reset.success" /></label>
											</c:if>
											<c:if test = "${pwdstatus == false}">
									    		<label id="regmsg" class="control-label col-sm-12" style="text-align: center;color:red; font-size:20px;"><spring:message code="message.pwd.reset.failure" /></label>
											</c:if>
									    	<c:if test = "${regstatus == true}">
									    		<label id="regmsg" class="control-label col-sm-12" style="text-align: center;color:green; font-size:20px;"><spring:message code="message.registration.success" /></label>
											</c:if>
											<c:if test = "${regstatus == false}">
									    		<label id="regmsg" class="control-label col-sm-12" style="text-align: center;color:red; font-size:20px;"><spring:message code="message.registration.failure" /></label>
											</c:if>
											<c:if test = "${msgInvalidCaptcha == true}">
									    		<label id="regmsg" class="control-label col-sm-12" style="text-align: center;color:red; font-size:20px;"><spring:message code="message.invalid.captcha" /></label>
											</c:if>
											
											
											 <c:if test = "${pwdustatus == true}">
				    							<label id="regmsg" class="control-label col-sm-12" style="text-align: center;color:green;"><spring:message code="message.pwd.reset.success" /></label>
											 </c:if>
											
										</td>
									</tr>
								</table>
								<div class="form-group">
									<div class="col-lg-12" align="center">
										<button type="submit" class="stybtn" id="btnCheck"
											style="background-color: #0f3e12; border-radius: 8px;"
											name="btnCheck" onclick="return changeform()">Login</button>
									</div>
								</div>
								<!-- <div class="form-group">
		                        	<div class="col-md-12 text-right">
		                            	<a href="./reset.html">Forgot Password?</a>
		                       		 </div>
		                   		 </div> -->
							</form>
						</div>
					</div>
				</div>
			</div>
			<div>
		</div>
	</div>
	</div>
	<!--/container--> <!-- FOOTER -->
	<div class="container-fluid" style="background: #0f3e12;position: fixed; color: #fff; left: 0px; bottom: 0px; width: 100%; z-index: 1">
		<div class="container" style="padding: 10px 0px 10px 0px;">
			<footer>
				<div class="row">
					<div class="col-lg-12" style="font-size: 12px;">
						<div style="text-align: center; font-size: 14px;">
							Website Content Managed by <span
								style="font-size: 14px; font-weight: 900;">Department of
								Odisha Primary Education programme Authority</span> Designed, Developed and
							Hosted by <span
								style="font-size: 14px; font-weight: 900;"><a
								target="_blank" href="http://www.silicontechlab.com"style="color:gold;">Silicon
									Techlab Pvt. Ltd.( STL )</a></span> © 2018
						</div>
					</div>
				</div>
			</footer>
		</div>
	</div>
  <script type="text/javascript">
    	$(window).load(function() {
    		if($('#regmsg').text() != ''){
    			setTimeout(function pageRedirect() {
    				$('#regmsg').hide();
    				//window.location.replace("./login.html");
    		    }, 20000);
    		}
 		});
    	//Disable cut copy paste
       $('body').bind('cut copy paste', function (e) {
           e.preventDefault();
       });
      
       //Disable mouse right click
       $("body").on("contextmenu",function(e){
       	alert("Right click is disabled.");
           return false;
       });
       
		$('#btnCaptchaReload').on({
		    'click': function(){
		        $('#img_captcha').attr('src','./captcha/getCaptcha.do'+'?'+Math.random());
		    }
		});

 </script>
</body>
</html>
