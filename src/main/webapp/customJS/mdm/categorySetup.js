$('document').ready(function(){
	
	var update = false;
	
	$('#categoryModal').on('hidden.bs.modal', function (e) {
		update = false;
		$('#catg_id').attr('readonly', false);
	})
	
	var categoryTable = $('#tblCategoryDetails').DataTable({
		 "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
		 "pageLength": 10,	
		 "bFilter": true,
		 "bLengthChange": false,
		 "scrollX": true,
		 "columnDefs":[
		   {"className": "dt-center", "targets": "_all"},  
           {"targets":0,"data":"catg_id"},
           {"targets":1,"data":"catg_desc"},
           {"targets":2,"data": null, 
		    	"defaultContent": '<button type="button" id="Edit" action="edit" class="btn btn-default btn-sm btn-edit"><span class="glyphicon glyphicon-edit"></span></button> <button type="button" action="delete" id="Delete" class="btn btn-default btn-sm btn-delete"><span class="glyphicon glyphicon-trash"></span></button>'}                  
		  
        ]
	});
	
	var getCategoryDetails = function(){
		$.ajax({
			 url: "./mdm/getCategoryDetails.do",
			 type: "POST",			
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			success: function(result){
				var data = result.data;
				console.log('--------categories---------');
				console.log('data', data)
				console.log('---------------------------');
				categoryTable.clear();
				categoryTable.rows.add(data).draw();
           },
		    failure: function () {
		        swal("Failed!");
		    }
			 
		});
	}
	
	getCategoryDetails();
	
	
	function objectifyForm(formArray) {//serialize data function
	   var returnArray = {};
	   for (var i = 0; i < formArray.length; i++){
	     returnArray[formArray[i]['name']] = formArray[i]['value'];
	   }
	   return returnArray;
	 }
	
	//save and update category
	
	var saveOrUpdateCategoryDetails = function(update){
		var postData = objectifyForm($("#formCategoryDetails").serializeArray());
		postData["update"] = update;
		console.log('postData: ',postData);
		$.ajax({
			 url: "./mdm/saveOrUpdateCategoryDetails.do",
			 type: "POST",
			 dataType:"JSON",
			 data: JSON.stringify(postData),
             contentType:"application/json",
             beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
  			},
			 success: function(data, textStatus, jqXHR) {
	                if (data.success) {
	                	$('#categoryModal').modal('hide');
	                	resetForm_Category();
	                	swal(data.msg);
	                	getCategoryDetails();
		              }
	            },
			 error:function(jqXHR, status, error){
			 	swal(error);
			 	resetForm_Category();
			 }
		});
		}
	
	
	$('#tblCategoryDetails tbody').on( 'click', 'button[action=edit]', function () {
		update = true;
		var data = categoryTable.row( $(this).parents('tr') ).data(); 
		$('#categoryModal').modal();
		$('#catg_id').attr('readonly', true);
		$('#catg_id').val(data.catg_id);
		$('#catg_desc').val(data.catg_desc);
	});
	
	$('#submit_category').click(function(){
		saveOrUpdateCategoryDetails(update);
	})
	
	//delete category
	
	var deleteCategory = function(catgId){
        $.ajax({
			 url: "./mdm/deleteCategory/"+catgId+".do",
			 type: "POST",		
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			 },
			 contentType : 'application/json',
	   		 dataType: "json",
			 success:function(result) {
				 console.log(result);
				 
			 },
			 error:function(jqXHR, status, error){
			 	swal(error);
			 }
		});
	}
	
	$('#tblCategoryDetails tbody').on( 'click', 'button[action=delete]', function () {
		var data = categoryTable.row( $(this).parents('tr') ).data();
		var catgId = data.catg_id;
		
		$.confirm({
			icon: 'glyphicon glyphicon-trash',/*'fa fa-spinner fa-spin',*/
    	    title: 'Confirm!',
    	    content: 'The record will be deleted!',
    	    draggable: true,
    	    theme: "modern",
    	    type: 'red',
    	    typeAnimated: true,
    	    buttons: {
    	        'delete': function () {
    	        	deleteCategory(catgId);
    	        },
    	        cancel: function () {
    	            $.alert('Canceled!');
    	        },
    	    }
    	});
	})
	
	
	var resetForm_Category = function(){
		$('#catg_id').val();
		$('#catg_desc').val();
	}
	
	/*ready function ends here*/
})