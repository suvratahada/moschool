$(document).ready(function(){
	
	$('#addEmployee').on('click', function(){
		$("#empId").removeAttr("readonly", "false"); 
		resetEmployeeForm();
		$('#employeeModal').modal();
	})
	
	var update = false;
	
	function objectifyForm(formArray) {//serialize data function
	   var returnArray = {};
	   for (var i = 0; i < formArray.length; i++){
	     returnArray[formArray[i]['name']] = formArray[i]['value'];
	   }
	   return returnArray;
	 }
	
	getDataToEmployeeDetailsTable();
	
	var employeeTable = $('#tblEmployeeDetails').DataTable({		
		 "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
		 "pageLength": 10,	
		 "bFilter": true,
		 "bLengthChange": false,
		 "scrollX": true,
		 "columnDefs":[
		   {"className": "dt-center", "targets": "_all"},  
		   /*{"targets":0,"data":"id"},*/
          {"targets":0,"data":"empId"},
          {"targets":1,"data":"empType"},
          /*{"targets":3,"data":"rural_urban"},*/
          {"targets":2,"data": null, 
        	  "render":function(data){
          	    var buttons='';
          	    if(data.recordStatus == 1){
      	buttons = `<button type="button" id="Edit" action="edit" class="btn btn-default btn-sm btn-edit"><span class="glyphicon glyphicon-edit"></span></button> 
      	 &nbsp;<button type="button" action="delete" id="Delete" class="btn btn-default btn-sm btn-delete"><span class="glyphicon glyphicon-trash"></span></button>
      	 &nbsp;<button type="button" action="toggle" id="toggle" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-off"></span></button>`;
          	    
          	    }else{
      	buttons = `<button type="button" id="Edit" action="edit" class="btn btn-default btn-sm btn-edit"><span class="glyphicon glyphicon-edit"></span></button> 
      	 &nbsp;<button type="button" action="delete" id="Delete" class="btn btn-default btn-sm btn-delete"><span class="glyphicon glyphicon-trash"></span></button>
      	 &nbsp;<button type="button" action="toggle" id="toggle" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-off"></span></button>`;
          	    }
          	    return buttons;
        	  }
		  }                  
		  
       ]
	});
	
	function getDataToEmployeeDetailsTable() {
		$.ajax({
			 url: "./mdm/getEmployeeDetails.do",
			 type: "GET",			
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			 success:function(result) {
				 console.log("====================getEmployeeDetails====================");
				 console.log(result);
				 console.log("====================getEmployeeDetails====================");
				 employeeTable.clear();
				 employeeTable.rows.add(result.data).draw();
			 },
			 error:function(jqXHR, status, error){
			 	swal(error);
			 }
		});
	}
	
	var saveOrUpdateEmployeeDetails = function(update){
		var postData = objectifyForm($("#formEmployeeDetails").serializeArray());
		postData["update"] = update;
		console.log('============postData============');
		console.log(postData);
		console.log('============postData============');
			$.ajax({
				 url: "./mdm/saveOrUpdateEmployeeDetails.do",
				 type: "POST",
				 dataType:"JSON",
				 data: JSON.stringify(postData),
	             contentType:"application/json",
	             beforeSend: function(xhr) {
		               xhr.setRequestHeader(header, token);
	  			},
				 success: function(data, textStatus, jqXHR) {
		                if (data.success) {
		                	//resetForm_CreateBlock();
		                	swal(data.msg);
		                	getDataToEmployeeDetailsTable();
		                	resetEmployeeForm();
			              }
		            },
				 error:function(jqXHR, status, error){
				 	swal(error);
				 	//resetForm_CreateBlock();
				 }
			});
		}
	
	var activateOrDeActiveEmployee = function(id, recordStatus){
			$.ajax({
				 url: "./mdm/activateOrDeActiveEmployee/"+id+"/"+recordStatus+".do",
				 type: "POST",
				 dataType:"JSON",
	             contentType:"application/json",
	             beforeSend: function(xhr) {
		               xhr.setRequestHeader(header, token);
	  			},
				 success: function(data, textStatus, jqXHR) {
		                if (data.success) {
		                	swal(data.msg);
		                	getDataToEmployeeDetailsTable();
			              }
		            },
				 error:function(jqXHR, status, error){
				 	swal(error);
				 }
			});
		}
	
		var deleteEmployee = function(id){
			$.ajax({
				 url: "./mdm/deleteEmployee/"+id+".do",
				 type: "POST",
				 dataType:"JSON",
	             contentType:"application/json",
	             beforeSend: function(xhr) {
		               xhr.setRequestHeader(header, token);
	  			},
				 success: function(data, textStatus, jqXHR) {
		                if (data.success) {
		                	swal(data.msg);
		                	getDataToEmployeeDetailsTable();
			              }
		            },
				 error:function(jqXHR, status, error){
				 	swal(error);
				 }
			});
		}
	
		$('#tblEmployeeDetails tbody').on( 'click', 'button[action=edit]', function () {
			update = true;
			var data = employeeTable.row( $(this).parents('tr') ).data(); 
			
			console.log('================tblEmployeeDetails data================');
			console.log(data);
			console.log('================tblEmployeeDetails data================');
			
			$('#empType').val(data.empType);
			$('#empId').val(data.empId);
			$('#employeeModal').modal();
			$("#empId").attr("readonly", "true"); 
			
		});
		
		$('#tblEmployeeDetails tbody').on( 'click', 'button[action=delete]', function () {
			var data = employeeTable.row( $(this).parents('tr') ).data(); 
			var empId = data.empId;
			
			$.confirm({
				icon: 'glyphicon glyphicon-trash',/*'fa fa-spinner fa-spin',*/
	    	    title: 'Confirm!',
	    	    content: 'The record will be deleted!',
	    	    draggable: true,
	    	    theme: "modern",
	    	    type: 'red',
	    	    typeAnimated: true,
	    	    buttons: {
	    	        'delete': function () {
	    	        	deleteEmployee(empId);
	    	        },
	    	        cancel: function () {
	    	            $.alert('Cancelled!');
	    	        },
	    	    }
	    	});
			
		});
		
		$('#tblEmployeeDetails tbody').on( 'click', 'button[action=toggle]', function () {
			update = true;
			var data = employeeTable.row( $(this).parents('tr') ).data(); 
			var empId = data.empId;
			var recordStatus = data.recordStatus;
			
			var content;
			var type;
			var button;
			var btn_class;
			
			if (recordStatus==1) {
				content='The record will be DeActivated!';
				//icon='glyphicon glyphicon-off';
				type='red';
				button='DeActivate';
				btn_class='btn-red'
			} else {
				content='The record will be Activated!';
				//icon='glyphicon glyphicon-off';
				type='green';
				button='Activate';
				btn_class='btn-green'
			}
			
			console.log('================tblEmployeeDetails data================');
			console.log(data);
			console.log('================tblEmployeeDetails data================');
			
			$.confirm({
				//icon: 'glyphicon glyphicon-off',/*'fa fa-spinner fa-spin',*/
	    	    title: 'Confirm!',
	    	    content: content,
	    	    draggable: true,
	    	    theme: "modern",
	    	    type: type,
	    	    typeAnimated: true,
	    	    buttons: {
	    	    	 specialKey: {
	    	             text: button,
	    	             btnClass: btn_class,
	    	             action: function(){
	    	            	 activateOrDeActiveEmployee(empId, recordStatus);
	    	             }
	    	         },
	    	    	cancel: function () {
	    	    		btnClass: 'btn-red'
	    	    		$.alert('Cancelled!');
	    	    	},
	    	    }
	    	});
			
		});
		
		/*
		$('#submit_category').click(function(){
			saveOrUpdateCategoryDetails(update);
		})*/
		
		function resetEmployeeForm(){
		//$("#formEmployeeDetails")[0].reset();
			$('#empType').val("");
			$('#empId').val("");
		}
		
		$('#reset').on('click', function(){
			resetEmployeeForm();
		})
		
		$('#employeeFormButton').on('click', function(){
			saveOrUpdateEmployeeDetails(update);
		})
	
})