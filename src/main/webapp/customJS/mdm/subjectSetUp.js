$(document).ready(function(){
	
	$('#addSubject').on('click', function(){
		update = false;
		$("#subjectId").removeAttr("readonly", "false"); 
		resetSubjectForm();
		$('#subjectModal').modal();
	})
	
	var update = false;
	
	function objectifyForm(formArray) {//serialize data function
	   var returnArray = {};
	   for (var i = 0; i < formArray.length; i++){
	     returnArray[formArray[i]['name']] = formArray[i]['value'];
	   }
	   return returnArray;
	 }
	
	getDataToSubjectDetailsTable();
	
	var subjectTable = $('#tblSubjectDetails').DataTable({
		
		 "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
		 "pageLength": 10,	
		 "bFilter": true,
		 "bLengthChange": false,
		 "scrollX": true,
		 "columnDefs":[
		   {"className": "dt-center", "targets": "_all"},  
		   /*{"targets":0,"data":"id"},*/
          {"targets":0,"data":"subjectId"},
          {"targets":1,"data":"subjectDesc"},
          /*{"targets":3,"data":"rural_urban"},*/
          {"targets":2,"data": null, 
        	  "render":function(data){
            	    var buttons='';
            	    if(data.recordStatus == 1){
        	buttons = `<button type="button" id="Edit" action="edit" class="btn btn-default btn-sm btn-edit"><span class="glyphicon glyphicon-edit"></span></button> 
        	 &nbsp;<button type="button" action="delete" id="Delete" class="btn btn-default btn-sm btn-delete"><span class="glyphicon glyphicon-trash"></span></button>
        	 &nbsp;<button type="button" action="toggle" id="toggle" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-off"></span></button>`;
            	    
            	    }else{
        	buttons = `<button type="button" id="Edit" action="edit" class="btn btn-default btn-sm btn-edit"><span class="glyphicon glyphicon-edit"></span></button> 
        	 &nbsp;<button type="button" action="delete" id="Delete" class="btn btn-default btn-sm btn-delete"><span class="glyphicon glyphicon-trash"></span></button>
        	 &nbsp;<button type="button" action="toggle" id="toggle" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-off"></span></button>`;
            	    }
            	    return buttons;
          	  }
          }                  
		  
       ]
	});
	
	function getDataToSubjectDetailsTable() {
		$.ajax({
			 url: "./mdm/getSubjectDetails.do",
			 type: "GET",			
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			 success:function(result) {
				 console.log("====================getSubjectDetails====================");
				 console.log(result);
				 console.log("====================getSubjectDetails====================");
				 subjectTable.clear();
				 subjectTable.rows.add(result.data).draw();
			 },
			 error:function(jqXHR, status, error){
			 	swal(error);
			 }
		});
	}
	
	var saveOrUpdateSubjectDetails = function(update){
		var postData = objectifyForm($("#formSubjectDetails").serializeArray());
		postData["update"] = update;
		console.log('============postData============');
		console.log(postData);
		console.log('============postData============');
			$.ajax({
				 url: "./mdm/saveOrUpdateSubjectDetails.do",
				 type: "POST",
				 dataType:"JSON",
				 data: JSON.stringify(postData),
	             contentType:"application/json",
	             beforeSend: function(xhr) {
		               xhr.setRequestHeader(header, token);
	  			},
				 success: function(data, textStatus, jqXHR) {
		                if (data.success) {
		                	//resetForm_CreateBlock();
		                	swal(data.msg);
		                	getDataToSubjectDetailsTable();
		                	resetSubjectForm();
			              }
		            },
				 error:function(jqXHR, status, error){
				 	swal(error);
				 	//resetForm_CreateBlock();
				 }
			});
		}
	
	var activateOrDeActiveSubjectDetails = function(id, recordStatus){
			$.ajax({
				 url: "./mdm/activateOrDeActiveSubjectDetails/"+id+"/"+recordStatus+".do",
				 type: "POST",
				 dataType:"JSON",
	             contentType:"application/json",
	             beforeSend: function(xhr) {
		               xhr.setRequestHeader(header, token);
	  			},
				 success: function(data, textStatus, jqXHR) {
		                if (data.success) {
		                	swal(data.msg);
		                	getDataToSubjectDetailsTable();
			              }
		            },
				 error:function(jqXHR, status, error){
				 	swal(error);
				 }
			});
		}
	
		var deleteSubject = function(id){
			$.ajax({
				 url: "./mdm/deleteSubject/"+id+".do",
				 type: "POST",
				 dataType:"JSON",
	             contentType:"application/json",
	             beforeSend: function(xhr) {
		               xhr.setRequestHeader(header, token);
	  			},
				 success: function(data, textStatus, jqXHR) {
		                if (data.success) {
		                	swal(data.msg);
		                	getDataToSubjectDetailsTable();
			              }
		            },
				 error:function(jqXHR, status, error){
				 	swal(error);
				 }
			});
		}
	
		$('#tblSubjectDetails tbody').on( 'click', 'button[action=edit]', function () {
			update = true;
			var data = subjectTable.row( $(this).parents('tr') ).data(); 
			
			console.log('================tblSubjectDetails data================');
			console.log(data);
			console.log('================tblSubjectDetails data================');
			
			$('#subjectDesc').val(data.subjectDesc);
			$('#subjectId').val(data.subjectId);
			$('#subjectModal').modal();
			$("#subjectId").attr("readonly", "true"); 
			
		});
		
		$('#tblSubjectDetails tbody').on( 'click', 'button[action=delete]', function () {
			var data = subjectTable.row( $(this).parents('tr') ).data(); 
			var subjectId = data.subjectId;
			
			$.confirm({
				icon: 'glyphicon glyphicon-trash',/*'fa fa-spinner fa-spin',*/
	    	    title: 'Confirm!',
	    	    content: 'The record will be deleted!',
	    	    draggable: true,
	    	    theme: "modern",
	    	    type: 'red',
	    	    typeAnimated: true,
	    	    buttons: {
	    	        'delete': function () {
	    	        	deleteSubject(subjectId);
	    	        },
	    	        cancel: function () {
	    	            $.alert('Cancelled!');
	    	        },
	    	    }
	    	});
			
		});
		
		$('#tblSubjectDetails tbody').on( 'click', 'button[action=toggle]', function () {
			update = true;
			var data = subjectTable.row( $(this).parents('tr') ).data(); 
			var subjectId = data.subjectId;
			var recordStatus = data.recordStatus;
			
			var content;
			var type;
			var button;
			var btn_class;
			
			if (recordStatus==1) {
				content='The record will be DeActivated!';
				//icon='glyphicon glyphicon-off';
				type='red';
				button='DeActivate';
				btn_class='btn-red'
			} else {
				content='The record will be Activated!';
				//icon='glyphicon glyphicon-off';
				type='green';
				button='Activate';
				btn_class='btn-green'
			}
			
			console.log('================activateOrDeActiveSubjectDetails data================');
			console.log(data);
			console.log('================activateOrDeActiveSubjectDetails data================');
			
			$.confirm({
				//icon: 'glyphicon glyphicon-off',/*'fa fa-spinner fa-spin',*/
	    	    title: 'Confirm!',
	    	    content: content,
	    	    draggable: true,
	    	    theme: "modern",
	    	    type: type,
	    	    typeAnimated: true,
	    	    buttons: {
	    	    	 specialKey: {
	    	             text: button,
	    	             btnClass: btn_class,
	    	             action: function(){
	    	            	 activateOrDeActiveSubjectDetails(subjectId, recordStatus);
	    	             }
	    	         },
	    	    	cancel: function () {
	    	    		btnClass: 'btn-red'
	    	    		$.alert('Cancelled!');
	    	    	},
	    	    }
	    	});
			
		});
		
		/*
		$('#submit_category').click(function(){
			saveOrUpdateCategoryDetails(update);
		})*/
		
		function resetSubjectForm(){
		//$("#formSubjectDetails")[0].reset();
			if(update){
				$('#subjectDesc').val("");
			}else{
				$('#subjectDesc').val("");
				$('#subjectId').val("");
			}
			console.log("===========is Update==================");
			console.log(update);
			console.log("===========is Update==================");
		}
		
		$('#reset').on('click', function(){
			resetSubjectForm();
		})
		
		$('#subjectFormButton').on('click', function(){
			saveOrUpdateSubjectDetails(update);
		})
	
})