$(document).ready(function(){
	
	$('#addSchoolDetails').on('click', function(){
		update = false;
		$("#schoolId").removeAttr("readonly", "false"); 
		resetSchoolDetailsForm();
		$('#schoolDetailsSetupModal').modal();
	})
	
	var update = false;
	
	function objectifyForm(formArray) {//serialize data function
	   var returnArray = {};
	   for (var i = 0; i < formArray.length; i++){
	     returnArray[formArray[i]['name']] = formArray[i]['value'];
	   }
	   return returnArray;
	 }
	
	getDataToSchoolDetailsTable();
	
	var schoolDetailsTable = $('#tblSchoolDetails').DataTable({
		
		 "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
		 "pageLength": 10,	
		 "bFilter": true,
		 "bLengthChange": false,
		 "scrollX": true,
		 "columnDefs":[
		   {"className": "dt-center", "targets": "_all"},  
		   /*{"targets":0,"data":"id"},*/
          {"targets":0,"data":"udiseCode"},
          {"targets":1,"data":"schoolId"},
          {"targets":2,"data":"affiliationBoard"},
          {"targets":3,"data":"schoolAffiliatedCode"},
          {"targets":4,"data":"mediumLanguageId"},
          {"targets":5,"data": null, 
        	  "render":function(data){
            	    var buttons='';
            	    if(data.recordStatus == 1){
        	buttons = `<button type="button" id="Edit" action="edit" class="btn btn-default btn-sm btn-edit"><span class="glyphicon glyphicon-edit"></span></button> 
        	 &nbsp;<button type="button" action="delete" id="Delete" class="btn btn-default btn-sm btn-delete"><span class="glyphicon glyphicon-trash"></span></button>
        	 &nbsp;<button type="button" action="toggle" id="toggle" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-off"></span></button>`;
            	    
            	    }else{
        	buttons = `<button type="button" id="Edit" action="edit" class="btn btn-default btn-sm btn-edit"><span class="glyphicon glyphicon-edit"></span></button> 
        	 &nbsp;<button type="button" action="delete" id="Delete" class="btn btn-default btn-sm btn-delete"><span class="glyphicon glyphicon-trash"></span></button>
        	 &nbsp;<button type="button" action="toggle" id="toggle" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-off"></span></button>`;
            	    }
            	    return buttons;
          	  }
          }                  
		  
       ]
	});
	
	function getDataToSchoolDetailsTable() {
		$.ajax({
			 url: "./mdm/getSchoolDetails.do",
			 type: "GET",			
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			 success:function(result) {
				 console.log("====================getSchoolDetails====================");
				 console.log(result);
				 console.log("====================getSchoolDetails====================");
				 schoolDetailsTable.clear();
				 schoolDetailsTable.rows.add(result.data).draw();
			 },
			 error:function(jqXHR, status, error){
			 	swal(error);
			 }
		});
	}
	
	var saveOrUpdateSubjectDetails = function(update){
		var postData = objectifyForm($("#formSchoolDetails").serializeArray());
		postData["update"] = update;
		console.log('============postData============');
		console.log(postData);
		console.log('============postData============');
			$.ajax({
				 url: "./mdm/saveOrUpdateSchoolDetails.do",
				 type: "POST",
				 dataType:"JSON",
				 data: JSON.stringify(postData),
	             contentType:"application/json",
	             beforeSend: function(xhr) {
		               xhr.setRequestHeader(header, token);
	  			},
				 success: function(data, textStatus, jqXHR) {
		                if (data.success) {
		                	//resetForm_CreateBlock();
		                	swal(data.msg);
		                	getDataToSchoolDetailsTable();
		                	resetSchoolDetailsForm();
			              }
		            },
				 error:function(jqXHR, status, error){
				 	swal(error);
				 	//resetForm_CreateBlock();
				 }
			});
		}
	
	var activateOrDeActiveSchoolDetails = function(id, recordStatus){
			$.ajax({
				 url: "./mdm/activateOrDeActiveSchoolDetails/"+id+"/"+recordStatus+".do",
				 type: "POST",
				 dataType:"JSON",
	             contentType:"application/json",
	             beforeSend: function(xhr) {
		               xhr.setRequestHeader(header, token);
	  			},
				 success: function(data, textStatus, jqXHR) {
		                if (data.success) {
		                	swal(data.msg);
		                	getDataToSchoolDetailsTable();
		                	getDataToSchoolDetailsTable();
			              }
		            },
				 error:function(jqXHR, status, error){
				 	swal(error);
				 }
			});
		}
	
		var deleteSchool = function(id){
			$.ajax({
				 url: "./mdm/deleteSchoolDetails/"+id+".do",
				 type: "POST",
				 dataType:"JSON",
	             contentType:"application/json",
	             beforeSend: function(xhr) {
		               xhr.setRequestHeader(header, token);
	  			},
				 success: function(data, textStatus, jqXHR) {
		                if (data.success) {
		                	swal(data.msg);
		                	getDataToSchoolDetailsTable();
			              }
		            },
				 error:function(jqXHR, status, error){
				 	swal(error);
				 }
			});
		}
	
		$('#tblSchoolDetails tbody').on( 'click', 'button[action=edit]', function () {
			update = true;
			var data = schoolDetailsTable.row( $(this).parents('tr') ).data(); 
			
			console.log('================tblSchoolDetails data================');
			console.log(data);tblSchoolDetails
			console.log('================tblSubjectDetails data================');
			
			$('#udiseCode').val(data.udiseCode);
			$('#schoolId').val(data.schoolId);
			$('#affiliationBoard').val(data.affiliationBoard);
			$('#schoolAffiliatedCode').val(data.schoolAffiliatedCode);
			$('#mediumLanguageId').val(data.mediumLanguageId);
			$('#schoolDetailsSetupModal').modal();
			$("#schoolId").attr("readonly", "true"); 
			
		});
		
		$('#tblSchoolDetails tbody').on( 'click', 'button[action=delete]', function () {
			var data = schoolDetailsTable.row( $(this).parents('tr') ).data(); 
			var schoolId = data.schoolId;
			$.confirm({
				icon: 'glyphicon glyphicon-trash',/*'fa fa-spinner fa-spin',*/
	    	    title: 'Confirm!',
	    	    content: 'The record will be deleted!',
	    	    draggable: true,
	    	    theme: "modern",
	    	    type: 'red',
	    	    typeAnimated: true,
	    	    buttons: {
	    	        'delete': function () {
	    	        	deleteSchool(schoolId);
	    	        },
	    	        cancel: function () {
	    	            $.alert('Cancelled!');
	    	        },
	    	    }
	    	});
			
		});
		
		$('#tblSchoolDetails tbody').on( 'click', 'button[action=toggle]', function () {
			update = true;
			var data = schoolDetailsTable.row( $(this).parents('tr') ).data(); 
			var schoolId = data.schoolId;
			var recordStatus = data.recordStatus;
			
			var content;
			var type;
			var button;
			var btn_class;
			
			if (recordStatus==1) {
				content='The record will be DeActivated!';
				//icon='glyphicon glyphicon-off';
				type='red';
				button='DeActivate';
				btn_class='btn-red'
			} else {
				content='The record will be Activated!';
				//icon='glyphicon glyphicon-off';
				type='green';
				button='Activate';
				btn_class='btn-green'
			}
			
			console.log('================activateOrDeActiveSubjectDetails data================');
			console.log(data);
			console.log('================activateOrDeActiveSubjectDetails data================');
			
			$.confirm({
				//icon: 'glyphicon glyphicon-off',/*'fa fa-spinner fa-spin',*/
	    	    title: 'Confirm!',
	    	    content: content,
	    	    draggable: true,
	    	    theme: "modern",
	    	    type: type,
	    	    typeAnimated: true,
	    	    buttons: {
	    	    	 specialKey: {
	    	             text: button,
	    	             btnClass: btn_class,
	    	             action: function(){
	    	            	 activateOrDeActiveSchoolDetails(schoolId, recordStatus);
	    	             }
	    	         },
	    	    	cancel: function () {
	    	    		btnClass: 'btn-red'
	    	    		$.alert('Cancelled!');
	    	    	},
	    	    }
	    	});
			
		});
		
		/*
		$('#submit_category').click(function(){
			saveOrUpdateCategoryDetails(update);
		})*/
		
		function resetSchoolDetailsForm(){
		//$("#formSubjectDetails")[0].reset();
			if(update){
				$('#udiseCode').val("");
				$('#affiliationBoard').val("");
				$('#schoolAffiliatedCode').val("");
				$('#mediumLanguageId').val("");
			}else{
				$('#udiseCode').val("");
				$('#schoolId').val("");
				$('#affiliationBoard').val("");
				$('#schoolAffiliatedCode').val("");
				$('#mediumLanguageId').val("");
			}
			console.log("===========is Update==================");
			console.log(update);
			console.log("===========is Update==================");
		}
		
		$('#reset').on('click', function(){
			resetSchoolDetailsForm();
		})
		
		$('#schoolDetailsFormButton').on('click', function(){
			saveOrUpdateSubjectDetails(update);
		})
	
})