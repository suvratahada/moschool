/**
 * 
 */$(document).ready(function(){
	
	$('#addStudentStrength').on('click', function(){
		update = false;
		$("#classNo").removeAttr("readonly", "false"); 
		resetStudentStrengthForm();
		$('#studentStrengthSetupModal').modal();
	})
	
	var update = false;
	
	function objectifyForm(formArray) {//serialize data function
	   var returnArray = {};
	   for (var i = 0; i < formArray.length; i++){
	     returnArray[formArray[i]['name']] = formArray[i]['value'];
	   }
	   return returnArray;
	 }
	
	getDataToStudentStrengthTable();
	
	var studentStrengthTable = $('#tblStudentStrength').DataTable({
		
		 "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
		 "pageLength": 10,	
		 "bFilter": true,
		 "bLengthChange": false,
		 "scrollX": true,
		 "columnDefs":[
		   {"className": "dt-center", "targets": "_all"},  
		   /*{"targets":0,"data":"id"},*/
          {"targets":0,"data":"classNo"},
          {"targets":1,"data":"section"},
          {"targets":2,"data":"male"},
          {"targets":3,"data":"feMale"},
          {"targets":4,"data": null, 
		    	"defaultContent": '<button type="button" id="Edit" action="edit" class="btn btn-default btn-sm btn-edit"><span class="glyphicon glyphicon-edit"></span></button> <button type="button" action="delete" id="Delete" class="btn btn-default btn-sm btn-delete"><span class="glyphicon glyphicon-trash"></span></button>'}                  
		  
       ]
	});
	
	function getDataToStudentStrengthTable() {
		$.ajax({
			 url: "./mdm/getStudentStrength.do",
			 type: "GET",			
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			 success:function(result) {
				 console.log("====================getStudentStrength====================");
				 console.log(result);
				 console.log("====================getStudentStrength====================");
				 studentStrengthTable.clear();
				 studentStrengthTable.rows.add(result.data).draw();
			 },
			 error:function(jqXHR, status, error){
			 	swal(error);
			 }
		});
	}
	
	var saveOrUpdateStudentStrength = function(update){
		var postData = objectifyForm($("#formStudentStrength").serializeArray());
		postData["update"] = update;
		console.log('============postData============');
		console.log(postData);
		console.log('============postData============');
			$.ajax({
				 url: "./mdm/saveOrUpdateStudentStrength.do",
				 type: "POST",
				 dataType:"JSON",
				 data: JSON.stringify(postData),
	             contentType:"application/json",
	             beforeSend: function(xhr) {
		               xhr.setRequestHeader(header, token);
	  			},
				 success: function(data, textStatus, jqXHR) {
		                if (data.success) {
		                	//resetForm_CreateBlock();
		                	swal(data.msg);
		                	getDataToStudentStrengthTable();
		                	resetStudentStrengthForm();
			              }
		            },
				 error:function(jqXHR, status, error){
				 	swal(error);
				 	//resetForm_CreateBlock();
				 }
			});
		}
	
	var deleteStudentStrength = function(id, section){
			$.ajax({
				 url: "./mdm/deleteStudentStrength/"+id+"/"+section+".do",
				 type: "POST",
				 dataType:"JSON",
	             contentType:"application/json",
	             beforeSend: function(xhr) {
		               xhr.setRequestHeader(header, token);
	  			},
				 success: function(data, textStatus, jqXHR) {
		                if (data.success) {
		                	swal(data.msg);
		                	getDataToStudentStrengthTable();
		                	resetStudentStrengthForm();
			              }
		            },
				 error:function(jqXHR, status, error){
				 	swal(error);
				 }
			});
		}
	
		$('#tblStudentStrength tbody').on( 'click', 'button[action=edit]', function () {
			update = true;
			var data = studentStrengthTable.row( $(this).parents('tr') ).data(); 
			
			console.log('================tblSchoolDetails data================');
			console.log(data);
			console.log('================tblSubjectDetails data================');
			
			$('#classNo').val(data.classNo);
			$('#section').val(data.section);
			$('#male').val(data.male);
			$('#feMale').val(data.feMale);
			$('#studentStrengthSetupModal').modal();
			$("#classNo").attr("readonly", "true"); 
			
		});
		
		$('#tblStudentStrength tbody').on( 'click', 'button[action=delete]', function () {
			var data = studentStrengthTable.row( $(this).parents('tr') ).data(); 
			var classNo = data.classNo;
			var section = data.section;
			$.confirm({
				icon: 'glyphicon glyphicon-trash',/*'fa fa-spinner fa-spin',*/
	    	    title: 'Confirm!',
	    	    content: 'The record will be deleted!',
	    	    draggable: true,
	    	    theme: "modern",
	    	    type: 'red',
	    	    typeAnimated: true,
	    	    buttons: {
	    	        'delete': function () {
	    	        	deleteStudentStrength(classNo, section);
	    	        },
	    	        cancel: function () {
	    	            $.alert('Canceled!');
	    	        },
	    	    }
	    	});
			
		});
		/*
		$('#submit_category').click(function(){
			saveOrUpdateCategoryDetails(update);
		})*/
		
		function resetStudentStrengthForm(){
		//$("#formSubjectDetails")[0].reset();
			if(update){
				$('#section').val("");
				$('#male').val("");
				$('#female').val("");
			}else{
				$('#classNo').val("");
				$('#section').val("");
				$('#male').val("");
				$('#feMale').val("");
			}
			console.log("===========is Update==================");
			console.log(update);
			console.log("===========is Update==================");
		}
		
		$('#reset').on('click', function(){
			resetStudentStrengthForm();
		})
		
		$('#studentStrengthFormButton').on('click', function(){
			saveOrUpdateStudentStrength(update);
		})
	
})