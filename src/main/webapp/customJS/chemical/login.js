$(document).ready(function() {
	$("#Loginmodal").modal(); //login Modal Open
	
/*Form validation*/
	$('#frmLogin').bootstrapValidator({
		framework: 'bootstrap',
		 feedbackIcons: {
             valid: 'fa fa-check-square',
             invalid: 'fa fa-times',
             validating: 'fa fa-refresh'
         },
         row: {
             valid: 'field-success',
             invalid: 'field-error'
         },
		/*fields: {
			txtCaptcha: {
				validators: {
					notEmpty: {
					message: 'The Captcha is required and can\'t be Empty'
					},
					identical: {
	                    field: 'captchaLabelId',
	                    message: 'The Captcha should be same'
	                }
				}

			},*/
			j_password: {
				validators: {
					notEmpty: {
					message: 'The password can\'t be Empty'
					},
				}
			},
			j_username: {
				 validators: {
					 notEmpty: {
					 message: 'User Id is required and cannot be Empty.'
					 },
					
			regexp: {
                regexp: /^[a-zA-Z0-9_@.-]+$/,
                message: 'Username must contain only letters, numbers and special characters like (@,_,.,-)'
            }
				 
				 }
			 },
			 /*captchaLabelId:{
				 excluded: 'false',
				 validators: {
					 
				 }
			 }*/
			
		
	})
	
	
	
/*Captcha  validation	*/

	$('#captchaLabelId').mousedown(function(event) {
		  event.preventDefault();
		});
	
	/*var getCaptchaCode = function getCaptchaCode() {
		  $.ajax({
				url : './generateCaptcha.do',
				type : 'POST',
				beforeSend: function(xhr) {
		               xhr.setRequestHeader(header, token);
	  			},
				success : function(result) {
					$("#captchaLabelId").val(result);	
				}
			})
	  }
//	 getCaptchaCode();
	 
	  
	  $("#refreshImageId").click(function(){
		  getCaptchaCode();
	  })*/
	  $("#txtCaptcha").bind("paste",function(e) {  
		    e.preventDefault();  
		});
	
})

function changeform()
{
	var username = $("input[name=j_username]").val();
	var password = $("input[name=password]").val();
	var saltKey = $("input[name=saltKey]").val();
	
//	var encSaltPass = md5LoginHash(password,saltKey);
	var encSaltSHAPass = sha512LoginHash(password,saltKey);
	$("input[name=password]").val();//changed
	$("input[name=j_password]").val(encSaltSHAPass);//changed
	
	console.log(encSaltPass);
	console.log(encSaltSHAPass);
	return true;
}