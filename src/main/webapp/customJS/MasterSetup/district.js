$(document).ready(function(){
	var table = $('#tblDistrictDetails').DataTable({
		
		 "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
		 "pageLength": 10,	
		 "bFilter": true,
		 "bLengthChange": false,
		 "scrollX": true,
		 "columnDefs":[
		   {"className": "dt-center", "targets": "_all"},  
		   /*{"targets":0,"data":"id"},*/
           {"targets":0,"data":"district_id"},
           {"targets":1,"data":"district_name"},
           {"targets":2,"data": null, 
		    	"defaultContent": '<button type="button" id="Edit" action="edit" class="btn btn-default btn-sm btn-edit"><span class="glyphicon glyphicon-edit"></span></button> <button type="button" action="delete" id="Delete" class="btn btn-default btn-sm btn-delete"><span class="glyphicon glyphicon-trash"></span></button>'}                  
		  
        ]
	});
	
	var getDataToDistrictDetailsTable = function() {	
		$.ajax({
			 url: "./mastersetup/getDistrictDetails.do",
			 type: "POST",			
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			 success:function(result) {
				 console.log(result);
				 table.clear();
				 table.rows.add(result.data).draw();
			 },
			 error:function(jqXHR, status, error){
			 	swal(error);
			 }
		});
	}
	
	getDataToDistrictDetailsTable();
	
	function objectifyForm(formArray) {//serialize data function

	   var returnArray = {};
	   for (var i = 0; i < formArray.length; i++){
	     returnArray[formArray[i]['name']] = formArray[i]['value'];
	   }
	   return returnArray;
	 }
	$("#district_form_button").click(function() {
		var district_id=$("#district_id").val();
		var id=$("#id").val();
		var district_name=$("#district_name").val();
		//Store the password field objects into variables ...
	    var message = document.getElementById('confirmMessage');
	    //Set the colors we will be using ...
	    var goodColor = "#66cc66";
	    var badColor = "#ff6666";
	    
  			//console.log("inside add.")
//  				console.log(("#city_code").val());
		/*if($("#district_id").val()==''){
			swal("Enter Your distr code");
			$("#district_id").focus();
		}
	
		else if($("#city_code").val()==''){
			swal("Enter Your city Code");
			$("#city_code").focus();
		}
		else if($("#country_id").val()==''){
			swal("Enter Your country Name");
			$("#country_id").focus();
		}
		else{
			 
			saveOrUpdateCity(type);
			//$("#formCityDetails").submit(type);	
		}*/
			
	    saveDistrict();
    });
	var saveDistrict = function saveDistrict() {
		var postData = $("#formDistrictDetails").serializeArray();
		console.log(postData);
		$.ajax({
			 url: "./mastersetup/saveDistrictDetails.do",
			 type: "POST",
//				 beforeSend: function(xhr) {
//		                xhr.setRequestHeader(header, token);
//	      		},
			 dataType:"JSON",
			 data: JSON.stringify(objectifyForm(postData)),
             contentType:"application/json",
             beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
  			},
			 success: function(data, textStatus, jqXHR) {
	                if (data.success) {
	                	resetForm_CreateDistrict();
	                	swal(data.msg);
	                	getDataToDistrictDetailsTable();
		              }
	            },
			 error:function(jqXHR, status, error){
			 	swal(error);
			 	resetForm_CreateDistrict();
			 }
		});
		}
	
	
	/*Method to update District Details*/
	
	var updateDistrictDetails = function updateDistrictDetails(){
		var postData = $('#formDistrictUpdateDetails').serializeArray();
		console.log(postData);
		$.ajax({
			 type: "POST",
			 url: "./mastersetup/updateDistrictDetails.do",
			 data: JSON.stringify(objectifyForm(postData)),
	         contentType:"application/json",
	         dataType:"JSON",
	         beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
				},
			 success: function(data) {
				 if (data.success) {
					 	resetForm_CreateDistrict();
	                	swal(data.msg);
	                	getDataToDistrictDetailsTable();
	                	
		            }else{
		            	resetForm_CreateDistrict();
		            	  swal(data.msg);
		              }
	            },
		 error:function(jqXHR, status, error){
		 	swal(error);
		 }
	});
	}
	
	
	$('#tblDistrictDetails tbody').on( 'click', 'button[action=edit]', function () {
		var data = table.row( $(this).parents('tr') ).data(); 
		$('#district_id1').val(data.district_id);
		console.log(data.district_id)
		$('#district_name1').val(data.district_name);
		$('#editDistrictModal').modal();
	});	
	
	var deleteDistrictDetails = function deleteDistrictDetails(district_id){
        var data =
        {
        		district_id:district_id
        };
        $.ajax({
			 url: "./mastersetup/deleteDistrict.do",
			 type: "POST",		
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			 },
			 contentType : 'application/json',
	   		 dataType: "json",
	   		 data: JSON.stringify(data),
			 success:function(result) {
				 console.log(result);
				 if(result.success){
					 swal(result.msg)
				 	getDataToDistrictDetailsTable();
				 }else{
				 	swal(result.msg);	
				 }
			 },
			 error:function(jqXHR, status, error){
			 	swal(error);
			 }
		});
	}
	
	$('#tblDistrictDetails tbody').on( 'click', 'button[action=delete]', function () {
        var data = table.row( $(this).parents('tr') ).data();
		deleteDistrictDetails(data.district_id);
	});	
	
	$('#update-district').on('click', function(){
		updateDistrictDetails();
		$('#editDistrictModal').modal('hide');
	})
	
	
	var resetForm_CreateDistrict = function(){
		$("#formDistrictDetails")[0].reset();
	}
	
	
});