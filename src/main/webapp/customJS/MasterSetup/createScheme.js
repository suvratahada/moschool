$(document).ready(function(){
	var table = $('#tblSchemeDetails').DataTable({
		
		 "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
		 "pageLength": 10,	
		 "bFilter": true,
		 "bLengthChange": false,
		 "scrollX": true,
		 "columnDefs":[
		   {"className": "dt-center", "targets": "_all"},  
           {"targets":0,"data":"scheme_id"},
           {"targets":1,"data":"scheme"},
           {"targets":2,"data":"scheme_desc"},
           {"targets":3,"data": null, 
		    	"defaultContent": '<button type="button" id="Edit" action="edit" class="btn btn-default btn-sm btn-edit"><span class="glyphicon glyphicon-edit"></span></button> <button type="button" action="delete" id="Delete" class="btn btn-default btn-sm btn-delete"><span class="glyphicon glyphicon-trash"></span></button>'}                  
		  
        ]
	});
	
	var getDataToSchemeDetailsTable = function() {
		$.ajax({
			 url: "./mastersetup/getSchemeDetails.do",
			 type: "POST",			
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			 success:function(result) {
				 console.log(result);
				 table.clear();
				 table.rows.add(result.data).draw();
			 },
			 error:function(jqXHR, status, error){
			 	swal(error);
			 }
		});
	}
	
	getDataToSchemeDetailsTable();
	
	function objectifyForm(formArray) {//serialize data function

	   var returnArray = {};
	   for (var i = 0; i < formArray.length; i++){
	     returnArray[formArray[i]['name']] = formArray[i]['value'];
	   }
	   return returnArray;
	 }
	
	$("#scheme_form_button").click(function() {
		console.log('scheme'+$('#scheme').val())
		var postData = $("#formSchemeDetails").serializeArray();
		console.log(postData);
		$.ajax({
			 url: "./mastersetup/saveSchemeDetails.do",
			 type: "POST",
			 dataType:"JSON",
			 data: JSON.stringify(objectifyForm(postData)),
             contentType:"application/json",
             beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
  			},
			 success: function(data, textStatus, jqXHR) {
                	swal(data.msg);
                	resetForm_CreateScheme();
                	getDataToSchemeDetailsTable();
	            },
			 error:function(jqXHR, status, error){
			 	swal(error);
            	resetForm_CreateScheme();
			 }
		});
		});

	/*Method to update Scheme Details*/
	
	var updateSchemeDetails = function updateSchemeDetails(){
		var postData = $('#formUpdateSchemeDetails').serializeArray();
		console.log(postData);
		$.ajax({
			 type: "POST",
			 url: "./mastersetup/updateSchemeDetails.do",
			 data: JSON.stringify(objectifyForm(postData)),
	         contentType:"application/json",
	         dataType:"JSON",
	         beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
				},
			 success: function(data) {
				 if (data.success) {
	                	swal(data.msg);
	                	resetForm_CreateScheme();
	                	getDataToSchemeDetailsTable();
	                	
		            }else{
		            	resetForm_CreateScheme();
		            	  swal(data.msg);
		              }
	            },
		 error:function(jqXHR, status, error){
		 	swal(error);
		 }
	});
	}
	
	$('#tblSchemeDetails tbody').on( 'click', 'button[action=edit]', function () {
		var data = table.row( $(this).parents('tr') ).data(); 
		$('#scheme_id_hidden').val(data.scheme_id);
		console.log("scheme_id_hidden " +data.scheme_id)
		$('#scheme1').val(data.scheme)
		$('#scheme_desc1').val(data.scheme_desc)
		console.log(data.scheme_id)
		$('#editSchemeModal').modal();
	});	

	$('#update-scheme').on('click', function(){
		updateSchemeDetails();
		$('#editSchemeModal').modal('hide');
	})
	
	var deleteSchemeDetails = function deleteSchemeDetails(scheme_id){
        var data =
        {
        		scheme_id:scheme_id
        };
        $.ajax({
			 url: "./mastersetup/deleteScheme.do",
			 type: "POST",		
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			 },
			 contentType : 'application/json',
	   		 dataType: "json",
	   		 data: JSON.stringify(data),
			 success:function(result) {
				 console.log(result);
				 if(result.success){
					 	swal(result.msg);	
					 	getDataToSchemeDetailsTable();
				 }else{
				 	swal(result.msg);	
				 }
			 },
			 error:function(jqXHR, status, error){
			 	swal(error);
			 }
		});
	}
	
	$('#tblSchemeDetails tbody').on( 'click', 'button[action=delete]', function () {
        var data = table.row( $(this).parents('tr') ).data();
        deleteSchemeDetails(data.scheme_id);
	});	
	
	
	
	function resetForm_CreateScheme(){
		$("#formSchemeDetails")[0].reset();
		
	}
	
	
});