$(document).ready(function(){
	var table = $('#tblYearDetails').DataTable({
		
		 "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
		 "pageLength": 10,	
		 "bFilter": true,
		 "bLengthChange": false,
		 "scrollX": true,
		 "columnDefs":[
		   {"className": "dt-center", "targets": "_all"},  
           {"targets":0,"data":"year_id"},
           {"targets":1,"data":"year_desc"},
           {"targets":2,"data": null, 
		    	"defaultContent": '<button type="button" id="Edit" action="edit" class="btn btn-default btn-sm btn-edit"><span class="glyphicon glyphicon-edit"></span></button> <button type="button" action="delete" id="Delete" class="btn btn-default btn-sm btn-delete"><span class="glyphicon glyphicon-trash"></span></button>'}                  
		  
        ]
	});
	
	var getDataToYearDetailsTable = function() {
		$.ajax({
			 url: "./mastersetup/getYearDetails.do",
			 type: "POST",			
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			 success:function(result) {
				 console.log(result);
				 table.clear();
				 table.rows.add(result.data).draw();
			 },
			 error:function(jqXHR, status, error){
			 	swal(error);
			 }
		});
	}
	
	getDataToYearDetailsTable();
	
	function objectifyForm(formArray) {//serialize data function

	   var returnArray = {};
	   for (var i = 0; i < formArray.length; i++){
	     returnArray[formArray[i]['name']] = formArray[i]['value'];
	   }
	   return returnArray;
	 }
	
	$("#year_form_button").click(function() {
		console.log('year desc'+$('#year_desc').val())
		var postData = $("#formFinYearDetails").serializeArray();
		console.log(postData);
		$.ajax({
			 url: "./mastersetup/saveYearDetails.do",
			 type: "POST",
			 dataType:"JSON",
			 data: JSON.stringify(objectifyForm(postData)),
             contentType:"application/json",
             beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
  			},
			 success: function(data, textStatus, jqXHR) {
	                if (data.success) {
	                	resetForm_CreateYear();
	                	swal(data.msg);
	                	getDataToYearDetailsTable();
		              }
	            },
			 error:function(jqXHR, status, error){
			 	swal(error);
			 	resetForm_CreateYear();
			 }
		});
		});

	/*Method to update Year Details*/
	
	var updateYearDetails = function updateYearDetails(){
		var postData = $('#formUpdateYearDetails').serializeArray();
		console.log(postData);
		$.ajax({
			 type: "POST",
			 url: "./mastersetup/updateYearDetails.do",
			 data: JSON.stringify(objectifyForm(postData)),
	         contentType:"application/json",
	         dataType:"JSON",
	         beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
				},
			 success: function(data) {
				 if (data.success) {
					 resetForm_CreateYear();
	                	swal(data.msg);
	                	getDataToYearDetailsTable();
	                	
		            }else{
		            	resetForm_CreateYear();
		            	  swal(data.msg);
		              }
	            },
		 error:function(jqXHR, status, error){
		 	swal(error);
		 }
	});
	}
	
	
	$('#tblYearDetails tbody').on( 'click', 'button[action=edit]', function () {
		var data = table.row( $(this).parents('tr') ).data(); 
		$('#year_id_hidden').val(data.year_id);
		$('#year_desc1').val(data.year_desc)
		console.log(data.year_id)
		$('#editYearModal').modal();
	});	

	$('#update-year').on('click', function(){
		updateYearDetails();
		$('#editYearModal').modal('hide');
	})
	
	var deleteYearDetails = function deleteYearDetails(year_id){
        var data =
        {
        		year_id:year_id
        };
        $.ajax({
			 url: "./mastersetup/deleteYear.do",
			 type: "POST",		
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			 },
			 contentType : 'application/json',
	   		 dataType: "json",
	   		 data: JSON.stringify(data),
			 success:function(result) {
				 console.log(result);
				 if(result.success){
					 	swal(result.msg);	
					 	resetForm_CreateYear();
					 	getDataToYearDetailsTable();
				 }else{
				 	swal(result.msg);	
				 }
			 },
			 error:function(jqXHR, status, error){
			 	swal(error);
			 }
		});
	}
	
	$('#tblYearDetails tbody').on( 'click', 'button[action=delete]', function () {
        var data = table.row( $(this).parents('tr') ).data();
        deleteYearDetails(data.year_id);
	});	
	
	
	
	function resetForm_CreateYear(){
		$("#formFinYearDetails")[0].reset();
		
	}
	
	
});