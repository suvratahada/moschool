$(document).ready(function(){
	//var isEdit = false;
	/****************************************************
	 * dynamic table creation in School module
	 *****************************************************/
	var table = $('#tblSchoolDetails').DataTable({

		"lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
		"pageLength": 10,	
		"bFilter": true,
		"bLengthChange": false,
		"scrollX": true,
		"columnDefs":[
			{"className": "dt-center", "targets": "_all"},  
			{"targets":0,"data":"school_id"},
			{"targets":1,"data":"school_name"},
			{"targets":2,"data":"block_id"},
			{"targets":3,"data":"block_name"},
			{"targets":4,"data": null, 
				"defaultContent": '<button type="button" id="Edit" action="edit" class="btn btn-default btn-sm btn-edit"><span class="glyphicon glyphicon-edit"></span></button> <button type="button" action="delete" id="Delete" class="btn btn-default btn-sm btn-delete"><span class="glyphicon glyphicon-trash"></span></button>'}                  

			]
	});
	
	$("#editSchoolModal").on("hidden.bs.modal", function(){
		 $('#formSchoolUpdateDetails')[0].reset();
	});
	
	/**********************************************************
	 * function to get all school present in the table
	 * table used:mst.school 
	 * MasterSetupController
	 **********************************************************/
	
	var getDataToSchoolDetailsTable = function() {	
		$('#loading').show();
		//swal("Inside getDataToSchoolDetailsTable");
		$.ajax({
			 url: "./mastersetup/getSchoolDetails.do",
			 type: "POST",	
			 async:false,
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			 success:function(result) {
				 $('#loading').hide();
				 table.clear();
				 table.rows.add(result.data).draw();
			 },
			 error:function(jqXHR, status, error){
				 $('#loading').hide();
			 	swal(error);
			 }
		});
	}
	
	getDataToSchoolDetailsTable();
	

	/**********************************************************
	 * function to get all blocks present in the table
	 * table used:master_block 
	 * MasterSetupController
	 **********************************************************/
	
	var getDataToBlockDropdown = function() {	
		//swal("Inside getDataToSchoolDetailsTable");
		$.ajax({
			 url: "./mastersetup/getBlockDetails.do",
			 type: "POST",			
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			 success:function(result) {
					if(result.status == false){
						 swal(result.msg);
					}else{
						var data = result.data;
						console.log(data)
						var option = '<option value="All">--Select Block--</option>';
						for(var i = 0;i<data.length;i++){
							option += '<option value="'+data[i].block_id+'">'+data[i].block_name+'</option>';
						}
						$('#block_id').val('');
						$('#block_id').empty();
						$('#block_id').append(option);
						$("#block_id").prop("selectedIndex", 0);
					}
				},
			 error:function(jqXHR, status, error){
			 	swal(error);
			 }
		});
	}

	getDataToBlockDropdown();
	
	/*******************************************************************************************
	 * This method url is present in MasterSetupController.java class
	 * Table Used admin.user
	 * taking the input value FormData
	 * modified on: 25-AUG-18 By: Suvrata Kumar Hada
	 ********************************************************************************************/

	 function objectifyForm(formArray) {//serialize data function

		   var returnArray = {};
		   for (var i = 0; i < formArray.length; i++){
		     returnArray[formArray[i]['name']] = formArray[i]['value'];
		   }
		   return returnArray;
		 }
	
 /*******************************************************************************************
	 * This methodis used to save School details in the master_school table
	 * taking the input value FormData
	 * modified on: 18-Sep-8 By: Bishwajeet
	 ********************************************************************************************/
	 
	var saveOrUpdateSchool = function saveOrUpdateSchool() {
		$('#loading').show();
		var postData = $("#formSchoolDetails").serializeArray();
		console.log('===================');
		console.log(postData);
		var formURL = "./mastersetup/saveSchoolDetails.do";
			//swal("inside Save School");
			$.ajax({
				 url: formURL,
				 type: "POST",
				 beforeSend: function(xhr) {
		                xhr.setRequestHeader(header, token);
				 },
				 data: JSON.stringify(objectifyForm(postData)),
	             contentType:"application/json",
	             dataType:"JSON",
	             beforeSend: function(xhr) {
		               xhr.setRequestHeader(header, token);
	  			},
				 success: function(data, textStatus, jqXHR) {
					 $('#loading').hide();
		                if (data.success) {
		                	resetForm_CreateSchool();
		                	swal(data.msg);
		                	console.log("======schoolData=====");
		                	getDataToSchoolDetailsTable();
			              }
		            },
				 error:function(jqXHR, status, error){
					 $('#loading').hide();
				 	swal(error);
				 	resetForm_CreateSchool();
				 }
			});
		}
	
	$("#school_form_button").on("click", function(){
		saveOrUpdateSchool();
	})
	
	
	/*Method to update School Details*/
	
	var updateSchoolDetails = function updateSchoolDetails(){
		$.LoadingOverlay("show");
		var postData = $('#formSchoolUpdateDetails').serializeArray();
		console.log(postData);
		$.ajax({
			 type: "POST",
			 url: "./mastersetup/updateSchoolDetails.do",
			 data: JSON.stringify(objectifyForm(postData)),
	         contentType:"application/json",
	         dataType:"JSON",
	         async:false,
	         beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
				},
			 success: function(data) {
				 $.LoadingOverlay("hide");
				 if (data.success) {
					 	resetForm_CreateSchool();
	                	swal(data.msg);
	                	getDataToSchoolDetailsTable();
	                	
		            }else{
		            	resetForm_CreateSchool();
		            	  swal(data.msg);
		              }
	            },
		 error:function(jqXHR, status, error){
			 $.LoadingOverlay("hide");
		 	swal(error);
		 }
	});
	}
	
	
	$('#tblSchoolDetails tbody').on( 'click', 'button[action=edit]', function () {
		var data = table.row( $(this).parents('tr') ).data(); 
		$('#school_id').val(data.school_id);
		console.log(data.school_id)
		$('#block_id1').val(data.block_id);
		$('#school_name1').val(data.school_name);
		$('#editSchoolModal').modal();
	});	
	
	var deleteSchoolDetails = function deleteSchoolDetails(school_id){
        var data =
        {
        		school_id:school_id
        };
        $.ajax({
			 url: "./mastersetup/deleteSchool.do",
			 type: "POST",		
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			 },
			 contentType : 'application/json',
	   		 dataType: "json",
	   		 data: JSON.stringify(data),
			 success:function(result) {
				 console.log(result);
				 if(result.success){
					 swal(result.msg);	
				 	getDataToSchoolDetailsTable();
				 }else{
				 	swal(result.msg);	
				 }
			 },
			 error:function(jqXHR, status, error){
			 	swal(error);
			 }
		});
	}
	
	$('#tblSchoolDetails tbody').on( 'click', 'button[action=delete]', function () {
        var data = table.row( $(this).parents('tr') ).data();
		deleteSchoolDetails(data.school_id);
	});	
	
	$('#update-school').on('click', function(){
		updateSchoolDetails();
		$('#editSchoolModal').modal('hide');
	})
	
	
	function resetForm_CreateSchool(){
		$("#formSchoolDetails")[0].reset();
		
	}
	
});