$('document').ready(function(){
	
	/*Financial Year*/
	
	var getFinancialYearFromMasterFinYear = function(){
		$.ajax({
			 url: "./projectmonitoring/getFinancialYear.do",
			 type: "POST",			
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			 success:function(result) {
				 console.log(result);
				 var data = result.data;
				 var option = '<option selected  value="0">--Select Financial Year--</option>';
					for(var i = 0;i<data.length;i++){
						option += '<option value="'+data[i].year_id+'">'+data[i].year_desc+'</option>';
					}
					$('#finyr_id').val('');
					$('#finyr_id').empty();
					$('#finyr_id').append(option);
					$("#finyr_id").prop("selectedIndex", 0);
					

					$('#finyear').val('');
					$('#finyear').empty();
					$('#finyear').append(option);
					$("#finyear").prop("selectedIndex", 0);
			 },
			 error:function(jqXHR, status, error){
			 	swal(error);
			 }
		});
	}
	
	getFinancialYearFromMasterFinYear();
	
	/*Category*/
	var getCategoryFromMasterWorkCatg = function(){
		$.ajax({
			 url: "./mastersetup/getCategoryDetails.do",
			 type: "POST",			
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			 success:function(result) {
				 console.log(result.data);
				 var data = result.data;
				 var option = '<option selected  value="0">--Select Category--</option>';
					for(var i = 0;i<data.length;i++){
						option += '<option value="'+data[i].workcatg_id+'">'+data[i].catg_desc+'</option>';
					}
					$('#workcatg_id').val('');
					$('#workcatg_id').empty();
					$('#workcatg_id').append(option);
					$("#workcatg_id").prop("selectedIndex", 0);

					$('#category').val('');
					$('#category').empty();
					$('#category').append(option);
					$("#category").prop("selectedIndex", 0);
					
			 },
			 error:function(jqXHR, status, error){
			 	swal(error);
			 }
		});
	}
	
	getCategoryFromMasterWorkCatg();
	

	/*SubCategory*/
	var getSubCategoryFromMasterWorkDesc = function(){
		$.ajax({
			 url: "./mastersetup/getAllSubcategoryFormMasterWorkDesc.do",
			 type: "POST",			
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			 success:function(result) {
				 console.log(result.data);
				 var data = result.data;
				 var option = '<option selected value="0">--Select Subcategory--</option>';
					for(var i = 0;i<data.length;i++){
						option += '<option value="'+data[i].work_desc_id+'">'+data[i].work_desc+'</option>';
					}
					$('#sub_work_desc').val('');
					$('#sub_work_desc').empty();
					$('#sub_work_desc').append(option);
					$("#sub_work_desc").prop("selectedIndex", 0);

					$('#subcategory_dd').val('');
					$('#subcategory_dd').empty();
					$('#subcategory_dd').append(option);
					$("#subcategory_dd").prop("selectedIndex", 0);
			 },
			 error:function(jqXHR, status, error){
			 	swal(error);
			 }
		});
	}
	
	getSubCategoryFromMasterWorkDesc();
	
	$('#sub_work_desc').on('change', function(){
		var work_descId = $('#sub_work_desc').val();
		$('#work_desc_id').val(work_descId);
		$('#work_desc').val($("#sub_work_desc option:selected").text());
		console.log($('#work_desc').val());	
	})
	
//	$('#subcategory_dd').on('change', function(){
//		
//		console.log($('#work_desc_hidden').val());	
//	})
	
	
	
	/*get data to tblSubcategoryDetails table*/
	
	var tblSubcategoryDetails = $('#tblSubcategoryDetails').DataTable({
		
		 "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
		 "pageLength": 10,	
		 "bFilter": true,
		 "bLengthChange": false,
		 "scrollX": true,
		 "columnDefs":[
		  {"className": "dt-center", "targets": "_all"},  
		  {"targets":0,"data":"work_desc_id", "visible":false},
		  {"targets":1,"data":"work_id"},
		  {"targets":2,"data":"year_desc"},
          {"targets":3,"data":"catg_desc"},
          {"targets":4,"data":"work_desc"},
          {"targets":5,"data":"cost_pu"},
          {"targets":6,"data": null, 
		    	"defaultContent": '<button type="button" id="Edit" action="edit" class="btn btn-default btn-sm btn-edit"><span class="glyphicon glyphicon-edit"></span></button> <button type="button" action="delete" id="Delete" class="btn btn-default btn-sm btn-delete"><span class="glyphicon glyphicon-trash"></span></button>'}                  
		  
       ]
	});
	
	var getDataToSubCategoryDetailsTable = function() {
		$.ajax({
			 url: "./mastersetup/getAllSubCategoryWorkDetails.do",
			 type: "POST",			
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			 success:function(result) {
				 console.log(result);
				 tblSubcategoryDetails.clear();
				 tblSubcategoryDetails.rows.add(result.data).draw();
			 },
			 error:function(jqXHR, status, error){
			 	swal(error);
			 }
		});
	}
	
	getDataToSubCategoryDetailsTable();
	
	function objectifyForm(formArray) {//serialize data function

		   var returnArray = {};
		   for (var i = 0; i < formArray.length; i++){
		     returnArray[formArray[i]['name']] = formArray[i]['value'];
		   }
		   return returnArray;
		 }
	
	$("#subcategory_form_button").click(function() {
		var postData = $("#formSubcategoryDetails").serializeArray();
		console.log(postData);
		$.ajax({
			 url: "./mastersetup/saveSubcategoryDetailsToMasterWork.do",
			 type: "POST",
			 dataType:"JSON",
			 data: JSON.stringify(objectifyForm(postData)),
             contentType:"application/json",
             beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
  			},
			 success: function(data, textStatus, jqXHR) {
	                if (data.success) {
	                	resetForm_CreateSubcategory();
	                	swal(data.msg);
	                	getDataToSubCategoryDetailsTable();
		              }
	            },
			 error:function(jqXHR, status, error){
			 	swal(error);
			 	resetForm_CreateSubcategory();
			 }
		});
		});
	
	/*Method to update Subcategory Details*/
	
	var updateSubcategoryDetails = function updateSubcategoryDetails(){
		var postData = $('#formUpdateSubcategoryDetails').serializeArray();
		console.log(postData);
		$.ajax({
			 type: "POST",
			 url: "./mastersetup/updateSubcategoryDetails.do",
			 data: JSON.stringify(objectifyForm(postData)),
	         contentType:"application/json",
	         dataType:"JSON",
	         beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
				},
			 success: function(data) {
				 if (data.success) {
					 resetForm_CreateSubcategory();
	                	swal(data.msg);
	                	getDataToSubCategoryDetailsTable();
	                	
		            }else{
		            	resetForm_CreateSubcategory();
		            	  swal(data.msg);
		              }
	            },
		 error:function(jqXHR, status, error){
		 	swal(error);
		 }
	});
	}
	
	
	$('#tblSubcategoryDetails tbody').on( 'click', 'button[action=edit]', function () {
		var data = tblSubcategoryDetails.row( $(this).parents('tr') ).data(); 
		$('#update_work_id_hidden').val(data.work_id);
		$('#finyear').val(data.finyr_id);
		$('#category').val(data.workcatg_id);
		$('#subcategory_dd').val(data.work_desc_id);
		$('#cost_per_unit').val(data.cost_pu);
		$('#work_desc_hidden').val($("#subcategory_dd option:selected").text());
		$('#editSubcategoryModal').modal();
	});	

	$('#update_subcategory').on('click', function(){
		updateSubcategoryDetails();
		$('#editSubcategoryModal').modal('hide');
	})
	
	var deleteSubCategoryDetails = function deleteSubCategoryDetails(work_id){
        var data =
        {
        		work_id:work_id
        };
        $.ajax({
			 url: "./mastersetup/deleteSubcategoryDetails.do",
			 type: "POST",		
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			 },
			 contentType : 'application/json',
	   		 dataType: "json",
	   		 data: JSON.stringify(data),
			 success:function(result) {
				 console.log(result);
				 if(result.success){
					 	swal(result.msg);	
					 	resetForm_CreateSubcategory();
					 	getDataToSubCategoryDetailsTable();
				 }else{
				 	swal(result.msg);	
				 }
			 },
			 error:function(jqXHR, status, error){
			 	swal(error);
			 }
		});
	}
	
	$('#tblSubcategoryDetails tbody').on( 'click', 'button[action=delete]', function () {
        var data = tblSubcategoryDetails.row( $(this).parents('tr') ).data();
        deleteSubCategoryDetails(data.work_id);
	});	
	
	
	
	function resetForm_CreateSubcategory(){
		$("#formSubcategoryDetails")[0].reset();
		
	}
	
})