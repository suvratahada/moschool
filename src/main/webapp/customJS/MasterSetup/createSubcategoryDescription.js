$('document').ready(function(){
	
	
	/*Category*/
	var getCategoryFromMasterWorkCatg = function(){
		$.ajax({
			 url: "./mastersetup/getCategoryDetails.do",
			 type: "POST",			
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			 success:function(result) {
				 console.log(result.data);
				 var data = result.data;
				 var option = '<option selected  value="0">--Select Category--</option>';
					for(var i = 0;i<data.length;i++){
						option += '<option value="'+data[i].workcatg_id+'">'+data[i].catg_desc+'</option>';
					}
					$('#workcatg_id').val('');
					$('#workcatg_id').empty();
					$('#workcatg_id').append(option);
					$("#workcatg_id").prop("selectedIndex", 0);

					$('#category').val('');
					$('#category').empty();
					$('#category').append(option);
					$("#category").prop("selectedIndex", 0);
					
			 },
			 error:function(jqXHR, status, error){
			 	swal(error);
			 }
		});
	}
	
	getCategoryFromMasterWorkCatg();
	
	
	$('#sub_work_desc').on('change', function(){
		var work_descId = $('#sub_work_desc').val();
		$('#work_desc_id').val(work_descId);
		$('#work_desc').val($("#sub_work_desc option:selected").text());
		console.log($('#work_desc').val());	
	})
	
	
	/*get data to tblSubcategoryDescDetails table*/
	
	var tblSubcategoryDescDetails = $('#tblSubcategoryDescDetails').DataTable({
		
		 "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
		 "pageLength": 10,	
		 "bFilter": true,
		 "bLengthChange": false,
		 "scrollX": true,
		 "columnDefs":[
		  {"className": "dt-center", "targets": "_all"},  
          {"targets":0,"data":"workcatg_id"},
          {"targets":1,"data":"work_desc"},
          {"targets":2,"data":"work_desc_id"},
          {"targets":3,"data": null, 
		    	"defaultContent": '<button type="button" action="delete" id="Delete" class="btn btn-default btn-sm btn-delete"><span class="glyphicon glyphicon-trash"></span></button>'}                  
		  
       ]
	});
	
	var getDataToSubCategoryDetailsTable = function() {$.ajax({
		 url: "./mastersetup/getAllSubcategoryFormMasterWorkDesc.do",
		 type: "POST",			
		 beforeSend: function(xhr) {
               xhr.setRequestHeader(header, token);
		},
		 success:function(result) {
			 console.log(result.data);
			 var data = result.data;
			 tblSubcategoryDescDetails.clear();
			 tblSubcategoryDescDetails.rows.add(result.data).draw();
		 },
		 error:function(jqXHR, status, error){
		 	swal(error);
		 }
	});}
	
	getDataToSubCategoryDetailsTable();
	
	function objectifyForm(formArray) {//serialize data function

		   var returnArray = {};
		   for (var i = 0; i < formArray.length; i++){
		     returnArray[formArray[i]['name']] = formArray[i]['value'];
		   }
		   return returnArray;
		 }
	
	$("#subcatdesc_form_button").click(function() {
		var postData = $("#formWorkDescDetails").serializeArray();
		console.log(postData);
		$.ajax({
			 url: "./mastersetup/saveSubcategoryDescriptionToMasterWorkDesc.do",
			 type: "POST",
			 dataType:"JSON",
			 data: JSON.stringify(objectifyForm(postData)),
             contentType:"application/json",
             beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
  			},
			 success: function(data, textStatus, jqXHR) {
	                if (data.success) {
	                	resetForm_SubcatDesc();
	                	swal(data.msg);
	                	getDataToSubCategoryDetailsTable();
		              }
	            },
			 error:function(jqXHR, status, error){
			 	swal(error);
			 	resetForm_SubcatDesc();
			 }
		});
		});
	

	var deleteSubCategoryDescDetails = function deleteSubCategoryDetails(work_desc_id){
        var data =
        {
        		work_desc_id:work_desc_id
        };
        $.ajax({
			 url: "./mastersetup/deleteSubcategoryDescDetails.do",
			 type: "POST",		
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			 },
			 contentType : 'application/json',
	   		 dataType: "json",
	   		 data: JSON.stringify(data),
			 success:function(result) {
				 console.log(result);
				 if(result.success){
					 	swal(result.msg);	
					 	resetForm_SubcatDesc();
					 	getDataToSubCategoryDetailsTable();
				 }else{
				 	swal(result.msg);	
				 }
			 },
			 error:function(jqXHR, status, error){
			 	swal(error);
			 }
		});
	}
	
	$('#tblSubcategoryDescDetails tbody').on( 'click', 'button[action=delete]', function () {
        var data = tblSubcategoryDescDetails.row( $(this).parents('tr') ).data();
        deleteSubCategoryDescDetails(data.work_desc_id);
	});	
	
	
	
	function resetForm_SubcatDesc(){
		$("#formWorkDescDetails")[0].reset();
		
	}
	
})