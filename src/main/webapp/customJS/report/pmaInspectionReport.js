$(document).ready(function(){

	var subcat=0;
	var name=null;
	var id=0;
	var caption="";
	var captionType="";

	var userDetails;
	var table1;
	getSubCategory();
	getDistrictName();
	table(0,null,0);

	function getSubCategory(){
		$.ajax({
			url:'./mastersetup/getAllSubcategoryFormMasterWorkDesc.do',
			type: 'POST',
			async: false,
			beforeSend: function(xhr) {
				xhr.setRequestHeader(header, token);
			},
			success: function(result){
				userDetails=result.user_detais;
				console.log('=====================subCategory=====================');
				console.log(result);
				console.log('=====================subCategory=====================');
				setSubCategoryToDropDown(result.data);
			},
			error: function(jqXHR, status, error){
				swal('error');
			}
		});
	}

	function setSubCategoryToDropDown(data){
		//console.log(data);
		var option = '<option value=0>ALL</option>';
		for(var i = 0;i<data.length;i++) {
			option += '<option value="'+ data[i].work_desc_id+ '">' + data[i].work_desc+ '</option>';
		}

		$("#subCatgId").html("");
		$("#subCatgId").append(option);
		$("#subCatgId").off().on('change', function(){
			$('#loading').show();
			//$('#district_name').children('option:not(:first)').remove();
			/*$('#block_name').children('option:not(:first)').remove();
			$('#school_name').children('option:not(:first)').remove();*/

			subcat = $(this).find(":selected").val();
			caption=$(this).find(":selected").text();
			captionType="Activity";
			console.log(subcat);
			if (subcat==0) {
				table(id,name,0);
				$('#name1').html('Activity Name');
			}else {	
				table(id,name,subcat);
				$('#name1').html('School Name');
			}

			console.log('=====================subCategory_onchange=====================');
			console.log(id);
			console.log(name);
			console.log(subcat);
			console.log('=====================subCategory_onchange=====================');
			$('#code').html('Activity Code');
			$('#name').html('Activity Name');
			$('#loading').fadeOut(2000);
		})
	}

	function getDistrictName(){
		$.ajax({
			url:'./report/getDistrictNameId.do',
			type: 'POST',
			async: false,
			beforeSend: function(xhr) {
				xhr.setRequestHeader(header, token);
			},
			success: function(result){
				userDetails=result.user_detais;
				console.log('=====================getDistrictName=====================');
				console.log(result);
				console.log('=====================getDistrictName=====================');
				setDataToDropDown(result.data, '#district_name');
			},
			error: function(jqXHR, status, error){
				swal('error');
			}
		});
	}

	function getBlockName(districtId){
		$.ajax({
			url:'./report/getBlockNameId.do',
			type: 'POST',
			async: false,
			data:{districtId:districtId},
			beforeSend: function(xhr) {
				xhr.setRequestHeader(header, token);
			},
			success: function(result){
				console.log(result);
				setDataToDropDownBlock(result.data, districtId);
			},
			error: function(jqXHR, status, error){
				swal('error');
			}
		});
	}

	function schoolName(blockId){
		$.ajax({
			url:'./report/getSchoolNameId.do',
			type: 'POST',
			async: false,
			data:{blockId:blockId},
			beforeSend: function(xhr) {
				xhr.setRequestHeader(header, token);
			},
			success: function(result){
				console.log(result);
				setDataToDropDownSchool(result.data, blockId);
			},
			error: function(jqXHR, status, error){
				swal('error');
			}
		});
	}

	function setDataToDropDown(data){
		//console.log(data);
		var option = '<option value=0>ALL</option>';
		for(var i = 0;i<data.length;i++) {
			option += '<option value="'+ data[i].district_id+ '">' + data[i].district_name+ '</option>';
		}
		$("#district_name").html("");
		$("#district_name").append(option);
		$("#district_name").off().on('change', function(){
			$('#loading').show();
			id = $(this).find(":selected").val();
			name = "district_id";
			console.log("district is "+name);
			caption=$(this).find(":selected").text();
			captionType="District";
			//alert(distId);
			$('#block_name').children('option:not(:first)').remove();
			$('#school_name').children('option:not(:first)').remove();
			if (id==0) {
				table(0, null, subcat);
			}else {	
				getBlockName(id);
				table(id, name, subcat);
			}

			console.log('=====================district_onchange=====================');
			console.log(id);
			console.log(name);
			console.log(subcat);
			console.log('=====================district_onchange=====================');
			$('#code').html('District Code');
			$('#name').html('District Name');
			$('#name1').html('Activity Name');
			$('#loading').fadeOut(2000);
		})
	}

	function setDataToDropDownBlock(data, districtId){
		var option = '<option value=0>ALL</option>';
		for(var i = 0;i<data.length;i++) {
			option += '<option value="'+ data[i].block_id+ '">' + data[i].block_name+ '</option>';
		}
		$("#block_name").html("");
		$("#block_name").append(option);
		$("#block_name").off().on('change', function(){

			$('#loading').show();

			$('#school_name').children('option:not(:first)').remove();

			id = $(this).find(":selected").val();
			name = "block_id";
			caption=$(this).find(":selected").text();
			captionType="Block";
			if (id==0) {
				$('#code').html('District Code');
				$('#name').html('District Name');
				table(districtId,"district_id",subcat);				
			} else {
				$('#code').html('Block Code');
				$('#name').html('Block Name');
				schoolName(id);
				table(id,name,subcat);
			}
			$('#name1').html('Activity Name');
			console.log('=====================block_onchange=====================');
			console.log(id);
			console.log(name);
			console.log(subcat);
			console.log('=====================block_onchange=====================');
			$('#loading').fadeOut(2000);
		})
	}

	function setDataToDropDownSchool(data, blockId){
		//console.log(data);
		var option = '<option value=0>ALL</option>';
		for(var i = 0;i<data.length;i++) {
			option += '<option value="'+ data[i].school_id+ '">' + data[i].school_name+ '</option>';
		}
		$("#school_name").html("");
		$("#school_name").append(option);
		$("#school_name").off().on('change', function(){
			$('#loading').show();
			id = $(this).find(":selected").val();
			name = "school_id";
			caption=$(this).find(":selected").text();
			captionType="School";
			if (id==0) {
				$('#code').html('Block Code');
				$('#name').html('Block Name');
				table(blockId,"block_id",subcat);				
			} else {
				$('#code').html('School Code');
				$('#name').html('School Name');
				table(id,name,subcat);				
			}
			$('#name1').html('Activity Name');
			console.log('=====================school_onchange=====================');
			console.log(id);
			console.log(name);
			console.log(subcat);
			console.log('=====================school_onchange=====================');
			$('#loading').fadeOut(2000);
		})
	}


	function table(id,name,subcat){
		console.log(id);
		console.log(name);
		var column=[];
		var columnDef=[];
		if(name=="district_id"||name==null){
		column=[{ "data": null ,
				"render": function ( data, type, row ) {
					return ++i;
				} },
				{ "data": "id" },
				{ "data": "name" },
				{ "data": null },
				{ "data": "no_activities" },
				{ "data": "visits_prev_mth" },
				{ "data": "visits_curr_mth"},
				{ "data": "activities_prev_mth" },
				{ "data": "activities_curr_mth"}];
		columnDef=[{"className": "dt-center", "targets": [0,1,3,4,5,6,7]},
			{"className": "r-click", "targets": [2]},
			{"targets": [ 3 ], "visible": false, "searchable": false}];
		}else if(name=="block_id"){
			column=[{ "data": null ,
				"render": function ( data, type, row ) {
					return ++i;
				} },
				{ "data": "id" },
				{ "data": "name" },
				{ "data":null },
				{ "data": "no_activities" },
				{ "data": "visits_prev_mth" },
				{ "data": "visits_curr_mth"},
				{ "data": "activities_prev_mth" },
				{ "data": "activities_curr_mth"}];
			columnDef=[{"className": "dt-center", "targets": [0,1,3,4,5,6,7]},
				{"className": "r-click", "targets": [2]},
				{"targets": [ 3 ], "visible": false, "searchable": false}];
		}else{
			column=[{ "data": null ,
				"render": function ( data, type, row ) {
					return ++i;
				} },
				{ "data": "id" },
				{ "data": "name" },
				{ "data": "name1" },
				{ "data": "no_activities" },
				{ "data": "visits_prev_mth" },
				{ "data": "visits_curr_mth"},
				{ "data": "activities_prev_mth" },
				{ "data": "activities_curr_mth"}];
			columnDef=[{"className": "dt-center", "targets": [0,1,3,4,5,6,7]},
				{"className": "r-click", "targets": [2]}
				/*{"targets": [ 3 ], "visible": false, "searchable": false}*/];
		}
		var i=0;
		$.ajax({
			url:'./report/pmaInspectionReport.do',
			type: 'POST',
			async: false,
			data:{id:id, name:name, subCatg:subcat},
			beforeSend: function(xhr) {
				xhr.setRequestHeader(header, token);
			},
			success: function(result){
				console.log(result);

				var my_columns = [];
				$.each( result.data[0], function( key, value ) {
					var my_item = {};
					my_item.data = key;
					my_item.title = key;
					my_columns.push(my_item);
				});

				table1=$('#inspectionTable').DataTable( {
					data: result.data,
					dom: 'Bfrtip',
					buttons: [						
						{
							extend: 'excelHtml5',
							className:"BUTTON_DPX",
							title: 'Pma Inspection Report '+caption+" "+captionType,
							action: function ( e, dt, node, config ) {
								i=0;
								$.fn.dataTable.ext.buttons.excelHtml5.action.call(this, e, dt, node, config);
							},
							//footer: true,
						},
						{
							extend: 'pdfHtml5',
							className:"BUTTON_DPX",
							title: 'Pma Inspection Report '+caption+" "+captionType,
							action: function ( e, dt, node, config ) {
								i=0;
								$.fn.dataTable.ext.buttons.pdfHtml5.action.call(this, e, dt, node, config);
							},
							//footer: true,
						},
						],
						"scrollCollapse": true,
						"paging":true,
						"destroy": true,
						"scrollX":true,
						columns:column, /*[
							{ "data": null ,
								"render": function ( data, type, row ) {
									return ++i;
								} },
								{ "data": "district_name" },
								//{ "data": "district_id" },
								{ "data": "block_name" },
								//{ "data": "block_id" },
								{ "data": "school_name" },
								{ "data": "school_id" },
								{ "data": "work_desc" },
								//{ "data": "work_desc_id" },
								{ "data": "id" },
								{ "data": "name" },
								{ "data": "name1" },
								{ "data": "no_activities" },
								{ "data": "visits_prev_mth" },
								{ "data": "visits_curr_mth"},
								{ "data": "activities_prev_mth" },
								{ "data": "activities_curr_mth"}
								],*/
								"columnDefs":columnDef,
				} );
			},
			error: function(jqXHR, status, error){
				swal('error');
			}
		});

		if(name=="block_id"){

		}else if(name=="school_id"){
			$('#code').html('School Code');
			$('#name').html('School Name');
		}else if(subcat.length==4){
			$('#code').html('Activity Code');
			$('#name').html('Activity Name');
		}

	}

	$('#inspectionTable tbody').on( 'click', '.r-click', function () {
		//get textContent of the TD
		console.log('TD cell textContent : ', this.textContent, this);

		//get the value of the TD using the API 
		//console.log('value by API : ', table.cell({ row: this.parentNode.rowIndex, column : this.cellIndex }).data());
		console.log(table1.column().data());
		var data = table1.row( $(this).parents('tr') ).data();
		var row=table1.cell( this ).index().row;
		console.log("==============data==================");
		console.log(data);
		console.log("==============data==================");
		id=data.id;
		console.log(id);
		console.log(id.toString().length);
		if(id.toString().length==4){
			name='block_id';
			$('#code').html('Block Code');
			$('#name').html('Block Name');
			//$('#district_name').val(data.name);
			//$("#district_name").prop("disabled", true);
			//getBlockName(userDetails.districtId);
		}else if(id.toString().length==6){
			$('#code').html('School Code');
			$('#name').html('School Name');
			//$('#block_name').html("<option value="+data.id+">"+data.name+"</option>");
			//$("#district_name").prop("disabled", true);
			name='school_id';
		}else if(id.toString().length==11){
			//$('#school_name').html("<option value="+data.id+">"+data.name+"</option>");
			name='school_id';
		}
		table(id,name,subcat);
	})

})