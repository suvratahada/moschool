/**
 * 	Custom Js For Admin Project Details
 * 	Added By Raghu
 * 	Created On: 31-Aug-2018
 * 
 */
$('document').ready(function(){
	
	if($('#blockfilter').prop('disabled', true)){
		$('#blockfilter').attr('title', 'Please select a district');
	}
	
	/*This function will retrieve all the fin year details */
	var getFinancialYear = function getFinancialYear(){
		$.ajax({
			url:'./projectmonitoring/getFinancialYear.do',
			type: "POST",
			async:false,
			beforeSend: function(xhr) {
	                xhr.setRequestHeader(header, token);
			 		},
			success:function(result){
				if(result.status == false){
					 swal(result.msg);
				}else{
					var data = result.data;
					console.log(data)
					var option = '<option   value="All">--Select Financial Year--</option>';
					for(var i = 0;i<data.length;i++){
						option += '<option value="'+data[i].year_id+'">'+data[i].year_desc+'</option>';
					}
					// for fin year filter
					$('#finyear').val('');
					$('#finyear').empty();
					$('#finyear').append(option);
					$("#finyear").prop("selectedIndex", 0);
				}
			}
			
		});
	}
	/*call to get all fin year details*/
	getFinancialYear();
	
	var getBlockDetails = function getBlockDetails(districtId) {
		console.log("inside method", districtId)
		$.ajax({
			 url: "./mastersetup/getBlockDetailsByDistrictID/"+districtId+".do",
			 type: "POST",	
			 async:false,
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
		
			 success:function(result) {
					if(result.status == false){
						 swal(result.msg);
					}else{
						var data = result.data;
						console.log("getBlockDetailsByDistrictID",data);
						 var option = '<option value="All">--Select Block--</option>';
						 for(var i = 0;i<data.length;i++){
								option += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
							}
							$('#blockfilter').val('');
							$('#blockfilter').empty();
							$('#blockfilter').append(option);
							$("#blockfilter").prop("selectedIndex", 0);
					 }
			 }
		})
	}
	
	
	var DistrictDetails = function DistrictDetails() {
		$.ajax({
			 url: "./mastersetup/getDistrictDetails.do",
			 type: "POST",	
			 async:false,
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			success: function(result)
            {
				if(result.status == false){
					 swal(result.msg);
				}else{
					var data = result.data;
					//console.log(data)
					 var option = '<option value="All">--Select District--</option>';
					 for(var i = 0;i<data.length;i++){
							option += '<option value="'+data[i].district_id+'">'+data[i].district_name+'</option>';
						}
						$('#districtfilter').val('');
						$('#districtfilter').empty();
						$('#districtfilter').append(option);
						$("#districtfilter").prop("selectedIndex", 0);
				 }
		 },
		    failure: function () {
		        swal("Failed!");
		    }
			 
		});
	}
	// call to get district details
	DistrictDetails();
	
	/*This function will retrieve all the scheme*/
	var getSchemeDetails = function getSchemeDetails(){
		$.ajax({
			url:'./projectmonitoring/getSchemeDetails.do',
			type: "POST",
			async:false,
			beforeSend: function(xhr) {
	                xhr.setRequestHeader(header, token);
			 		},
			success:function(result){
				if(result.status == false){
					 swal(result.msg);
				}else{
					var data = result.data;
					//console.log(data)
					var option = '<option value="All">--Select Scheme--</option>';//<option value="-1" selected disabled>-- Select --</option>
					for(var i = 0;i<data.length;i++){
						option += '<option value="'+data[i].scheme_id+'">'+data[i].scheme+'</option>';
					}
					// for scheme filter
					$('#schemeFilter').val('');
					$('#schemeFilter').empty();
					$('#schemeFilter').append(option);
					$("#schemeFilter").prop("selectedIndex", 0);
				}
			}
			
		});
	}
	/*call to get all scheme details*/
	getSchemeDetails();
	
	var tblDistrictSchoolMonitoringDetails = $('#tblDistrictSchoolMonitoringDetails').DataTable({
		
	        "scrollX": true,
			"bProcessing" : false,
			//"lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
			"pageLength": 10,
			"bProcessing": true,
			"bServerSide": true, 
			"bStateSave":false,
			"bPaginate": true,
			"bLengthChange": false,
			"bFilter": true,
			"bSort": true,
			"bInfo": true,
			"cache":false,
			"bAutoWidth": false,
			"bRetrieve": true,
			"bDestroy":false,
			"searching": false,
			"language": 
				{          
					"processing": "Fetching Projects...",
				},
			"aoColumns":[
			   //{"className": "dt-center", "targets": "_all"}, 
			   {"data":"district_name"},
			   {"data":"block_name"},
		       {"data":"school_name"},
			   {"data":"school_id"},
			   {"data":"year_desc"},
			   {"data":"work_desc"},
			   {"data":"no_wrksanc"},
			   {"data":"cost_pu"},
			   {"data":"sanc_amt"},
			   {"data":"balance_amount"},
			   {"data":"np_id"},
			   {"data":"scheme"},
			   {"data":"catg_desc"},
			   ],
			   "sAjaxSource":"./projectmonitoring/getProjectDetails/All/All/All/All.do",
	});

	var isDisabled = $('#blockfilter').prop('disabled');
	
	/*var district='';
	var block= '';
	var finyear='';
	
	This function will retrieve all the Projects from master_project table*/
	/*var getProjectDetails = function getProjectDetails(){
		$('#loading').show();
		district= $('#districtfilter').val();
		scheme=$('#schemeFilter').val();
		finyear= $('#finyear').val();
		if(isDisabled){
			block= 'All';
		}else{
			block= $('#blockfilter').val();
			console.log('inside condition')
		}
		console.log("district= "+district)
		console.log("block= "+block)
		console.log("finyear= "+finyear)
		$.ajax({
			url:"./projectmonitoring/getProjectDetails/"+district+"/"+block+"/"+finyear+"/"+scheme+".do",
			type: "POST",
			async:false,
			beforeSend: function(xhr) {
	                xhr.setRequestHeader(header, token);
			 		},
			success:function(result){
				$('#loading').hide();
				$.LoadingOverlay("hide");
				if(result.status == false){
					 swal(result.msg);
				}else{
					
					var data = result.data;
					//console.log(data)
					 console.log(result);
					 tblDistrictSchoolMonitoringDetails.clear();
					 tblDistrictSchoolMonitoringDetails.rows.add(result.data).draw();
					
						console.log(data);
				}
			}
			
		});
	}
	call to get all the project details
	getProjectDetails();*/
	
	
	//Filter Functions
	var districtId = '';
	
	$('#districtfilter').on('change', function(){
		$("#blockfilter").prop('disabled', false);
		$('#blockfilter').attr('title', 'Block');
		var isDisabled = $('#blockfilter').prop('disabled');
		district = $('#districtfilter').val();
		/*if(isDisabled){
			block = 'All';
		}else{
			block = $('#blockfilter').val();
		}*/
		block = 'All';
		console.log("block is:: "+isDisabled+" "+block)
		finyear = $('#finyear').val();
		scheme = $('#schemeFilter').val();
		
		console.log('districtId', district)
		getBlockDetails(district);
		tblDistrictSchoolMonitoringDetails.ajax.url("./projectmonitoring/getProjectDetails/"+district+"/"+block+"/"+finyear+"/"+scheme+".do").load();
	})
	isDisabled = console.log($('#blockfilter').prop('disabled'))
	$('#blockfilter').on('change', function(){
		var isDisabled = $('#blockfilter').prop('disabled');
		district = $('#districtfilter').val();
		if(isDisabled){
			block = 'All';
		}else{
			block = $('#blockfilter').val();
		}
		finyear = $('#finyear').val();
		scheme = $('#schemeFilter').val();
		
		console.log('block', block)
		tblDistrictSchoolMonitoringDetails.ajax.url("./projectmonitoring/getProjectDetails/"+district+"/"+block+"/"+finyear+"/"+scheme+".do").load();
	})
	
	$('#finyear').on('change', function(){
		finyear = $('#finyear').val();
		var isDisabled = $('#blockfilter').prop('disabled');
		district = $('#districtfilter').val();
		if(isDisabled){
			block = 'All';
		}else{
			block = $('#blockfilter').val();
		}
		finyear = $('#finyear').val();
		scheme = $('#schemeFilter').val();
		console.log(isDisabled+" "+block)
		console.log('finyear', finyear)
		tblDistrictSchoolMonitoringDetails.ajax.url("./projectmonitoring/getProjectDetails/"+district+"/"+block+"/"+finyear+"/"+scheme+".do").load();
	})

	$('#schemeFilter').on('change', function() {
		scheme = $('#schemeFilter').val();
		console.log('scheme', scheme)
		var isDisabled = $('#blockfilter').prop('disabled');
		district = $('#districtfilter').val();
		if(isDisabled){
			block = 'All';
		}else{
			block = $('#blockfilter').val();
		}
		finyear = $('#finyear').val();
		scheme = $('#schemeFilter').val();
		
		tblDistrictSchoolMonitoringDetails.ajax.url("./projectmonitoring/getProjectDetails/"+district+"/"+block+"/"+finyear+"/"+scheme+".do").load();
	})
	
	//export excel
	
	$('#admin-allocation-excel').click(function(){
		scheme = $('#schemeFilter').val();
		district = $('#districtfilter').val();
		var isDisabled = $('#blockfilter').prop('disabled');
		if(isDisabled){
			block = 'All';
		}else{
			block = $('#blockfilter').val();
		}
		finyear = $('#finyear').val();
		$.LoadingOverlay("show");
		$('#admin-allocation-excel').attr('href','./projectmonitoring/exportProjectsToExcelForAdmin/'+scheme+'/'+district+'/'+block+'/'+finyear+'.do');
		$.LoadingOverlay("hide");
		
	})
	
});