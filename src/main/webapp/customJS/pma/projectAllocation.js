/**
 * Custom Js For Project Allocation
 * Added By Raghu
 * Created On:- 01-Oct-2018
 */
$(document).ready(function(){
	
	function showLoading(){
		$('#loading').show();
	}
	
	function hideLoading(){
		$('#loading').hide();
	}
	
	/* Modal Reset Functions */
	$("#projectAllocationUploadModal").on("hidden.bs.modal", function(){
		 $('#projectAllocationUploadForm')[0].reset();
	});
	$("#viewProjectAllocationLoggerDetailsModal").on("hidden.bs.modal", function(){
		 $('#viewLoggerDetailsform')[0].reset();
	});
	
	/*This function will retrieve all the fin year */
	var getFinancialYear = function getFinancialYear(){
		$.ajax({
			url:'./projectmonitoring/getFinancialYear.do',
			type: "POST",
			async:false,
			beforeSend: function(xhr) {
	                xhr.setRequestHeader(header, token);
			 		},
			success:function(result){
				if(result.status == false){
					 swal(result.msg);
				}else{
					var data = result.data;
					console.log(data)
					var option = '<option value="NA">--Select Financial Year--</option>';
					for(var i = 0;i<data.length;i++){
						option += '<option value="'+data[i].year_id+'">'+data[i].year_desc+'</option>';
					}
					$('#project_fin_year').val('');
					$('#project_fin_year').empty();
					$('#project_fin_year').append(option);
					$("#project_fin_year").prop("selectedIndex", 0);
				}
			}
			
		});
	}
	// call the function to get finYear
	getFinancialYear();
	
	 /*******************************************************
	 * dynamic table creation in project allocation module
	 *********************************************************/
	var table = $('#projectAllocationFileLoggerTable').DataTable({
		 "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
		 "pageLength": 10,	
		 "bFilter": true,
		 "bLengthChange": false,
		 "scrollX": true,
		 "columnDefs":[
		   {"className": "text-center", "targets":1},  
		   {"className": "text-center", "targets":2},
		   {"className": "text-center", "targets":4},
		   {"className": "text-center", "targets":5},
		   {"targets":0,"data":null,
			   render: function (data, type, row, meta) {
    		        return meta.row + meta.settings._iDisplayStart + 1;
    		    }
		   },
			{"targets":1,"data":null,"mRender":function(data) {
				if(data.process_status==true){
					return "<div class='btn-group'>"+
	                "<button type='button' id='uploadprojectAllocationFile' action='uploadprojectAllocationFile' disabled='true'  class='btn btn-primary'>"+
		                "<i class='fa fa-upload'></i>"+
		            "</button>"+
	            "</div>"
				}else{
					return "<div class='btn-group'>"+
	                "<button type='button' id='uploadprojectAllocationFile' action='uploadprojectAllocationFile'  class='btn btn-primary'>"+
	                "<i class='fa fa-upload'></i>"+
		            "</button>"+
	            "</div>"
				}
				}
		   
			},
	        {"targets":2,"data":null,"mRender":function(data) {
	        	if(data.process_status==true){
	        		return "<div class='btn-group'>"+
		                "<button type='button'  id='processProjectAllocationFile' action='processProjectAllocationFile' disabled='true'  class='btn btn-primary'>"+
		                "<i class='fa fa-spinner'></i>"+
			            "</button>"+
		            "</div>"
	        	}else{
	        		return "<div class='btn-group'>"+
		                "<button type='button'  id='processProjectAllocationFile' action='processProjectAllocationFile'   class='btn btn-primary'>"+
			                "<i class='fa fa-spinner'></i>"+
			            "</button>"+
		            "</div>"
	        	}
 			}},
  		   {"targets":3,"data":"year_desc"},
  		   {"targets":4,"data":null,"mRender":function(data) {
  		  				if(data.file_name!=null){
  		  					return '<a href="./projectmonitoring/downloadProjectAllocationFiles/'+data.file_name+'.do"><button type="button"  id="" class="btn btn-success"><i class="fa fa-download"></i></button></a>';
  		  				}else{
  		  					return "NA";
  		  				}
  		  	}},
	        {"targets":5,"data": null,"mRender":function(data) {
  		  		if(data.process_status==true){
	  		  		return  "<div class='btn-group'>"+
	                "<button type='button'  id='viewProjectAllocationLogFile' action='viewProjectAllocationLogFile'  class='btn btn-secondary'>"+
	                "<i class='fa fa-history'></i></button></div>"
  		  		}else{
	  		  		return "<div class='btn-group'>"+
	                "<button type='button'  id='viewProjectAllocationLogFile' action='viewProjectAllocationLogFile'  class='btn btn-secondary' disabled>"+
	                "<i class='fa fa-history'></i></button></div>"
  		  		}
  		  	  }
  		  	},
			{"targets":6,"data":"created_by"},
			{"targets":7,"data":"created_on"}
	     ]
	  });
	
	var getDataToProjectAllocationLoggerTable = function getDataToProjectAllocationLoggerTable() {
		showLoading();
		$.ajax({
			 url: "./projectmonitoring/getProjectAllocationLoggerDetails.do",
			 type: "GET",	
			 beforeSend: function(xhr) {
	                xhr.setRequestHeader(header, token);
   			},
			 success:function(result) {
				 hideLoading();
				 console.log(result)
				 table.clear();
				 table.rows.add(result.data).draw();
			
			 },
			 error:function(error){
				 hideLoading();
				 	swal(error);
				 }
		});
	}
	getDataToProjectAllocationLoggerTable();
	
	function objectifyForm(formArray) {
		   var returnArray = {};
		   for (var i = 0; i < formArray.length; i++){
		     returnArray[formArray[i]['name']] = formArray[i]['value'];
		   }
		   return returnArray;
	}
	// create file logger button click function
	$("#createProjectAllocationFileLogbtn").click(function(){
		var finYear=$("#project_fin_year").val();
		if(finYear!="NA"){
			createProjectAllocationFileLogger();
		}else{
			swal("Please select financial year");
		}
	}) 
	function createProjectAllocationFileLogger(){
		showLoading();
		var postData = $("#createProjectAllocationFileForm").serializeArray();
		var formURL = "./projectmonitoring/createProjectAllocationFileLogger.do";
		$.ajax({
		 url: formURL,
		 type: "POST",
		 async:false,
		 beforeSend: function(xhr) {
                xhr.setRequestHeader(header, token);
  		},
  		data: JSON.stringify(objectifyForm(postData)),
        contentType:"application/json",
        dataType:"JSON",
		 success: function(data) {
			  	hideLoading();
                if (data.status) {
                	swal(data.msg)
                	getDataToProjectAllocationLoggerTable();
	              }else{
	            	  swal(data.msg);
	              }
            },
		 error:function(jqXHR, status, error){
			 hideLoading();
		 	swal(error);
		 	}
		});
	}
	
	$('#projectAllocationFileLoggerTable tbody').on( 'click', 'button[action=uploadprojectAllocationFile]', function () {		
 		var data = table.row( $(this).parents('tr') ).data();
		 	$('#projectAllocationUploadModal').modal();
		 	$('#hidden_slno').val(data.slno);
		 	$('#hidden_fin_year').val(data.finyr_id);
	});
	/*	Upload Project Allocation Template Function */
	$("#uploadProjectAllocationTemplateButton").click(function() {
			showLoading();
			var form = $("#projectAllocationUploadForm")[0];
			var slno=$("#hidden_slno").val();
			var finYear=$("#hidden_fin_year").val();
		       
	        var formData = new FormData(form);
			var formURL = "./projectmonitoring/uploadProjectAllocationFile"+slno+"/"+finYear+".do";
			$.ajax({
			 url: formURL,
			 type: "POST",
	         enctype: 'multipart/form-data',
	         beforeSend: function(xhr) {
	              xhr.setRequestHeader(header, token);
	         },
	         data: formData,
	         processData: false,
	         contentType: false,
	         cache: false,
	         timeout: 600000,
			 success: function(data) {
				 	hideLoading();
	                if (data.status) {
	                	swal(data.msg);
	                	$('#projectAllocationUploadModal').modal('hide');
	                	getDataToProjectAllocationLoggerTable();
		              }else{
		            	  swal(data.msg);
		            	  $('#projectAllocationUploadModal').modal('hide');
		              }
	            },
			 error:function(jqXHR, status, error){
				hideLoading();
			 	swal(error);
			 	}
			});
		});
	/* Process Project Allocation Excel File */ 
	$('#projectAllocationFileLoggerTable tbody').on( 'click', 'button[action=processProjectAllocationFile]', function () {		
		showLoading();
 		var data = table.row( $(this).parents('tr') ).data();
 		var process_data =
        {
		 file_name:data.file_name,
		 slno:data.slno,
		 finyr_id:data.finyr_id
        };
 		if(data.upload_status==true){
 			  $.ajax({
 				 url: "./projectmonitoring/processProjectAllocationFile.do",
 				 type: "POST",	
 				 beforeSend: function(xhr) {
 		                xhr.setRequestHeader(header, token);
 	   			},
 	   			contentType : 'application/json',
 	   			dataType: "json",
 	   			data: JSON.stringify(process_data),
 				 success:function(data) {
 					 hideLoading();
 					 if(data.status){
 						 swal(data.msg);
 						 getDataToProjectAllocationLoggerTable();
 					 }else{
 						 getDataToProjectAllocationLoggerTable();
 						 swal(data.msg);	
 					 }
 				 },
 				 error:function(jqXHR, status, error){
 					hideLoading();
 					swal(error);
 				 }
 			});
 		}else{
 			hideLoading();
 			swal('Please upload and validate the project allocation template');
 		}
		});
	// view log button click function
	$('#projectAllocationFileLoggerTable tbody').on( 'click', 'button[action=viewProjectAllocationLogFile]', function () {		
 		var data = table.row( $(this).parents('tr') ).data();
		 	$('#viewProjectAllocationLoggerDetailsModal').modal();
		 	$('#log_upload_start_time').html(":&nbsp;"+data.upload_start_time);
		 	$('#log_upload_comp_time').html(":&nbsp;"+data.completion_time);
		 	$('#log_success_count').html(":&nbsp;"+data.success_log_text);
		 	$('#log_failed_count').html(":&nbsp;"+data.error_log_text);
		 	if(data.duplicate_log_text!=0){
		 		$('#duplicate_data_hidden').show();
		 		$('#log_duplicated_count').html(":&nbsp;"+data.duplicate_log_text);
		 	}
		 	else{
		 		$('#duplicate_data_hidden').hide();
		 	}
		 	
	});
});