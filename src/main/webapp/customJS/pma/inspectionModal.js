$('document').ready(function(){
	
		$('#fundreleaseyes').hide();
		$('#fundreleaseno').hide();
		$('#layoutyes').hide();
		$('#layoutno').hide();
		$('#foundationyes').hide();
		$('#foundationno').hide();
		$('#plinthyes').hide();
		$('#plinthno').hide();
		$('#lintelyes').hide();
		$('#lintelno').hide();
		$('#roofyes').hide();
		$('#roofno').hide();
		$('#roofcastyes').hide();
		$('#roofcastno').hide();
		$('#finishingyes').hide();
		$('#finishingno').hide();
		$('#completedyes').hide();
		$('#completedno').hide();
		$('#recievedyes').hide();
		$('#recievedno').hide();
		$('#checkyes').hide();
		$('#checkno').hide();
		$('#handedyes').hide();
		$('#handedno').hide();
		$('#submittedyes').hide();
		$('#submittedno').hide();
		$('#approvedyes').hide();
		$('#approvedno').hide();
	
		$('#fundremarks').hide();
		$('#layoutremarks').hide();
		$('#foundationremarks').hide();
		$('#plinthremarks').hide();
		$('#lintelremarks').hide();
		$('#roofremarks').hide();
		$('#roofcastremarks').hide();
		$('#finishingremarks').hide();
		$('#completedremarks').hide();
		$('#checkremarks').hide();
		$('#handedremarks').hide();
		$('#submittedremarks').hide();
		$('#approvedremarks').hide();
	
	$('input[type=radio][name=fund]').change(function(){
		if($('input[name=fund]:checked').val() == 1){
			$('#fundreleaseno').hide();
			$('#fundreleaseyes').show();
			$('#fundremarks').show();
		}else{
			$('#fundreleaseyes').hide();
			$('#fundreleaseno').show();
			$('#fundremarks').show();
		}
	});
	
	$('input[type=radio][name=layout]').change(function(){
		if($('input[name=layout]:checked').val() == 1){
			$('#layoutno').hide();
			$('#layoutyes').show();
			$('#layoutremarks').show();
		}else{
			$('#layoutyes').hide();
			$('#layoutno').show();
			$('#layoutremarks').show();
		}
	});
	
	$('input[type=radio][name=foundation]').change(function(){
		if($('input[name=foundation]:checked').val() == 1){
			$('#foundationno').hide();
			$('#foundationyes').show();
			$('#foundationremarks').show();
		}else{
			$('#foundationyes').hide();
			$('#foundationno').show();
			$('#foundationremarks').show();
		}
	});
	
	$('input[type=radio][name=plinth]').change(function(){
		if($('input[name=plinth]:checked').val() == 1){
			$('#plinthno').hide();
			$('#plinthyes').show();
			$('#plinthremarks').show();
		}else{
			$('#plinthyes').hide();
			$('#plinthno').show();
			$('#plinthremarks').show();
		}
	});
	
	$('input[type=radio][name=lintel]').change(function(){
		if($('input[name=lintel]:checked').val() == 1){
			$('#lintelno').hide();
			$('#lintelyes').show();
			$('#lintelremarks').show();
		}else{
			$('#lintelyes').hide();
			$('#lintelno').show();
			$('#lintelremarks').show();
		}
	});
	
	$('input[type=radio][name=roof]').change(function(){
		if($('input[name=roof]:checked').val() == 1){
			$('#roofno').hide();
			$('#roofyes').show();
			$('#roofremarks').show();
		}else{
			$('#roofyes').hide();
			$('#roofno').show();
			$('#roofremarks').show();
		}
	});
	
	$('input[type=radio][name=roofcast]').change(function(){
		if($('input[name=roofcast]:checked').val() == 1){
			$('#roofcastno').hide();
			$('#roofcastyes').show();
			$('#roofcastremarks').show();
		}else{
			$('#roofcastyes').hide();
			$('#roofcastno').show();
			$('#roofcastremarks').show();
		}
	});
	
	$('input[type=radio][name=finishing]').change(function(){
		if($('input[name=finishing]:checked').val() == 1){
			$('#finishingno').hide();
			$('#finishingyes').show();
			$('#finishingremarks').show();
		}else{
			$('#finishingyes').hide();
			$('#finishingno').show();
			$('#finishingremarks').show();
		}
	});
	
	$('input[type=radio][name=completed]').change(function(){
		if($('input[name=completed]:checked').val() == 1){
			$('#completedno').hide();
			$('#completedyes').show();
			$('#completedremarks').show();
		}else{
			$('#completedyes').hide();
			$('#completedno').show();
			$('#completedremarks').show();
		}
	});
	
	$('input[type=radio][name=recieved]').change(function(){
		if($('input[name=recieved]:checked').val() == 1){
			$('#recievedno').hide();
			$('#recievedyes').show();
		}else{
			$('#recievedyes').hide();
			$('#recievedno').show();
		}
	});
	
	$('input[type=radio][name=check]').change(function(){
		if($('input[name=check]:checked').val() == 1){
			$('#checkno').hide();
			$('#checkyes').show();
			$('#checkremarks').show();
		}else{
			$('#checkyes').hide();
			$('#checkno').show();
			$('#checkremarks').show();
		}
	});
	
	$('input[type=radio][name=handed]').change(function(){
		if($('input[name=handed]:checked').val() == 1){
			$('#handedno').hide();
			$('#handedyes').show();
			$('#handedremarks').show();
		}else{
			$('#handedyes').hide();
			$('#handedno').show();
			$('#handedremarks').show();
		}	
	});
	
	$('input[type=radio][name=submitted]').change(function(){
		if($('input[name=submitted]:checked').val() == 1){
			$('#submittedno').hide();
			$('#submittedyes').show();
			$('#submittedremarks').show();
		}else{
			$('#submittedyes').hide();
			$('#submittedno').show();
			$('#submittedremarks').show();
		}
	});
	
	$('input[type=radio][name=approved]').change(function(){
		if($('input[name=approved]:checked').val() == 1){
			$('#approvedno').hide();
			$('#approvedyes').show();
			$('#approvedremarks').show();
		}else{
			$('#approvedyes').hide();
			$('#approvedno').show();
			$('#approvedremarks').show();
		}
	});
	
	
//	Non Building Modal Show Hide
	
	$('#nbfundreleaseyes').hide();
	$('#nbfundreleaseno').hide();
	$('#nbstartyes').hide();
	$('#nbstartno').hide();
	$('#nbcompletedyes').hide();
	$('#nbcompletedno').hide();
	$('#nbprogressyes').hide();
	$('#nbprogressno').hide();
	$('#nbhandedyes').hide();
	$('#nbhandedno').hide();
	$('#nbsubmittedyes').hide();
	$('#nbsubmittedno').hide();
	$('#nbapprovedyes').hide();
	$('#nbapprovedno').hide();
	
	$('#nbfundremarks').hide();
	$('#nbstartremarks').hide();
	$('#nbprogressremarks').hide();
	$('#nbcompletedremarks').hide();
	$('#nbhandedremarks').hide();
	$('#nbsubmittedremarks').hide();
	$('#nbapprovedremarks').hide();
	
	$('input[type=radio][name=nbfund]').change(function(){
		if($('input[name=nbfund]:checked').val() == 1){
			$('#nbfundreleaseno').hide();
			$('#nbfundreleaseyes').show();
			$('#nbfundremarks').show();
		}else{
			$('#nbfundreleaseyes').hide();
			$('#nbfundreleaseno').show();
			$('#nbfundremarks').show();
		}
	});
	
	$('input[type=radio][name=nbstart]').change(function(){
		if($('input[name=nbstart]:checked').val() == 1){
			$('#nbstartno').hide();
			$('#nbstartyes').show();
			$('#nbstartremarks').show();
		}else{
			$('#nbstartyes').hide();
			$('#nbstartno').show();
			$('#nbstartremarks').show();
		}
	});
	
	$('input[type=radio][name=nbprogress]').change(function(){
		if($('input[name=nbprogress]:checked').val() == 1){
			$('#nbprogressno').hide();
			$('#nbprogressyes').show();
			$('#nbprogressremarks').show();
		}else{
			$('#nbprogressyes').hide();
			$('#nbprogressno').show();
			$('#nbprogressremarks').show();
		}
	}); 
	
	$('input[type=radio][name=nbcompleted]').change(function(){
		if($('input[name=nbcompleted]:checked').val() == 1){
			$('#nbcompletedno').hide();
			$('#nbcompletedyes').show();
			$('#nbcompletedremarks').show();
		}else{
			$('#nbcompletedyes').hide();
			$('#nbcompletedno').show();
			$('#nbcompletedremarks').show();
		}
	}); 

	$('input[type=radio][name=nbhanded]').change(function(){
		if($('input[name=nbhanded]:checked').val() == 1){
			$('#nbhandedno').hide();
			$('#nbhandedyes').show();
			$('#nbhandedremarks').show();
		}else{
			$('#nbhandedyes').hide();
			$('#nbhandedno').show();
			$('#nbhandedremarks').show();
		}	
	});
	
	$('input[type=radio][name=nbsubmitted]').change(function(){
		if($('input[name=nbsubmitted]:checked').val() == 1){
			$('#nbsubmittedno').hide();
			$('#nbsubmittedyes').show();
			$('#nbsubmittedremarks').show();
		}else{
			$('#nbsubmittedyes').hide();
			$('#nbsubmittedno').show();
			$('#nbsubmittedremarks').show();
		}
	});
	
	$('input[type=radio][name=nbapproved]').change(function(){
		if($('input[name=nbapproved]:checked').val() == 1){
			$('#nbapprovedno').hide();
			$('#nbapprovedyes').show();
			$('#nbapprovedremarks').show();
		}else{
			$('#nbapprovedyes').hide();
			$('#nbapprovedno').show();
			$('#nbapprovedremarks').show();
		}
	});
})


