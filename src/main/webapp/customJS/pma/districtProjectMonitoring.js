/**
 * 	Custom Js For District Monitoring 
 * 	Added By Raghu
 * 	Created On: 31-Aug-2018
 * 
 */

$(document).ready(function(){
	// for dynamic reallocation
	var x = 0; //initial field count
	var tot_all= 0;
	var varyingData = {};
	// for date picker
	$('#release_date').datepicker({
		autoclose:true,
		format:"dd-mm-yyyy",
		todayHighlight: true,
		autoclose: true,
		endDate: '+0d',
	});
	// date picker ends

	
	// modal loader function
	/*	var myApp;
	myApp = myApp || (function () {
	    return {
	        showPleaseWait: function() {
	            $("#pleaseWaitDialog").modal();
	        },
	        hidePleaseWait: function () {
	        	$("#pleaseWaitDialog").modal('hide');
	        },

	    };
	})();*/
	// modal loader function ends
	
	/*  Modal reset Starts   */
	$("#reAllocationFundsModal").on("hidden.bs.modal", function(){
		$('#reallocateFundsForm')[0].reset();
		$('#get_allocation').attr('disabled',false);
		$("#budget_allocation_wrap").html("");
		$('#rem_bal0').html('');
		$('#reAllocationFundsSubmit').attr('disabled',false);
		$("#reAllocationFundsSubmit").button('reset');
		var validator = $( "#reallocateFundsForm" ).validate();
		 validator.resetForm();
		x=0;
		tot_all= 0;
	});
	$("#releasedAmountModal").on("hidden.bs.modal", function(){
		 $('#releaseAmountForm')[0].reset();
		 $('#releaseAmountModalSubmit').button('reset');
		 var validator = $( "#releaseAmountForm" ).validate();
		 validator.resetForm();
		 $('#release_amount').val('');
		 $('#release_amount').attr('readonly',false);
 		 $('#release_amount').css('color', '#000');
 		 $('#total_balance_amt').css('color', '#000');
 		 $('#releaseAmountModalSubmit').attr('disabled',false);
	});
	$("#releaseAmountTrackingModal").on("hidden.bs.modal", function(){
			$('#timelineDynamic').empty();
	});
	/*  Modal reset Ends   */
	
	
	/*Validation Methods Starts here*/
	var onlyCharAllowMsg = "Only characters are allowed";
	var dateFormatAllowMsg = "date format should be dd-mm-yyyy";
	var onlyNumberMsg = "Only numeric values allowed"

	$.validator.addMethod('letter', function (value, element) {
		return value.match(/^[a-zA-Z ]+$/);
	});

	$.validator.addMethod('customdate', function (value, element) {		
		return value.match(/^\d\d?-\d\d?-\d\d\d\d$/);// DD-MM-YYYY
	});
	$.validator.addMethod('onlyNumber', function (value, element) {
		return value.match(/^[0-9.]*$/);
	});
	
	$.validator.addMethod('validateDescription', function (value, element) {
		return value.match(/^[A-Za-z0-9 !@%&*(){}+=';:/_-]*$/);
	});
	
	/*Validation Methods Ends here*/
	
	/*This function will retrieve all the block from master_block table*/
	var getBlockDetailsByDistrict = function getBlockDetailsByDistrict(){
		$.ajax({
			url:'./projectmonitoring/getBlockDetails.do',
			type: "POST",
			async:false,
			beforeSend: function(xhr) {
	                xhr.setRequestHeader(header, token);
			 		},
			success:function(result){
				if(result.status == false){
					 swal(result.msg);
				}else{
					var data = result.data;
					console.log(data)
					var option = '<option selected  value="All">--Select Block Name--</option>';
					for(var i = 0;i<data.length;i++){
						option += '<option value="'+data[i].block_id+'">'+data[i].block_name+'</option>';
					}
					
					$('#blockfilter').val('');
					$('#blockfilter').empty();
					$('#blockfilter').append(option);
					$("#blockfilter").prop("selectedIndex", 0);
					
					
				}
			}
			
		});
	}
	/*call to get all the block details*/
	getBlockDetailsByDistrict();
	
	/*This function will retrieve all the fin year */
	var getFinancialYear = function getFinancialYear(){
		$.ajax({
			url:'./projectmonitoring/getFinancialYear.do',
			type: "POST",
			async:false,
			beforeSend: function(xhr) {
	                xhr.setRequestHeader(header, token);
			 		},
			success:function(result){
				if(result.status == false){
					 swal(result.msg);
				}else{
					var data = result.data;
					console.log(data)
					var option = '<option value="All">--Select Financial Year--</option>';
					for(var i = 0;i<data.length;i++){
						option += '<option value="'+data[i].year_id+'">'+data[i].year_desc+'</option>';
					}
					
					// for fin year filter
					$('#finyear').val('');
					$('#finyear').empty();
					$('#finyear').append(option);
					$("#finyear").prop("selectedIndex", 0);
				}
			}
			
		});
	}
	/*call to get all fin year details*/
	getFinancialYear();
	
	
	/*This function will retrieve all the scheme*/
	var getSchemeDetails = function getSchemeDetails(){
		$.ajax({
			url:'./projectmonitoring/getSchemeDetails.do',
			type: "POST",
			async:false,
			beforeSend: function(xhr) {
	                xhr.setRequestHeader(header, token);
			 		},
			success:function(result){
				if(result.status == false){
					 swal(result.msg);
				}else{
					var data = result.data;
					//console.log(data)
					var option = '<option value="All">--Select Scheme--</option>';//<option value="-1" selected disabled>-- Select --</option>
					for(var i = 0;i<data.length;i++){
						option += '<option value="'+data[i].scheme_id+'">'+data[i].scheme+'</option>';
					}
					
					// for scheme filter
					$('#schemeFilter').val('');
					$('#schemeFilter').empty();
					$('#schemeFilter').append(option);
					$("#schemeFilter").prop("selectedIndex", 0);
				}
			}
			
		});
	}
	/*call to get all scheme details*/
	getSchemeDetails();
	
	/*	var getCategoryDetails = function getCategoryDetails(){
		$.ajax({
			url:'./projectmonitoring/getCategoryDetails.do',
			type: "POST",
			async:false,
			beforeSend: function(xhr) {
	                xhr.setRequestHeader(header, token);
			 		},
			success:function(result){
				if(result.status == false){
					 swal(result.msg);
				}else{
					var data = result.data;
					//console.log(data)
					var option = '<option selected disabled value="">--Select Category--</option>';//<option value="-1" selected disabled>-- Select --</option>
					for(var i = 0;i<data.length;i++){
						option += '<option value="'+data[i].work_id+'">'+data[i].work_desc+'</option>';
					}
					// for category filter
					$('#work_id').val('');
					$('#work_id').empty();
					$('#work_id').append(option);
					$("#work_id").prop("selectedIndex", 0);

				}
			}
			
		});
	}
	call to get all category details
	getCategoryDetails();*/
	
	//Filter for sub category
	/*This function will retrieve all the sub categories from master_work table*/
	var getSubCategoryDetails = function getSubCategoryDetails(){
		$.ajax({
			url:'./projectmonitoring/getSubCategoryDetailsForFilter.do',
			type: "POST",
			async:false,
			beforeSend: function(xhr) {
	                xhr.setRequestHeader(header, token);
			 		},
			success:function(result){
				if(result.status == false){
					 swal(result.msg);
				}else{
					var data = result.data;
					//console.log(data)
					var option = '<option selected value="All">--Select SubCategory--</option>';
					for(var i = 0;i<data.length;i++){
						option += '<option value="'+data[i].work_desc_id+'">'+data[i].work_desc+'</option>';
					}

					// for sub category filter
					$('#subcategoryfilter').val('');
					$('#subcategoryfilter').empty();
					$('#subcategoryfilter').append(option);
					$("#subcategoryfilter").prop("selectedIndex", 0)
				}
			}
			
		});
	}
	/*call to get all subcategory details*/
	getSubCategoryDetails();

	
	$('#allocated_total_units0').on('keyup', function(){
		var total_units = $('#allocated_total_units0').val();
		var unit_cost = $('#allocated_cost').val();
		var app_amt=parseFloat(unit_cost)*parseFloat(total_units);
		var total_amount=app_amt.toFixed(2);
		$('#total_approved_amount0').val(total_amount);
	})
	
	
	$('#release_amount').on('keyup', function(){
		var r = $('#release_amount').val();
		var a = $('#rel_app_amount').val();
		if(parseFloat(r) > parseFloat(a)){
			swal("Release amount cannot be greater than Approved amount!");
			$('#release_amount').val('');
		}
	})
	
	
	var tblDistrictSchoolMonitoringDetails = $('#tblDistrictSchoolMonitoringDetails').DataTable({
			"scrollX": true,
			//"lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
			"pageLength": 10,
			"bProcessing": true,
			"bServerSide": true, 
			"bStateSave":false,
			"bPaginate": true,
			"bLengthChange": false,
			"bFilter": true,
			"bSort": true,
			"bInfo": true,
			"cache":false,
			"bAutoWidth": false,
			"bRetrieve": true,
			"bDestroy":false,
			"searching": false,
			"language": 
				{          
				"processing": "Fetching Projects...",
				},
			"aoColumns":[
//		   {"className": "dt-center", "targets": "_all"},  
		   {"targets":0,"data":null,
	        	"render":function(data){
	        		var buttons='';
	        		
	        		if(data.balance_units == 0){
	        			buttons+='<button class="btn btn-sm btn-primary btn-reallocation" action="budgetReallocation" title="Budget Re-Allocation Details" disabled><i class="fa fa-repeat"></i></button>&nbsp;'+
	        				'<button class="btn btn-sm btn-success" action="releaseAmount" title="Release Amount" disabled><i class="fa fa-rupee" ></i></button>'+
	        				'&nbsp;<button class="btn btn-sm btn-warning" action="trackReleaseAmount"  title="Track Release Amount" disabled><i class="fa fa-sitemap"></i></button>'
	        		}		
	        		else if(data.released_amount!=0){
	        			buttons+='<button class="btn btn-sm btn-primary btn-reallocation" action="budgetReallocation" title="Budget Re-Allocation Details" disabled><i class="fa fa-repeat"></i></button>&nbsp;'+
	        				'<button class="btn btn-sm btn-success" action="releaseAmount" title="Release Amount" ><i class="fa fa-rupee" ></i></button>'+
	        				'&nbsp;<button class="btn btn-sm btn-warning" action="trackReleaseAmount"  title="Track Release Amount"><i class="fa fa-sitemap"></i></button>'
	        		}
	        		else if(data.released_units != 0){
	        			buttons+='<button class="btn btn-sm btn-primary btn-reallocation" action="budgetReallocation" title="Budget Re-Allocation Details"><i class="fa fa-repeat"></i></button>&nbsp;'+
        				'<button class="btn btn-sm btn-success" action="releaseAmount" title="Release Amount" disabled><i class="fa fa-rupee"></i></button>'+
        				'&nbsp;<button class="btn btn-sm btn-warning" action="trackReleaseAmount"  title="Track Release Amount" disabled><i class="fa fa-sitemap"></i></button>'
	        		}else{
	        			buttons+='<button class="btn btn-sm btn-primary btn-reallocation" action="budgetReallocation" title="Budget Re-Allocation Details"><i class="fa fa-repeat"></i></button>&nbsp;'+
        				'<button class="btn btn-sm btn-success" action="releaseAmount" title="Release Amount"><i class="fa fa-rupee" ></i></button>'+
        				'&nbsp;<button class="btn btn-sm btn-warning" action="trackReleaseAmount"  title="Track Release Amount"><i class="fa fa-sitemap"></i></button>'
	        		}
	        		return buttons;
	        	}
	        },
		       {"targets":1,"data":"block_name"},
		       {"targets":2,"data":"school_name"},
			   {"targets":3,"data":"school_id"},
			   {"targets":4,"data":"year_desc"},
			   {"targets":5,"data":"work_desc"},
			   {"targets":6,"data":"no_wrksanc"},
			   {"targets":7,"data":"balance_units"},
			   {"targets":8,"data":"cost_pu"},
			   {"targets":9,"data":"sanc_amt"},
			   {"targets":10,"data":"balance_amount"},
			   {"targets":11,"data":"released_amount"},
			   {"targets":12,"data":"np_id"},
			   {"targets":13,"data":"scheme"},
			   {"targets":14,"data":"catg_desc"},
     ],
	"sAjaxSource":"./projectmonitoring/getDistrictLevelProjectDetails/All/All/All/All.do",
	});
	


	function objectifyForm(formArray) {
		var returnArray = {};
		for (var i = 0; i < formArray.length; i++) {
			returnArray[formArray[i]['name']] = formArray[i]['value'];
		}
		return returnArray;
	}
	

	   /*Validation starts */
	var releaseAmountFormValidate = $("#releaseAmountForm");
	releaseAmountFormValidate.validate({
		rules: {	        	 
			amount_released:{
				required: true,
				onlyNumber: true
			},
			date_of_rel:{
				required: true,
				customdate: true
			},
		},
		messages: {
			//Release Amount error messages
			amount_released:{
				required: "Please enter release amount",
				onlyNumber: onlyNumberMsg,
			},
			date_of_rel:{
				required: "Please select release date",
				customdate:dateFormatAllowMsg ,
			},
		},
		highlight: function (input) {
			$(input).parents('.validation_error_msg').addClass('error');
		},
		unhighlight: function (input) {
			$(input).parents('.validation_error_msg').removeClass('error');
		},
		errorPlacement: function (error, element) {
			$(element).parents('.form-group').append(error);
		}
	});

	/**Validating Release Amount Forms **/
	$('#release_amount').keypress(function(){
		$('#release_amount').valid();
	});
	
	$('#release_date').change(function(){
		$('#release_date').valid();
	});

	/*Validation ends*/
	
	$("#releaseAmountModalSubmit").click(function() {
		var isReleaseAmountFormValidate = releaseAmountFormValidate.valid();
		if(isReleaseAmountFormValidate != false){
			saveReleaseAmountDetails();
		}
		
    });
	
	function saveReleaseAmountDetails(){
		$("#releaseAmountModalSubmit").button('loading');
		$.LoadingOverlay("show");
		//myApp.showPleaseWait();
		var postData = $("#releaseAmountForm").serializeArray();
		var formURL = "./projectmonitoring/districtMonitoringAmountSanctionPhaseWiseDetails.do";
		$.ajax({
			 url: formURL,
			 type: "POST",
			 async:true,
			 beforeSend: function(xhr) {
	                xhr.setRequestHeader(header, token);
	  		},
	  		data: JSON.stringify(objectifyForm(postData)),
	        contentType:"application/json",
	        dataType:"JSON",
			 success: function(data) {
				// myApp.hidePleaseWait();
				 $.LoadingOverlay("hide");
				 $("#releaseAmountModalSubmit").button('reset');
	                if (data.status) {
	                	swal(data.msg)
	                	$("#releasedAmountModal").modal('hide');
	                	getProjectDetails();
		              }else{
		            	  $("#releasedAmountModal").modal('hide');
		            	  swal(data.msg);
		              }
	            },
			 error:function(jqXHR, status, error){
				 $("#releaseAmountModalSubmit").button('reset');
				 $.LoadingOverlay("hide");
				// myApp.hidePleaseWait();
			 	swal(error);
			 }
	});
	}
	
	//Filter Functions
	
	$('#blockfilter').on('change', function(){
		finyear = $('#finyear').val();
		block= $('#blockfilter').val();
		scheme= $('#schemeFilter').val();
		subcategory = $('#subcategoryfilter').val();
		isDisabled = console.log($('#blockfilter').prop('disabled'))
		tblDistrictSchoolMonitoringDetails.ajax.url("./projectmonitoring/getDistrictLevelProjectDetails/"+block+"/"+finyear+"/"+scheme+"/"+subcategory+".do").load();
	})
	
	$('#finyear').on('change', function(){
		finyear = $('#finyear').val();
		block= $('#blockfilter').val();
		scheme= $('#schemeFilter').val();
		subcategory = $('#subcategoryfilter').val();
		tblDistrictSchoolMonitoringDetails.ajax.url("./projectmonitoring/getDistrictLevelProjectDetails/"+block+"/"+finyear+"/"+scheme+"/"+subcategory+".do").load();
	})

	$('#schemeFilter').on('change', function() {
		finyear = $('#finyear').val();
		block= $('#blockfilter').val();
		scheme= $('#schemeFilter').val();
		subcategory = $('#subcategoryfilter').val();
		tblDistrictSchoolMonitoringDetails.ajax.url("./projectmonitoring/getDistrictLevelProjectDetails/"+block+"/"+finyear+"/"+scheme+"/"+subcategory+".do").load();
	})
	
	$('#subcategoryfilter').on('change', function() {
		finyear = $('#finyear').val();
		block= $('#blockfilter').val();
		scheme= $('#schemeFilter').val();
		subcategory = $('#subcategoryfilter').val();
		tblDistrictSchoolMonitoringDetails.ajax.url("./projectmonitoring/getDistrictLevelProjectDetails/"+block+"/"+finyear+"/"+scheme+"/"+subcategory+".do").load();
	})
	
	 $('#tblDistrictSchoolMonitoringDetails tbody').on( 'click', 'button[action=budgetReallocation]', function () {		
	 var data = tblDistrictSchoolMonitoringDetails.row( $(this).parents('tr') ).data();
	 	$('#reAllocationFundsModal').modal();
	 	$('#hidden_project_id').val(data.project_id);
	    $('#allocate_scheme_name').val(data.scheme);
	    $('#allocate_financial_year').val(data.year_desc);
		$('#allocated_block').val(data.block_name);
		$('#allocated_category').val(data.catg_desc);
		$('#allocated_sub_category').val(data.work_desc);
		$('#mapping_school_id0').val(data.school_id);
		$('#allocated_school_name').val(data.school_name);
		$('#allocated_total_units0').val(data.balance_units);
		$('#fixed_units').val(data.balance_units);
		$('#allocated_cost').val(data.unit_cost);
		$('#total_approved_amount0').val(data.sanc_amt);
		$('#totalunits_hidden').val(data.balance_units);
		$('#project_id_hidden').val(data.np_id);
		$('#tot_bal0').html(data.balance_units);
		$('#rem_bal0').html(parseInt($('#totalunits_hidden').val())-parseInt($('#fixed_units').val()));

		$('#school_id_hidden').val(data.school_id);
		$('#work_id_hidden').val(data.work_id);
		$('#block_id_hidden0').val(data.block_id);
		$('#scheme_id_hidden').val(data.scheme_id);
		$('#finyr_id_hidden').val(data.finyr_id);
		
	})
	
	$('#tblDistrictSchoolMonitoringDetails tbody').on( 'click', 'button[action=releaseAmount]', function () {		
		 var data = tblDistrictSchoolMonitoringDetails.row( $(this).parents('tr') ).data();
		 	$('#releasedAmountModal').modal();
		 	$('#rel_hidden_project_id').val(data.np_id);
		 	$('#rel_app_amount').val(data.balance_amount);
		 	$('#total_sanc_amt').html(":&nbsp;"+data.sanc_amt + "&nbsp;(in lakhs)");
		 	$('#total_balance_amt').html(":&nbsp;"+data.balance_amount + "&nbsp;(in lakhs)");
		 	$('#total_released_amt').html(":&nbsp;"+data.released_amount +"&nbsp;(in lakhs)");
		 	
		 	if(data.balance_amount==0.00){
		 		$('#release_amount').attr('readonly', true);
		 		$('#release_amount').val('Insufficient balance amount');
		 		$('#release_amount').css('color', 'red');
		 		$('#total_balance_amt').css('color', 'red');
		 	}else{
		 		$('#release_amount').attr('readonly', false);
		 	}
		 	
		 	var project_id=data.np_id;
		 	
			// for phase count
	    	$.ajax({
				 url: "./projectmonitoring/getTotalPhaseCountForReleaseAmount/"+project_id+".do",
				 type: "GET",	
				 async:false,
				 beforeSend: function(xhr) {
		               xhr.setRequestHeader(header, token);
				},
				success: function(result)
				{
					if(result.status == false){
						 swal(result.msg);
					}else{
						var data = result.data;
						console.log("allocated phases are  "+data);
						if(data==4){
							$('#releaseAmountModalSubmit').attr('disabled',true);
							swal(result.msg);
						}
					 }
				},
			    failure: function () {
			        swal("Failed!");
			    }
				 
			});
	})

	
	$('#tblDistrictSchoolMonitoringDetails tbody').on( 'click', 'button[action=trackReleaseAmount]', function () {		
		 var data = tblDistrictSchoolMonitoringDetails.row( $(this).parents('tr') ).data();
			var projectId = data.np_id;
			getReleaseAmountDetailsPhasewise(projectId)
	 });

	
	
	/********************************************************
	 * function for (table-row) track on Track button
	 * @author Raghu
	 *******************************************************/
	function getReleaseAmountDetailsPhasewise(projectId){
		$('#loading').show();
		$.ajax({
			url: "./projectmonitoring/getReleaseAmountTrackingDetailsPhasewise/"+projectId+".do",
			type: "GET",	
			beforeSend: function(xhr) {
				xhr.setRequestHeader(header, token);
			},
			success:function(result) {
				$('#loading').fadeOut(1000);
				if(result.status==true){
					var data = result.data;
					 var html;
					 if(data.length!=0){
					 $('#releaseAmountTrackingModal').modal();
					 for(var i=0;i<data.length;i++){
						 var release_date=(data[i].date_of_rel!=null)?data[i].date_of_rel:"NA";
						// var approver_remark=(data[i].approver_remark!=null)?data[i].approver_remark:"NA";
								
										html='<div class="row">'+
		                             '<div class="col-md-2 col-md-offset-1">'+
		                                '<div class="timeline-date">'+release_date+''+	                                                 	                                                  
		                                '</div>'+
		                                '</div>'+
		                                '<div class="col-md-1">'+
		                                    '<div class="timeline2-icon bg-indigo">'+
		                                        '<i class="fa fa-rupee"></i>'+
		                                    '</div>'+
		                                '</div>'+
		                                '<div class="col-md-7">'+
		                                    '<div class="timeline-hover">'+
		                                        '<div class="timeline-heading bg-indigo">'+
		                                            '<div class="timeline-arrow arrow-indigo"></div>'+data[i].school_name+'('+data[i].school_id+')'+                                                      
		                                            '<span class="label label-override pull-right">Phase : '+data[i].phase+'</span>'+
		                                        '</div>'+
		                                        '<div class="timeline-content">Released Amount : '+data[i].amount_released+'</div>'+		                                                        
		                                    '</div>'+
		                                '</div>'+
		                        '</div>';
							    $('#timelineDynamic').append(html);
								}
				   }else{
						 swal('No Release Amount Found');
				}
				}else{
					swal(data.msg)
				}
			},
			error:function(jqXHR, status, error){
				$('#loading').fadeOut(1000);
				swal(error);
			}
		});
	}
	
	/*Dynamically generating Budget Allocation fields*/

	
	var max_fields      = 3; //maximum fields allowed
    var wrapper         = $('#budget_allocation_wrap'); //Fields wrapper
    var get_allocation  = $("#get_allocation"); //Add button ID
    var total_allocated = [];
  
	
	    $(get_allocation).click(function(e){ 
	    	$('#remove').attr('disabled',false);
	        e.preventDefault();
	        if(x < max_fields){ //max fields allowed check
	            x++; //fields increment
	            $(wrapper).append('<div id = new_allocation_'+x+'><fieldset id = "budgetalloc'+x+'" class="col-sm-12" style="margin-bottom: 10px">'
		           +'<legend class="pacs-header">Budget Allocation '+x+'</legend>'
		           +'<div class="row clearfix">'
						+'<div class="col-sm-6">'
							+'<div class="form-group">'
								+'<label class="control-label col-sm-4" for="">School Code:</label>'
								+'<div class="col-sm-8">'
								+'<div style="display: flex">'
									+'<input type="text" class="form-control map_school" id="mapping_school_id'+x+'" name="mapping_school'+x+'">'
									+'<button type="button" id = "get'+x+'" class="btn btn-primary btn-sm">Get</button>'
								+'</div>'
								+'<small class="validation_error_msg"></small>'
								+'</div>'
							+'</div>'
						+'</div>'
						+'<div class="col-sm-6">'
							+'<div class="form-group">'
								+'<label class="control-label col-sm-4" for="">School Name:</label>'
								+'<div class="col-sm-8">'
									+'<input type="text" class="form-control" readonly="true" id="school_name'+x+'" name="mapped_school_name'+x+'">'
								+'</div>'
							+'</div>'
						+'</div>'
					+'</div>'
					+'<div class="row clearfix">'
						+'<div class="col-sm-6">'
							+'<div class="form-group">'
								+'<label class="control-label col-sm-4" for="">Block Name:</label>'
								+'<div class="col-sm-8">'
									+'<input class="form-control" readonly="true" id="allocated_block'+x+'">'
								+'</div>'
							+'</div>'
						+'</div>'
					+'</div>'
					+'<div class="row clearfix">'
						+'<div class="col-sm-6">'
							+'<div class="form-group">'
								+'<label class="control-label col-sm-4" for="">Allocated Units:</label>'
								+'<div class="col-sm-8">'
									+'<input type="text" class="form-control bal_units" id="allocated_total_units'+x+'" name="balanced_units'+x+'">'
								+'</div>'
							+'</div>'
						+'</div>'
						+'<div class="col-sm-6">'
							+'<div class="form-group">'
								+'<label class="control-label col-sm-4" for="">Unit Cost:</label>'
								+'<div class="col-sm-8">'
									+'<input type="text" class="form-control" id="allocated_cost'+x+'" readonly>'
								+'</div>'
							+'</div>'
						+'</div>'
					+'</div>'
					+'<div class="row">'
					+'<div class="col-sm-6">'
						+'<div class="form-group">'
							+'<label class="control-label col-sm-4" for="">Approved Amount:</label>'
							+'<div class="col-sm-8">'
								+'<input type="text" class="form-control" id="total_approved_amount'+x+'" name="" readonly  value = 0>'
							+'</div>'
						+'</div>'
					+'</div><input type="hidden" class="form-control" id="balanced_unit'+x+'" name="" readonly>'
					+'<input type="hidden" class="form-control" id="block_id_hidden'+x+'" name="" readonly>'

					+'<input type="hidden" class="form-control" id="scheme_id_hidden'+x+'" name="" readonly>'
					+'<input type="hidden" class="form-control" id="finyr_id_hidden'+x+'" name="" readonly>'
					+'<input type="hidden" class="form-control" id="project_id_hidden'+x+'" name="" readonly>'
				+'</div><span class="mute-text rem-bal">Remaining Balance Units: &nbsp;<label class="rem-bal" id="rem_bal'+x+'">0</label></span>'
		           +'</fieldset><div style="margin-bottom: 10px"></div></div>');
	            
	            y=x-1;
	            setToBalancedUnit(x);
	            
	        }else{
	        	swal("You exceeded maximum(3) school re-allocations.")
	        }
	    });
	    
	       
        $('#remove').on("click", function(e){ 
	        e.preventDefault();
	        $('#new_allocation_'+x).remove();
            x--;
            if(x<=0){
            	$('#remove').attr('disabled',true)
            }
        })
        
	    
	    $('#allocated_total_units0').on("keyup", function(e){
	    	var cost_pu = $('#allocated_cost').val();
	    	var fixed_units = $('#fixed_units').val()
			var tu = $('#allocated_total_units0').val();
			var totalunits = $('#totalunits_hidden').val()
			var newBal = totalunits-tu;
			if(tu>fixed_units || tu < 0){
				swal({
		            title: "Balanced Units Exceeded!!!",
		            text: "Balanced Units cannot be greater than "+fixed_units+"!!",
		            icon: "warning"
		        }, function() {
		        	$('#rem_bal0').html(totalunits-fixed_units);
		        	$('#allocated_total_units0').css("background-color", "#fff")
					$('#allocated_total_units0').val(fixed_units);
		        });
				$('#allocated_total_units0').css("background-color", "#f38080")
				$('#rem_bal0').html("0");
				$('#total_approved_amount0').val((fixed_units * cost_pu).toFixed(2));
			}else{
				$('#rem_bal0').html(newBal);
			}
			$('#balanced_unit').val(newBal);
		});
	    
	    var tu = 0;
	    function setToBalancedUnit(x){
		    var y = x-1;
		    var cost_pu = $('#allocated_cost').val();
		    $('#allocated_cost'+x+'').val(cost_pu);
		    $('#allocated_total_units'+x+'').on("keyup", function(e){ 
		    	y = x-1;
		    	bu = parseInt($('#rem_bal'+y).html());
				totalunits = parseInt($('#allocated_total_units'+x+'').val())
				newBal = parseInt(bu-totalunits);
				
				$('#rem_bal'+x).html(newBal);
				var remaining_units = parseInt($('#rem_bal'+x).html())
				if(x>0 && remaining_units==0){
					$('#get_allocation').attr('disabled', true)
				}else{
			    	$('#get_allocation').attr('disabled', false)
				}
				$('#balanced_unit'+x).val(newBal);
				$('#total_approved_amount'+x+'').val((cost_pu * totalunits).toFixed(2));
				if(newBal<0){
					swal({
						html:true,
			            title: "Warning!",
			            text: "Sorry insufficient balance!<br/><span style='font-size: 10px; color: green'><b>Hint:</b> Enter a value less than Remaining Balance Units</span>",
			            icon: "warning"
			        },function() {
			        	$('#allocated_total_units'+x+'').val('');
			        });
					$('#rem_bal'+x).val('');
					$('#total_approved_amount'+x+'').val('');
					$('#allocated_total_units'+x+'').val('');
					$('#rem_bal'+x).html('Re-enter Allocated Units');
				}
				
			});
		    
		    
		    $('#get'+x+'').on('click', function(){
		    	$('#school_name'+x+'-error').html('');
		    	var school_id = $('#mapping_school_id'+x+'').val();
		    	getSchool(school_id, x);
		    })
		    
	    }
	    
	    function getSchool(school_id, x){
	    	$.ajax({
				 url: "./projectmonitoring/getSchoolReallocateDetails/"+school_id+".do",
				 type: "GET",	
				 async:true,
				 beforeSend: function(xhr) {
		               xhr.setRequestHeader(header, token);
				},
				success: function(result)
	            {
					if(result.status == false){
						 $('#mapping_school_id'+x+'').val('');
						 swal(result.msg);
					}else{
						var data = result.data;
						console.log('getSchool', data)
						$('#school_name'+x+'').val(data[0].school_name);
						$('#allocated_block'+x+'').val(data[0].block_name);
						$('#block_id_hidden'+x).val(data[0].block_id)
					 }
			 },
			    failure: function () {
			        swal("Failed!");
			    }
				 
			});
	    }
	    
	    
	    /*Validation starts */
		var releaseReAllocationFormValidate = $("#reallocateFundsForm");
		releaseReAllocationFormValidate.validate({
			rules: {	        	 
				mapping_school0:{
					required: true,
					onlyNumber: true
				},	        	 
				mapping_school1:{
					required: true,
					onlyNumber: true
				},	        	 
				mapping_school2:{
					required: true,
					onlyNumber: true
				},	        	 
				mapping_school3:{
					required: true,
					onlyNumber: true
				},
				balanced_units0:{
					required: true,
					onlyNumber: true
				},
				balanced_units1:{
					required: true,
					onlyNumber: true
				},
				balanced_units2:{
					required: true,
					onlyNumber: true
				},
				balanced_units3:{
					required: true,
					onlyNumber: true
				},
				mapped_school_name1:{
					required: true,
				},
				mapped_school_name2:{
					required: true,
				},
				mapped_school_name3:{
					required: true,
				},
				
			},
			messages: {
				mapping_school0:{
					required: "Please enter a school code",
					onlyNumber: onlyNumberMsg,
				},
				mapping_school1:{
					required: "Please enter a school code",
					onlyNumber: onlyNumberMsg,
				},
				mapping_school2:{
					required: "Please enter a school code",
					onlyNumber: onlyNumberMsg,
				},
				mapping_school3:{
					required: "Please enter a school code",
					onlyNumber: onlyNumberMsg,
				},
				balanced_units0:{
					required: "Please enter a number",
					onlyNumber: onlyNumberMsg,
				},
				balanced_units1:{
					required: "Please enter a number",
					onlyNumber: onlyNumberMsg,
				},
				balanced_units2:{
					required: "Please enter a number",
					onlyNumber: onlyNumberMsg,
				},
				balanced_units3:{
					required: "Please enter a number",
					onlyNumber: onlyNumberMsg,
				},
				mapped_school_name1:{
					required: "Please click on Get Button",
				},
				mapped_school_name2:{
					required: "Please click on Get Button",
				},
				mapped_school_name3:{
					required: "Please click on Get Button",
				},
			},
			highlight: function (input) {
				$(input).parents('.validation_error_msg').addClass('error');
			},
			unhighlight: function (input) {
				$(input).parents('.validation_error_msg').removeClass('error');
			},
			errorPlacement: function (error, element) {
				$(element).parents('.form-group').append(error);
			}
		});
	    
		/**Validating ReAllocation Form **/
		$('#mapping_school_id0').keypress(function(){
			$('#mapping_school_id0').valid();
		});
		$('#mapping_school_id1').keypress(function(){
			$('#mapping_school_id1').valid();
		});
		$('#mapping_school_id2').keypress(function(){
			$('#mapping_school_id2').valid();
		});
		$('#mapping_school_id3').keypress(function(){
			$('#mapping_school_id3').valid();
		});
		
		$('#allocated_total_units0').keypress(function(){
			$('#allocated_total_units0').valid();
		});
		$('#allocated_total_units1').keypress(function(){
			$('#allocated_total_units1').valid();
		});
		$('#allocated_total_units2').keypress(function(){
			$('#allocated_total_units2').valid();
		});
		$('#allocated_total_units3').keypress(function(){
			$('#allocated_total_units3').valid();
		});
	
		/*Validation ends*/
		
		$("#reAllocationFundsSubmit").click(function() {
			var isReAllocationFormValidate = releaseReAllocationFormValidate.valid();
			if(isReAllocationFormValidate != false){
				saveReAllocationDetails();
			}
			
	    });
	    
	    //Method to submit reallocation details
		var saveReAllocationDetails = function(){
			$("#reAllocationFundsSubmit").button('loading');
			tot_all = 0;
			for(i = 0; i<x+1; i++){
				console.log('i='+i)
				total_allocated[i]=parseInt($('#allocated_total_units'+i).val());
				console.log(total_allocated[i])
				tot_all = tot_all + total_allocated[i];
			}

			console.log("tot_all"+tot_all+"  tot_bal"+$('#tot_bal0').html())
			if(tot_all>$('#tot_bal0').html()){
				swal({
		            title: "Error! Hint: Reallocated units must sum up to Total Balance Units.",
		            icon: "warning"
		        },function() {
		        	$('#allocated_total_units'+x).val('');
		        });
			}else{
				reAllocationFundsDetails(x);
			}
	    }
	    
		function reAllocationFundsDetails(count){
			console.log('count:: '+count)
			var schoolDataFixed = {
				work_id: $('#work_id_hidden').val(),
				scheme_id: $('#scheme_id_hidden').val(),
				finyr_id: $('#finyr_id_hidden').val(),
				project_id: $('#project_id_hidden').val(),
			}
			var postData = [];
			
			for(var i = 0 ; i <= count; i++){
				var schools = {
					school_id: $('#school_id_hidden').val(),
					mapping_school_id: $('#mapping_school_id'+i).val(),
					block_id: $('#block_id_hidden'+i).val(),
					allocated_units: $('#allocated_total_units'+i).val(),
					balance_unit: $('#rem_bal'+i).html(),
					sanc_amt: $('#total_approved_amount'+i).val(),
					steps: i+1
				};
			if(schools.allocated_units != 0){
				postData.push(schools)
			}
			
			}
			
			var fData = {
					work_id: $('#work_id_hidden').val(),
					scheme_id: $('#scheme_id_hidden').val(),
					finyr_id: $('#finyr_id_hidden').val(),
					project_id: $('#project_id_hidden').val(),
					"schools" : postData
			}
			
			console.log(fData);
			var formURL = "./projectmonitoring/saveSchoolReAllocationDetails.do";
			/*myApp.showPleaseWait();*/
			$.ajax({
				 url: formURL,
				 type: "POST",
				 beforeSend: function(xhr) {
		                xhr.setRequestHeader(header, token);
		  		 },
		         data:JSON.stringify(fData),
		         contentType:"application/json",
		         dataType:"JSON",
				 success: function(data) {
					 $("#reAllocationFundsSubmit").button('reset');
		                if (data.status) {
		                	swal(data.msg)
		                	$("#reAllocationFundsModal").modal('hide');
		                	getProjectDetails();
		                	
			              }else{
			            	  swal(data.msg);
			              }
		            },
				 error:function(data){
					 $("#reAllocationFundsSubmit").button('reset');
				 	swal("Failed");
				 }
			});
		}
		
		//export-excel
		$('#district-allocation-excel').click(function(){
			finyear = $('#finyear').val();
			block= $('#blockfilter').val();
			scheme= $('#schemeFilter').val();
			subcategory = $('#subcategoryfilter').val();
			$.LoadingOverlay("show");
			$('#district-allocation-excel').attr('href','./projectmonitoring/exportProjectsToExcelForDistrict/'+block+'/'+finyear+'/'+scheme+'/'+subcategory+'.do');
			$.LoadingOverlay("hide");
			
		})
});

