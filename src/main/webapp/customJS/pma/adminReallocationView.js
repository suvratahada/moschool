/**
 * 
 * Custom Js For Admin Reallocation View
 * Created On:28-Dec-2018
 * Created By:Raghu
 * 
 */

$('document').ready(function(){
	
	if($('#blockFilter').prop('disabled', true)){
		$('#blockFilter').attr('title', 'Please select a district');
	}
	
	/*This function will retrieve all the fin year details */
	var getFinancialYear = function getFinancialYear(){
		$.ajax({
			url:'./projectmonitoring/getFinancialYear.do',
			type: "POST",
			async:false,
			beforeSend: function(xhr) {
	                xhr.setRequestHeader(header, token);
			 		},
			success:function(result){
				if(result.status == false){
					 swal(result.msg);
				}else{
					var data = result.data;
					console.log(data)
					var option = '<option   value="All">--Select Financial Year--</option>';
					for(var i = 0;i<data.length;i++){
						option += '<option value="'+data[i].year_id+'">'+data[i].year_desc+'</option>';
					}
					// for fin year filter
					$('#finyearFilter').val('');
					$('#finyearFilter').empty();
					$('#finyearFilter').append(option);
					$("#finyearFilter").prop("selectedIndex", 0);
				}
			}
			
		});
	}
	/*call to get all fin year details*/
	getFinancialYear();
	
	var getBlockDetails = function getBlockDetails(districtId) {
		console.log("inside method", districtId)
		$.ajax({
			 url: "./mastersetup/getBlockDetailsByDistrictID/"+districtId+".do",
			 type: "POST",	
			 async:false,
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
		
			 success:function(result) {
					if(result.status == false){
						 swal(result.msg);
					}else{
						var data = result.data;
						console.log("getBlockDetailsByDistrictID",data);
						 var option = '<option value="All">--Select Block--</option>';
						 for(var i = 0;i<data.length;i++){
								option += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
							}
							$('#blockFilter').val('');
							$('#blockFilter').empty();
							$('#blockFilter').append(option);
							$("#blockFilter").prop("selectedIndex", 0);
					 }
			 }
		})
	}
	
	
	var DistrictDetails = function DistrictDetails() {
		$.ajax({
			 url: "./mastersetup/getDistrictDetails.do",
			 type: "POST",	
			 async:false,
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			success: function(result)
            {
				if(result.status == false){
					 swal(result.msg);
				}else{
					var data = result.data;
					//console.log(data)
					 var option = '<option value="All">--Select District--</option>';
					 for(var i = 0;i<data.length;i++){
							option += '<option value="'+data[i].district_id+'">'+data[i].district_name+'</option>';
						}
						$('#districtFilter').val('');
						$('#districtFilter').empty();
						$('#districtFilter').append(option);
						$("#districtFilter").prop("selectedIndex", 0);
				 }
		 },
		    failure: function () {
		        swal("Failed!");
		    }
			 
		});
	}
	// call to get district details
	DistrictDetails();
	
	/*This function will retrieve all the scheme*/
	var getSchemeDetails = function getSchemeDetails(){
		$.ajax({
			url:'./projectmonitoring/getSchemeDetails.do',
			type: "POST",
			async:false,
			beforeSend: function(xhr) {
	                xhr.setRequestHeader(header, token);
			 		},
			success:function(result){
				if(result.status == false){
					 swal(result.msg);
				}else{
					var data = result.data;
					//console.log(data)
					var option = '<option value="All">--Select Scheme--</option>';//<option value="-1" selected disabled>-- Select --</option>
					for(var i = 0;i<data.length;i++){
						option += '<option value="'+data[i].scheme_id+'">'+data[i].scheme+'</option>';
					}
					// for scheme filter
					$('#schemeFilter').val('');
					$('#schemeFilter').empty();
					$('#schemeFilter').append(option);
					$("#schemeFilter").prop("selectedIndex", 0);
				}
			}
			
		});
	}
	/*call to get all scheme details*/
	getSchemeDetails();
	
	var districtAll="All";
	var blockAll="All";
	var schemeAll="All";
	var finYearAll="All";	
	var adminTotalReallocationViewTable = $('#adminLevelTotalReallocationViewTable').DataTable({
	        "scrollX": true,
			"bProcessing" : false,
			//"lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
			"pageLength": 10,
			"bProcessing": true,
			"bServerSide": true, 
			"bStateSave":false,
			"bPaginate": true,
			"bLengthChange": false,
			"bFilter": true,
			"bSort": true,
			"bInfo": true,
			"cache":false,
			"bAutoWidth": false,
			"bRetrieve": true,
			"bDestroy":false,
			"searching": false,
			"language": 
				{          
					"processing": "Fetching Projects...",
				},
			"aoColumns":[
			   //{"className": "dt-center", "targets": "_all"}, 
			   {"targets":0 ,"data":"block_name"},
			   {"targets":1 ,"data":"from_school_id"},
			   {"targets":2 ,"data":"from_school"},
			   {"targets":3 ,"data":"to_school_id"},
			   {"targets":4 ,"data":"to_school"},
			   {"targets":5 ,"data":"year_desc"},
			   {"targets":6 ,"data":"work_desc"},
			   {"targets":7 ,"data":"allocated_units"},
			   {"targets":8 ,"data":"unit_cost"},
			   {"targets":9 ,"data":"sanc_amt"},
			   {"targets":10 ,"data":"balance_amount"},
			   {"targets":11 ,"data":"released_amount"},
			   {"targets":12 ,"data":"project_id"},
			   {"targets":13 ,"data":"scheme"},
			   {"targets":14 ,"data":"catg_desc"},
			   ],
			   "sAjaxSource":"./projectmonitoring/getAdminTotalReallocationDetails/"+districtAll+"/"+blockAll+"/"+finYearAll+"/"+schemeAll+".do",
	});

	var isDisabled = $('#blockFilter').prop('disabled');
	
	//Filter Functions
	var districtId = '';
	
	$('#districtFilter').on('change', function(){
		$("#blockFilter").prop('disabled', false);
		$('#blockFilter').attr('title', 'Block');
		var isDisabled = $('#blockFilter').prop('disabled');
		district = $('#districtFilter').val();
		/*if(isDisabled){
			block = 'All';
		}else{
			block = $('#blockFilter').val();
		}*/
		block = 'All';
		console.log("block is:: "+isDisabled+" "+block)
		finyearFilter = $('#finyearFilter').val();
		scheme = $('#schemeFilter').val();
		
		console.log('districtId', district)
		getBlockDetails(district);
		adminTotalReallocationViewTable.ajax.url("./projectmonitoring/getAdminTotalReallocationDetails/"+district+"/"+block+"/"+finyearFilter+"/"+scheme+".do").load();
	})
	isDisabled = console.log($('#blockFilter').prop('disabled'))
	$('#blockFilter').on('change', function(){
		var isDisabled = $('#blockFilter').prop('disabled');
		district = $('#districtFilter').val();
		if(isDisabled){
			block = 'All';
		}else{
			block = $('#blockFilter').val();
		}
		finyearFilter = $('#finyearFilter').val();
		scheme = $('#schemeFilter').val();
		
		console.log('block', block)
		adminTotalReallocationViewTable.ajax.url("./projectmonitoring/getAdminTotalReallocationDetails/"+district+"/"+block+"/"+finyearFilter+"/"+scheme+".do").load();
	})
	
	$('#finyearFilter').on('change', function(){
		finyearFilter = $('#finyearFilter').val();
		var isDisabled = $('#blockFilter').prop('disabled');
		district = $('#districtFilter').val();
		if(isDisabled){
			block = 'All';
		}else{
			block = $('#blockFilter').val();
		}
		finyearFilter = $('#finyearFilter').val();
		scheme = $('#schemeFilter').val();
		console.log(isDisabled+" "+block)
		console.log('finyearFilter', finyearFilter)
		adminTotalReallocationViewTable.ajax.url("./projectmonitoring/getAdminTotalReallocationDetails/"+district+"/"+block+"/"+finyearFilter+"/"+scheme+".do").load();
	})

	$('#schemeFilter').on('change', function() {
		scheme = $('#schemeFilter').val();
		console.log('scheme', scheme)
		var isDisabled = $('#blockFilter').prop('disabled');
		district = $('#districtFilter').val();
		if(isDisabled){
			block = 'All';
		}else{
			block = $('#blockFilter').val();
		}
		finyearFilter = $('#finyearFilter').val();
		scheme = $('#schemeFilter').val();
		
		adminTotalReallocationViewTable.ajax.url("./projectmonitoring/getAdminTotalReallocationDetails/"+district+"/"+block+"/"+finyearFilter+"/"+scheme+".do").load();
	})
	
	
	$('#admin-reallocation-excel').click(function(){
		scheme = $('#schemeFilter').val();
		district = $('#districtFilter').val();
		var isDisabled = $('#blockFilter').prop('disabled');
		if(isDisabled){
			block = 'All';
		}else{
			block = $('#blockFilter').val();
		}
		finyear = $('#finyearFilter').val();
		$.LoadingOverlay("show");
		$('#admin-reallocation-excel').attr('href','./projectmonitoring/exportReAllocationProjectsToExcelForAdmin/'+district+"/"+block+"/"+finyear+"/"+scheme+'.do');
		$.LoadingOverlay("hide");
		
	})
	
});