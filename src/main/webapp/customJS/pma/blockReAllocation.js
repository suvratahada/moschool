$(document).ready(function(){
	
	$("#releaseAmountTrackingModal").on("hidden.bs.modal", function(){
		$('#timelineDynamic').empty();
	});
	
	
	/*This function will retrieve all the fin year */
	var getFinancialYear = function getFinancialYear(){
		$.ajax({
			url:'./projectmonitoring/getFinancialYear.do',
			type: "POST",
			async:false,
			beforeSend: function(xhr) {
	                xhr.setRequestHeader(header, token);
			 		},
			success:function(result){
				if(result.status == false){
					 swal(result.msg);
				}else{
					var data = result.data;
					console.log(data)
					var option = '<option   value="All">--Select Financial Year--</option>';
					for(var i = 0;i<data.length;i++){
						option += '<option value="'+data[i].year_id+'">'+data[i].year_desc+'</option>';
					}
					$('#finyr_id').val('');
					$('#finyr_id').empty();
					$('#finyr_id').append(option);
					$("#finyr_id").prop("selectedIndex", 0);
					
					// for fin year filter
					$('#finyear').val('');
					$('#finyear').empty();
					$('#finyear').append(option);
					$("#finyear").prop("selectedIndex", 0);
				}
			}
			
		});
	}
	/*call to get all fin year details*/
	getFinancialYear();
	
	//Filter for sub category
	/*This function will retrieve all the sub categories from master_workcatg table*/
	var getSubCategoryDetails = function getSubCategoryDetails(){
		$.ajax({
			url:'./projectmonitoring/getSubCategoryDetailsForFilter.do',
			type: "POST",
			async:false,
			beforeSend: function(xhr) {
	                xhr.setRequestHeader(header, token);
			 		},
			success:function(result){
				if(result.status == false){
					 swal(result.msg);
				}else{
					var data = result.data;
					//console.log("data"+data)
					var option = '<option selected value="All">--Select SubCategory--</option>';
					for(var i = 0;i<data.length;i++){
						option += '<option value="'+data[i].work_desc_id+'">'+data[i].work_desc+'</option>';
					}

					// for sub category filter
					$('#subcategoryfilter').val('');
					$('#subcategoryfilter').empty();
					$('#subcategoryfilter').append(option);
					$("#subcategoryfilter").prop("selectedIndex", 0)
				}
			}
			
		});
	}
	
	getSubCategoryDetails();
	
	
	var tblBlockSchoolReAllocationDetails = $('#tblBlockSchoolReAllocationDetails').DataTable({
		dom: 'Bfrtip',
        buttons: [
				{
                      extend: 'excelHtml5',
                      text: '<button class="btn-primary export-excel-btn"><i class="fa fa-file-excel-o"></i></button>',
                      titleAttr: 'export to excel',
                      title:'Allocation Details Report',
                      filename: 'block_reallocation_report',
                      exportOptions: {
                    	  columns: [1,2,3,4,5,6,7,8,9,10,11,12],
            	   		}
				},
					
	        ],
		 "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
		 "pageLength": 10,	
		 "bFilter": true,
		 "bLengthChange": false,
		 "scrollX": true,
		 "paging": true,
		 "columnDefs":[
		   {"className": "dt-center", "targets": "_all"},  
		   {"targets":0 , "data": null,
	        	"defaultContent":'&nbsp;<button class="btn btn-sm btn-success" action="releaseAmountTrack" title="Tracking Status"><i class="fa fa-sitemap" ></i></button>'
		   },
		   {"targets":1,"data":"project_id"},
		   {"targets":2,"data":"scheme"},
		   {"targets":3,"data":"to_school_id"},
		   {"targets":4,"data":"to_school"},
		   {"targets":5,"data":"catg_desc"},
		   {"targets":6,"data":"work_desc"},
		   {"targets":7,"data":"year_desc"},
		   {"targets":8,"data":"allocated_units"},
		   {"targets":9,"data":"unit_cost"},
		   {"targets":10,"data":"sanc_amt"},
		   {"targets":11,"data":"balance_amount"},
		   {"targets":12,"data":"released_amount"},
     ]
	});

	
	
	/*This function will retrieve all the Projects from master_project table*/
	var getProjectDetails = function getProjectDetails(){
		$('#loading').show();
		finyear=$('#finyear').val();
		subcategory=$('#subcategoryfilter').val();
		console.log("finyear= "+finyear)
		console.log("subcategory="+subcategory)
		$.ajax({
			url:"./projectmonitoring/getBlockLevelReAllocatedProjectDetails/"+finyear+"/"+subcategory+".do",
			type: "POST",
			async:false,
			beforeSend: function(xhr) {
	                xhr.setRequestHeader(header, token);
			 		},
			success:function(result){
				$('#loading').hide();
				$.LoadingOverlay("hide");
				if(result.status == false){
					 swal(result.msg);
				}else{
					var data = result.data;
					 tblBlockSchoolReAllocationDetails.clear();
					 tblBlockSchoolReAllocationDetails.rows.add(result.data).draw();
				}
			}
			
		});
	}
	/*call to get all the project details*/
	getProjectDetails();
	
	// on filters on change functions
	$('#finyear').on('change', function(){
		$.LoadingOverlay("show",{
			size : 10
		});
		getProjectDetails();
	})
	
	$('#subcategoryfilter').on('change', function() {
		$.LoadingOverlay("show",{
			size : 10
		});
		getProjectDetails();
	})

	/********************************************************
	 * function for (table-row) track on Track button
	 * @author Raghu
	 *******************************************************/
	function getReleaseAmountDetailsPhasewise(projectId){
		$('#loading').show();
		$.ajax({
			url: "./projectmonitoring/getReleaseAmountTrackingDetailsPhasewiseReallocation/"+projectId+".do",
			type: "GET",	
			beforeSend: function(xhr) {
				xhr.setRequestHeader(header, token);
			},
			success:function(result) {
				$('#loading').fadeOut(1000);
				if(result.status==true){
					var data = result.data;
					console.log(data)
					 var html;
					 if(data.length!=0){
					 $('#releaseAmountTrackingModal').modal();
					 for(var i=0;i<data.length;i++){
						 var release_date=(data[i].date_of_rel!=null)?data[i].date_of_rel:"NA";
						// var approver_remark=(data[i].approver_remark!=null)?data[i].approver_remark:"NA";
								
										html='<div class="row">'+
		                             '<div class="col-md-2 col-md-offset-1">'+
		                                '<div class="timeline-date">'+release_date+''+	                                                 	                                                  
		                                '</div>'+
		                                '</div>'+
		                                '<div class="col-md-1">'+
		                                    '<div class="timeline2-icon bg-indigo">'+
		                                        '<i class="fa fa-rupee"></i>'+
		                                    '</div>'+
		                                '</div>'+
		                                '<div class="col-md-7">'+
		                                    '<div class="timeline-hover">'+
		                                        '<div class="timeline-heading bg-indigo">'+
		                                            '<div class="timeline-arrow arrow-indigo"></div>'+data[i].school_name+'('+data[i].school_id+')'+                                                      
		                                            '<span class="label label-override pull-right m-t-10">Phase : '+data[i].phase+'</span>'+
		                                        '</div>'+
		                                        '<div class="timeline-content">Released Amount : '+data[i].amount_released+'</div>'+		                                                        
		                                    '</div>'+
		                                '</div>'+
		                        '</div>';
							    $('#timelineDynamic').append(html);
								}
				   }else{
						 swal('No Release Amount Found');
				}
				}else{
					swal(data.msg)
				}
			},
			error:function(jqXHR, status, error){
				$('#loading').fadeOut(1000);
				swal(error);
			}
		});
	}
	
	$('#tblBlockSchoolReAllocationDetails tbody').on( 'click', 'button[action=releaseAmountTrack]', function () {		
		 var data = tblBlockSchoolReAllocationDetails.row( $(this).parents('tr') ).data();
		 	var projectId = data.project_id;
			getReleaseAmountDetailsPhasewise(projectId);
	})
	
	
});