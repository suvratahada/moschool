$('document').ready(function(){
	// for date picker
	$('#release_date').datepicker({
		autoclose:true,
		format:"dd-mm-yyyy",
		todayHighlight: true,
		autoclose: true,
		endDate: '+0d',
	});
	// date picker ends
	
	// modal loader function
	/*	var myApp;
	myApp = myApp || (function () {
	    return {
	        showPleaseWait: function() {
	            $("#pleaseWaitDialog").modal();
	        },
	        hidePleaseWait: function () {
	        	$("#pleaseWaitDialog").modal('hide');
	        },

	    };
	})();*/
	// modal loader function ends
	
	/*  Modal reset Starts   */
	$("#releasedAmountModal").on("hidden.bs.modal", function(){
		 $('#releaseAmountForm')[0].reset();
		 $("#releaseAmountModalSubmit").button('reset');
		 var validator = $( "#releaseAmountForm" ).validate();
		 validator.resetForm();
		 $('#releaseAmountModalSubmit').attr('disabled',false);
	});
	$("#releaseAmountTrackingModal").on("hidden.bs.modal", function(){
			$('#timelineDynamic').empty();
	});
	
	/*  Modal reset Ends   */
	

	/*Validation Methods Starts here*/
	var onlyCharAllowMsg = "Only characters are allowed";
	var dateFormatAllowMsg = "date format should be dd-mm-yyyy";
	var onlyNumberMsg = "Only numeric values allowed";

	$.validator.addMethod('letter', function (value, element) {
		return value.match(/^[a-zA-Z ]+$/);
	});

	$.validator.addMethod('customdate', function (value, element) {		
		return value.match(/^\d\d?-\d\d?-\d\d\d\d$/);// DD-MM-YYYY
	});
	$.validator.addMethod('onlyNumber', function (value, element) {
		return value.match(/^[0-9.]*$/);
	});
	
	$.validator.addMethod('validateDescription', function (value, element) {
		return value.match(/^[A-Za-z0-9 !@%&*(){}+=';:/_-]*$/);
	});
	
	/*Validation Methods Ends here*/
	
	/*This function will retrieve all the block from master_block table*/
	var getBlockDetailsByDistrict = function getBlockDetailsByDistrict(){
		$.ajax({
			url:'./projectmonitoring/getBlockDetails.do',
			type: "POST",
			async:false,
			beforeSend: function(xhr) {
	                xhr.setRequestHeader(header, token);
			 		},
			success:function(result){
				if(result.status == false){
					 swal(result.msg);
				}else{
					var data = result.data;
					console.log(data)
					var option = '<option selected  value="All">--Select Block Name--</option>';
					for(var i = 0;i<data.length;i++){
						option += '<option value="'+data[i].block_id+'">'+data[i].block_name+'</option>';
					}
					$('#blockId').val('');
					$('#blockId').empty();
					$('#blockId').append(option);
					$("#blockId").prop("selectedIndex", 0);
					
					$('#blockfilter').val('');
					$('#blockfilter').empty();
					$('#blockfilter').append(option);
					$("#blockfilter").prop("selectedIndex", 0);
					
					
				}
			}
			
		});
	}
	/*call to get all the block details*/
	getBlockDetailsByDistrict();
	
	/*This function will retrieve all the fin year */
	var getFinancialYear = function getFinancialYear(){
		$.ajax({
			url:'./projectmonitoring/getFinancialYear.do',
			type: "POST",
			async:false,
			beforeSend: function(xhr) {
	                xhr.setRequestHeader(header, token);
			 		},
			success:function(result){
				if(result.status == false){
					 swal(result.msg);
				}else{
					var data = result.data;
					console.log(data)
					var option = '<option   value="All">--Select Financial Year--</option>';
					for(var i = 0;i<data.length;i++){
						option += '<option value="'+data[i].year_id+'">'+data[i].year_desc+'</option>';
					}
					$('#finyr_id').val('');
					$('#finyr_id').empty();
					$('#finyr_id').append(option);
					$("#finyr_id").prop("selectedIndex", 0);
					
					// for fin year filter
					$('#finyear').val('');
					$('#finyear').empty();
					$('#finyear').append(option);
					$("#finyear").prop("selectedIndex", 0);
				}
			}
			
		});
	}
	/*call to get all fin year details*/
	getFinancialYear();
	
	
	/*This function will retrieve all the scheme*/
	var getSchemeDetails = function getSchemeDetails(){
		$.ajax({
			url:'./projectmonitoring/getSchemeDetails.do',
			type: "POST",
			async:false,
			beforeSend: function(xhr) {
	                xhr.setRequestHeader(header, token);
			 		},
			success:function(result){
				if(result.status == false){
					 swal(result.msg);
				}else{
					var data = result.data;
					//console.log(data)
					var option = '<option value="All">--Select Scheme--</option>';//<option value="-1" selected disabled>-- Select --</option>
					for(var i = 0;i<data.length;i++){
						option += '<option value="'+data[i].scheme_id+'">'+data[i].scheme+'</option>';
					}
					$('#scheme_id').val('');
					$('#scheme_id').empty();
					$('#scheme_id').append(option);
					$("#scheme_id").prop("selectedIndex", 0);
					
					// for scheme filter
					$('#schemeFilter').val('');
					$('#schemeFilter').empty();
					$('#schemeFilter').append(option);
					$("#schemeFilter").prop("selectedIndex", 0);
				}
			}
			
		});
	}
	/*call to get all scheme details*/
	getSchemeDetails();
	
	//Filter for sub category
	/*This function will retrieve all the sub categories from master_workcatg table*/
	var getSubCategoryDetails = function getSubCategoryDetails(){
		$.ajax({
			url:'./projectmonitoring/getSubCategoryDetailsForFilter.do',
			type: "POST",
			async:false,
			beforeSend: function(xhr) {
	                xhr.setRequestHeader(header, token);
			 		},
			success:function(result){
				if(result.status == false){
					 swal(result.msg);
				}else{
					var data = result.data;
					//console.log(data)
					var option = '<option selected value="All">--Select SubCategory--</option>';
					for(var i = 0;i<data.length;i++){
						option += '<option value="'+data[i].work_desc_id+'">'+data[i].work_desc+'</option>';
					}

					// for sub category filter
					$('#subcategoryfilter').val('');
					$('#subcategoryfilter').empty();
					$('#subcategoryfilter').append(option);
					$("#subcategoryfilter").prop("selectedIndex", 0)
				}
			}
			
		});
	}
	/*call to get all subcategory details*/
	getSubCategoryDetails();
	
	$('#release_amount').on('keyup', function(){
		var r = $('#release_amount').val();
		var a = $('#rel_app_amount').val();
		if(parseFloat(r) > parseFloat(a)){
			swal("Release amount cannot be greater than Approved amount!");
			$('#release_amount').val('');
		}
	})
	
	var tblDistrictSchoolMonitoringDetails = $('#tblDistrictSchoolMonitoringDetails').DataTable({
	        "scrollX": true,
			"bProcessing" : false,
			//"lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
			"pageLength": 10,
			"bProcessing": true,
			"bServerSide": true, 
			"bStateSave":false,
			"bPaginate": true,
			"bLengthChange": false,
			"bFilter": true,
			"bSort": true,
			"bInfo": true,
			"cache":false,
			"bAutoWidth": false,
			"bRetrieve": true,
			"bDestroy":false,
			"searching": false,
			"language": 
				{          
					"processing": "Fetching Projects...",
				},
			"aoColumns":[
		   //{"className": "dt-center", "targets": "_all"},  
		   {"targets":0 , "data": null,
			   "render":function(data){
	        		var buttons='';
	        		if(data.balance_amount!=0.00){
	        			buttons='<button class="btn btn-sm btn-success" action="releaseAmount" title="Release Amount" ><i class="fa fa-rupee" ></i></button>&nbsp'
	        				+'<button class="btn btn-sm btn-warning" action="trackReleaseAmount"  title="Track Release Amount" ><i class="fa fa-sitemap"></i></button>'
	        		}
	        		else{
	        			buttons='<button class="btn btn-sm btn-success" action="releaseAmount" disabled title="Release Amount" ><i class="fa fa-rupee" ></i></button>&nbsp'
	        				+'<button class="btn btn-sm btn-warning" action="trackReleaseAmount"  title="Track Release Amount" ><i class="fa fa-sitemap"></i></button>'
	        		}	
	        		return buttons;
	        	}
	        	
		   },
		   {"targets":1 ,"data":"block_name"},
		   {"targets":2 ,"data":"from_school_id"},
		   {"targets":3 ,"data":"from_school"},
		   {"targets":4 ,"data":"to_school_id"},
		   {"targets":5 ,"data":"to_school"},
		   {"targets":6 ,"data":"year_desc"},
		   {"targets":7 ,"data":"work_desc"},
		   {"targets":8 ,"data":"allocated_units"},
		   {"targets":9 ,"data":"unit_cost"},
		   {"targets":10 ,"data":"sanc_amt"},
		   {"targets":11 ,"data":"balance_amount"},
		   {"targets":12 ,"data":"released_amount"},
		   {"targets":13 ,"data":"project_id"},
		   {"targets":14 ,"data":"scheme"},
		   {"targets":15 ,"data":"catg_desc"},
     ],
     "sAjaxSource":"./projectmonitoring/getDistrictLevelReAllocatedProjectDetails/All/All/All/All.do",
	});


	function objectifyForm(formArray) {
		var returnArray = {};
		for (var i = 0; i < formArray.length; i++) {
			returnArray[formArray[i]['name']] = formArray[i]['value'];
		}
		return returnArray;
	}
	

	   /*Validation starts */
	var releaseAmountFormValidate = $("#releaseAmountForm");
	releaseAmountFormValidate.validate({
		rules: {	        	 
			amount_released:{
				required: true,
				onlyNumber: true
			},
			date_of_rel:{
				required: true,
				customdate: true
			},
		},
		messages: {
			//Release Amount error messages
			amount_released:{
				required: "Please enter release amount",
				onlyNumber: onlyNumberMsg,
			},
			date_of_rel:{
				required: "Please select release date",
				customdate:dateFormatAllowMsg ,
			},
		},
		highlight: function (input) {
			$(input).parents('.validation_error_msg').addClass('error');
		},
		unhighlight: function (input) {
			$(input).parents('.validation_error_msg').removeClass('error');
		},
		errorPlacement: function (error, element) {
			$(element).parents('.form-group').append(error);
		}
	});

	/**Validating Release Amount Forms **/
	$('#release_amount').keypress(function(){
		$('#release_amount').valid();
	});
	
	$('#release_date').change(function(){
		$('#release_date').valid();
	});

	/*Validation ends*/
	
	$("#releaseAmountModalSubmit").click(function() {
		var isReleaseAmountFormValidate = releaseAmountFormValidate.valid();
		if(isReleaseAmountFormValidate != false){
			saveReleaseAmountDetails();
		}
		
	});
	
	function saveReleaseAmountDetails(){
		$.LoadingOverlay("show");
		$("#releaseAmountModalSubmit").button('loading');
	var postData = $("#releaseAmountForm").serializeArray();
	var formURL = "./projectmonitoring/districtMonitoringAmountSanctionPhaseWiseDetails.do";
	$.ajax({
		 url: formURL,
		 type: "POST",
		 beforeSend: function(xhr) {
                xhr.setRequestHeader(header, token);
  		},
  		data: JSON.stringify(objectifyForm(postData)),
        contentType:"application/json",
        dataType:"JSON",
		 success: function(data) {
			 $.LoadingOverlay("hide");
			 $("#releaseAmountModalSubmit").button('reset');
                if (data.status) {
                	swal(data.msg)
                	$("#releasedAmountModal").modal('hide');
                	getProjectDetails();
	              }else{
	            	  swal(data.msg);
	            	  $("#releasedAmountModal").modal('hide');
	              }
            },
		 error:function(jqXHR, status, error){
			 $.LoadingOverlay("hide");
			 $("#releaseAmountModalSubmit").button('reset');
		 	swal(error);
		 }
	});
	}


	//Filter Functions
	
	$('#blockfilter').on('change', function(){
		finyear = $('#finyear').val();
		block= $('#blockfilter').val();
		scheme= $('#schemeFilter').val();
		subcategory = $('#subcategoryfilter').val();
		tblDistrictSchoolMonitoringDetails.ajax.url("./projectmonitoring/getDistrictLevelReAllocatedProjectDetails/"+block+"/"+finyear+"/"+scheme+"/"+subcategory+".do").load();
	})
	
	$('#finyear').on('change', function(){
		finyear = $('#finyear').val();
		block= $('#blockfilter').val();
		scheme= $('#schemeFilter').val();
		subcategory = $('#subcategoryfilter').val();
		tblDistrictSchoolMonitoringDetails.ajax.url("./projectmonitoring/getDistrictLevelReAllocatedProjectDetails/"+block+"/"+finyear+"/"+scheme+"/"+subcategory+".do").load();
	})

	$('#schemeFilter').on('change', function() {
		finyear = $('#finyear').val();
		block= $('#blockfilter').val();
		scheme= $('#schemeFilter').val();
		subcategory = $('#subcategoryfilter').val();
		tblDistrictSchoolMonitoringDetails.ajax.url("./projectmonitoring/getDistrictLevelReAllocatedProjectDetails/"+block+"/"+finyear+"/"+scheme+"/"+subcategory+".do").load();
	})
	
	$('#subcategoryfilter').on('change', function() {
		finyear = $('#finyear').val();
		block= $('#blockfilter').val();
		scheme= $('#schemeFilter').val();
		subcategory = $('#subcategoryfilter').val();
		tblDistrictSchoolMonitoringDetails.ajax.url("./projectmonitoring/getDistrictLevelReAllocatedProjectDetails/"+block+"/"+finyear+"/"+scheme+"/"+subcategory+".do").load();
	})
	
	$('#tblDistrictSchoolMonitoringDetails tbody').on( 'click', 'button[action=releaseAmount]', function () {		
		 var data = tblDistrictSchoolMonitoringDetails.row( $(this).parents('tr') ).data();
		 	$('#releasedAmountModal').modal();
		 	$('#rel_hidden_project_id').val(data.project_id);
		 	$('#rel_app_amount').val(data.balance_amount);
		 	$('#total_sanc_amt').html(":&nbsp;"+data.sanc_amt + "&nbsp;(in lakhs)");
		 	$('#total_balance_amt').html(":&nbsp;"+data.balance_amount + "&nbsp;(in lakhs)");
		 	$('#total_released_amt').html(":&nbsp;"+data.released_amount + "&nbsp;(in lakhs)");
		 	
	 	var project_id=data.project_id;
		 	
			// for phase count
	    	$.ajax({
				 url: "./projectmonitoring/getTotalPhaseCountForReleaseAmount/"+project_id+".do",
				 type: "GET",	
				 async:true,
				 beforeSend: function(xhr) {
		               xhr.setRequestHeader(header, token);
				},
				success: function(result)
				{
					if(result.status == false){
						 swal(result.msg);
					}else{
						var data = result.data;
						console.log("allocated phases are  "+data);
						if(data==4){
							$('#releaseAmountModalSubmit').attr('disabled',true);
							swal(result.msg);
						}
					 }
				},
			    failure: function () {
			        swal("Failed!");
			    }
				 
			});
	})

	
	$('#tblDistrictSchoolMonitoringDetails tbody').on( 'click', 'button[action=trackReleaseAmount]', function () {		
		 var data = tblDistrictSchoolMonitoringDetails.row( $(this).parents('tr') ).data();
			var projectId = data.project_id;
			getReleaseAmountDetailsPhasewise(projectId)
	 });
	
	/********************************************************
	 * function for (table-row) track on Track button
	 * @author Raghu
	 *******************************************************/
	function getReleaseAmountDetailsPhasewise(projectId){
		$('#loading').show();
		$.ajax({
			url: "./projectmonitoring/getReleaseAmountTrackingDetailsPhasewiseReallocation/"+projectId+".do",
			type: "GET",	
			beforeSend: function(xhr) {
				xhr.setRequestHeader(header, token);
			},
			success:function(result) {
				$('#loading').fadeOut(1000);
				if(result.status==true){
					var data = result.data;
					console.log(data)
					 var html;
					 if(data.length!=0){
					 $('#releaseAmountTrackingModal').modal();
					 for(var i=0;i<data.length;i++){
						 var release_date=(data[i].date_of_rel!=null)?data[i].date_of_rel:"NA";
						// var approver_remark=(data[i].approver_remark!=null)?data[i].approver_remark:"NA";
								
										html='<div class="row">'+
		                             '<div class="col-md-2 col-md-offset-1">'+
		                                '<div class="timeline-date">'+release_date+''+	                                                 	                                                  
		                                '</div>'+
		                                '</div>'+
		                                '<div class="col-md-1">'+
		                                    '<div class="timeline2-icon bg-indigo">'+
		                                        '<i class="fa fa-rupee"></i>'+
		                                    '</div>'+
		                                '</div>'+
		                                '<div class="col-md-7">'+
		                                    '<div class="timeline-hover">'+
		                                        '<div class="timeline-heading bg-indigo">'+
		                                            '<div class="timeline-arrow arrow-indigo"></div>'+data[i].school_name+'('+data[i].school_id+')'+                                                      
		                                            '<span class="label label-override pull-right m-t-10">Phase : '+data[i].phase+'</span>'+
		                                        '</div>'+
		                                        '<div class="timeline-content">Released Amount : '+data[i].amount_released+'</div>'+		                                                        
		                                    '</div>'+
		                                '</div>'+
		                        '</div>';
							    $('#timelineDynamic').append(html);
								}
				   }else{
						 swal('No Release Amount Found');
				}
				}else{
					swal(data.msg)
				}
			},
			error:function(jqXHR, status, error){
				$('#loading').fadeOut(1000);
				swal(error);
			}
		});
	}
	
	
	$('#district-reallocation-excel').click(function(){
		finyear = $('#finyear').val();
		block= $('#blockfilter').val();
		scheme= $('#schemeFilter').val();
		subcategory = $('#subcategoryfilter').val();
		$.LoadingOverlay("show");
		$('#district-reallocation-excel').attr('href','./projectmonitoring/exportReAllocationProjectsToExcelForDistrict/'+block+"/"+finyear+"/"+scheme+"/"+subcategory+'.do');
		$.LoadingOverlay("hide");
		
	})
		
});