$('document').ready(function(){
	var on = false;
	$('#btn-desc').hide();
	
	$('.recieved_date').datepicker({
		format:"dd-mm-yyyy",
		todayHighlight: true,
		autoclose: true,
		endDate: '+0d',
	});
	
	$("#projectInspectionModalBuilding").on("hidden.bs.modal", function(){
	      $("div.tab-head>div.list-group>button").removeClass('active');
	      $("div.tab-head>div.list-group>button").eq(0).addClass('active');
		$('input[type=radio]').prop('checked', false);
		$(".bform")[0].reset();
		$('#bapprovedSubmit').attr('disabled', false);
		 if($('#gpstoggle').is(":checked")==true){
			 $('.geo-location-status').html('Geo-location is disabled.')
		 }else{
			 $('.geo-location-status').html('Geo-location is enabled.')
		 }
		hideActionForms();
	});
	
	$("#projectInspectionModalNonBuilding").on("hidden.bs.modal", function(){
	      $("div.tab-head-nb>div.list-group>button").removeClass('active');
	      $("div.tab-head-nb>div.list-group>button").eq(0).addClass('active');
		$('input[type=radio]').prop('checked', false);
		$(".nbform")[0].reset();
		 $('#nbapprovedSubmit').attr('disabled', false);
		 if($('#gpstoggle').is(":checked")==true){
			 $('.geo-location-status').html('Geo-location is disabled.')
		 }else{
			 $('.geo-location-status').html('Geo-location is enabled.')
		 }
		hideActionForms();
	});
	
	//get school for dropdown
	var getSchoolByBlock = function(){
		$.ajax({
			url:'./projectmonitoring/getSchoolDetailsByBlock.do',
			type: "GET",
			beforeSend: function(xhr) {
	                xhr.setRequestHeader(header, token);
			 		},
			success:function(result){
				if(result.status == false){
					 swal(result.msg);
				}else{
					var data = result.data;
					 console.log("schools:: ",result);
						var option = '<option value="0">--Select School--</option>';
						for(var i = 0;i<data.length;i++){
							option += '<option value="'+data[i].school_id+'">'+data[i].school_name+' ('+data[i].school_id+') '+'</option>';
						}
						$('#school_filter').val('');
						$('#school_filter').empty();
						$('#school_filter').append(option);
						$("#school_filter").prop("selectedIndex", 0);
				}
			}
			
		});
	}
	
	getSchoolByBlock();
	
	$("#projectStartDateModal").on("hidden.bs.modal", function(){
		$('#projectStartDateForm')[0].reset();
		$(".dynamic_start_date_field").html("");
		$('#closeModal').removeClass('alert-button blink')
		$('#projectStartDateModalSubmit').attr("disabled", false)
        $('#myBar').css('width', 10+'%')
        $('#progressLabel').html(10+'%')
        
        if($('#gpstoggle').is(":checked")==true){
			 $('.geo-location-status').html('Geo-location is disabled.')
		 }else{
			 $('.geo-location-status').html('Geo-location is enabled.')
		 }
	});
	
	$('input[type=radio][name=yesno]').change(function() {
		if($('input[name=yesno]:checked').val() == 1){
	        $('.dynamic_start_date_field').append(
	        		'<hr>'
	        		+'<div class="row">'
	        		+'<div class="form-group start-date-field col-sm-6">'
	        		+'<div>Please select a start date: </div>'
	        		+'<input id="start_date" name="date_of_receive" class="form-control" readonly>'
	        		+'</div>'
	        		+'</div>'
	        		)
	        		
	        		$('#start_date').datepicker({
						autoclose:true,
						format:"dd-mm-yyyy",
						todayHighlight: true,
						endDate: '+0d',
					});
	        		
				$('#closeModal').removeClass('alert-button blink')
				$('#projectStartDateModalSubmit').attr("disabled", false)
				
		}else{
			$(".dynamic_start_date_field").html("");
			$('#projectStartDateModalSubmit').attr("disabled", true)
			$('#closeModal').addClass('alert-button blink')
		}
	});
	
	function objectifyForm(formArray) {
		var returnArray = {};
		for (var i = 0; i < formArray.length; i++) {
			returnArray[formArray[i]['name']] = formArray[i]['value'];
		}
		return returnArray;
	}
	
	var getDataForMasterTimelineTable = function getDataForMasterTimelineTable(schoolId){
		$.ajax({
			url:'./projectmonitoring/getDataForMasterTimelineTable/'+schoolId+'.do',
			type: "GET",
			beforeSend: function(xhr) {
	                xhr.setRequestHeader(header, token);
			 		},
			success:function(result){
				if(result.status == false){
					 swal(result.msg);
				}else{
					var data = result.data;
					/*console.log('getDataForMasterTimelineTable', data)*/
					 if(data.length != 0){
						 
						 for(var i = 0; i<data.length; i++){
							 $('.append-cards').append(
									 '<div class="cards testcard">'
									 +'<div class="card-img-top"><i class="fa fa-university"></i><span class="card-header">'+data[i].school_name+'</span></div>'
									 +'<hr>'
									 +'<div class="card-body">'
									 +'<div class="row">'
									 +'<div class="col-sm-12"><label>Project Id:&nbsp; </label><span>'+data[i].new_pr_id+'</span></div>'
									 +'</div>'
									 +'<div class="row">'
									 +'<div class="col-sm-6"><label>Scheme:&nbsp; </label><span>'+data[i].scheme+'</span></div>'
									 +'<div class="col-sm-6"><label>Year:&nbsp; </label><span>'+data[i].year_desc+'</span></div>'
									 +'</div>'
									 +'<div class="row">'
									 +'<div class="col-sm-6"><label>District:&nbsp; </label><span>'+data[i].district_name+'</span></div>'
									 +'<div class="col-sm-6"><label>Block:&nbsp; </label><span>'+data[i].block_name+'</span></div>'
									 +'</div>'
									 +'<div class="row">'
									 +'<div class="col-sm-12"><label>Category:&nbsp; </label><span>'+data[i].catg_desc+'</span></div>'
									 +'</div>'
									 +'<div class="row">'
									 +'<div class="col-sm-12"><label>Activity:&nbsp; </label><span>'+data[i].work_desc+'</span></div>'
									 +'</div>'
									 +'<div class="row">'
									 +'<div class="col-sm-12"><label>Released Amount:&nbsp; </label><span>'+data[i].amount_released+'</span></div>'
									 +'</div>'
									 +'</div>'
									 +'<hr>'
									 +'<div class="card-footer">'
									 +'<button id="start-project'+i+'" class="start-project btn btn-danger btn-sm" title="Start Project" data-new_pr_id ="'+data[i].new_pr_id+'" data-scheme="'+data[i].scheme
										+'" data-year_desc="'+data[i].year_desc+'" data-school_name="'+data[i].school_name+'" data-district_name="'+data[i].district_name
										+'" data-block_name="'+data[i].block_name+'" data-catg_desc="'+data[i].catg_desc+'" data-work_desc="'+data[i].work_desc
										+'" data-school_id="'+data[i].school_id+'" data-amount_released="'+data[i].amount_released+'" data-finyr_id="'+data[i].finyr_id
										+'" data-work_id="'+data[i].work_id+'" data-scheme_id="'+data[i].scheme_id+'" data-work_desc_id="'+data[i].work_desc_id
										+'" data-unit_cost="'+data[i].unit_cost+'" data-workcatg_id="'+data[i].workcatg_id+'" data-date_of_receive="'+data[i].date_of_receive+'"><i class="fa fa-send"></i></button>'
										
									 +'&nbsp;<button id="inspect-project'+i+'" class="inspect-project btn btn-info btn-sm" title="Inspect" data-new_pr_id ="'+data[i].new_pr_id+'" data-scheme="'+data[i].scheme
										+'" data-year_desc="'+data[i].year_desc+'" data-school_name="'+data[i].school_name+'" data-district_name="'+data[i].district_name
										+'" data-block_name="'+data[i].block_name+'" data-catg_desc="'+data[i].catg_desc+'" data-work_desc="'+data[i].work_desc
										+'" data-school_id="'+data[i].school_id+'" data-amount_released="'+data[i].amount_released+'" data-finyr_id="'+data[i].finyr_id
										+'" data-work_id="'+data[i].work_id+'" data-scheme_id="'+data[i].scheme_id+'" data-work_desc_id="'+data[i].work_desc_id
										+'" data-unit_cost="'+data[i].unit_cost+'" data-workcatg_id="'+data[i].workcatg_id+'" data-date_of_receive="'+data[i].date_of_receive+'"><i class="fa fa-info"></i></button>'
									+'<div class="geo-location-status">Geo-location is enabled.</div>'
									 +'</div>'
									 +'</div>'
									 /*+'<hr>'*/
							 )
							 
						 }
						 
						 
						 $('#gpstoggle').change(function(){
							 if($('#gpstoggle').is(":checked")==true){
								 $('.geo-location-status').html('Geo-location is disabled.')
							 }else{
								 $('.geo-location-status').html('Geo-location is enabled.')
							 }
					 	});
						 
						 //Get longitude and latitude of the school from master_school table
						 var getLongAndLatOfTheSchool = function(schoolId, isInTheLocation){
							 $.ajax({
									url:'./projectmonitoring/getLongAndLatOfTheSchool/'+schoolId+'.do',
									type: "GET",
									async : false,
									beforeSend: function(xhr) {
							                xhr.setRequestHeader(header, token);
									 		},
									success:function(result){
										if(result.status == false){
											 swal(result.msg);
										}else{
											var data = result.longlat[0];
											 var school_longitude= parseFloat(data.longitude).toFixed(3);
											 var school_latitude= parseFloat(data.latitude).toFixed(3);
											 $('#latitude').val(school_latitude);
											 $('#longitude').val(school_longitude);
											 /*console.log("school_longitude:: "+school_longitude +" ; school_latitude:: "+school_latitude)*/
										}
									}
									
								});
						 }
						 
						 //Get current longitude and latitude of the user's device
						 var getCurrentLocationCoordinates = function(){
							 if (navigator.geolocation) {
						        navigator.geolocation.getCurrentPosition(
						        		showPosition,
						        		displayError,
						        		{ enableHighAccuracy: true, timeout : 5000 }
						        	    );
						    } else { 
						    	swal("Geolocation is not supported by this browser.");
						    }
						 }
						 
						 
						 function showPosition(position) {
							var current_latitude= parseFloat(position.coords.latitude).toFixed(3);
							var current_longitude= parseFloat(position.coords.longitude).toFixed(3);
							/*console.log("current_longitude:: "+current_longitude +" ; current_latitude:: "+current_latitude)*/
							 
							 var sch_lon = $('#longitude').val();
							 var sch_lat = $('#latitude').val();
							 if(current_latitude == sch_lat && current_longitude == sch_lon){
								 doInspection();
							 }else{
								 swal("Please go to the school location.")
							 }

						}
						 
						 function displayError(error) {
							  var errors = { 
							    1: 'Permission denied',
							    2: 'Position unavailable',
							    3: 'Request timeout'
							  };
							  swal("Error: " + errors[error.code]);
							}
						 
						 $('.inspect-project').click(function(){
							 var schoolId = $(this).data("school_id")
							 var isInTheLocation = false;
							 var id = $(this).attr("id");
							 if($('#gpstoggle').is(":checked") == true){
								 doInspection(id);
							 }else{
								 getCurrentLocationCoordinates();
								 getLongAndLatOfTheSchool(schoolId);
							 }
							 
						 })
						 
						 var doInspection = function(id){
							 var dateOfReceive = $('#'+id).data("date_of_receive");
							 if(dateOfReceive == null){
								 swal('Project Not Started Yet!')
							 }else{
								 var projectID = $('#'+id).data("new_pr_id");
								 var workCatgID = $('#'+id).data("workcatg_id");
								 var finYearID = $('#'+id).data("finyr_id");
								 var workID = $('#'+id).data("work_id");
								 getMaxConstructionLevelIdWithConstructionStatusOne(projectID, workCatgID);
								 // for settting hidden fields
								 for(var i = 1; i<=13; i++){
									 $('#hidden_inspection_finyear_id_step'+i).val(finYearID);
									 $('#hidden_inspection_project_id_step'+i).val(projectID);
									 $('#hidden_inspection_work_id_step'+i).val(workID);
								 }
								 
								 for(var i = 1; i<=7; i++){
									 $('#hidden_inspection_nb_finyear_id_step'+i).val(finYearID);
									 $('#hidden_inspection_nb_project_id_step'+i).val(projectID);
									 $('#hidden_inspection_nb_work_id_step'+i).val(workID);
								 }
								 if(workCatgID == 1001){
									 $('#projectId').html($('#'+id).data("new_pr_id"));
									 $('#schoolName').html($('#'+id).data("school_name"));
									 $('#schoolId').html($('#'+id).data("school_id"));
									 $('#financialYear').html($('#'+id).data("year_desc"));
									 $('#subcategoryName').html($('#'+id).data("work_desc"));

									 $('#projectInspectionModalNonBuilding').modal('hide');
									 $('#projectInspectionModalBuilding').modal();
								      $('#myBar').css('width', (1*(100/13))+'%')
								      $('#progressLabel').html((1*(100/13)).toFixed(0)+'%')
								      
									 $("div.tab-head>div.list-group>button").click(function(e) {
									      e.preventDefault();
									      $(this).siblings('button.active').removeClass("active");
									      $(this).addClass("active");
									      var index = $(this).index();
									      var i = $(this).index()+1;
									      $("div.tab-content>div.tab-pane").removeClass("active");
									      $("div.tab-content>div.tab-pane").eq(index).addClass("active");
									      /*console.log(index)*/
									      $('#myBar').css('width', (i*(100/13))+'%')
									      $('#progressLabel').html((i*(100/13)).toFixed(0)+'%')
									  });
								      
								      getAllDelayReasons();
									
								 }else{
									 $('#nb-projectId').html($('#'+id+'').data("new_pr_id"));
									 $('#nb-schoolName').html($('#'+id+'').data("school_name"));
									 $('#nb-schoolId').html($('#'+id+'').data("school_id"));
									 $('#nb-financialYear').html($('#'+id+'').data("year_desc"));
									 $('#nb-subcategoryName').html($('#'+id+'').data("work_desc"));
									 
									 $('#projectInspectionModalBuilding').modal('hide');
									 $('#projectInspectionModalNonBuilding').modal();
								      $('#myBarNonBuilding').css('width', (1*(100/7))+'%')
								      $('#progressLabelNonBuilding').html((1*(100/7)).toFixed(0)+'%')
								      
									 $("div.tab-head-nb>div.list-group>button").click(function(e) {
									      e.preventDefault();
									      $(this).siblings('button.active').removeClass("active");
									      $(this).addClass("active");
									      var index = $(this).index();
									      var i = $(this).index()+1;
									      $("div.tab-content-nb>div.tab-pane-nb").removeClass("active");
									      $("div.tab-content-nb>div.tab-pane-nb").eq(index).addClass("active");
									      /*console.log('index:: '+index)*/
									      $('#myBarNonBuilding').css('width', (i*(100/7))+'%')
									      $('#progressLabelNonBuilding').html((i*(100/7)).toFixed(0)+'%')
									  });
									 
								      getAllDelayReasons();
								 }
							 }
						 }
						 
						 $('.start-project').click(function(){
							 var dateOfReceive = $(this).data("date_of_receive");
							 if(dateOfReceive == null){
								 $('#project_id').val($(this).data("new_pr_id"));
									$('#school_id').html($(this).data("school_id"));
									$('#school_name').html($(this).data("school_name"));
									$('#finyear').html($(this).data("year_desc"));
									$('#subcategory').html($(this).data("work_desc"));
									$('#released_amount').html($(this).data("amount_released"));
									$('#hidden_fin_year_id').val($(this).data("finyr_id"));
									$('#hidden_work_id').val($(this).data("work_id"));
									$('#hidden_scheme_id').val($(this).data("scheme_id"));
									$('#hidden_work_desc_id').val($(this).data("work_desc_id"));
									$('#hidden_amount_approved').val($(this).data("unit_cost"));
									$('#hidden_amount_released').val($(this).data("amount_released"));
								 	$("#projectStartDateModal").modal(); 
							 }else{
								 swal('Project Already Started!')
							 }
							 	
						 })
							
					 }else{
						 $('.append-cards').html('');
						 $('#btn-desc').hide();
						 swal("No projects found for this school!!!")
					 }
				}
			}
			
		});
	}
	
	$('#school_filter').on('change', function(){
		$('.append-cards').html('');
		$('#btn-desc').show();
		$('#gpstoggle').prop("checked", false);
		var schoolId = $('#school_filter').val();
		$('#hidden_school_id').val(schoolId);
		getDataForMasterTimelineTable(schoolId);
		
	})

	
	var saveProjectStartDate = function saveProjectStartDate(){
		var postData = $('#projectStartDateForm').serializeArray();
		/*console.log(postData);*/
		$.ajax({
			 url: "./projectmonitoring/saveProjectStartDate.do",
			 type: "POST",
			 data: JSON.stringify(objectifyForm(postData)),
	         contentType:"application/json",
	         dataType:"JSON",
	         beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
				},
			 success: function(data) {
	                if (data.status) {
	                	swal(data.msg)
	                	$("#projectStartDateModal").modal('hide');
	                	$('.append-cards').html('');
	                	getDataForMasterTimelineTable($('#hidden_school_id').val());
	                	
		              }else{
		            	  swal(data.msg);
		              }
	            },
		 error:function(jqXHR, status, error){
		 	swal(error);
		 }
	});
	}
	
	
	$("#projectStartDateModalSubmit").on('click', function(){
		saveProjectStartDate();
	});
	
	var getAllDelayReasons = function(){
		$.ajax({
			url:'./projectmonitoring/getAllDelayReasons.do',
			type: "GET",
			beforeSend: function(xhr) {
	                xhr.setRequestHeader(header, token);
			 		},
			success:function(result){
				if(result.status == false){
					 swal(result.msg);
				}else{
					var data = result.data;
					 /*console.log('getAllDelayReasons:: ',result);*/
						var option = '<option value="0">--Select a Reason--</option>';
						for(var i = 0;i<data.length;i++){
							option += '<option value="'+data[i].delay_reason_id+'">'+data[i].delay_reason+'</option>';
						}
						$('.delay_reason').val('');
						$('.delay_reason').empty();
						$('.delay_reason').append(option);
						$(".delay_reason").prop("selectedIndex", 0);
				}
			}
			
		});
	}
	
	var getMaxConstructionLevelIdWithConstructionStatusOne = function getMaxConstructionLevelIdWithConstructionStatusOne(projectId, catgId){
		$.ajax({
			url:"./projectmonitoring/getMaxConstructionLevelIdWithConstructionStatusOne/"+projectId+".do",
			type: "POST",
			data: JSON.stringify(projectId),
	        contentType:"application/json",
	        dataType:"JSON",
			beforeSend: function(xhr) {
	                xhr.setRequestHeader(header, token);
			 		},
			success: function(result){
						if(result.status == false){
							 swal(result.msg);
						}else{
							var data = result.const_id;
							 /*console.log('Building data:: ',data);*/
							 var levelId = data.levelId;
							 var approved_amount = data.approved_amount;
							 var balance_amount = data.balance_amount;
							 var expended_amount = data.expended_amount;
							 var released_amount = data.released_amount;
							 var inspection_date = data.inspection_date;
							 if(catgId == 1001){
								 console.log("=======Building========")
								 for(var i = 1; i<=13; i++){
									 $('#hidden_approved_amount_step'+i).val(approved_amount);
									 $('#hidden_balance_amount_step'+i).html(balance_amount);
									 $('#hidden_expended_amount_step'+i).html(expended_amount);
									 $('#hidden_released_amount_step'+i).val(released_amount);
									 $('#hidden_last_inspection_date_step'+i).html(inspection_date);
								 }
								 
								 console.log('Building levelId:: '+levelId)
								 
								 if(levelId>=13){
									 levelId = 12;
									 $('#bapprovedSubmit').attr('disabled', true);
								 }
							      $("div.tab-head>div.list-group>button").removeClass('active');
							      $("div.tab-head>div.list-group>button").eq(levelId).addClass('active');
							      
							      $("div.tab-content>div.tab-pane").removeClass("active");
							      $("div.tab-content>div.tab-pane").eq(levelId).addClass("active");

							      for(var i = 0; i<levelId; i++){
							    	  $("div.tab-head>div.list-group>button").eq(i).attr("disabled","disabled");
							    	  $("div.tab-head>div.list-group>button").eq(i).css("opacity",0.8);
							      }
							      
							      if($("div.tab-head>div.list-group>button.active").index()!=levelId){
							    	  $('#myBar').css('width', ((levelId)*(100/13))+'%')
								      $('#progressLabel').html(((levelId)*(100/13)).toFixed(0)+'%')
							      }else if($("div.tab-head>div.list-group>button.active").index()==levelId){
							    	  $('#myBar').css('width', ((levelId+1)*(100/13))+'%')
								      $('#progressLabel').html(((levelId+1)*(100/13)).toFixed(0)+'%')
							      }
							 }else if(catgId == 1002){
								 console.log("=======Non Building========")
								 for(var i = 1; i<=7; i++){
									 $('#hidden_inspection_nb_approved_amount_step'+i).val(approved_amount);
									 $('#hidden_inspection_nb_balance_amount_step'+i).html(balance_amount);
									 $('#hidden_inspection_nb_expended_amount_step'+i).html(expended_amount);
									 $('#hidden_inspection_nb_released_amount_step'+i).val(released_amount);
									 $('#hidden_inspection_nb_inspected_date_step'+i).html(inspection_date);
								 }
								 
								 console.log('Non Building levelId:: '+levelId)
								 
								 if(levelId>=7){
									 levelId = 6;
									 $('#nbapprovedSubmit').attr('disabled', true);
								 }
								 
							      $("div.tab-head-nb>div.list-group>button").removeClass('active');
							      $("div.tab-head-nb>div.list-group>button").eq(levelId).addClass('active');
							      
							      $("div.tab-content-nb>div.tab-pane-nb").removeClass("active");
							      $("div.tab-content-nb>div.tab-pane-nb").eq(levelId).addClass("active");

							      for(var i = 0; i<levelId; i++){
							    	  $("div.tab-head-nb>div.list-group>button").eq(i).attr("disabled","disabled");
							    	  $("div.tab-head>div.list-group>button").eq(i).css("opacity",0.8);
							      }
							      
							      if($("div.tab-head-nb>div.list-group>button.active").index()!=levelId){
							    	  $('#myBarNonBuilding').css('width', ((levelId)*(100/7))+'%')
								      $('#progressLabelNonBuilding').html(((levelId)*(100/7)).toFixed(0)+'%')
							      }else if($("div.tab-head-nb>div.list-group>button.active").index()==levelId){
							    	  $('#myBarNonBuilding').css('width', ((levelId+1)*(100/7))+'%')
								      $('#progressLabelNonBuilding').html(((levelId+1)*(100/7)).toFixed(0)+'%')
							      }
							      
							 }	
								 
						}
					},
	 		error: function(result){
	 			swal(result.msg)
	 		}
			
		});
	}
	
	$("#bfundreleaseSubmit").click(function(){
		var cnstrn_lvl = $("div.tab-head>div.list-group>button.active").index()+1;
		console.log( " construction level "+cnstrn_lvl);
		 var approved_amount = $('#hidden_approved_amount_step1').val();
		 var balance_amount = $('#hidden_balance_amount_step1').html();
		 var expended_amount = $('#hidden_expended_amount_step1').html();
		 var released_amount = $('#hidden_released_amount_step1').val();
		validateBlockInspectionDetailsLevelWise(cnstrn_lvl, 1, approved_amount, balance_amount, expended_amount, released_amount);
	});
	
	$("#blayoutSubmit").click(function(){
		var cnstrn_lvl = $("div.tab-head>div.list-group>button.active").index()+1;
		console.log( " construction level "+cnstrn_lvl);
		 var approved_amount = $('#hidden_approved_amount_step2').val();
		 var balance_amount = $('#hidden_balance_amount_step2').html();
		 var expended_amount = $('#hidden_expended_amount_step2').html();
		 var released_amount = $('#hidden_released_amount_step2').val();
		validateBlockInspectionDetailsLevelWise(cnstrn_lvl, 1, approved_amount, balance_amount, expended_amount, released_amount);
	});
	
	$("#bfoundationSubmit").click(function(){
		var cnstrn_lvl = $("div.tab-head>div.list-group>button.active").index()+1;
		console.log( " construction level "+cnstrn_lvl);
		 var approved_amount = $('#hidden_approved_amount_step3').val();
		 var balance_amount = $('#hidden_balance_amount_step3').html();
		 var expended_amount = $('#hidden_expended_amount_step3').html();
		 var released_amount = $('#hidden_released_amount_step3').val();
		validateBlockInspectionDetailsLevelWise(cnstrn_lvl, 1, approved_amount, balance_amount, expended_amount, released_amount);
	});
	
	$("#bplinthSubmit").click(function(){
		var cnstrn_lvl = $("div.tab-head>div.list-group>button.active").index()+1;
		console.log( " construction level "+cnstrn_lvl);
		 var approved_amount = $('#hidden_approved_amount_step4').val();
		 var balance_amount = $('#hidden_balance_amount_step4').html();
		 var expended_amount = $('#hidden_expended_amount_step4').html();
		 var released_amount = $('#hidden_released_amount_step4').val();
		validateBlockInspectionDetailsLevelWise(cnstrn_lvl, 1, approved_amount, balance_amount, expended_amount, released_amount);
	});
	
	$("#blintelSubmit").click(function(){
		var cnstrn_lvl = $("div.tab-head>div.list-group>button.active").index()+1;
		console.log( " construction level "+cnstrn_lvl);
		 var approved_amount = $('#hidden_approved_amount_step5').val();
		 var balance_amount = $('#hidden_balance_amount_step5').html();
		 var expended_amount = $('#hidden_expended_amount_step5').html();
		 var released_amount = $('#hidden_released_amount_step5').val();
		validateBlockInspectionDetailsLevelWise(cnstrn_lvl, 1, approved_amount, balance_amount, expended_amount, released_amount);
	});
	
	$("#broofSubmit").click(function(){
		var cnstrn_lvl = $("div.tab-head>div.list-group>button.active").index()+1;
		console.log( " construction level "+cnstrn_lvl);
		 var approved_amount = $('#hidden_approved_amount_step6').val();
		 var balance_amount = $('#hidden_balance_amount_step6').html();
		 var expended_amount = $('#hidden_expended_amount_step6').html();
		 var released_amount = $('#hidden_released_amount_step6').val();
		validateBlockInspectionDetailsLevelWise(cnstrn_lvl, 1, approved_amount, balance_amount, expended_amount, released_amount);
	});
	
	$("#broofcastSubmit").click(function(){
		var cnstrn_lvl = $("div.tab-head>div.list-group>button.active").index()+1;
		console.log( " construction level "+cnstrn_lvl);
		 var approved_amount = $('#hidden_approved_amount_step7').val();
		 var balance_amount = $('#hidden_balance_amount_step7').html();
		 var expended_amount = $('#hidden_expended_amount_step7').html();
		 var released_amount = $('#hidden_released_amount_step7').val();
		validateBlockInspectionDetailsLevelWise(cnstrn_lvl, 1, approved_amount, balance_amount, expended_amount, released_amount);
	});
	
	$("#bfinishingSubmit").click(function(){
		var cnstrn_lvl = $("div.tab-head>div.list-group>button.active").index()+1;
		console.log( " construction level "+cnstrn_lvl);
		 var approved_amount = $('#hidden_approved_amount_step8').val();
		 var balance_amount = $('#hidden_balance_amount_step8').html();
		 var expended_amount = $('#hidden_expended_amount_step8').html();
		 var released_amount = $('#hidden_released_amount_step8').val();
		validateBlockInspectionDetailsLevelWise(cnstrn_lvl, 1, approved_amount, balance_amount, expended_amount, released_amount);
	});
	
	$("#bcompletedSubmit").click(function(){
		var cnstrn_lvl = $("div.tab-head>div.list-group>button.active").index()+1;
		console.log( " construction level "+cnstrn_lvl);
		 var approved_amount = $('#hidden_approved_amount_step9').val();
		 var balance_amount = $('#hidden_balance_amount_step9').html();
		 var expended_amount = $('#hidden_expended_amount_step9').html();
		 var released_amount = $('#hidden_released_amount_step9').val();
		validateBlockInspectionDetailsLevelWise(cnstrn_lvl, 1, approved_amount, balance_amount, expended_amount, released_amount);
	});
	
	$("#bcheckSubmit").click(function(){
		var cnstrn_lvl = $("div.tab-head>div.list-group>button.active").index()+1;
		console.log( " construction level "+cnstrn_lvl);
		 var approved_amount = $('#hidden_approved_amount_step10').val();
		 var balance_amount = $('#hidden_balance_amount_step10').html();
		 var expended_amount = $('#hidden_expended_amount_step10').html();
		 var released_amount = $('#hidden_released_amount_step10').val();
		validateBlockInspectionDetailsLevelWise(cnstrn_lvl, 1, approved_amount, balance_amount, expended_amount, released_amount);
	});
	
	$("#bhandedSubmit").click(function(){
		var cnstrn_lvl = $("div.tab-head>div.list-group>button.active").index()+1;
		console.log( " construction level "+cnstrn_lvl);
		 var approved_amount = $('#hidden_approved_amount_step11').val();
		 var balance_amount = $('#hidden_balance_amount_step11').html();
		 var expended_amount = $('#hidden_expended_amount_step11').html();
		 var released_amount = $('#hidden_released_amount_step11').val();
		validateBlockInspectionDetailsLevelWise(cnstrn_lvl, 1, approved_amount, balance_amount, expended_amount, released_amount);
	});
	
	$("#bsubmittedSubmit").click(function(){
		var cnstrn_lvl = $("div.tab-head>div.list-group>button.active").index()+1;
		console.log( " construction level "+cnstrn_lvl);
		 var approved_amount = $('#hidden_approved_amount_step12').val();
		 var balance_amount = $('#hidden_balance_amount_step12').html();
		 var expended_amount = $('#hidden_expended_amount_step12').html();
		 var released_amount = $('#hidden_released_amount_step12').val();
		validateBlockInspectionDetailsLevelWise(cnstrn_lvl, 1, approved_amount, balance_amount, expended_amount, released_amount);
	});
	
	$("#bapprovedSubmit").click(function(){
		var cnstrn_lvl = $("div.tab-head>div.list-group>button.active").index()+1;
		console.log( " construction level "+cnstrn_lvl);
		 var approved_amount = $('#hidden_approved_amount_step13').val();
		 var balance_amount = $('#hidden_balance_amount_step13').html();
		 var expended_amount = $('#hidden_expended_amount_step13').html();
		 var released_amount = $('#hidden_released_amount_step13').val();
		validateBlockInspectionDetailsLevelWise(cnstrn_lvl, 1, approved_amount, balance_amount, expended_amount, released_amount);
	});
	
	//method calls for nonbuilding
	$("#nbfundreleaseSubmit").click(function(){
		var cnstrn_lvl = $("div.tab-head-nb>div.list-group>button.active").index()+1;
		console.log( " construction level "+cnstrn_lvl);
		 var approved_amount = $('#hidden_inspection_nb_approved_amount_step1').val();
		 var balance_amount = $('#hidden_inspection_nb_balance_amount_step1').html();
		 var expended_amount = $('#hidden_inspection_nb_expended_amount_step1').html();
		 var released_amount = $('#hidden_inspection_nb_released_amount_step1').val();
		 validateBlockInspectionDetailsLevelWise(cnstrn_lvl, 2, approved_amount, balance_amount, expended_amount, released_amount);
	});
	
	$("#nbstartSubmit").click(function(){
		var cnstrn_lvl = $("div.tab-head-nb>div.list-group>button.active").index()+1;
		console.log( " construction level "+cnstrn_lvl);
		 var approved_amount = $('#hidden_inspection_nb_approved_amount_step2').val();
		 var balance_amount = $('#hidden_inspection_nb_balance_amount_step2').html();
		 var expended_amount = $('#hidden_inspection_nb_expended_amount_step2').html();
		 var released_amount = $('#hidden_inspection_nb_released_amount_step2').val();
		 validateBlockInspectionDetailsLevelWise(cnstrn_lvl, 2, approved_amount, balance_amount, expended_amount, released_amount);
	});
	
	$("#nbprogressSubmit").click(function(){
		var cnstrn_lvl = $("div.tab-head-nb>div.list-group>button.active").index()+1;
		console.log( " construction level "+cnstrn_lvl);
		 var approved_amount = $('#hidden_inspection_nb_approved_amount_step3').val();
		 var balance_amount = $('#hidden_inspection_nb_balance_amount_step3').html();
		 var expended_amount = $('#hidden_inspection_nb_expended_amount_step3').html();
		 var released_amount = $('#hidden_inspection_nb_released_amount_step3').val();
		 validateBlockInspectionDetailsLevelWise(cnstrn_lvl, 2, approved_amount, balance_amount, expended_amount, released_amount);
	});
	
	$("#nbcompletedSubmit").click(function(){
		var cnstrn_lvl = $("div.tab-head-nb>div.list-group>button.active").index()+1;
		console.log( " construction level "+cnstrn_lvl);
		 var approved_amount = $('#hidden_inspection_nb_approved_amount_step4').val();
		 var balance_amount = $('#hidden_inspection_nb_balance_amount_step4').html();
		 var expended_amount = $('#hidden_inspection_nb_expended_amount_step4').html();
		 var released_amount = $('#hidden_inspection_nb_released_amount_step4').val();
		 validateBlockInspectionDetailsLevelWise(cnstrn_lvl, 2, approved_amount, balance_amount, expended_amount, released_amount);
	});
	
	$("#nbhandedSubmit").click(function(){
		var cnstrn_lvl = $("div.tab-head-nb>div.list-group>button.active").index()+1;
		console.log( " construction level "+cnstrn_lvl);
		 var approved_amount = $('#hidden_inspection_nb_approved_amount_step5').val();
		 var balance_amount = $('#hidden_inspection_nb_balance_amount_step5').html();
		 var expended_amount = $('#hidden_inspection_nb_expended_amount_step5').html();
		 var released_amount = $('#hidden_inspection_nb_released_amount_step5').val();
		 validateBlockInspectionDetailsLevelWise(cnstrn_lvl, 2, approved_amount, balance_amount, expended_amount, released_amount);
	});
	
	$("#nbsubmittedSubmit").click(function(){
		var cnstrn_lvl = $("div.tab-head-nb>div.list-group>button.active").index()+1;
		console.log( " construction level "+cnstrn_lvl);
		 var approved_amount = $('#hidden_inspection_nb_approved_amount_step6').val();
		 var balance_amount = $('#hidden_inspection_nb_balance_amount_step6').html();
		 var expended_amount = $('#hidden_inspection_nb_expended_amount_step6').html();
		 var released_amount = $('#hidden_inspection_nb_released_amount_step6').val();
		 validateBlockInspectionDetailsLevelWise(cnstrn_lvl, 2, approved_amount, balance_amount, expended_amount, released_amount);
	});
	
	$("#nbapprovedSubmit").click(function(){
		var cnstrn_lvl = $("div.tab-head-nb>div.list-group>button.active").index()+1;
		console.log( " construction level "+cnstrn_lvl);
		 var approved_amount = $('#hidden_inspection_nb_approved_amount_step7').val();
		 var balance_amount = $('#hidden_inspection_nb_balance_amount_step7').html();
		 var expended_amount = $('#hidden_inspection_nb_expended_amount_step7').html();
		 var released_amount = $('#hidden_inspection_nb_released_amount_step7').val();
		 validateBlockInspectionDetailsLevelWise(cnstrn_lvl, 2, approved_amount, balance_amount, expended_amount, released_amount);
	});
	
	// block inspection construction level wise data submit function
	var validateBlockInspectionDetailsLevelWise = function validateBlockInspectionDetailsLevelWise(cnstrn_lvl, formtype, approved_amount, balance_amount, expended_amount, released_amount){
		if(formtype == 1){
			console.log("=======Building========")
			console.log('cnstrn_lvl:: '+cnstrn_lvl);
			console.log('formtype:: '+formtype);
			console.log('approved_amount:: '+approved_amount);
			console.log('balance_amount:: '+balance_amount);
			console.log('expended_amount:: '+expended_amount);
			console.log('released_amount:: '+released_amount);
			var inputWithdrawnAmount = (parseFloat($('#amount_expended'+cnstrn_lvl).val())-parseFloat($('#hidden_expended_amount_step'+cnstrn_lvl).html())).toFixed(2);
			console.log('inputWithdrawnAmount:: '+inputWithdrawnAmount);
			$('#input_amount_expended'+cnstrn_lvl).val(inputWithdrawnAmount);
			var postData = $('#blockInspectionform'+cnstrn_lvl).serializeArray();
			console.log('value:: '+parseFloat($('#input_amount_expended'+cnstrn_lvl).val()))
			console.log('amount_expended:: '+$('#amount_expended'+cnstrn_lvl).val())
			if($('#amount_expended'+cnstrn_lvl).val()==''){
				swal('Please enter a valid Expended amount');
			}else{
				var amount_expened_input = $('#amount_expended'+cnstrn_lvl).val();
				if(parseFloat(amount_expened_input) <= parseFloat(expended_amount) || parseFloat(inputWithdrawnAmount) > parseFloat(balance_amount)){
					$.confirm({
					    title: 'Invalid Withdrawn Amount!',
					    content: 'Hint: Withdrawn amount cannot be less than Total Amount Withdrawn Till Date or greater than Balance Amount. ',
					    type: 'red',
					    typeAnimated: true,
					    buttons: {
					        tryAgain: {
					            text: 'Ok',
					            btnClass: 'btn-red',
					            action: function(){
					            	if(formtype == 1){
					            		console.log('#amount_expended'+cnstrn_lvl)
					            		$('#amount_expended'+cnstrn_lvl).val('');
					            		$('#amount_expended'+cnstrn_lvl).focus();
					            	}else{
					            		console.log('#nb-amount_expended'+cnstrn_lvl)
					            		$('#nb-amount_expended'+cnstrn_lvl).val('');
					            		$('#nb-amount_expended'+cnstrn_lvl).focus();
					            	}
					            }
					        },
					    }
					});
				}else{
					saveBlockInspectionDetailsLevelWise(postData, cnstrn_lvl, formtype, approved_amount, balance_amount, expended_amount, released_amount, inputWithdrawnAmount);
				}
				
			}
		}else{
			console.log("=======Non Building========")
			console.log('cnstrn_lvl:: '+cnstrn_lvl);
			console.log('formtype:: '+formtype);
			console.log('approved_amount:: '+approved_amount);
			console.log('balance_amount:: '+balance_amount);
			console.log('expended_amount:: '+expended_amount);
			console.log('released_amount:: '+released_amount);
			console.log('nb-amount-expended:: '+$('#nb-amount_expended'+cnstrn_lvl).val());
			console.log('hidden_inspection_nb_inspected_date_step:: '+$('#hidden_inspection_nb_expended_amount_step'+cnstrn_lvl).html());
			
			var inputWithdrawnAmount =(parseFloat($('#nb-amount_expended'+cnstrn_lvl).val())-parseFloat($('#hidden_inspection_nb_expended_amount_step'+cnstrn_lvl).html())).toFixed(2);
			$('#nb_input_amount_expended'+cnstrn_lvl).val(parseFloat(inputWithdrawnAmount).toFixed(2));
			var postData = $('#blockInspectionNonBuildingform'+cnstrn_lvl).serializeArray();
			
			if($('#nb-amount_expended'+cnstrn_lvl).val()==''){
				swal('Please enter a valid Expended amount');
			}else{
				var amount_expened_input = $('#nb-amount_expended'+cnstrn_lvl).val();
				if(parseFloat(amount_expened_input) < parseFloat(expended_amount) || parseFloat(inputWithdrawnAmount) > parseFloat(balance_amount)){
					$.confirm({
					    title: 'Invalid Withdrawn Amount!',
					    content: 'Hint: Withdrawn amount cannot be less than Total Amount Withdrawn Till Date or greater than Balance Amount. ',
					    type: 'red',
					    typeAnimated: true,
					    buttons: {
					        tryAgain: {
					            text: 'Ok',
					            btnClass: 'btn-red',
					            action: function(){
					            	if(formtype == 1){
					            		console.log('#amount_expended'+cnstrn_lvl)
					            		$('#amount_expended'+cnstrn_lvl).val('');
					            		$('#amount_expended'+cnstrn_lvl).focus();
					            	}else{
					            		console.log('#nb-amount_expended'+cnstrn_lvl)
					            		$('#nb-amount_expended'+cnstrn_lvl).val('');
					            		$('#nb-amount_expended'+cnstrn_lvl).focus();
					            	}
					            }
					        },
					    }
					});
				}else{
					saveBlockInspectionDetailsLevelWise(postData, cnstrn_lvl, formtype, approved_amount, balance_amount, expended_amount, released_amount, inputWithdrawnAmount);
				}
			}
		}
		
	}
	
	var saveBlockInspectionDetailsLevelWise = function(postData, cnstrn_lvl, formtype, approved_amount, balance_amount, expended_amount, released_amount, inputWithdrawnAmount){
		console.log('inputWithdrawnAmount:: '+inputWithdrawnAmount)
		console.log('expended_amount:: '+expended_amount)
		console.log('released_amount:: '+released_amount)
		
		console.log(postData);
		$.ajax({
			 url: "./projectmonitoring/saveBlockInspectionDetails.do",
			 type: "POST",
			 data: JSON.stringify(objectifyForm(postData)),
	         contentType:"application/json",
	         dataType:"JSON",
	         beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
				},
			 success: function(data) {
	                if (data.status) {
	                	swal(data.msg);
	            		/*$(".bform")[0].reset();
	            		$(".nbform")[0].reset();*/
	                	
	                	if(formtype == 1){
	                		$('#blockInspectionform'+cnstrn_lvl)[0].reset();
	                	}else if(formtype == 2){
	                		$('#blockInspectionNonBuildingform'+cnstrn_lvl)[0].reset();
	                	}
	                	
	            		$('.append-cards').html('');
	            		$("div.tab-head-nb>div.list-group>button.active").removeClass();
	            		$("div.tab-head-nb>div.list-group>button").eq(cnstrn_lvl+1).addClass();
	            		
	            		$("div.tab-content-nb>div.tab-pane-nb").removeClass("active");
					    $("div.tab-content-nb>div.tab-pane-nb").eq(cnstrn_lvl+1).addClass("active");
						 $('#projectInspectionModalNonBuilding').modal('hide');
						 $('#projectInspectionModalBuilding').modal('hide');
	                	getDataForMasterTimelineTable($('#hidden_school_id').val());
	                	 if($('#gpstoggle').is(":checked")==true){
							 $('.geo-location-status').html('Geo-location is disabled.')
						 }else{
							 $('.geo-location-status').html('Geo-location is enabled.')
						 }
		              }else{
		            	  swal(data.msg);
		            		$(".bform")[0].reset();
		            		$(".nbform")[0].reset();
		              }
		            },
			 error:function(jqXHR, status, error){
			 	swal(error);
			 }
		});
			
		}
	
	function readURL(input, x, type) {
		  if (input.files && input.files[0]) {
		    var reader = new FileReader();
		    reader.onload = function(e) {
		    	if(type == 0){
		    		$('#blah'+x).attr('src', e.target.result);
		    	}else{
		    		$('#nb-blah'+x).attr('src', e.target.result);
		    	}
		    }
		    reader.readAsDataURL(input.files[0]);
		  }
		}

		$("#imgInp1").change(function() {
		  readURL(this, 1, 0);
		});
		
		$("#imgInp2").change(function() {
		  readURL(this, 2, 0);
		});
		
		$("#imgInp3").change(function() {
			  readURL(this, 3, 0);
		});
			
		$("#imgInp4").change(function() {
		  readURL(this, 4, 0);
		});
		
		$("#imgInp5").change(function() {
			  readURL(this, 5, 0);
		});
			
		$("#imgInp6").change(function() {
		  readURL(this, 6, 0);
		});
		
		$("#imgInp7").change(function() {
			  readURL(this, 7, 0);
		});
			
		$("#imgInp8").change(function() {
		  readURL(this, 8, 0);
		});
		
		$("#imgInp9").change(function() {
			  readURL(this, 9, 0);
		});
			
		$("#imgInp10").change(function() {
		  readURL(this, 10, 0);
		});
			
		$("#imgInp11").change(function() {
		  readURL(this, 11, 0);
		});
		
		$("#imgInp12").change(function() {
			  readURL(this, 12, 0);
		});
			
		$("#imgInp13").change(function() {
		  readURL(this, 13, 0);
		});
		
		
		$("#nb-imgInp1").change(function() {
		  readURL(this, 1, 1);
		});
		
		$("#nb-imgInp2").change(function() {
		  readURL(this, 2, 1);
		});
		
		$("#nb-imgInp3").change(function() {
		  readURL(this, 3, 1);
		});
			
		$("#nb-imgInp4").change(function() {
		  readURL(this, 4, 1);
		});
		
		$("#nb-imgInp5").change(function() {
		  readURL(this, 5, 1);
		});
			
		$("#nb-imgInp6").change(function() {
		  readURL(this, 6, 1);
		});
		
		$("#nb-imgInp7").change(function() {
		  readURL(this, 7, 1);
		});
	
	var hideActionForms = function(){
		$('#fundreleaseyes').hide();
		$('#fundreleaseno').hide();
		$('#layoutyes').hide();
		$('#layoutno').hide();
		$('#foundationyes').hide();
		$('#foundationno').hide();
		$('#plinthyes').hide();
		$('#plinthno').hide();
		$('#lintelyes').hide();
		$('#lintelno').hide();
		$('#roofyes').hide();
		$('#roofno').hide();
		$('#roofcastyes').hide();
		$('#roofcastno').hide();
		$('#finishingyes').hide();
		$('#finishingno').hide();
		$('#completedyes').hide();
		$('#completedno').hide();
		$('#recievedyes').hide();
		$('#recievedno').hide();
		$('#checkyes').hide();
		$('#checkno').hide();
		$('#handedyes').hide();
		$('#handedno').hide();
		$('#submittedyes').hide();
		$('#submittedno').hide();
		$('#approvedyes').hide();
		$('#approvedno').hide();
		$('#nbfundreleaseyes').hide();
		$('#nbfundreleaseno').hide();
		$('#nbstartyes').hide();
		$('#nbstartno').hide();
		$('#nbcompletedyes').hide();
		$('#nbcompletedno').hide();
		$('#nbprogressyes').hide();
		$('#nbprogressno').hide();
		$('#nbhandedyes').hide();
		$('#nbhandedno').hide();
		$('#nbsubmittedyes').hide();
		$('#nbsubmittedno').hide();
		$('#nbapprovedyes').hide();
		$('#nbapprovedno').hide();
		$('#fundremarks').hide();
		$('#layoutremarks').hide();
		$('#foundationremarks').hide();
		$('#plinthremarks').hide();
		$('#lintelremarks').hide();
		$('#roofremarks').hide();
		$('#roofcastremarks').hide();
		$('#finishingremarks').hide();
		$('#completedremarks').hide();
		$('#checkremarks').hide();
		$('#handedremarks').hide();
		$('#submittedremarks').hide();
		$('#approvedremarks').hide();
		$('#nbfundremarks').hide();
		$('#nbstartremarks').hide();
		$('#nbprogressremarks').hide();
		$('#nbcompletedremarks').hide();
		$('#nbhandedremarks').hide();
		$('#nbsubmittedremarks').hide();
		$('#nbapprovedremarks').hide();
	}
	
})