$(document).ready(function() {
	
	$("#blockMapModal").on("hidden.bs.modal", function(){
		 $('#blockForm')[0].reset();
	});
/******************************************
 * Constructing group details table
 ******************************************/	
	var table = $('#grpRoleParamMapTbl').DataTable({		
		"lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
		"pageLength": 5,	
		"bFilter": false,
		"bLengthChange": false,
		"scrollX": false,
		"bDestroy": false,
		"paging": true,
		"searching": true,
		"columnDefs":[
		    {"className": "dt-center", "targets": "_all"},         
            {"targets":0,"data":"groupName"},
            {"targets":1,"data":"description"},
            {"targets":2,"data":"noOfUsers"},
            {"targets": -2, "data": null, "defaultContent": '<button type="button" class="btn btn-default btn-sm btn-rolemap"><span class="glyphicon glyphicon-user"></span></button>'},
            {"targets":-1,"data":null,
         	   render : function (data, type, row) {
         		  if(data.groupId == 2 || data.groupId == 3) {
         			 return  '<button type="button" class="btn btn-default btn-sm btn-parametermap"><span class="glyphicon glyphicon-road"></span></button>'
         		   }else {
         			  return  '<button type="button" class="btn btn-default btn-sm btn-parametermap" disabled ><span class="glyphicon glyphicon-road"></span></button>'
         		   }
         	   }
            },
           // {"targets": -1, "data": null, "defaultContent": '<button type="button" class="btn btn-default btn-sm btn-parametermap"><span class="glyphicon glyphicon-road"></span></button>'}             
		  ]
	});
	
	var getGroupDetails = function() {		
		$.ajax({
			 "url": "./setting/getGroupDetails.do",
			 "type": "POST",	
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			 success:function(result) {
				 console.log(result);
				 table.clear();
				 table.rows.add(result.data).draw();
			 },
			 error:function(jqXHR, status, error){
			 	swal("error");
			 }
		});
	}		
	getGroupDetails();
	
	
	/*****************************************************
	 * Map role click on group details table
	 *****************************************************/
		
	var selectedGroupId = 0;
	$('#grpRoleParamMapTbl tbody').on('click', '.btn-rolemap', function () {
        var data = table.row( $(this).parents('tr') ).data();
        $('#groupRoleMapModal').modal('show');
        selectedGroupId = data.groupId;
        //(selectedGroupId);
    });
    
	/*******************************************************************
	 * Map Parameter click on group details table
	 *******************************************************************/
	var groupName ;
    $('#grpRoleParamMapTbl tbody').on('click', '.btn-parametermap', function () {
        var data = table.row( $(this).parents('tr') ).data();
        console.log(data)
        $('#groupParamMapModal').modal('show');
        selectedGroupId = data.groupId;
       
    });
	
    /***********************************************
     * Construct Mapped parameter to usergroup table
     ***********************************************/
    var mappedParamTbl = null;
    $('#groupParamMapModal').on('shown.bs.modal', function (e) {
    	mappedParamTbl = $('#groupParamMapTbl').DataTable({			
			 "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
			 "pageLength": 5,	
			 "bFilter": false,
			 "bLengthChange": false,
			 "scrollX": false,
			 "bDestroy": true,
			 "searching":true,
			 "columnDefs":[
			    {"className": "dt-center", "targets": "_all"},
			    {"targets":0,"data":"userId"},
			    {"targets":1,"data":"firstName"},
			    {"targets":2,"data":"email"},
			    {"targets":3,"data":"locationId"},
            	{"targets": -1, "data": null, "defaultContent": '<button type="button" class="btn btn-default btn-sm btn-getlocation"><span class="glyphicon glyphicon-map-marker"></span></button>'} 
			 ]
		});	
    	
    	loadMappedParameters();
    })
    
    var loadMappedParameters = function() {	
    	var Data =
        {
    			groupId: selectedGroupId
    			
    			
        };
		$.ajax({
			 url: "./setting/getMappedParametersToGroup.do",
			 type: "POST",	
			 contentType : 'application/json',
	   		 dataType: "json",
	   		 data: JSON.stringify(Data),
	   		 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			 success:function(result) {
				 console.log(result);
				 mappedParamTbl.clear();
				 mappedParamTbl.rows.add(result.data).draw();
			 },
			 error:function(jqXHR, status, error){
			 	swal("error");
			 }
		});
	}
    
    
    
    /************************************************
     * construct mapped role to usergroup table
     *************************************************/
    var table2 = null;
	$('#groupRoleMapModal').on('shown.bs.modal', function (e) {
		table2 = $('#groupRoleMapTbl').DataTable({			
			 "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
			 "pageLength": 5,	
			 "bFilter": false,
			 "bLengthChange": false,
			 "scrollX": false,
			 "bDestroy": true,
			 "columnDefs":[
			    {"className": "dt-center", "targets": "_all"},
			    {"targets":0,"data":"roleName"},
            	{"targets": -1, "data": null, "defaultContent": '<button type="button" class="btn btn-default btn-sm btn-delete"><span class="glyphicon glyphicon-trash"></span></button>'}        
			 ]
		});	
		
		loadMappedRoles();	
		
	});
	
	var loadMappedRoles = function() {	
		var Data =
        {
    			groupId: selectedGroupId
        };
		$.ajax({
			 url: "./setting/getMappedRolesToGroup.do",
			 type: "POST",	
			 contentType : 'application/json',
	   		 dataType: "json",
	   		 data: JSON.stringify(Data),
	   		 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			 success:function(result) {
				 console.log(result);
				 table2.clear();
				 table2.rows.add(result.data).draw();
			 },
			 error:function(jqXHR, status, error){
			 	swal("error");
			 }
		});
	}
	
	$('#groupRoleMapTbl tbody').on('click', '.btn-delete', function () {
        var data = table2.row( $(this).parents('tr') ).data();
        var Data =
        {
        		groupId: selectedGroupId,
			 	roleId: data.roleId
        };
        $.ajax({
			 url: "./setting/deleteMappedRole.do",
			 type: "POST",		
   			contentType : 'application/json',
   			dataType: "json",
   			data: JSON.stringify(Data),
   		 beforeSend: function(xhr) {
             xhr.setRequestHeader(header, token);
		},
			 success:function(result) {
				 console.log(result);
				 if(result.success){
					swal(result.msg,"","success");
				 	loadMappedRoles();
				 }else{
				 	swal(result.msg,"","error");	
				 }
			 },
			 error:function(jqXHR, status, error){
				 swal(error);
			 }
		});
    });
	
	/*********************************************************************
	 * method is used to get all district and block details
	 * on click of action button of group param map
	 *********************************************************************/
	var mapUserId;
	$('#groupParamMapTbl tbody').on('click', '.btn-getlocation', function () {
		var data = mappedParamTbl.row( $(this).parents('tr') ).data();
		mapUserId = data.userId;
		if(selectedGroupId == 2) {
			 $('#parameterMapModal').modal('show');
		}else {
			$('#blockMapModal').modal('show');
		}
		 
		 
		
		
	})
	
	var tableLocationDetails = null;
	$('#parameterMapModal').on('shown.bs.modal', function (e) {
		tableLocationDetails = $('#allLocationTable').DataTable({			
			 "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
			 "pageLength": 5,	
			 "bFilter": false,
			 "bLengthChange": false,
			 "scrollX": true,
			 "bDestroy": true,
			 "paging": true,
		 	 "searching": true,
			 "columnDefs":[		 
	        	{"targets":0,"data":"districtId"},
	        	{"targets":1,"data":"districtName"},
	        	{"targets": -1, "data": null, "defaultContent": '<button type="button" class="btn btn-default btn-sm btn-locmaptouser"><span class="glyphicon glyphicon-eye-open"></span></button>'}
			]
		});
		 $.ajax({
			 url: "./setting/"+selectedGroupId+"/getAllLocationAccordingToRole.do",
			 type: "GET",		
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			 success:function(result) {
					 console.log(result.data);
					 tableLocationDetails.clear();
					 tableLocationDetails.rows.add(result.data).draw();
				
			 },
			 error:function(jqXHR, status, error){
			 	(error);
			 }
		});
		
			
	});

	//get all district details
	var getAllDistrictList = function getAllDistrictList() {	
		$.ajax({
			 url: "./setting/getAllDistrict.do",
			 type: "GET",	
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			 success:function(result) {
				var districtList = result.data;
				var option = '<option value="select">---SELECT----</option>';
				for(var i = 0;i<districtList.length;i++) {
					option += '<option value="'+ districtList[i].districtId + '">' + districtList[i].districtName + '</option>';
				}
				$('#district_dpdn').html("");
				$('#district_dpdn').empty();
				$('#district_dpdn').append(option);
			 },
			 error:function(jqXHR, status, error){
			 	(error);
			 }
		});
	}
	getAllDistrictList();
	
	var tableBlockLocationDetails;
	 $("#district_dpdn").change(function() {
	        var districtcode = $('#district_dpdn').val();
	        tableBlockLocationDetails = $('#allBlockLocationTable').DataTable({			
				 "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
				 "pageLength": 5,	
				 "bFilter": false,
				 "bLengthChange": false,
				 "scrollX": true,
				 "bDestroy": true,
				 "paging": true,
			 	 "searching": true,
				 "columnDefs":[		 
		        	{"targets":0,"data":"districtId"},
		        	{"targets":1,"data":"districtName"},
		        	{"targets": -1, "data": null, "defaultContent": '<button type="button" class="btn btn-default btn-sm btn-locmaptouser"><span class="glyphicon glyphicon-eye-open"></span></button>'}
				]
			});
			 $.ajax({
				 url: "./setting/"+districtcode+"/getAllBlockLocationAccordingToRole.do",
				 type: "GET",		
				 beforeSend: function(xhr) {
		               xhr.setRequestHeader(header, token);
				},
				 success:function(result) {
						 console.log(result.data);
						 tableBlockLocationDetails.clear();
						 tableBlockLocationDetails.rows.add(result.data).draw();
					
				 },
				 error:function(jqXHR, status, error){
				 	(error);
				 }
			});
	        
	    });
	
	
		$('#allBlockLocationTable tbody').on('click', '.btn-locmaptouser', function () { 
			var data = tableBlockLocationDetails.row( $(this).parents('tr') ).data();
			//console.log(data);
			var districtId = data.districtId;
			///swal(mapUserId)
			$.ajax({
				 url: "./setting/"+districtId+"/"+mapUserId+"/mapLocationToUser.do",
				 type: "GET",
				 dataType: "json",
				 beforeSend: function(xhr) {
		               xhr.setRequestHeader(header, token);
				},
				 success:function(result) {
					 loadMappedParameters();
						 swal(result.msg);
						 $('#blockMapModal').modal('hide');
				 }
			});
			
			
		})
	
	
	
	$('#allLocationTable tbody').on('click', '.btn-locmaptouser', function () { 
		var data = tableLocationDetails.row( $(this).parents('tr') ).data();
		//console.log(data);
		var districtId = data.districtId;
		///swal(mapUserId)
		$.ajax({
			 url: "./setting/"+districtId+"/"+mapUserId+"/mapLocationToUser.do",
			 type: "GET",
			 dataType: "json",
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			 success:function(result) {
				 loadMappedParameters();
					 swal(result.msg);
					 $('#parameterMapModal').modal('hide');
			 }
		});
		
		
	})
	
	
	
	
	
	
	//delete button click function
	/*$('#groupParamMapTbl tbody').on('click', '.btn-paramdelete', function () {
        var data = mappedParamTbl.row( $(this).parents('tr') ).data();
        var Data =
        {
        		sl_no : data.slno
        };
        $.ajax({
			 url: "./setting/deleteMappedParam.do",
			 type: "POST",		
			 contentType : 'application/json',
	   		 dataType: "json",
	   		 data: JSON.stringify(Data),
	   		 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			 success:function(result) {
				 console.log(result);
				 if(result.success){
					 loadMappedParameters();
				 }else{
				 	swal(result.msg,"","error");	
				 }
			 },
			 error:function(jqXHR, status, error){
			 	(error);
			 }
		});
    });*/
	
	/************************************************************************
	 * Add Param button click on Assigned Parameters modal
	 **************************************************************************/
	
	
/*	$("#btn_addparamstogroup").click(function() {
		$('#parameterMapModal').modal('show');
		$("#dynamicTbl").empty();
		getAllParameterList();
	});*/
	
	//it will show the all parameter in drop down
	
	/****************************************
	 * add role button click on Assigned Role Modal
	 ***************************************/
	$("#btn_addrolestogroup").click(function (){
		$('#roleMapModal').modal('show');
		
	});
	
	var table3 = null;
	$('#roleMapModal').on('shown.bs.modal', function (e) {
		table3 = $('#modalRolesTbl').DataTable({			
			 "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
			 "pageLength": 5,	
			 "bFilter": false,
			 "bLengthChange": false,
			 "scrollX": true,
			 "bDestroy": true,
			 "paging": true,
		 	 "searching": true,
			 "columnDefs":[		 
			 	{"targets":0, "data": null,
	     		 'className': 'dt-body-center',
			     'render': function (data, type, full, meta){
			          return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
			      }
		        },
	        	{"targets":1,"data":"roleId"},
	        	{"targets":2,"data":"roleName"},
	        	{"targets":3,"data":"description"},
	        	{"targets": -1, "data": null, "defaultContent": '<button type="button" class="btn btn-default btn-sm btn-preview"><span class="glyphicon glyphicon-eye-open"></span></button>'}
			]
		});
		
		$.ajax({
			 url: "./setting/getRoleList.do",
			 type: "POST",		
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			 success:function(result) {
				 console.log(result);
				 table3.clear();
				 table3.rows.add(result.data).draw();
			 },
			 error:function(jqXHR, status, error){
			 	(error);
			 }
		});
			
	});
	
	// Handle click on "Select all" control
	$('#roles-select-all').on('click', function() {
		// Get all rows with search applied
	    var rows = table3.rows({
	    	'search' : 'applied'
	    }).nodes();
	    // Check/uncheck checkboxes for all rows in the table
	    $('input[type="checkbox"]', rows).prop('checked', this.checked);
	});
	
	// Handle click on checkbox to set state of "Select all" control
	$('#modalRolesTbl tbody').on('change', 'input[type="checkbox"]', function() {
		// If checkbox is not checked
	    if (!this.checked) {
	    	var el = $('#roles-select-all').get(0);
	      	// If "Select all" control is checked and has 'indeterminate' property
	      	if (el && el.checked && ('indeterminate' in el)) {
	        	// Set visual state of "Select all" control as 'indeterminate'
	        	el.indeterminate = true;
	      	}
	    }
	});
	
	$("#btn_savemappedroles").click(function() {			
		var selectedRoles = [];
	    table3.$('input[type="checkbox"]').each(function(){
            if(this.checked){
				var data = table3.row( $(this).parents('tr') ).data();		
				selectedRoles.push(data);					    	
            }	         
	    });	      
	    
	    if(selectedRoles.length>0){	    	
	    	$.ajax({
				 url: "./setting/mapRoleToGroup.do?groupId="+selectedGroupId,
				 type: "POST",
				 contentType : 'application/json; charset=utf-8',
    			 dataType : 'json',
				 data: JSON.stringify(selectedRoles),
				 beforeSend: function(xhr) {
		               xhr.setRequestHeader(header, token);
	  			},
				 success:function(result) {
					 console.log(result);
					 if(result.success){
					 	$('#roleMapModal').modal('hide');
	    				loadMappedRoles();
					 }
					 (result.msg);
				 },
				 error:function(jqXHR, status, error){
				 	(error);
				 }
			});
	    }else{
	    	("Please select roles for mapping..............")
	    }
	});
	
	var selectedRoleId = 0;
	$('#modalRolesTbl tbody').on('click', '.btn-preview', function () {		
        var data = table3.row( $(this).parents('tr') ).data();
        $('#mappedResourcesModal').modal('show');
        selectedRoleId = data.roleId;
    });
    var table5 = null;
    $('#mappedResourcesModal').on('shown.bs.modal', function (e) {
        table5 = $('#mappedResourcesTbl').DataTable({			
			 "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
			 "pageLength": 5,	
			 "bFilter": false,
			 "bLengthChange": false,
			 "scrollX": true,
			 "bDestroy": true,
			 "paging": true,
		 	 "searching": true,
			 "columnDefs":[		 	
	        	{"targets":0,"data":"resourceName"},
	        	{"targets":1,"data":"resourceType"}
			 ]
		});
        
        var Data =
        {
        		roleId: selectedRoleId
        };
        $.ajax({
			 url: "./setting/getMappedResources.do",
			 type: "POST",		
			 contentType : 'application/json',
	   		 dataType: "json",
	   		 data: JSON.stringify(Data),
	   		 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			 success:function(result) {
				 console.log(result);
				 table5.clear();
				 table5.rows.add(result.data).draw();
			 },
			 error:function(jqXHR, status, error){
			 	swal("error");
			 }
		});
	});
    
   
	
	
	
	/*Unused Code Commented*/
/*	
	$("#btn_map_resources2").click(function (){
		$('#mapLocModal').modal('show');
	});
	var table4 = null;
	$('#mapLocModal').on('shown.bs.modal', function (e) {
		table4 = $('#modalUserLocationMapTbl').DataTable({
			 "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
			 "pageLength": 5,	
			 "bFilter": false,
			 "bLengthChange": false,
			 "scrollX": true,
			 "bDestroy": true,
			 "columnDefs":[
			             
			 ]
		});
	});
	
	
	$("#modal_add_btn").click(function(e) {		
		$("modal_locations").modal('show');
	});
	
	$('#modal_locations').on('shown.bs.modal', function (e) {
		 $('#modallocationTable').DataTable({			
			 "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
			 "pageLength": 5,	
			 "bFilter": false,
			 "bLengthChange": false,
			 "scrollX": true,
			 "bDestroy": true,
			 "columnDefs":[
			             
			 ]

		});
	});
	
	var getAllParameterList = function() {		
		$.ajax({
			 url: "./setting/getParameterDetails.do",
			 type: "POST",	
			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
			 success:function(result) {
				var parameterList = result.data;
				console.log(parameterList);
				var option = '<option value="select">---SELECT----</option>';
				for(var i = 0;i<parameterList.length;i++) {
					option += '<option value="'+ parameterList[i].param_id + '">' + parameterList[i].param_name + '&nbsp;&nbsp;(' + parameterList[i].tbl_name + ')</option>';
				}
				$('#param_sel_dropdown').html("");
				$('#param_sel_dropdown').append(option);
			 },
			 error:function(jqXHR, status, error){
			 	(error);
			 }
		});
	}
	
	//the parameter dropdown change function
	var dynamic = null;
	$('#param_sel_dropdown').on('change', function() {
		  var paramId =  this.value;
		  var name = $('#param_sel_dropdown option:selected').text();
		  var paramName =  name.substring(0,name.lastIndexOf("(") -1);
		  var tableName = name.substring(name.lastIndexOf("(") + 1, name.lastIndexOf(")") );
		  $("#dynamicTbl").empty();
		  console.log(paramId + "::" + paramName + "::" + tableName)
		  getParamDescription(paramId,paramName,tableName);
		  
		})
	
	var getParamDescription = function(paramId,paramName,tableName) {	
		var Data =
        {
				paramId: paramId,
				 paramName: paramName,
				 tableName:tableName
        };
		$.ajax({
			 url: "./setting/getParameterValueTable.do",
			 type: "POST",	
			 contentType : 'application/json',
	   			dataType: "json",
	   			data: JSON.stringify(Data),
				dataType:'json',
				 beforeSend: function(xhr) {
		               xhr.setRequestHeader(header, token);
	  			},
			 success:function(result) {
				 var col_arr = result.col_name;
				 var dataArr = result.data;
				 var table ='<table class="table table-condensed table-striped table-bordered" id="modalTblDesc">'+
				 '<thead>'
					
				 var th = '<th></th>';
				<input type="checkbox" name="parameters-select-all" value="1" id="parameters-select-all">
				 var tbody = '<tbody>';
				 var tr = '';
				 for(var i=0;i<col_arr.length;i++) {
					 //console.log(col_arr[i]);
					 th = th+'<th>'+col_arr[i]+'</th>'
				 }
				 $.each(dataArr,function(i,data){
					 tr = tr+'<tr>'+'<td>'+'<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">'+'</td>';
					 for(var j=0;j<col_arr.length;j++) {
						 tr = tr+'<td>'+data[col_arr[j]]+'</td>';
					 }
					 tr = tr+'</tr>';
				 }) 
				 tbody=tbody+tr+'</tbody>';
				 table = table+th+'</tr></thead>'+tbody+'</table>';
				// var vv = $('#modalTblDesc').DataTable();
				 $("#dynamicTbl").append(table);
				 dynamic = $('#modalTblDesc').DataTable({			
					 "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
					 "pageLength": 5,	
					 "bFilter": false,
					 "bLengthChange": false,
					 "scrollX": false,
					 "bDestroy": false,
					 "orderCellsTop":false
				});
			 },
			 error:function(jqXHR, status, error){
			 	(error);
			 }
		});
	}
	
	
	$("#btn_savemappedParams").click(function() {
		
		var name = $('#param_sel_dropdown option:selected').text();
		var paramName =  name.substring(0,name.lastIndexOf("(") -2);//need to store in table(group_param)
		var column = dynamic.column(':contains('+paramName+')');
		var columnindex = column[0];
		
		var selectedParamValues = [];//need to store in table group_param
		dynamic.$('input[type="checkbox"]').each(function(){
            if(this.checked){
				var data = dynamic.row( $(this).parents('tr')).data();	
				//console.log();
				var colData = data[columnindex];
				selectedParamValues.push(colData);					    	
            }	         
	    });	  
		console.log(selectedParamValues);
		//console.log(selectedParamValues.length);
	    if(selectedParamValues.length>0){
	    	
	    	$.ajax({
				 url: "./setting/mapParamWithValues.do",
				 type: "POST",
				 data: {selectedParamValues:selectedParamValues,selectedGroupId:selectedGroupId,paramName:paramName},
				 beforeSend: function(xhr) {
		               xhr.setRequestHeader(header, token);
	  			},
				 success:function(result) {
					
					 if(result.success){
					 	$('#parameterMapModal').modal('hide');
					 	loadMappedParameters();
					 }
					 (result.msg);
				 },
				 error:function(jqXHR, status, error){
				 	(error);
				 }
			});
	    }else{
	    	("Please select roles for mapping")
	    }
	});
	*/
	
	
});