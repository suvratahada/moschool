$(document).ready(function(){
    //swal("44444")
  isEdit = false;
  var table = $("#UserResourceDetails").DataTable({   	
		 "paging":  true,
		 "lengthMenu": [[10,20, 30, -1], [10,20, 30, "All"]],
		 "processing": false,
	     "ordering": false,
	     "info": true,
	     "bFilter": true,
	     "searching": true,
	     "scrollCollapse": true,
	     "columnDefs":[
	             {"className": "dt-center", "targets": "_all"}, 
			     {"targets":0,"data":"resource_id"},
			     {"targets":1,"data":"resource_name"},
			     {"targets":2,"data":"resource_type"}, 
			     {"targets":3,"data":"description"},
			     {"targets":4,"data": null, 
			     "defaultContent": '<button type="button" action="deleteResource" id="deleteResource" class="btn btn-default btn-sm btn-delete"><span class="glyphicon glyphicon-trash"></span></button>  <button type="button" id="Edit" action="Edit" class="btn btn-default btn-sm btn-edit"><span class="glyphicon glyphicon-edit"></span></button></span></button>'}            
			     
			      ],
			   		
	});
     getResourceTableData(table);
     
     /* *******************************************
  	 *    Form Data to the Create Group table
  	 ***********************************************/
  	   
     $('#addResource').click(function(){
// 		swal("Add button click.....................");
 		var type = "";
 		
 		if(isEdit){
 			type = "UPDATE"
// 			swal(type);
 		}else{
 			type = "ADD"
// 			swal(type);	
 		}
 		
 		var resourcename = $("#resource_name").val();
 		
 		var resourceType = $("#resource_type").val();
 		//swal(resourceType);
 		var description= $("#descriptaion").val();
 		var id= $("#resourceId").val();
 		
 		var Data =
        {
 				resource_name:resourcename,
 				resource_type:resourceType,
 				description:description,
 				type:type,
 				id:id
        };
 		
 		if(resourcename==''){
 			swal("Please Enter Resource Name!");
 			$("#resource_name").focus();
 		}
 		else if(description==''){
 			swal("Please Enter Description!");
 			$("#descriptaion").focus();
 		}else{
 			$.ajax({
				 url: "./setting/saveUserResourceTable.do",
				 type: "POST",
				 contentType : 'application/json',
	       		 dataType: "json",
	       		 data: JSON.stringify(Data),
	       		 beforeSend: function(xhr) {
		               xhr.setRequestHeader(header, token);
	  			},
				 success:function(result) {
					 if(result.success){
						 swal(result.msg,"","success");
						 resetFormCreateResource();
						 getResourceTableData(table);
	 				 }else{
	 					 swal(result.msg,"","error");
						 resetFormCreateResource();
						 getResourceTableData(table);	
	 				 }
				 },
				 error:function(jqXHR, status, error){
				 	swal(error);
				 }
			});
 		}
     }) ; 
     
     /*********************************************
      * Edit button click function
      *********************************************/
     $('#UserResourceDetails tbody').on( 'click', 'button[action=Edit]', function () {
    	 //swal('hii')
             var data = table.row( $(this).parents('tr') ).data();
             isEdit = true;
             $("#resource_name").val(data.resource_name); 
             $("#resource_type").val(data.resource_type);
             $("#descriptaion").val(data.description);
             $("#resourceId").val(data.resource_id);
             $('#addResource').html('Update');
     });   
     
     /*********************************************
      * Delete button click function
      *********************************************/
     
     $('#UserResourceDetails tbody').on( 'click', 'button[action=deleteResource]', function () {
         var data = table.row( $(this).parents('tr') ).data();
         var Data =
         {
        		 resource_id: data.resource_id
         };
         $.ajax({
 			 url: "./setting/deleteCreateUserResource.do",
 			 type: "POST",		
 			contentType : 'application/json',
   			dataType: "json",
   			data: JSON.stringify(Data),
	   		 beforeSend: function(xhr) {
	             xhr.setRequestHeader(header, token);
			},
 			 success:function(result) {
 				 if(result.success){
 					getResourceTableData(table);
 					swal(result.msg,"","success");
 				 }else{
 					swal(result.msg,"","error");	
 				 }
 			 },
 			 error:function(jqXHR, status, error){
 				swal(error);
 			 }
 		});
     });
})
		 		    
      function getResourceTableData(table){
 		//swal('hii')
 		$.ajax({
 			 url: "./setting/getDataResourceTable.do",
 			 type: "POST",		
 			 beforeSend: function(xhr) {
	               xhr.setRequestHeader(header, token);
			},
 			 success:function(result) {
 				 table.clear().draw();
 				 table.rows.add(result.data).draw();
 			 }
 		});
 	}
     
var resetFormCreateResource = function(){
	$('#addResource').html('Add');
	isEdit = false;
	$("#resource_name").val(''); 
	$("#descriptaion").val('');
	$("#resourceId").val('123'); //default value
}


