$('document').ready(function(){
	
	$("#changePasswordModal").on("hidden.bs.modal", function(){
		 $("#formPassword")[0].reset();
	});
	
	$('#submit_changePassword').on('click', function(){
		checkPasswordValidity();
	})
	
	var checkPasswordValidity = function(){
		var old_password = $("#old_psw").val();
		var password = $("#new_psw").val();
		var cnf_password = $("#con_psw").val();
		
		if(password.trim() != cnf_password.trim()){
			swal("Passwords doesn't match..");
		}else{
			var oldPassword = sha512Hash(old_password);
			var password = sha512Hash(password);
			var confPassword = sha512Hash(cnf_password);
			
			$("#old_psw").val(oldPassword);//changed
			$("#new_psw").val(password);//changed
			$("#con_psw").val(confPassword);//changed
			
			
			postData = {
					oldPassword : $("#old_psw").val(),
					password : $("#new_psw").val(),
					confPassword : $("#con_psw").val()
			}
			
			console.log($("#old_psw").val())
			
			$.ajax({
				 url: './passwordcontroller/changePasswordOnValid.do',
				 type: "POST",
				 beforeSend: function(xhr) {
		                xhr.setRequestHeader(header, token);
		  		},
		  		data: JSON.stringify(postData),
		        contentType:"application/json",
		        dataType:"JSON",
				 success: function(data) {
					 $("#formPassword")[0].reset();
		                if (data.status) {
		                	swal(data.msg)
		                	$('#changePasswordModal').modal('hide')
			              }else{
			            	  swal(data.msg);
			              }
		            },
				 error:function(jqXHR, status, error){
				 	swal(error);
				 }
			});
		}
	}
	
	
	
})

